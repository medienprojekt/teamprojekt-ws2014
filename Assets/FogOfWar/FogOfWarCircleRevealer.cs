using UnityEngine;
using System.Linq;

public class FogOfWarCircleRevealer : MonoBehaviour
{
    int frameCount;
    FogOfWar[] fow;

    [SerializeField]
    float radius = 15f;

    [SerializeField]
    float autoRevealRadius = 5f;

    [SerializeField]
    float sightHeight = 1f;

    [SerializeField]
    float fadeDelay = 1f;

    [SerializeField]
    int updateRate = 10;

    private Menu menu;
    private bool initLater;

    void Start(){
        menu = GameObject.Find("Menu").GetComponent<Menu>();

        fow = FindObjectsOfType(typeof(FogOfWar)).Cast<FogOfWar>().ToArray();
        frameCount = Random.Range(0, updateRate);

        if (menu.iAmOnTeam != 1 && menu.iAmOnTeam != 2) {
            initLater = true;
        }
        else {

            if (menu.iAmOnTeam == 1 && this.GetComponent<MonsterAttributesScript>() != null) {
                enabled = false;
            }

            if (menu.iAmOnTeam == 2 && (this.GetComponent<RangerAttributesScript>() != null ||
                this.GetComponent<HealerAttributesScript>() != null ||
                this.GetComponent<TankAttributesScript>() != null)) {
                enabled = false;
            }

        }
    }

    void Update(){

        if (initLater) {
            
           if(menu.iAmOnTeam != 1 && menu.iAmOnTeam != 2){
               return;
           }
           else {

               if (menu.iAmOnTeam == 1 && this.GetComponent<MonsterAttributesScript>() != null) {
                   enabled = false;
               }

               if (menu.iAmOnTeam == 2 && (this.GetComponent<RangerAttributesScript>() != null ||
                   this.GetComponent<HealerAttributesScript>() != null ||
                   this.GetComponent<TankAttributesScript>() != null)) {
                   enabled = false;
               }
               initLater = false;
           }

        }


        if (++frameCount >= updateRate)
        {
            frameCount = 0; 

            for (int i = 0; i < fow.Length; ++i)
            {
                fow[i].RevealCircle(transform.position, radius, sightHeight, fadeDelay, autoRevealRadius);
            }
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, radius);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, autoRevealRadius);
    }
}