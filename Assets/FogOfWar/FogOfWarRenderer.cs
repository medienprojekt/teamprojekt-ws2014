using UnityEngine;
using System.Collections;

public class FogOfWarRenderer : MonoBehaviour
{
    [SerializeField]
    FogOfWar fogOfWar;

    void Start() {
        fogOfWar = GameObject.Find("FogOfWar").GetComponent<FogOfWar>();

    }


    void Update()
    {
        if (fogOfWar != null && fogOfWar.enabled && fogOfWar.gameObject.active)
        {
            renderer.enabled = fogOfWar.IsRevealed(transform.position);
        }
    }
}