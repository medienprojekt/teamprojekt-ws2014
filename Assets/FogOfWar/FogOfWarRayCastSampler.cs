using UnityEngine;
using System.Collections;

public class FogOfWarRayCastSampler : FogOfWarHeightSampler {

    [SerializeField]
    float height = 2048f;

    [SerializeField]
    float distance = 4096f;

    [SerializeField]
    Vector3 direction = Vector3.down;

    [SerializeField]
    LayerMask layers = -1;

    public override float SampleHeight(Vector3 worldPosition)
    {
        worldPosition.z = -height;

        RaycastHit hit;

        if (Physics.Raycast(worldPosition, direction, out hit, distance, layers))
        {
            return -5;  //hit.point.z ;
        }

        return 0f;
    }
}
