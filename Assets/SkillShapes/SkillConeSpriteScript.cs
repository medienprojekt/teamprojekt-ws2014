﻿using UnityEngine;
using System.Collections;

public class SkillConeSpriteScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GetComponentInParent<MeshRenderer>().enabled) {
            renderer.enabled = true;
        }
        else {
            renderer.enabled = false;
        }


	}
}
