﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillConeScript : Photon.MonoBehaviour {

    public GameObject target;
    public List<GameObject> insideList;

	// Use this for initialization
	void Start () {
	    insideList = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {

        if (target != null) {
            Vector3 v3 = target.transform.position;
            v3.z = 3;
            transform.position = v3;
        }

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //winkel zwischen pos und maus ausrechnen

        //animationszeug winkel berechnung
        float laengeA = mousePos.x - transform.position.x;
        if (laengeA < 0) {
            laengeA = laengeA * -1;
        }

        float laengeC = mousePos.y - transform.position.y;
        if (laengeC < 0) {
            laengeC = laengeC * -1;
        }

        float laengeB = Mathf.Sqrt(laengeA * laengeA + laengeC * laengeC);

        float winkelAlpha = Mathf.Acos(laengeA / laengeB);
        winkelAlpha = winkelAlpha * Mathf.Rad2Deg;

        if (mousePos.x > transform.position.x && mousePos.y > transform.position.y) {
            //nichts machen
        }
        else if (mousePos.x < transform.position.x && mousePos.y > transform.position.y) {
            winkelAlpha = 180 - winkelAlpha;
        }
        else if (mousePos.x < transform.position.x && mousePos.y < transform.position.y) {
            winkelAlpha = 180 + winkelAlpha;
        }
        else if (mousePos.x > transform.position.x && mousePos.y < transform.position.y) {
            winkelAlpha = 360 - winkelAlpha;
        }
        else if (mousePos.x > transform.position.x && mousePos.y == transform.position.y) {
            winkelAlpha = 0;
        }
        else if (mousePos.x < transform.position.x && mousePos.y == transform.position.y) {
            winkelAlpha = 180;
        }
        else if (mousePos.x == transform.position.x && mousePos.y > transform.position.y) {
            winkelAlpha = 90;
        }
        else if (mousePos.x == transform.position.x && mousePos.y < transform.position.y) {
            winkelAlpha = 270;
        }

        winkelAlpha += 180;

        transform.eulerAngles = new Vector3(-winkelAlpha, 90, 0);
	}

    void OnTriggerEnter(Collider coll) {
        if (coll.tag == "Team") {
            insideList.Add(coll.gameObject);

        }

    }

    void OnTriggerExit(Collider coll) {
        if (coll.tag == "Team") {
            insideList.Remove(coll.gameObject);
        }
    }
}
