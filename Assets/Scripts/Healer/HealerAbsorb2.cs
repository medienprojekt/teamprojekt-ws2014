﻿using UnityEngine;
using System.Collections;

public class HealerAbsorb2 : Photon.MonoBehaviour {

    public GameObject target;
    float time;

    // Use this for initialization
    void Start() {
        time = Time.time + 0.5f;
        Vector3 pos = transform.position;
        pos.z = 1;
        transform.position = pos;
    }

    void Update() {
        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.z = 1;
            transform.position = pos;
        }
        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }
}

