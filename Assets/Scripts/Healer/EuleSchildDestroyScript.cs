﻿using UnityEngine;
using System.Collections;

public class EuleSchildDestroyScript : Photon.MonoBehaviour {

    public GameObject target;
    public float time;
    Color curColor;

    // Use this for initialization
    void Start() {
        curColor = renderer.material.color;
        curColor.a = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate() {
        //fade in & out
        if (curColor.a < 0.5f && Time.time < time - 0.3) {
            curColor.a += 0.05f;
        }

        if (Time.time > time - 0.2) {
            curColor.a -= 0.05f;
        }
        renderer.material.color = curColor;
    }

    void Update() {


        if (target != null && !target.renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }

        if (target != null) {
            Vector3 v3 = target.transform.position;
            v3.z = 1;
            transform.position = v3;
        }
        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }
}
