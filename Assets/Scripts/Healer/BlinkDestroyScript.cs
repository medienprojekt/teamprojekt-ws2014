﻿using UnityEngine;
using System.Collections;

public class BlinkDestroyScript : Photon.MonoBehaviour {

    float time;


	// Use this for initialization
	void Start () {
        time = Time.time + 0.3f;

	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
	
	}
}
