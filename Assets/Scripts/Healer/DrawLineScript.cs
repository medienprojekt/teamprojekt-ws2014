﻿using UnityEngine;
using System.Collections;

public class DrawLineScript : Photon.MonoBehaviour {

    private LineRenderer lineRenderer;
    private float counter;
    private float dist;

    public Transform origin;
    public Transform destination;

    //public Transform originNew;
   // public Transform destinationNew;

    private float lineDrawSpeed = 8.0f;



	// Use this for initialization
	void Start () {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (origin == null || destination == null) {
            lineRenderer.enabled = false;
            counter = 0;
        }



        if (lineRenderer.enabled) {

            Vector3 originPos = origin.position;
            originPos.z = originPos.z + 1;
            lineRenderer.SetPosition(0, originPos);

            dist = Vector3.Distance(origin.position, destination.position);


            counter += 0.1f / (lineDrawSpeed * Time.deltaTime);

            float x = Mathf.Lerp(0, dist, counter);

            Vector3 pointA = originPos;
            Vector3 pointB = destination.position;
            pointB.z = pointA.z;


            Vector3 pointAlongLine = x * Vector3.Normalize(pointB - pointA) + pointA;


            lineRenderer.SetPosition(1, pointAlongLine);

        }


	}

    public void startDrawLine(Transform start, Transform end) {
        origin = start;
        destination = end;

        lineRenderer.SetWidth(0.2f, 0.2f);


        lineRenderer.SetPosition(0, origin.position);
        dist = Vector3.Distance(origin.position, destination.position);
        counter += 0.1f / lineDrawSpeed;
        float x = Mathf.Lerp(0, dist, counter);
        Vector3 pointA = origin.position;
        Vector3 pointB = destination.position;
        Vector3 pointAlongLine = x * Vector3.Normalize(pointB - pointA) + pointA;
        lineRenderer.SetPosition(1, pointAlongLine);


        lineRenderer.enabled = true;
    }

    public void stopDrawLine() {
        lineRenderer.enabled = false;
        counter = 0;
    }



}
