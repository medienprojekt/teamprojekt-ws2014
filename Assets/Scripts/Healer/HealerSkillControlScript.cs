﻿using UnityEngine;
using System.Collections;

public class HealerSkillControlScript : Photon.MonoBehaviour {

	private Menu menuScript;

    private MouseHitboxScript mouseHitboxScript;
    private HealerControlScript healerControlScript;
    private HealerAttributesScript healerAttributesScript;
    private Movement movementScript;

    private int skillNumer; //0 nichts, 1-4 q-r
    public GameObject skill1Prefab;
    public bool skillMode;
    public GameObject skillCirclePrefab;
    private GameObject skillCircle;
    private GameObject curTarget;

    private GameObject portal1;
    private GameObject portal2;

    private int portalCount;

    public CooldownTimer cdTSkill1;
    public CooldownTimer cdTSkill2;
    public CooldownTimer cdTSkill3;
    public CooldownTimer cdTSkill11;
    public CooldownTimer cdTSkill21;
    public CooldownTimer cdTSkill31;

    private bool attackSkill1Active;
    private bool attackSkill1Active2;
    private bool attackSkill11Active;
    private bool attackSkill11Active2;
    private bool attackSkill2Active;
    private bool attackSkill2Active2;
    private bool attackSkill21Active;

    public int skill11Stage;
    private bool skill11GetCooldown;

    private DrawLineScript drawLineScript;

    private bool soullinkAlive;
    private GameObject soullinkTarget;
    private CooldownTimer hOTSkill21Timer;

    public Transform prefabBlink1;
    public Transform prefabShield;
    public Transform prefabPort1;
    public Transform prefabHeiligesLichtAni;
    public Transform prefabHeiligesLichtAttack;
    public Transform prefabHeiligesLichtHeal;
    public Transform prefabAbsorb1;
    public Transform prefabAbsorb2;
    private bool attackSkill31Active;

    private AnimaitonScript animationScript;

    private bool offerForQSkill;
    private bool offerForWSkill;
    private bool offerForESkill;

    private int qSkill;
    private int wSkill;
    private int eSkill;

    public bool iAmSilenced;
    private float silenceEnd;

    public bool passivActive;
    private float passivTick;

	public AudioClip[] sounds;
	public AudioSource asound;
	public AudioClip winSound;
	public AudioClip loseSound;
	private bool winSoundPlayed;
	private bool loseSoundPlayed;

    public bool hoverQ;
    public bool hoverW;
    public bool hoverE;

    private bool mouseClickSkill;

    // Use this for initialization
    void Start() {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];
		winSoundPlayed = false;
		loseSoundPlayed = false;

        healerControlScript = transform.GetComponent<HealerControlScript>();
        healerAttributesScript = transform.GetComponent<HealerAttributesScript>();

        if (photonView.isMine) {


            soullinkAlive = false;

            skill11Stage = 0;
            skill11GetCooldown = false;
            portalCount = 0;

            attackSkill1Active = false;
            attackSkill1Active2 = false;
            attackSkill11Active = false;
            attackSkill11Active2 = false;
            attackSkill2Active = false;
            attackSkill2Active2 = false;
            attackSkill21Active = false;
            attackSkill31Active = false;

            skillCircle = Instantiate(skillCirclePrefab, transform.position, transform.rotation) as GameObject;
            skillCircle.transform.parent = transform;
            Vector3 sCPos = new Vector3(transform.position.x, transform.position.y, 3);
            skillCircle.transform.position = sCPos;
            skillCircle.renderer.enabled = false;

            animationScript = transform.GetComponent<AnimaitonScript>();

            skillMode = false;
            skillNumer = 0;

            mouseHitboxScript = GameObject.Find("MouseHitbox").GetComponent<MouseHitboxScript>();
            movementScript = transform.GetComponent<Movement>();

            cdTSkill1 = new CooldownTimer(healerAttributesScript.cdSkill1);
            cdTSkill2 = new CooldownTimer(healerAttributesScript.cdSkill2);
            cdTSkill3 = new CooldownTimer(healerAttributesScript.cdSkill3);
            cdTSkill11 = new CooldownTimer(healerAttributesScript.cdSkill11);
            cdTSkill21 = new CooldownTimer(healerAttributesScript.cdSkill21);
            cdTSkill31 = new CooldownTimer(healerAttributesScript.cdSkill31);

            hOTSkill21Timer = new CooldownTimer(healerAttributesScript.hOTTickSkill21);

        } else {
            enabled = false;
        }

        drawLineScript = GameObject.Find("LineRenderer").GetComponent<DrawLineScript>();

    }

    // Update is called once per frame
    void Update() {
        if (healerAttributesScript.isAlive && !iAmSilenced && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked)
        {
            checkUpKeyPressed();
            skillActive();
        }
        if (!hoverQ && !hoverW && !hoverE) {
            skillCircle.renderer.enabled = false;
        }

        if (skillMode) {
            if (skillNumer == 1) {
                skillCircle.renderer.enabled = true;
                float x = (healerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (healerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }
            if (skillNumer == 11) {
                skillCircle.renderer.enabled = true;
                float x = (healerAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (healerAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }
            if (skillNumer == 2) {
                skillCircle.renderer.enabled = true;
                float x = (healerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (healerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }
            if (skillNumer == 21) {
                skillCircle.renderer.enabled = true;
                float x = (healerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (healerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }
            if (skillNumer == 3) {
                skillCircle.renderer.enabled = true;
                float x = (healerAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (healerAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }
            if (skillNumer == 31) {
                skillCircle.renderer.enabled = true;
                float x = (healerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (healerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }



            if (Input.GetMouseButtonDown(0) && !mouseClickSkill) {
                //fire skill
                fireSkill(skillNumer);
            }
            else if (mouseClickSkill) {
                mouseClickSkill = false;
            }
        }

        //abfrage für den soullink
        if (soullinkAlive && soullinkTarget != null) {

            //heilen über zeit
            if (!hOTSkill21Timer.IsOnCooldown()) {
                photonView.RPC("HealerAttackSkill1Heal", PhotonTargets.MasterClient, this.gameObject.GetPhotonView().viewID, healerAttributesScript.hOTHealSkill21);
                photonView.RPC("HealerAttackSkill1Heal", PhotonTargets.MasterClient, soullinkTarget.GetPhotonView().viewID, healerAttributesScript.hOTHealSkill21);
                hOTSkill21Timer.StartCooldown();
            }

            if (Vector3.Distance(transform.position, soullinkTarget.transform.position) > healerAttributesScript.rangeSkill21 + 1.0f) {
                photonView.RPC("HealerAttackSkill21deaktivate", PhotonTargets.All, soullinkTarget.GetPhotonView().viewID);
                soullinkAlive = false;
                soullinkTarget = null;
                cdTSkill21.StartCooldown();
            }
        }


        if (iAmSilenced && Time.time > silenceEnd) {
            iAmSilenced = false;
        }


        //passiv skill
        if (passivActive && Time.time > passivTick) {
            passivTick = Time.time + 2.0f;

            if (GameObject.Find("Ranger") != null) {
                if (Vector3.Distance(GameObject.Find("Ranger").transform.position, transform.position) < healerAttributesScript.passivRange) {
                    photonView.RPC("PassivHeal", PhotonTargets.MasterClient, GameObject.Find("Ranger").GetPhotonView().viewID, healerAttributesScript.passivHeal);
                }
            }

            if (GameObject.Find("Tank") != null) {
                if (Vector3.Distance(GameObject.Find("Tank").transform.position, transform.position) < healerAttributesScript.passivRange) {
                    photonView.RPC("PassivHeal", PhotonTargets.MasterClient, GameObject.Find("Tank").GetPhotonView().viewID, healerAttributesScript.passivHeal);
                }
            }

        }

		//WIN | LOSE
		if (GameObject.Find ("GameManager") != null) {
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().huntersWin == true && winSoundPlayed == false) {
				PlayWinSound ();
				winSoundPlayed = true;
			}
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().monsterWin == true && loseSoundPlayed == false) {
				PlayLoseSound ();
				loseSoundPlayed = true;
			}
		}


    }

    void fireSkill(int number) {
        GetComponent<AnimaitonScript>().lookAt(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z));
        healerControlScript.attackMode = false;

        //holy light
        if (number == 1) {
            //heal oder attack?
            if (mouseHitboxScript.mouseOverTeammate) {

                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastTeammateGO.transform.position) < healerAttributesScript.rangeSkill1) {
                    photonView.RPC("HealerAttackSkill1Heal", PhotonTargets.MasterClient, mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID, healerAttributesScript.healPowerSkill1);
                    cdTSkill1.StartCooldown();
                    photonView.RPC("HealerHeiligesLichtAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                    animationScript.startAttackAnimationOne();
                    photonView.RPC("HealerHeiligesLichtHeal", PhotonTargets.All, mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID);

                    //SOUND HIER holylight heal
					photonView.RPC ("PlayHealerHolyLightHealSound", PhotonTargets.All, null);
                    movementScript.stopMove();
                } else {
                    attackSkill1Active = true;
                    curTarget = mouseHitboxScript.mouseOverLastTeammateGO;
                }
 
            } else if (mouseHitboxScript.mouseOverEnemy) {
                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastEnemyGO.transform.position) < healerAttributesScript.rangeSkill1) {
                    photonView.RPC("HealerAttackSkill1Damage", PhotonTargets.MasterClient, mouseHitboxScript.mouseOverLastEnemyGO.GetPhotonView().viewID, healerAttributesScript.damageSkill1);
                    cdTSkill1.StartCooldown();

                    photonView.RPC("HealerHeiligesLichtAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                    animationScript.startAttackAnimationOne();
                    photonView.RPC("HealerHeiligesLichtAttack", PhotonTargets.All, mouseHitboxScript.mouseOverLastEnemyGO.GetPhotonView().viewID);
                    revealMeHealer();
                    movementScript.stopMove();
                    //SOUND HIER holy light dmg
					photonView.RPC ("PlayHealerHolyLightDamageSound", PhotonTargets.All, null);

                } else {
                    attackSkill1Active2 = true;
                    curTarget = mouseHitboxScript.mouseOverLastEnemyGO;
                }

            }

            skillMode = false;
        }

        //absorb
        else if (number == 11) {


            if (skill11Stage == 1 && mouseHitboxScript.mouseOverEnemy) {
                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastEnemyGO.transform.position) < healerAttributesScript.rangeSkill11) {
                    photonView.RPC("HealerAttackSkill11Absorb", PhotonTargets.MasterClient, mouseHitboxScript.mouseOverLastEnemyGO.GetPhotonView().viewID, healerAttributesScript.stealSkill11);
                    animationScript.startAttackAnimationOne();
                    skill11Stage = 2;
                    skill11GetCooldown = true;
                    photonView.RPC("HealerAbsorbAni2", PhotonTargets.All, mouseHitboxScript.mouseOverLastEnemyGO.GetPhotonView().viewID);
                    revealMeHealer();

                    //SOUND HIER absorb 1 schaden
					photonView.RPC ("PlayHealerAbsorbDamageSound", PhotonTargets.All, null);
                    movementScript.stopMove();
                    return;
                } else {
                    attackSkill11Active = true;
                    curTarget = mouseHitboxScript.mouseOverLastEnemyGO;
                    skillMode = false;
                    return;
                }
            }

            if (skill11Stage == 2 && mouseHitboxScript.mouseOverTeammate) {
                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastTeammateGO.transform.position) < healerAttributesScript.rangeSkill11) {
                    photonView.RPC("HealerAttackSkill11Heal", PhotonTargets.MasterClient, mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID, healerAttributesScript.stealSkill11);
                    animationScript.startAttackAnimationOne();
                    skillMode = false;
                    photonView.RPC("HealerAbsorbAni1", PhotonTargets.All, mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID);

                    //SOUND HIER absorb 2 heal
					photonView.RPC ("PlayHealerAbsorbHealSound", PhotonTargets.All, null);
                    movementScript.stopMove();
                } else {
                    attackSkill11Active2 = true;
                    curTarget = mouseHitboxScript.mouseOverLastTeammateGO;
                    skillMode = false;
                    return;
                }

                skill11Stage = 0;
                if (skill11GetCooldown) {
                    cdTSkill11.StartCooldown();
                    skill11GetCooldown = false;
                }
            }
        }

        //schild
        else if (number == 2) {

            //nur auf teammate
            if (mouseHitboxScript.mouseOverTeammate) {
                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastTeammateGO.transform.position) < healerAttributesScript.rangeSkill2) {
                    photonView.RPC("HealerAttackSkill2", PhotonTargets.MasterClient, mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID, healerAttributesScript.powerSkill2, healerAttributesScript.durationSkill2);
                    cdTSkill2.StartCooldown();
                    photonView.RPC("startShildAnimation", PhotonTargets.All, mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID, healerAttributesScript.durationSkill2);
                    animationScript.startAttackAnimationOne();

                    //SOUND HIER schild
					photonView.RPC ("PlayHealerShieldSound", PhotonTargets.All, null);
                    movementScript.stopMove();
                } else {
                    attackSkill2Active = true;
                    curTarget = mouseHitboxScript.mouseOverLastTeammateGO;
                }
            }
            skillMode = false;
        }

        //soullink
        else if (number == 21) {

            //nur auf teammate
            if (mouseHitboxScript.mouseOverTeammate && mouseHitboxScript.mouseOverLastTeammateGO != null && mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<HealerAttributesScript>() == null) {
                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastTeammateGO.transform.position) < healerAttributesScript.rangeSkill21) {
                    photonView.RPC("HealerAttackSkill21", PhotonTargets.All, this.gameObject.GetPhotonView().viewID,
                        mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID, healerAttributesScript.speedFactorSkill21);
                    //hot CD starten
                    hOTSkill21Timer.StartCooldown();
                    soullinkAlive = true;
                    soullinkTarget = mouseHitboxScript.mouseOverLastTeammateGO;
                    animationScript.startAttackAnimationOne();

                    //SOUND HIER soullink
					photonView.RPC ("PlayHealerSoulLinkSound", PhotonTargets.All, null);
                    movementScript.stopMove();
                } else {
                    attackSkill21Active = true;
                    curTarget = mouseHitboxScript.mouseOverLastTeammateGO;
                }
            }
            skillMode = false;
        }



          //blink
          else if (number == 3) {
            //ziel berechnen
            Vector3 blinkTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            blinkTarget.z = transform.position.z;
            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            if (Vector3.Distance(blinkTarget, transform.position) > healerAttributesScript.rangeSkill3) {
                blinkTarget = blinkTarget - transform.position;
                blinkTarget.Normalize();
                blinkTarget = blinkTarget * healerAttributesScript.rangeSkill3;
                blinkTarget = blinkTarget + transform.position; 
            }

            photonView.RPC("startBlinkAnimation", PhotonTargets.All, transform.position, blinkTarget);
            animationScript.startAttackAnimationOne();
            movementScript.iBlinked(blinkTarget);   
            cdTSkill3.StartCooldown();
            skillMode = false;
            //SOUND HIER blink
			photonView.RPC ("PlayHealerBlinkSound", PhotonTargets.All, null);
        }
        /*

            //Portal
          else if (number == 31) {
            //Portalpoint 1
            Vector3 portalPoint1 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            portalPoint1.z = transform.position.z;

            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            if (Vector3.Distance(portalPoint1, transform.position) > healerAttributesScript.rangeSkill3) {
                portalPoint1 = portalPoint1 - transform.position;
                portalPoint1.Normalize();
                portalPoint1 = portalPoint1 * healerAttributesScript.rangeSkill3;

                portalPoint1 = portalPoint1 + transform.position;
            }
           portalPoint1.z = transform.position.z+1;

            if (portalCount == 0) {
                //portal 1 erstellen
                photonView.RPC("createPortal", PhotonTargets.MasterClient, 1, "Portal1", 0, portalPoint1, 5.0f);
                portalCount++;
            } else if (portalCount == 1) {
                //portal 2 erstellen und Timer Starten
                photonView.RPC("createPortal", PhotonTargets.MasterClient, 2, "Portal2", 0, portalPoint1, 5.0f);
                cdTSkill31.StartCooldown();
                skillMode = false;
                portalCount = 0;
            }
        }

         */

          //Port
        else if (number == 31) {
            //über mate
            if (mouseHitboxScript.mouseOverTeammate) {

                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastTeammateGO.transform.position) < healerAttributesScript.rangeSkill31) {
                    
                    //port
                    animationScript.startAttackAnimationOne();
                    photonView.RPC("HealerAttackSkill31", PhotonTargets.All, mouseHitboxScript.mouseOverLastTeammateGO.GetPhotonView().viewID, transform.position);
                    photonView.RPC("startPortAnimation", PhotonTargets.All, transform.position, mouseHitboxScript.mouseOverLastTeammateGO.transform.position);

                    //SOUND HIER port
					photonView.RPC ("PlayHealerPortSound", PhotonTargets.All, null);

                    cdTSkill31.StartCooldown();
                    movementScript.stopMove();
                }
                else {
                    attackSkill31Active = true;
                    curTarget = mouseHitboxScript.mouseOverLastTeammateGO;
                }

            }
            skillMode = false;
        }



    }

    private void skillActive() {

        if (attackSkill1Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) < healerAttributesScript.rangeSkill1) {
                photonView.RPC("HealerAttackSkill1Heal", PhotonTargets.MasterClient, curTarget.GetPhotonView().viewID, healerAttributesScript.healPowerSkill1);
                attackSkill1Active = false;
                cdTSkill1.StartCooldown();
                photonView.RPC("HealerHeiligesLichtAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                animationScript.startAttackAnimationOne();
                photonView.RPC("HealerHeiligesLichtHeal", PhotonTargets.All, curTarget.GetPhotonView().viewID);
                movementScript.stopMove();
                //SOUND HIER holylight heal
				photonView.RPC ("PlayHealerHolyLightHealSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, healerAttributesScript.rangeSkill1);
                }
            }
        } else if (attackSkill1Active2) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) < healerAttributesScript.rangeSkill1) {
                photonView.RPC("HealerAttackSkill1Damage", PhotonTargets.MasterClient, curTarget.GetPhotonView().viewID, healerAttributesScript.damageSkill1);
                attackSkill1Active2 = false;
                cdTSkill2.StartCooldown();
                photonView.RPC("HealerHeiligesLichtAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                animationScript.startAttackAnimationOne();
                photonView.RPC("HealerHeiligesLichtAttack", PhotonTargets.All, curTarget.GetPhotonView().viewID);
                revealMeHealer();
                movementScript.stopMove();
                //SOUND HIER holy light dmg
				photonView.RPC ("PlayHealerHolyLightDamageSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, healerAttributesScript.rangeSkill1);
                }
            }
        } else if (attackSkill11Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) < healerAttributesScript.rangeSkill11) {
                photonView.RPC("HealerAttackSkill11Absorb", PhotonTargets.MasterClient, curTarget.GetPhotonView().viewID, healerAttributesScript.stealSkill11);
                attackSkill11Active = false;
                skillMode = true;
                skill11Stage = 2;
                skill11GetCooldown = true;
                animationScript.startAttackAnimationOne();
                photonView.RPC("HealerAbsorbAni2", PhotonTargets.All, curTarget.GetPhotonView().viewID);
                revealMeHealer();
                movementScript.stopMove();
                //SOUND HIER absorb dmg
				photonView.RPC ("PlayHealerAbsorbDamageSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, healerAttributesScript.rangeSkill11);
                }
            }
        } else if (attackSkill11Active2) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) < healerAttributesScript.rangeSkill11) {
                photonView.RPC("HealerAttackSkill11Heal", PhotonTargets.MasterClient, curTarget.GetPhotonView().viewID, healerAttributesScript.stealSkill11);
                attackSkill11Active2 = false;
                skill11Stage = 0;
                cdTSkill11.StartCooldown();
                skill11GetCooldown = false;
                animationScript.startAttackAnimationOne();
                photonView.RPC("HealerAbsorbAni1", PhotonTargets.All, curTarget.GetPhotonView().viewID);
                movementScript.stopMove();
                //SOUND HIER absorb heal
				photonView.RPC ("PlayHealerAbsorbHealSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, healerAttributesScript.rangeSkill11);
                }
            }
        } else if (attackSkill2Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.transform.position) < healerAttributesScript.rangeSkill2) {
                photonView.RPC("HealerAttackSkill2", PhotonTargets.MasterClient, curTarget.GetPhotonView().viewID, healerAttributesScript.powerSkill2, healerAttributesScript.durationSkill2);
                attackSkill2Active = false;
                photonView.RPC("startShildAnimation", PhotonTargets.All, curTarget.GetPhotonView().viewID, healerAttributesScript.durationSkill2);
                animationScript.startAttackAnimationOne();
                movementScript.stopMove();
                //SOUND HIER schild
				photonView.RPC ("PlayHealerShieldSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, healerAttributesScript.rangeSkill2);
                }
            }
        } else if (attackSkill21Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.transform.position) < healerAttributesScript.rangeSkill21) {
                photonView.RPC("HealerAttackSkill21", PhotonTargets.All, this.gameObject.GetPhotonView().viewID, curTarget.GetPhotonView().viewID, healerAttributesScript.speedFactorSkill21);
                animationScript.startAttackAnimationOne();
                soullinkAlive = true;
                soullinkTarget = mouseHitboxScript.mouseOverLastTeammateGO;
                //hot CD starten
                hOTSkill21Timer.StartCooldown();
                attackSkill2Active = false;
                movementScript.stopMove();
                //SOUND HIER soullink
				photonView.RPC ("PlayHealerSoulLinkSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, healerAttributesScript.rangeSkill21);
                }
            }
        }
        else if (attackSkill31Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) < healerAttributesScript.rangeSkill31) {
                photonView.RPC("HealerAttackSkill31", PhotonTargets.All, curTarget.GetPhotonView().viewID, transform.position);
                cdTSkill31.StartCooldown();
                attackSkill31Active = false;
                photonView.RPC("startPortAnimation", PhotonTargets.All, transform.position, curTarget.transform.position);
                animationScript.startAttackAnimationOne();
                movementScript.stopMove();
                //SOUND HIER port
				photonView.RPC ("PlayHealerPortSound", PhotonTargets.All, null);
            }
            else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, healerAttributesScript.rangeSkill31);
                }
            }
        }
    }

    public void resetActiveSkills() {
        attackSkill1Active = false;
        attackSkill1Active2 = false;
        attackSkill11Active = false;
        attackSkill11Active2 = false;
        attackSkill2Active = false;
        attackSkill21Active = false;

        if (skill11GetCooldown && skill11Stage != 2) {
            cdTSkill11.StartCooldown();
            skill11GetCooldown = false;
        }

    }


    private void checkUpKeyPressed() {

            if (!offerForQSkill && qSkill == 0 && Input.GetKeyDown(KeyCode.Q) && !cdTSkill1.IsOnCooldown())
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 1;

            }
            else if (!offerForWSkill && wSkill == 0 && Input.GetKeyDown(KeyCode.W) && !cdTSkill2.IsOnCooldown())
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 2;

            }
            else if (!offerForESkill && eSkill == 0 && Input.GetKeyDown(KeyCode.E) && !cdTSkill3.IsOnCooldown())
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 3;
            }
            else if (qSkill == 1 && Input.GetKeyDown(KeyCode.Q) && !cdTSkill11.IsOnCooldown() && !skill11GetCooldown)
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 11;
                skill11Stage = 1;
            }
            else if (!soullinkAlive && wSkill == 1 && Input.GetKeyDown(KeyCode.W) && !cdTSkill21.IsOnCooldown())
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 21;
            }
            else if (eSkill == 1 && Input.GetKeyDown(KeyCode.E) && !cdTSkill31.IsOnCooldown())
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 31;
            }




            //LÖSCHEN
        /*
            if (Input.GetKeyDown(KeyCode.A) && !cdTSkill11.IsOnCooldown() && !skill11GetCooldown)
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 11;
                skill11Stage = 1;
            }
            else if (!soullinkAlive && Input.GetKeyDown(KeyCode.S) && !cdTSkill21.IsOnCooldown())
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 21;
            }
            else if (Input.GetKeyDown(KeyCode.D) && !cdTSkill31.IsOnCooldown())
            {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 31;
            }
         */
    }

    public void mouseClickOnSkill(int skill) {

        if (healerAttributesScript.isAlive && !iAmSilenced && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {

            mouseClickSkill = true;

            if (!offerForQSkill && qSkill == 0 && skill == 1 && !cdTSkill1.IsOnCooldown()) {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 1;

            }
            else if (!offerForWSkill && wSkill == 0 && skill == 2 && !cdTSkill2.IsOnCooldown()) {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 2;

            }
            else if (!offerForESkill && eSkill == 0 && skill == 3 && !cdTSkill3.IsOnCooldown()) {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 3;
            }
            else if (qSkill == 1 && skill == 1 && !cdTSkill11.IsOnCooldown() && !skill11GetCooldown) {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 11;
                skill11Stage = 1;
            }
            else if (!soullinkAlive && wSkill == 1 && skill == 2 && !cdTSkill21.IsOnCooldown()) {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 21;
            }
            else if (eSkill == 1 && skill == 3 && !cdTSkill31.IsOnCooldown()) {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 31;
            }
        }
    }





    public int getQSkill()
    {
        return qSkill;
    }

    public int getWSkill()
    {
        return wSkill;
    }

    public int getESkill()
    {
        return eSkill;
    }


    [RPC]
    void HealerAttackSkill1Heal(int viewID, float healPower) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (PhotonNetwork.isMasterClient) {
            if (trans.GetComponent<RangerAttributesScript>() != null) {
                trans.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(healPower, false);
            }
            else if (trans.GetComponent<TankAttributesScript>() != null) {
                trans.GetComponent<TankAttributesScript>().AddjustCurrentHealth(healPower, false);
            }
            else if (trans.GetComponent<HealerAttributesScript>() != null) {
                trans.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(healPower, false);
            }
        }
    }

    [RPC]
    void HealerAttackSkill1Damage(int viewID, float damage) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (PhotonNetwork.isMasterClient && trans.GetComponent<MonsterAttributesScript>() != null) {
            trans.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(-damage);
        }
    }

    [RPC]
    void HealerAttackSkill11Absorb(int viewID, float damage) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (PhotonNetwork.isMasterClient && trans.GetComponent<MonsterAttributesScript>() != null) {
            trans.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(-damage);
        }
    }

    [RPC]
    void HealerAttackSkill11Heal(int viewID, float healPower) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (PhotonNetwork.isMasterClient) {
            if (trans.GetComponent<RangerAttributesScript>() != null) {
                trans.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(healPower, false);
            }
            else if (trans.GetComponent<TankAttributesScript>() != null) {
                trans.GetComponent<TankAttributesScript>().AddjustCurrentHealth(healPower, false);
            }
            else if (trans.GetComponent<HealerAttributesScript>() != null) {
                trans.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(healPower, false);
            }
        }
    }


    [RPC]
    void createPortal(int portalNr, string name, int viewID, Vector3 portalPoint, float sec) {
        //Master
        if (viewID == 0) {
            if (portalNr == 1) {
                portal1 = PhotonNetwork.Instantiate("Portal", portalPoint, transform.rotation, 0) as GameObject;
                portal1.name = name;
                photonView.RPC("createPortal", PhotonTargets.Others, portalNr, name, portal1.GetPhotonView().viewID, portalPoint, sec);
            }
            if (portalNr == 2) {
                portal2 = PhotonNetwork.Instantiate("Portal", portalPoint, transform.rotation, 0) as GameObject;
                portal2.name = name;
                photonView.RPC("createPortal", PhotonTargets.Others, portalNr, name, portal2.GetPhotonView().viewID, portalPoint, sec);

                //Timer Starten
                portal1.GetComponent<PortalScript>().startTimer(sec);
                portal2.GetComponent<PortalScript>().startTimer(sec);
            }
        } else {
            //Clients
            if (portalNr == 1) {
                portal1 = PhotonView.Find(viewID).gameObject;
                portal1.name = name;
            }
            if (portalNr == 2) {
                portal2 = PhotonView.Find(viewID).gameObject;
                portal2.name = name;

                //Timer Starten
                portal1.GetComponent<PortalScript>().startTimer(sec);
                portal2.GetComponent<PortalScript>().startTimer(sec);
            }

        }

    }

    [RPC]
    void HealerAttackSkill2(int viewID, float shieldPower, float shieldDuration) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (trans.GetComponent<RangerAttributesScript>() != null) {
            trans.GetComponent<RangerAttributesScript>().AddjustCurrentShield(shieldPower, shieldDuration);
        } else if (trans.GetComponent<TankAttributesScript>() != null) {
            trans.GetComponent<TankAttributesScript>().AddjustCurrentShield(shieldPower, shieldDuration);
        } else if (trans.GetComponent<HealerAttributesScript>() != null) {
            trans.GetComponent<HealerAttributesScript>().AddjustCurrentShield(shieldPower, shieldDuration);
        }
    }

    [RPC]
    void HealerAttackSkill21(int originViewID, int destViewID, float speedFactor) {
        Transform origin = PhotonView.Find(originViewID).transform;
        Transform dest = PhotonView.Find(destViewID).transform;

        //line starten
        drawLineScript.startDrawLine(origin, dest);

        //schneller werden
        healerAttributesScript.movementSpeed = healerAttributesScript.movementSpeed * speedFactor;
        if (dest.GetComponent<RangerAttributesScript>() != null) {
            dest.GetComponent<RangerAttributesScript>().movementSpeed = dest.GetComponent<RangerAttributesScript>().movementSpeed * speedFactor;
        }
        if (dest.GetComponent<TankAttributesScript>() != null) {
            dest.GetComponent<TankAttributesScript>().movementSpeed = dest.GetComponent<TankAttributesScript>().movementSpeed * speedFactor;
        }

    }

    [RPC]
    void HealerAttackSkill21deaktivate(int destViewID) {
        drawLineScript.stopDrawLine();



        //speed zurücksetzen
        Transform dest = PhotonView.Find(destViewID).transform;
        healerAttributesScript.movementSpeed = healerAttributesScript.oldMovementSpeed;

        if (dest.GetComponent<RangerAttributesScript>() != null) {
            dest.GetComponent<RangerAttributesScript>().movementSpeed = dest.GetComponent<RangerAttributesScript>().oldMovementSpeed;
        }
        if (dest.GetComponent<TankAttributesScript>() != null) {
            dest.GetComponent<TankAttributesScript>().movementSpeed = dest.GetComponent<TankAttributesScript>().oldMovementSpeed;
        }
    }

   [RPC]
    void startBlinkAnimation(Vector3 target1, Vector3 target2) {
        Instantiate(prefabBlink1, target1, transform.rotation);
        Instantiate(prefabBlink1, target2, transform.rotation);
    }

   [RPC]
   void startPortAnimation(Vector3 target1, Vector3 target2) {
       Instantiate(prefabPort1, target1, transform.rotation);
       Instantiate(prefabPort1, target2, transform.rotation);
   }

   [RPC]
   void startShildAnimation(int viewID, float time) {
       Transform go = Instantiate(prefabShield, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
       go.GetComponent<EuleSchildDestroyScript>().target = PhotonView.Find(viewID).gameObject;
       go.GetComponent<EuleSchildDestroyScript>().time = Time.time + time;
   }

   [RPC]
   void HealerAttackSkill31(int viewID, Vector3 pos) {
       GameObject go = PhotonView.Find(viewID).gameObject;
       go.GetComponent<Movement>().iBlinked(pos);
   }

    
   [RPC]
   void HealerHeiligesLichtAni(int viewID) {
       Transform go = Instantiate(prefabHeiligesLichtAni, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
       go.GetComponent<HealerHeiligesLichtAni>().target = this.gameObject;
       go.GetComponent<HealerHeiligesLichtAni>().animatoreule = this.GetComponent<Animator>();
   }

   [RPC]
   void HealerHeiligesLichtAttack(int viewID) {
       Transform go = Instantiate(prefabHeiligesLichtAttack, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
       go.GetComponent<HealerHeiligesLichtAttackHeal>().target = PhotonView.Find(viewID).gameObject;
   }

   [RPC]
   void HealerHeiligesLichtHeal(int viewID) {
       Transform go = Instantiate(prefabHeiligesLichtHeal, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
       go.GetComponent<HealerHeiligesLichtAttackHeal>().target = PhotonView.Find(viewID).gameObject;
   }

   [RPC]
   void HealerAbsorbAni1(int viewID) {
       Transform go = Instantiate(prefabAbsorb1, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
       go.GetComponent<HealerAbsorb1>().target = PhotonView.Find(viewID).gameObject;
   }

   [RPC]
   void HealerAbsorbAni2(int viewID) {
       Transform go = Instantiate(prefabAbsorb2, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
       go.GetComponent<HealerAbsorb2>().target = PhotonView.Find(viewID).gameObject;
   }

   public void offerToChangeSkill(int number) {
       if (photonView.isMine) {
           if (number == 1) {
               offerForQSkill = true;
           }
           if (number == 2) {
               offerForWSkill = true;
           }
           if (number == 3) {
               offerForESkill = true;
           }
       }
   }



   public bool getOfferForQSkill()
   {
       return offerForQSkill;
   }

   public bool getOfferForWSkill()
   {
       return offerForWSkill;
   }
   public bool getOfferForESkill()
   {
       return offerForESkill;
   }

   public void ChangeQSkill()
   {
       PlaySkillActivatedSound();

       offerForQSkill = false;
       qSkill = 1;
   }
   public void ChangeWSkill()
   {
       PlaySkillActivatedSound();

       offerForWSkill = false;
       wSkill = 1;
   }
   public void ChangeESkill()
   {
       PlaySkillActivatedSound();

       offerForESkill = false;
       eSkill = 1;
   }

   public void improveQSkill()
   {
			PlaySkillActivatedSound();
            healerAttributesScript.rangeSkill1 = healerAttributesScript.rangeSkill1 + healerAttributesScript.rangeSkill1 * 0.1f;
            healerAttributesScript.damageSkill1 = healerAttributesScript.damageSkill1 + healerAttributesScript.damageSkill1 * 0.1f;
            healerAttributesScript.healPowerSkill1 = healerAttributesScript.healPowerSkill1 + healerAttributesScript.healPowerSkill1 * 0.1f;

            offerForQSkill = false;
   }

   public void improveWSkill()
   {
			PlaySkillActivatedSound();
            healerAttributesScript.powerSkill2 = healerAttributesScript.powerSkill2 + healerAttributesScript.powerSkill2 * 0.1f;
            healerAttributesScript.durationSkill2 = healerAttributesScript.durationSkill2 + healerAttributesScript.durationSkill2 * 0.1f;
            healerAttributesScript.rangeSkill2 = healerAttributesScript.rangeSkill2 + healerAttributesScript.rangeSkill2 * 0.1f;

            offerForWSkill = false;
   }
   public void improveESkill()
   {
			PlaySkillActivatedSound();
            healerAttributesScript.rangeSkill3 = healerAttributesScript.rangeSkill3 + healerAttributesScript.rangeSkill3 * 0.1f;

            offerForESkill = false;
   }

   public void silenceMe(float time) {
       if (photonView.isMine) {
           iAmSilenced = true;
           silenceEnd = Time.time + time;
       }
   }

   [RPC]
   void PassivHeal(int viewID, float heal) {
       Transform trans = PhotonView.Find(viewID).transform;
       //fallunterscheidung
       if (PhotonNetwork.isMasterClient) {
           if (trans.GetComponent<RangerAttributesScript>() != null) {
               trans.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(heal, false);
           }
           else if (trans.GetComponent<TankAttributesScript>() != null) {
               trans.GetComponent<TankAttributesScript>().AddjustCurrentHealth(heal, false);
           }
           else if (trans.GetComponent<HealerAttributesScript>() != null) {
               trans.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(heal, false);
           }
       }
   }

   public void revealMeHealer() {
       if (GameObject.Find("Monster") != null)
           photonView.RPC("revealMeHealerRPC", GameObject.Find("Monster").GetPhotonView().owner);
   }


   [RPC]
   public void revealMeHealerRPC() {
       if (GameObject.Find("Monster") != null) {
           GameObject.Find("Monster").GetComponent<InviChecker>().revealHealer();
       }
   }

	// Sound Methods
	private void PlaySkillActivatedSound() 
	{
		PlaySound(0,0.45f);
	}

	[RPC]
	public void PlayHealerHolyLightHealSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (1);
	}

	[RPC]
	public void PlayHealerHolyLightDamageSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (2);
	}

	[RPC]
	public void PlayHealerAbsorbHealSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
		PlaySound (3);
	}

	[RPC]
	public void PlayHealerAbsorbDamageSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (4);
	}

	[RPC]
	public void PlayHealerShieldSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (5);
	}

	[RPC]
	public void PlayHealerSoulLinkSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (6);
	}

	[RPC]
	public void PlayHealerBlinkSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (7);
	}

	[RPC]
	public void PlayHealerPortSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (8);
	}


	private void PlaySound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	private void PlaySound(int Number, float volume)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, volume*menuScript.scaleVolumeFactor);
	}

	private void PlayWinSound()
	{
		AudioSource.PlayClipAtPoint(winSound, transform.position, 0.75f*menuScript.scaleVolumeFactor);
	}

	private void PlayLoseSound()
	{
		AudioSource.PlayClipAtPoint(loseSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}

    public void showCircle(int which) {
        if (which == 1 && qSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (healerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (healerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 1 && qSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (healerAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (healerAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 2 && wSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (healerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (healerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 2 && wSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (healerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (healerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 3 && eSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (healerAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (healerAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 3 && eSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (healerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (healerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
    }


}
