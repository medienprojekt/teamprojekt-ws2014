﻿using UnityEngine;
using System.Collections;

public class HealerAttributesScript : Photon.MonoBehaviour {

	private Menu menuScript;

    //Health
    public float maxHealth = 3000;
    public float curHealth = 3000;
    public float healthBarLength;

    //Other Attributes
    public float attackDamage;
    public float attackSpeed;
    public float attackRange;
    public float movementSpeed = 5.0f;
    public string playerClass;

    public float oldMovementSpeed;

    public float cdSkill1 = 3.0f;
    public float cdSkill2 = 3.0f;
    public float cdSkill3 = 3.0f;
    public float cdSkill11 = 3.0f;
    public float cdSkill21 = 3.0f;
    public float cdSkill31 = 3.0f;

    public float damageSkill1 = 20;
    public float healPowerSkill1 = 20;
    public float rangeSkill1 = 5.0f;

    public float stealSkill11 = 15;
    public float rangeSkill11 = 5.0f;

    public float powerSkill2 = 40;
    public float durationSkill2 = 5.0f;
    public float rangeSkill2 = 5.0f;

    public float speedFactorSkill21 = 1.5f;
    public float hOTHealSkill21 = 10.0f;
    public float hOTTickSkill21 = 2.0f;
    public float rangeSkill21 = 5.0f;

    public float rangeSkill3 = 6.0f;

    public float rangeSkill31 = 6.0f;



    public float shieldAmount;
    private float shieldCD;

    private float battleCryCD;
    private bool isBattleCryBuffed;
    private float oldAttackDamage;
    private float oldAttackSpeed;


    private bool inFire;
    private float fireTick;

    private HealerControlScript healerControlScript;

    public float passivHeal = 5.0f;
    public float passivRange = 6.0f;

    public bool isAlive;

    private bool startHitAnimation;
    private float hitAnimationTimer;
    private Color oldColor;

	public AudioClip[] sounds;
	public AudioClip[] sounds2;
	public AudioClip buffSound;
	public AudioSource asound;

    public Texture hitTex;
    private float alphaHit;

    public Texture deadTex;

    private Movement movementScript;

    // Use this for initialization
    void Start() {

		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

        name = "Healer";
        isAlive = true;

        oldColor = renderer.material.color;

        healerControlScript = GetComponent<HealerControlScript>();

        healthBarLength = Screen.width / 2;
        attackRange = 5;
        shieldAmount = 0;
        shieldCD = 0;
        isBattleCryBuffed = false;

        movementScript = GetComponent<Movement>();

        //Attribute initialisieren
        oldMovementSpeed = movementSpeed;

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];
    }

    // Update is called once per frame
    void Update() {

        if (!isAlive) {
            curHealth = 0;
            movementScript.stopMove();
        }

        if (startHitAnimation &&  Time.time > hitAnimationTimer) {
            startHitAnimation = false;
            this.GetComponent<SpriteRenderer>().color = oldColor;

			//Sound
			PlayHitVoiceSound();
			PlayRPCHitSound();
        }

        if (PhotonNetwork.isMasterClient) {
            //alive checkup
            if (isAlive && curHealth <= 0) {
                photonView.RPC("deathRPCHealer", PhotonTargets.All);
            }

            //shieldCheckup
            if (shieldCD != 0 && shieldCD < Time.time) {
                shieldAmount = 0;
                //allen informieren -> schild vorbei
                photonView.RPC("AddjustOthersCurrentShieldHealer", PhotonTargets.Others, 0.0f, 0.0f);
            }
        }




        if (photonView.isMine == true) {


            //buff checkup
            if (isBattleCryBuffed && Time.time > battleCryCD) {
                //stats zurücksetzen
                battleCryUndoBuff();
                photonView.RPC("battleCryTimeIsUpHealer", PhotonTargets.Others);
            }

            if (inFire && fireTick <= Time.time) {
                fireTick = Time.time + 1.0f;
                photonView.RPC("fireDamageHealer", PhotonTargets.MasterClient, 60.0f);
            }
        }
    }

    public void reanimateMe() {
        photonView.RPC("reanimateMeHealer", PhotonTargets.All);
    }

    public void battleCryUndoBuff() {
        attackDamage = oldAttackDamage;
        attackSpeed = oldAttackSpeed;
        isBattleCryBuffed = false;
    }

    [RPC]
    public void fireDamageHealer(float damage){
        if(PhotonNetwork.isMasterClient)
        AddjustCurrentHealth(-damage, true);
    }

    public void AddjustCurrentHealth(float adj, bool soulstones) {
        if (adj < 0) {
            if (GameObject.Find("Monster") != null && soulstones) {
                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().soulsplinter += 10.0f;
            }

            //hit animation
            startHitAnimation = true;
            hitAnimationTimer = Time.time + 0.1f;
            Color color = new Color(1.0f, 0.0f, 0.0f, renderer.material.color.a);
            this.GetComponent<SpriteRenderer>().color = color;
            alphaHit = 1.0f;
        }

        if (adj < 0 && healerControlScript.reanimateMode) {
            healerControlScript.reanimateMode = false;
        }

        if (adj < 0 && healerControlScript.captureWard) {
            healerControlScript.captureWard = false;
        }
        

        //schild nicht vorhanden oder adj > 0 (heal)
        if (shieldAmount <= 0 || adj > 0) {
            curHealth += adj;

            if (curHealth < 1) {
                curHealth = 0;
            }

            if (curHealth > maxHealth) {
                curHealth = maxHealth;
            }

            if (maxHealth < 1) {
                maxHealth = 1;
            }

            healthBarLength = (Screen.width / 2) * (curHealth / maxHealth);

            //wenn vom master gecallt
            if (PhotonNetwork.isMasterClient) {
                //bei allen anderen das leben anpassen
                photonView.RPC("AddjustOthersCurrentHealthHealer", PhotonTargets.Others, adj, soulstones);
            }
        }
        else {
            //schild vorhanden und damage
            shieldAmount += adj;
            if (shieldAmount < 0) {
                curHealth += shieldAmount;
                shieldAmount = 0;
            }

            healthBarLength = (Screen.width / 2) * ((curHealth + shieldAmount) / (maxHealth+shieldAmount));
        }
    }

    public void AddjustCurrentShield(float shield, float duration) {
        shieldAmount = shield;
        shieldCD = Time.time + duration;

        //alle anderen informieren
        if (PhotonNetwork.isMasterClient) {
            //bei allen anderen das leben anpassen
            photonView.RPC("AddjustOthersCurrentShieldHealer", PhotonTargets.Others, shield, duration);
        }
    }

    [RPC]
    void AddjustOthersCurrentShieldHealer(float shield, float duration) {
        gameObject.GetComponent<HealerAttributesScript>().AddjustCurrentShield(shield, duration);
    }

    [RPC]
    void AddjustOthersCurrentHealthHealer(float adj, bool soulstones) {
        gameObject.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(adj, soulstones);
    }

    public void battleCryBuffMeCheck(float percent, float time, float range, Vector3 tankPos) {
        if (Vector3.Distance(transform.position, tankPos) < range) {
            //buffen
            battleCryBuffMe(percent, time);
            if (PhotonNetwork.isMasterClient) {
                photonView.RPC("battleCryBuffOthersHealer", PhotonTargets.Others, percent, time);
            }
        }
    }

    public void battleCryBuffMe(float percent, float time) {
        isBattleCryBuffed = true;
        battleCryCD = Time.time + time;
        //stats buffen
        oldAttackDamage = attackDamage;
        oldAttackSpeed = attackSpeed;
        attackDamage = attackDamage * percent;
        attackSpeed = attackSpeed * percent;
    }

    [RPC]
    public void battleCryBuffOthersHealer(float percent, float time) {
        battleCryBuffMe(percent, time);
    }

    [RPC]
    public void battleCryTimeIsUpHealer() {
        battleCryUndoBuff();
    }

    [RPC]
    public void deathRPCHealer() {
        isAlive = false;
        curHealth = 0;
        if (photonView.isMine) {
            this.GetComponent<Movement>().iMoved(transform.position);
        }
    }

    [RPC]
    public void reanimateMeHealer() {
        curHealth = maxHealth / 3;
        isAlive = true;
    }


    public void iAmInFire(bool b) {

        if (b && !inFire) {
            inFire = true;
            fireTick = 0.0f;
        }

        if (!b) {
            inFire = false;
        }

    }


    public void buffMe(float percent) {
        if (photonView.isMine) {
            attackDamage = attackDamage + attackDamage * percent;
            attackSpeed = attackSpeed - attackSpeed * percent;
            healerControlScript.autoAttackCD = new CooldownTimer(attackSpeed);
            curHealth = curHealth + maxHealth * percent;
            maxHealth = maxHealth + maxHealth * percent;
            photonView.RPC("buffOnOthersHealer", PhotonTargets.Others, curHealth, maxHealth);

			PlayBuffSound();
        }
    }

    [RPC]
    public void buffOnOthersHealer(float newC, float newM) {
        curHealth = newC;
        maxHealth = newM;
    }

    void OnGUI() {


        if (photonView.isMine) {

            if (alphaHit > 0) {
                float rx = Screen.width / 1920.0f;
                float ry = Screen.height / 1080.0f;
                GL.PushMatrix();
                GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
                GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

                alphaHit -= 0.5f * Time.deltaTime;


                Color colPreviousGUIColor = GUI.color;

                GUI.color = new Color(colPreviousGUIColor.r, colPreviousGUIColor.g, colPreviousGUIColor.b, alphaHit);
                GUI.DrawTexture(new Rect(0, 0, hitTex.width, hitTex.height), hitTex);

                GUI.color = colPreviousGUIColor;

                GL.PopMatrix();
            }
            else if (GUI.color.a <= 1.0f) {
                GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            }

            if (!isAlive) {
                float rx = Screen.width / 1920.0f;
                float ry = Screen.height / 1080.0f;
                GL.PushMatrix();
                GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
                GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

                Color colPreviousGUIColor = GUI.color;

                GUI.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                GUI.DrawTexture(new Rect(0, 0, deadTex.width, deadTex.height), deadTex);

                GUI.color = colPreviousGUIColor;

                GL.PopMatrix();
            }




        }

    }

	//Sound Methods
	private void PlayHitSound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	private void PlayHitVoiceSound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds2[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	private void PlayRPCHitSound()
	{
		photonView.RPC ("PlayHitSound", PhotonTargets.Others, null);
	}
	
	public void PlayHitVoiceSound()
	{
		int r = Random.Range (0, 4);
		PlayHitVoiceSound (r);
	}

	public void PlayBuffSound()
	{
		AudioSource.PlayClipAtPoint(buffSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}

	[RPC]
	public void PlayHitSound()
	{
		int r = Random.Range (0, 3);
		PlayHitSound (r);
	}


}

