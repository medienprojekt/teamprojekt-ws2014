﻿using UnityEngine;
using System.Collections;

public class PortalScript : Photon.MonoBehaviour {

    public float destroyTime;
    public bool active;

    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        if (PhotonNetwork.isMasterClient) {
            if (active && Time.time > destroyTime) {
                PhotonNetwork.Destroy(this.gameObject);
            }
        }
    }

    public void startTimer(float sec) {
        destroyTime = Time.time + sec;
        active = true;
    }

    void OnDestroy() {
        transform.renderer.enabled = false;
    }

}
