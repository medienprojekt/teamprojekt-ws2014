﻿using UnityEngine;
using System.Collections;

public class HealerHeiligesLichtAni : Photon.MonoBehaviour {

    public Animator animatoreule;
    Animator animator;
    public GameObject target;
    float time;
	// Use this for initialization
	void Start () {
	    animator = this.GetComponent<Animator>();
        Vector3 pos = transform.position;
        pos.z = 1;
        transform.position = pos;
        time = Time.time + 0.4f;
	}
	
	// Update is called once per frame
	void Update () {

        if (!GameObject.Find("Healer").renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }

        Vector3 pos = transform.position;

        if (target != null) {
            pos = target.transform.position;
        }

        if (animatoreule != null) {
            animator.SetInteger("Direction", animatoreule.GetInteger("Direction"));

            if (animatoreule.GetInteger("Direction") == 2 && animatoreule.GetInteger("Direction") == 3) {
                pos.z = 3;
            }
            else {
                pos.z = 1;
            }

        }

        transform.position = pos;


        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
	}
}
