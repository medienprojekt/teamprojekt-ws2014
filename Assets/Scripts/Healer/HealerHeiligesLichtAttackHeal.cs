﻿using UnityEngine;
using System.Collections;

public class HealerHeiligesLichtAttackHeal : Photon.MonoBehaviour {

    public GameObject target;
    float time;

    // Use this for initialization
    void Start() {
        time = Time.time + 0.7f;
        Vector3 pos = transform.position;
        pos.y += 0.2f;
        pos.z = 1;
        transform.position = pos;
    }

    void Update() {

        if (!target.renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }

        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.y += 0.2f;
            pos.z = 1;
            transform.position = pos;
        }
        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }
}
