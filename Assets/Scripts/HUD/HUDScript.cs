﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts;

public class HUDScript : MonoBehaviour {

    private int whatAmI; //0 monster, 1 ranger, 2 healer, 3 tank
    private int qCD;
    private int wCD;
    private int eCD;
    private GameObject go;

    //Textures
    public Texture viewRect_Texture;
    public Texture skillBar_Back;
    public Texture skillBar_SkillFrame_Q;
    public Texture skillBar_SkillFrame_W;
    public Texture skillBar_SkillFrame_E;
    public Texture skillBar_SkillFrame_P;
    public Texture skillBar_SkillFrame_P_Active;
    public Texture skillBar_SkillFrame_Monster;
    public Texture skillBar_SkillFrame_Lantern;

    public Texture playerLife;
    public Texture playerLife_Back;
    public Texture daemonLife_Frame;
    public Texture daemonLife;
    public Texture daemonLife_Back;
    public Texture soulsplinters_Frame;
    public Texture soulsplinters_Count;
    public Texture teammates_Window;
    public Texture teammates_Frame;
    public Texture teammates_Tank;
    public Texture teammates_Support;
    public Texture teammates_Ranger;
    public Texture teammates_Life;
    public Texture teammates_Life_Back;

    public Texture ranger100;
    public Texture ranger50;
    public Texture ranger0;

    public Texture support100;
    public Texture support50;
    public Texture support0;

    public Texture tank100;
    public Texture tank50;
    public Texture tank0;

    //UpgradeWindow
    public Texture upgradeWindow_Back;
    public Texture selection_Frame1;
    public Texture selection_Frame1_Highlighted;
    public Texture selection_Frame2;
    public Texture selection_Frame2_Highlighted;
    public Texture improveSkill;
    public Texture changeSkill;

    //MonsterLoadout
    public Texture monsterLoadout_back;
    public Texture monsterLoadout_Spalte_Q;
    public Texture monsterLoadout_Spalte_W;
    public Texture monsterLoadout_Spalte_E;
    public Texture monsterLoadout_Spalte_R;
    public Texture monsterLoadout_button;

    //Win/Lose
    public Texture win_Screen;
    public Texture lost_Screen;
    public Texture button_Continue;

    //Skills
    public Texture skill_Cooldown;
    public Texture skill_Cooldown_Lantern;
    public Texture DescriptionBox;
        //Ranger
        public Texture skill_Schnellschuss;
        public Texture skill_Anker;
        public Texture skill_Backflip;
        public Texture skill_ChargeShot;
        public Texture skill_Trap;
        public Texture skill_Dash;
        public Texture skill_Passiv_Ranger;
        public Texture skill_Passiv_Ranger_active;

        //Support
        public Texture skill_HeiligesLicht;
        public Texture skill_Absorb;
        public Texture skill_Schild;
        public Texture skill_SoulLink;
        public Texture skill_Port;
        public Texture skill_Blink;
        public Texture skill_Passiv_Support;
        public Texture skill_Passiv_Support_active;

        //Tank
        public Texture skill_KnockBack;
        public Texture skill_Bash;
        public Texture skill_Barriere;
        public Texture skill_Taunt;
        public Texture skill_Charge;
        public Texture skill_WarCry;
        public Texture skill_Passiv_Tank;
        public Texture skill_Passiv_Tank_active;


        //Monster
        public Texture skill_Lantern;
        public Texture skill_Fireblow;
        public Texture skill_Fireblow_bw;
        public Texture skill_Astral;
        public Texture skill_Astral_bw;
        public Texture skill_Cleave;
        public Texture skill_Cleave_bw;
        public Texture skill_Flamecharge;
        public Texture skill_Flamecharge_bw;
        public Texture skill_Hands;
        public Texture skill_Hands_bw;
        public Texture skill_Hook;
        public Texture skill_Hook_bw;
        public Texture skill_Jump;
        public Texture skill_Jump_bw;
        public Texture skill_Silence;
        public Texture skill_Silence_bw;
        public Texture skill_Soulvolcano;
        public Texture skill_Soulvolcano_bw;
        public Texture skill_Soulvortex;
        public Texture skill_Soulvortex_bw;
        public Texture skill_Swordstrike;
        public Texture skill_Swordstrike_bw;

    //PauseMenu
        public Texture menu_Back;
        public Texture menu_Button_Continue;
        public Texture menu_Button_Options;
        public Texture menu_Button_Exit;
        public Texture menu_Options_Back;
        public Texture menu_Options_Knob;
        public Texture menu_Options_Apply;

    //Rects
    private Rect skillBar_Back_Rect;
    private Rect skill1_Rect;
    private Rect skill2_Rect;
    private Rect skill3_Rect;
    private Rect skill4_Rect;
    private Rect skill5_Rect;
    private Rect playerLife_Rect;
    private Rect MonsterLifeFrame_Rect;
    private Rect daemonLife_Rect;
    private Rect soulsplinters_Rect;
    private Rect teammates_Window_Rect;
    private Rect supportLife_Rect;
    private Rect rangerLife_Rect;
    private Rect tankLife_Rect;
    private Rect volume_Knob_Rect;
    private Rect scrollspeed_Knob_Rect;



    //PlayerLifeBar
    private float playerLife_Rect_Width;


    //playerlife
    private float maxLife;
    private float currentLife;
    private float playerlifePercent;


    //MatesLife
    private float supportMaxLife;
    private float supportCurrentLife;
    private float supportLifePercent;

    private float rangerMaxLife;
    private float rangerCurrentLife;
    private float rangerLifePercent;

    private float tankMaxLife;
    private float tankCurrentLife;
    private float tankLifePercent;


    //DaemonLife
    private float maxLife_Daemon;
    private float currentLife_Daemon;
    private float daemonlifepercent;

    private Material grayShader;

    private SkillDescriptionBox[] skillDescriptionBoxes;
    private SkillDescriptionBox[] monsterDescriptionBoxes;


    bool descSet;
    bool qDescChanged;
    bool wDescChanged;
    bool eDescChanged;
    bool rDescChanged;

    float currentLife_Altar1;
    float currentLife_Altar2;
    float currentLife_Altar3;
    float currentLife_Altar4;

    bool altar1_attacked;
    bool altar2_attacked;
    bool altar3_attacked;
    bool altar4_attacked;

    float currentTime;
    float savedTime;
    bool timerStarted;

    public SkillUpgradeWindow[] upgradeWindows;
    private bool qUpgradeVisible;
    private bool wUpgradeVisible;
    private bool eUpgradeVisible;

    private bool clickTimerActive;
    private bool activateTimer;
    private float currentTimer;

    public bool monsterLoadout;

    float current_Soulsplinters;
    float max_Soulsplinters;
    float soulplinters_Percent;

    public bool showMenu;
    public bool showOptions;

    public bool dragVolume;
    public bool dragScrollspeed;

    private float volume_knob_x;
    private float scrollspeed_knob_x;

    Rect options_Rect;
    Rect options_Volume_Rect;
    Rect options_Scrollspeed_Rect;
    GUIStyle style;

	// Use this for initialization
	void Start () {

        playerLife_Rect_Width = 625f;
        skillDescriptionBoxes = new SkillDescriptionBox[4];
        monsterDescriptionBoxes=new SkillDescriptionBox[13];
        descSet = false;
        qDescChanged = false;
        wDescChanged = false;
        eDescChanged = false;
        rDescChanged = false;

        currentLife_Altar1 = GameObject.Find("AltarPrefab1").GetComponent<AltarsScript>().health;
        currentLife_Altar2 = GameObject.Find("AltarPrefab2").GetComponent<AltarsScript>().health;
        currentLife_Altar3 = GameObject.Find("AltarPrefab3").GetComponent<AltarsScript>().health;
        currentLife_Altar4 = GameObject.Find("AltarPrefab4").GetComponent<AltarsScript>().health;

        altar1_attacked = false;
        altar2_attacked = false;
        altar3_attacked = false;
        altar4_attacked = false;
        timerStarted = false;

        upgradeWindows = new SkillUpgradeWindow[3];
        qUpgradeVisible = false;
        wUpgradeVisible = false;
        eUpgradeVisible = false;
        activateTimer = false;
        monsterLoadout = false;
        showMenu = false;
        showOptions = false;
        style = new GUIStyle();
        options_Rect = new Rect(1920 / 2 - menu_Options_Back.width / 2, 1080 / 2 - menu_Options_Back.height / 2, menu_Options_Back.width, menu_Options_Back.height);
        options_Volume_Rect = new Rect(options_Rect.x + 110, options_Rect.y + 97, 235, 26);
        options_Scrollspeed_Rect = new Rect(options_Rect.x + 110, options_Rect.y + 147, 235, 26);
        volume_knob_x = ((GameObject.Find("Menu").GetComponent<Menu>().getVolume() * options_Volume_Rect.width) + options_Volume_Rect.x);
        scrollspeed_knob_x = (options_Scrollspeed_Rect.x + options_Scrollspeed_Rect.width / 2) - menu_Options_Knob.width / 2;
        
	}

    public void reset()
    {
        descSet = false;
        qDescChanged = false;
        wDescChanged = false;
        eDescChanged = false;
        rDescChanged = false;

        altar1_attacked = false;
        altar2_attacked = false;
        altar3_attacked = false;
        altar4_attacked = false;
        timerStarted = false;

        qUpgradeVisible = false;
        wUpgradeVisible = false;
        eUpgradeVisible = false;
        activateTimer = false;
        monsterLoadout = false;
        showMenu = false;
        showOptions = false;
    }
	
	// Update is called once per frame
	void Update () {
        //if (whatAmI == 0) {
        //    enabled = false;
        //}

        volume_Knob_Rect = new Rect(volume_knob_x, (options_Volume_Rect.y + options_Volume_Rect.height / 2) - menu_Options_Knob.height / 2, menu_Options_Knob.width, menu_Options_Knob.height);
        scrollspeed_Knob_Rect = new Rect(scrollspeed_knob_x, (options_Scrollspeed_Rect.y + options_Scrollspeed_Rect.height / 2) - menu_Options_Knob.height / 2, menu_Options_Knob.width, menu_Options_Knob.height);



        //PlayerLife
        if (go != null)
        {
            if (whatAmI == 0)
            {
                max_Soulsplinters=100;
                current_Soulsplinters = go.GetComponent<MonsterSkillControlScript>().soulsplinter;
                if (current_Soulsplinters <= 3 && current_Soulsplinters>0)
                   {
                       soulplinters_Percent = 0.03f;
                   }
                 else
                   {
                       soulplinters_Percent = (100 / max_Soulsplinters * current_Soulsplinters) / 100;
                   }
                        
                 //SkillDescriptions
                        
                 //Q1
                monsterDescriptionBoxes[0] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[0].setKey("Q");
                monsterDescriptionBoxes[0].setTitle("Fire Blow");
                monsterDescriptionBoxes[0].setDescription("The Golem casts a cone of fire in\nthe selected direction.");

                //Q2
                monsterDescriptionBoxes[1] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[1].setKey("Q");
                monsterDescriptionBoxes[1].setTitle("Cleave");
                monsterDescriptionBoxes[1].setDescription("The Golem cleaves around him and hurts everyone in range.");

                //Q3
                monsterDescriptionBoxes[2] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[2].setKey("Q");
                monsterDescriptionBoxes[2].setTitle("Sword Strike");
                monsterDescriptionBoxes[2].setDescription("The Golem performs a hard sword strike\non the selected target.");

                //W1
                monsterDescriptionBoxes[3] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[3].setKey("W");
                monsterDescriptionBoxes[3].setTitle("Daemonic Hands");
                monsterDescriptionBoxes[3].setDescription("The Golem summons hands on the selected position,\nwhich grab all targets in it after a short delay.");

                //W2
                monsterDescriptionBoxes[4] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[4].setKey("W");
                monsterDescriptionBoxes[4].setTitle("Chain Hook");
                monsterDescriptionBoxes[4].setDescription("The Golem throws a chain in the selected\ndirection and pulls the target it hits to him.");

                //W3
                monsterDescriptionBoxes[5] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[5].setKey("W");
                monsterDescriptionBoxes[5].setTitle("Disturbing Cry");
                monsterDescriptionBoxes[5].setDescription("The Golem cries very loud, making everyone\nhit from it unable to cast any spells");

                //E1
                monsterDescriptionBoxes[6] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[6].setKey("E");
                monsterDescriptionBoxes[6].setTitle("Flame Charge");
                monsterDescriptionBoxes[6].setDescription("The Golem sprints in a selected direction,\nleaving a trail of flames behind.");

                //E2
                monsterDescriptionBoxes[7] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[7].setKey("E");
                monsterDescriptionBoxes[7].setTitle("Astral Spirit");
                monsterDescriptionBoxes[7].setDescription("The Golem changes himself into an astral spirit\nfor a short time, which can not be hit by anything.");

                //E3
                monsterDescriptionBoxes[8] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[8].setKey("E");
                monsterDescriptionBoxes[8].setTitle("Leap");
                monsterDescriptionBoxes[8].setDescription("The Golem jump in the selected area\nand hurts everyone he lands on.");

                //R1
                monsterDescriptionBoxes[9] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[9].setKey("R");
                monsterDescriptionBoxes[9].setTitle("Soul Vertex");
                monsterDescriptionBoxes[9].setDescription("The Golem pulls everyone in range\nto himself and damages them.\nThis Spell needs 100 Soulsplinters to be activated!");

                //R2
                monsterDescriptionBoxes[10] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[10].setKey("R");
                monsterDescriptionBoxes[10].setTitle("Soul Volcano");
                monsterDescriptionBoxes[10].setDescription("The Golem summons evil souls which fly\n to random targets in range and\ncause damage on them.\nThis Spell needs 100 Soulsplinters to be activated!");

                //Passive
                monsterDescriptionBoxes[11] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[11].setKey("P");
                monsterDescriptionBoxes[11].setTitle("Soulsplinters");
                monsterDescriptionBoxes[11].setDescription("The Golem collects Soulsplinters with damage he causes to his enemies.\n100 Soulsplinters are needed to perform his most powerful attacks.");

                //Laterne
                monsterDescriptionBoxes[12] = new SkillDescriptionBox(DescriptionBox);
                monsterDescriptionBoxes[12].setKey("T");
                monsterDescriptionBoxes[12].setTitle("Lantern");
                monsterDescriptionBoxes[12].setDescription("The Golem can leave his lantern behind to\nreveal the area around it and to teleport\nback to it again on second activation.");

                       if (GameObject.Find("AltarPrefab1").GetComponent<AltarsScript>().health < currentLife_Altar1)
                       {
                           currentLife_Altar1 = GameObject.Find("AltarPrefab1").GetComponent<AltarsScript>().health;
                           altar1_attacked = true;
                       }else if (GameObject.Find("AltarPrefab2").GetComponent<AltarsScript>().health < currentLife_Altar2){
                           currentLife_Altar2 = GameObject.Find("AltarPrefab2").GetComponent<AltarsScript>().health;
                           altar2_attacked = true;
                       }
                       else if (GameObject.Find("AltarPrefab3").GetComponent<AltarsScript>().health < currentLife_Altar3)
                       {
                           currentLife_Altar3 = GameObject.Find("AltarPrefab3").GetComponent<AltarsScript>().health;
                           altar3_attacked = true;
                       }
                       else if (GameObject.Find("AltarPrefab4").GetComponent<AltarsScript>().health < currentLife_Altar4)
                       {
                           currentLife_Altar4 = GameObject.Find("AltarPrefab4").GetComponent<AltarsScript>().health;
                           altar4_attacked = true;
                       }

                       currentLife = go.GetComponent<MonsterAttributesScript>().curHealth;
                       maxLife = go.GetComponent<MonsterAttributesScript>().maxHealth;

            }

            if (whatAmI == 1)
            {
                if (!descSet)
                {
                    skillDescriptionBoxes[0] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[0].setKey("Q");
                    skillDescriptionBoxes[0].setTitle("Fast Arrows");
                    skillDescriptionBoxes[0].setDescription("The Gecko shoots 3 fast and \n1 Charged Arrow at the Target");
                    skillDescriptionBoxes[1] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[1].setKey("W");
                    skillDescriptionBoxes[1].setTitle("Hook");
                    skillDescriptionBoxes[1].setDescription("The Gecko uses this skill to retain the Daemon.\nShoot the first Hook in the ground and the second on the Daemon.");
                    skillDescriptionBoxes[2] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[2].setKey("E");
                    skillDescriptionBoxes[2].setTitle("Backflip");
                    skillDescriptionBoxes[2].setDescription("The Gecko performs a backflip to \nget more range to the target.");
                    skillDescriptionBoxes[3] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[3].setKey("P");
                    skillDescriptionBoxes[3].setTitle("Passive");
                    skillDescriptionBoxes[3].setDescription("Destroy the Altar in the bottom\nright to unlock");
                    descSet = true;
                }

                if (go.GetComponent<RangerSkillControlScript>().getQSkill() == 1 && !qDescChanged)
                {
                    skillDescriptionBoxes[0].setTitle("Charge Shot");
                    skillDescriptionBoxes[0].setDescription("The Gecko charges a powerful arrow");
                    qDescChanged = true;
                }
                if (go.GetComponent<RangerSkillControlScript>().getWSkill() == 1 && !wDescChanged)
                {
                    skillDescriptionBoxes[1].setTitle("Trap");
                    skillDescriptionBoxes[1].setDescription("The Gecko sets up a trap which damages\nthe target on activation.");
                    wDescChanged = true;
                }
                if (go.GetComponent<RangerSkillControlScript>().getESkill() == 1 && !eDescChanged)
                {
                    skillDescriptionBoxes[2].setTitle("Dash");
                    skillDescriptionBoxes[2].setDescription("The Gecko moves very fast in the selected direction. Cloaks for a short moment.");
                    eDescChanged = true;
                }
                if (go.GetComponent<RangerSkillControlScript>().passivActive&&!rDescChanged)
                {
                    skillDescriptionBoxes[3].setTitle("Passive");
                    skillDescriptionBoxes[3].setDescription("Every shot inflicts poison to the target.\nStacks up to 4 times.");
                }

                //UpgradeWindows
                float upgradeWindowsY=610;

                upgradeWindows[0] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_Q, skill_Schnellschuss, skill_ChargeShot, DescriptionBox, upgradeWindowsY,"Q",1);
                upgradeWindows[1] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_W, skill_Anker, skill_Trap, DescriptionBox, upgradeWindowsY, "W", 1);
                upgradeWindows[2] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_E, skill_Backflip, skill_Dash, DescriptionBox, upgradeWindowsY, "E", 1);



                currentLife = go.GetComponent<RangerAttributesScript>().curHealth;
                maxLife = go.GetComponent<RangerAttributesScript>().maxHealth;
                
                //MatesLife
                if(GameObject.Find("Healer")!=null){

                    supportMaxLife = GameObject.Find("Healer").GetComponent<HealerAttributesScript>().maxHealth;
                    supportCurrentLife = GameObject.Find("Healer").GetComponent<HealerAttributesScript>().curHealth;

                    if (supportCurrentLife <= 350)
                    {
                        supportLifePercent = 0.11f;
                    }
                    else
                    {
                        supportLifePercent = (100 / supportMaxLife * supportCurrentLife) / 100;
                    }


                }
                else
                {
                    supportMaxLife = 3000;
                    supportCurrentLife = 3000;
                    supportLifePercent = 1.0f;
                }

                if (GameObject.Find("Tank") != null)
                {

                    tankMaxLife = GameObject.Find("Tank").GetComponent<TankAttributesScript>().maxHealth;
                    tankCurrentLife = GameObject.Find("Tank").GetComponent<TankAttributesScript>().curHealth;

                    if (tankCurrentLife <= 350)
                    {
                        tankLifePercent = 0.11f;
                    }
                    else
                    {
                        tankLifePercent = (100 / tankMaxLife * tankCurrentLife) / 100;
                    }

                }
                else
                {
                    tankMaxLife = 3000;
                    tankCurrentLife = 3000;
                    tankLifePercent = 1.0f;
                }


            }
            else if (whatAmI == 2)
            {

                if (!descSet)
                {
                    skillDescriptionBoxes[0] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[0].setKey("Q");
                    skillDescriptionBoxes[0].setTitle("Holy Light");
                    skillDescriptionBoxes[0].setDescription("The Owl uses this skill to heal \nitself or its teammates");
                    skillDescriptionBoxes[1] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[1].setKey("W");
                    skillDescriptionBoxes[1].setTitle("Shield");
                    skillDescriptionBoxes[1].setDescription("The Owl applies a shield to the target \nwhich absorbs a certain amount of damage");
                    skillDescriptionBoxes[2] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[2].setKey("E");
                    skillDescriptionBoxes[2].setTitle("Blink");
                    skillDescriptionBoxes[2].setDescription("The Owl teleports itself to the pointed location.");
                    skillDescriptionBoxes[3] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[3].setKey("P");
                    skillDescriptionBoxes[3].setTitle("Passive");
                    skillDescriptionBoxes[3].setDescription("Destroy the Altar in the bottom\nright to unlock");
                    descSet = true;
                }

                if (go.GetComponent<HealerSkillControlScript>().getQSkill() == 1 && !qDescChanged)
                {
                    skillDescriptionBoxes[0].setTitle("Absorb");
                    skillDescriptionBoxes[0].setDescription("The Owl absorbs life from the enemy and links it to a teammate.");
                    qDescChanged = true;
                }
                if (go.GetComponent<HealerSkillControlScript>().getWSkill() == 1 && !wDescChanged)
                {
                    skillDescriptionBoxes[1].setTitle("Soul Link");
                    skillDescriptionBoxes[1].setDescription("The Owl buffs the power of the selected teammate.\nThe lower the distance, the more effect does it have.");
                    wDescChanged = true;
                }
                if (go.GetComponent<HealerSkillControlScript>().getESkill() == 1 && !eDescChanged)
                {
                    skillDescriptionBoxes[2].setTitle("Port");
                    skillDescriptionBoxes[2].setDescription("The Owl can teleport the selected teammate to its own position");
                    eDescChanged = true;
                }
                if (go.GetComponent<HealerSkillControlScript>().passivActive && !rDescChanged)
                {
                    skillDescriptionBoxes[3].setTitle("Passive");
                    skillDescriptionBoxes[3].setDescription("The Owl has an aura which heals\nall teammates around it");
                }

                //UpgradeWindows
                float upgradeWindowsY = 610;
                upgradeWindows[0] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_Q, skill_HeiligesLicht, skill_Absorb, DescriptionBox, upgradeWindowsY, "Q", 2);
                upgradeWindows[1] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_W, skill_Schild, skill_SoulLink, DescriptionBox, upgradeWindowsY, "W", 2);
                upgradeWindows[2] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_E, skill_Blink, skill_Port, DescriptionBox, upgradeWindowsY, "E",2);

                currentLife = go.GetComponent<HealerAttributesScript>().curHealth;
                maxLife = go.GetComponent<HealerAttributesScript>().maxHealth;

                //MatesLife
                if (GameObject.Find("Ranger") != null)
                {

                    rangerMaxLife = GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().maxHealth;
                    rangerCurrentLife = GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().curHealth;

                    if (rangerCurrentLife <= 350)
                    {
                        rangerLifePercent = 0.11f;
                    }
                    else
                    {
                        rangerLifePercent = (100 / rangerMaxLife * rangerCurrentLife) / 100;
                    }

                }
                else
                {
                    rangerMaxLife = 3000;
                    rangerCurrentLife = 3000;
                    rangerLifePercent = 1.0f;
                }

                if (GameObject.Find("Tank") != null)
                {

                    tankMaxLife = GameObject.Find("Tank").GetComponent<TankAttributesScript>().maxHealth;
                    tankCurrentLife = GameObject.Find("Tank").GetComponent<TankAttributesScript>().curHealth;
                    if (tankCurrentLife <= 350)
                    {
                        tankLifePercent = 0.11f;
                    }
                    else
                    {
                        tankLifePercent = (100 / tankMaxLife * tankCurrentLife) / 100;
                    }

                }
                else
                {
                    tankMaxLife = 3000;
                    tankCurrentLife = 3000;
                    tankLifePercent = 1.0f;
                }


            }
            else if (whatAmI == 3)
            {

                if (!descSet)
                {
                    skillDescriptionBoxes[0] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[0].setKey("Q");
                    skillDescriptionBoxes[0].setTitle("Knockback");
                    skillDescriptionBoxes[0].setDescription("The Armadillo uses charges at the target and knocks it back.");
                    skillDescriptionBoxes[1] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[1].setKey("W");
                    skillDescriptionBoxes[1].setTitle("Barrier");
                    skillDescriptionBoxes[1].setDescription("The Armadillo summons a barrier in the picked direction.\nNothing can pierce the barrier while it's active.");
                    skillDescriptionBoxes[2] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[2].setKey("E");
                    skillDescriptionBoxes[2].setTitle("Charge");
                    skillDescriptionBoxes[2].setDescription("The Armadillo charges in the picked direction.\nIf it hits an enemy while in charge, the enemy gets damaged.");
                    skillDescriptionBoxes[3] = new SkillDescriptionBox(DescriptionBox);
                    skillDescriptionBoxes[3].setKey("P");
                    skillDescriptionBoxes[3].setTitle("Passive");
                    skillDescriptionBoxes[3].setDescription("Destroy the Altar in the bottom\nright to unlock");
                    descSet = true;
                }

                if (go.GetComponent<TankSkillControlScript>().getQSkill() == 1 && !qDescChanged)
                {
                    skillDescriptionBoxes[0].setTitle("Bash");
                    skillDescriptionBoxes[0].setDescription("The Armadillo performs a powerful strike,\nwhich slows the enemy");
                    qDescChanged = true;
                }
                if (go.GetComponent<TankSkillControlScript>().getWSkill() == 1 && !wDescChanged)
                {
                    skillDescriptionBoxes[1].setTitle("Taunt");
                    skillDescriptionBoxes[1].setDescription("The Armadillo forces the enemy to attack him");
                    wDescChanged = true;
                }
                if (go.GetComponent<TankSkillControlScript>().getESkill() == 1 && !eDescChanged)
                {
                    skillDescriptionBoxes[2].setTitle("War Cry");
                    skillDescriptionBoxes[2].setDescription("The Armadillo performs a war cry to buff all teammates in range");
                    eDescChanged = true;
                }
                if (go.GetComponent<TankSkillControlScript>().passivActive && !rDescChanged)
                {
                    skillDescriptionBoxes[3].setTitle("Passive");
                    skillDescriptionBoxes[3].setDescription("Reflects a percentage of taken damage\nback to the attacker.");
                }

                //UpgradeWindows
                float upgradeWindowsY = 610;
                upgradeWindows[0] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_Q, skill_KnockBack, skill_Bash, DescriptionBox, upgradeWindowsY, "Q", 3);
                upgradeWindows[1] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_W, skill_Barriere, skill_Taunt, DescriptionBox, upgradeWindowsY, "W", 3);
                upgradeWindows[2] = new SkillUpgradeWindow(upgradeWindow_Back, selection_Frame1, selection_Frame1_Highlighted, selection_Frame2, selection_Frame2_Highlighted, improveSkill, changeSkill, skillBar_SkillFrame_E, skill_Charge, skill_WarCry, DescriptionBox, upgradeWindowsY, "E", 3);


                currentLife = go.GetComponent<TankAttributesScript>().curHealth;
                maxLife = go.GetComponent<TankAttributesScript>().maxHealth;

                //MatesLife
                if (GameObject.Find("Healer") != null)
                {

                    supportMaxLife = GameObject.Find("Healer").GetComponent<HealerAttributesScript>().maxHealth;
                    supportCurrentLife = GameObject.Find("Healer").GetComponent<HealerAttributesScript>().curHealth;
                    if (supportCurrentLife <= 350)
                    {
                        supportLifePercent = 0.11f;
                    }
                    else
                    {
                        supportLifePercent = (100 / supportMaxLife * supportCurrentLife) / 100;
                    }

                }
                else
                {
                    supportMaxLife = 3000;
                    supportCurrentLife = 3000;
                    supportLifePercent = 1.0f;
                }

                if (GameObject.Find("Ranger") != null)
                {

                    rangerMaxLife = GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().maxHealth;
                    rangerCurrentLife = GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().curHealth;;

                    if (rangerCurrentLife <= 350)
                    {
                        rangerLifePercent = 0.11f;
                    }
                    else
                    {
                        rangerLifePercent = (100 / rangerMaxLife * rangerCurrentLife) / 100;
                    }
                }
                else
                {
                    rangerMaxLife = 3000;
                    rangerCurrentLife = 3000;
                    rangerLifePercent = 1.0f;
                }


            }
            if (currentLife <= 70)
            {
                playerlifePercent = 0.02f;
            }
            else
            {
                playerlifePercent = (100 / maxLife * currentLife) / 100;
            }
        }

        //MonsterLife
        if (GameObject.Find("Monster") != null)
        {
            maxLife_Daemon = GameObject.Find("Monster").GetComponent<MonsterAttributesScript>().maxHealth;
            currentLife_Daemon = GameObject.Find("Monster").GetComponent<MonsterAttributesScript>().curHealth;
            if (currentLife_Daemon <= 80)
            {
                daemonlifepercent = 0.02f;
            }
            else
            {
                daemonlifepercent = (100 / maxLife_Daemon * currentLife_Daemon) / 100;
            }

        }
        else
        {
            maxLife_Daemon = 3000;
            currentLife_Daemon = 3000;
            daemonlifepercent = 1.0f;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (showMenu)
            {

                showMenu = false;
            }
            else
            {
                showMenu = true;
            }

            if (showOptions)
            {
                showOptions = false;
                showMenu = true;
            }        
        }

	}

    public void setUp(int whatAmI, GameObject go) {
        this.go = go;
        this.whatAmI = whatAmI;
        enabled = true;
        GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().viewRect_Texture = viewRect_Texture;

        if (whatAmI == 1)
        {
            currentLife = go.GetComponent<RangerAttributesScript>().curHealth;
            maxLife = go.GetComponent<RangerAttributesScript>().maxHealth;
        }
        else if (whatAmI == 2)
        {
            currentLife = go.GetComponent<HealerAttributesScript>().curHealth;
            maxLife = go.GetComponent<HealerAttributesScript>().maxHealth;
        }
        else if (whatAmI == 3)
        {
            currentLife = go.GetComponent<TankAttributesScript>().curHealth;
            maxLife = go.GetComponent<TankAttributesScript>().maxHealth;
        }
        reset();
    }


    

    void clickTimer()
    {
        if (activateTimer&&!clickTimerActive)
        {
            currentTimer = Time.time;
            clickTimerActive = true;
        }
        if (clickTimerActive && Time.time >= currentTimer + 0.5f)
        {
            activateTimer = false;
            clickTimerActive = false;
        }
    }





    float native_width = 1920;
    float native_height = 1080;


    void OnGUI() {

        GUI.DrawTexture(new Rect(400, 1272, 100, 100), playerLife);
        if (!GameObject.Find("Menu").GetComponent<Menu>().inMenu)
        {
            int currentLife = Mathf.CeilToInt(this.currentLife);
            int maxLife = Mathf.CeilToInt(this.maxLife);
            int currentLife_Daemon = Mathf.CeilToInt(this.currentLife_Daemon);
            int maxLife_Daemon = Mathf.CeilToInt(this.maxLife_Daemon);

            float rx = Screen.width / native_width;
            float ry = Screen.height / native_height;
            GL.PushMatrix();
            GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
            GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));   
            
            if (whatAmI != 0)
            {
                for (int i = 0; i < skillDescriptionBoxes.Length; ++i)
                {
                    skillDescriptionBoxes[i].Draw();
                }
            }

            //Skillbar
            skillBar_Back_Rect = new Rect(1920 / 2 - skillBar_Back.width / 2, 1080 - skillBar_Back.height, skillBar_Back.width, skillBar_Back.height);

            GUI.DrawTexture(skillBar_Back_Rect, skillBar_Back);

            if (whatAmI != 0)
            {
                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 100, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_Q);

                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 242, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_W);

                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 384, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_E);


                skill1_Rect = new Rect(skillBar_Back_Rect.x + 100 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
                skill2_Rect = new Rect(skillBar_Back_Rect.x + 242 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
                skill3_Rect = new Rect(skillBar_Back_Rect.x + 384 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
                skill4_Rect = new Rect(skillBar_Back_Rect.x + 550 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
            }
            else
            {
                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 67, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_Monster);

                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 210, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_Monster);

                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 353, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_Monster);

                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 496, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_Monster);

                GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 625, skillBar_Back_Rect.y + 160, skillBar_SkillFrame_Lantern.width, skillBar_SkillFrame_Lantern.height), skillBar_SkillFrame_Lantern);

                skill1_Rect = new Rect(skillBar_Back_Rect.x + 67 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
                skill2_Rect = new Rect(skillBar_Back_Rect.x + 210 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
                skill3_Rect = new Rect(skillBar_Back_Rect.x + 353 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
                skill4_Rect = new Rect(skillBar_Back_Rect.x + 496 + (skillBar_SkillFrame_Q.width / 2) - (skill_Schnellschuss.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) - (skill_Schnellschuss.height / 2), skill_Schnellschuss.width, skill_Schnellschuss.height);
                skill5_Rect = new Rect(skillBar_Back_Rect.x + 625 + (skillBar_SkillFrame_Lantern.width / 2) - (skill_Lantern.width / 2), skillBar_Back_Rect.y + 160 + (skillBar_SkillFrame_Lantern.height / 2) - (skill_Lantern.height / 2), skill_Lantern.width, skill_Lantern.height);
            }


            Graphics.DrawTexture(new Rect(skillBar_Back_Rect.x + (skillBar_Back.width / 2) - 314f, 927, 625, 26), playerLife_Back, 9, 9, 0, 0);

            playerLife_Rect = new Rect(skillBar_Back_Rect.x + (skillBar_Back.width / 2) - 314f, 927, playerLife_Rect_Width * playerlifePercent, 26);
            if (this.currentLife > 0)
            {
                Graphics.DrawTexture(playerLife_Rect, playerLife, 9, 9, 0, 0);
            
            }



            style.fontSize = 25;
            style.normal.textColor = Color.black;
            GUI.Label(new Rect((((skillBar_Back_Rect.x + (skillBar_Back.width / 2) - 314f)+playerLife_Rect_Width/2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x / 2) + 2, ((927 + 26 / 2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y / 2) + 2, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y), "" + currentLife + "/" + maxLife, style);
            style.normal.textColor = Color.white;
            GUI.Label(new Rect(((skillBar_Back_Rect.x + (skillBar_Back.width / 2) - 314f) + playerLife_Rect_Width / 2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x / 2, (927 + 26 / 2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y / 2, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y), "" + currentLife + "/" + maxLife, style);



            if (whatAmI != 0)
            {
                //DescriptionBoxes
                if (skill1_Rect.Contains(Event.current.mousePosition))
                {
                    skillDescriptionBoxes[0].DrawBox(true);

                    if (whatAmI == 2 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().showCircle(1);
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().hoverQ = true;
                        if (Input.GetMouseButtonDown(0)){
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().mouseClickOnSkill(1);
                        }

                    }
                    else if (whatAmI == 1 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().showCircle(1);
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().hoverQ = true;
                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().mouseClickOnSkill(1);
                        }
                    }
                    else if (whatAmI == 3 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().showCircle(1);
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().hoverQ = true;
                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().mouseClickOnSkill(1);
                        }
                    }

                }
                else
                {
                    skillDescriptionBoxes[0].DrawBox(false);
                    if (whatAmI == 2) {
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().hoverQ = false;
                    }
                    else if (whatAmI == 1) {
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().hoverQ = false;
                    }
                    else if (whatAmI == 3) {
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().hoverQ = false;
                    }
                }

                if (skill2_Rect.Contains(Event.current.mousePosition))
                {
                    skillDescriptionBoxes[1].DrawBox(true);
                    if (whatAmI == 2 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().showCircle(2);
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().hoverW = true;

                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().mouseClickOnSkill(2);
                        }
                    }
                    else if (whatAmI == 1 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().showCircle(2);
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().hoverW = true;
                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().mouseClickOnSkill(2);
                        }
                    }
                    else if (whatAmI == 3 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().showCircle(2);
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().hoverW = true;
                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().mouseClickOnSkill(2);
                        }
                    }
                }
                else
                {
                    skillDescriptionBoxes[1].DrawBox(false);
                    if (whatAmI == 2) {
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().hoverW = false;
                    }
                    else if (whatAmI == 1) {
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().hoverW = false;
                    }
                    else if (whatAmI == 3) {
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().hoverW = false;
                    }
                }

                if (skill3_Rect.Contains(Event.current.mousePosition))
                {
                    skillDescriptionBoxes[2].DrawBox(true);
                    if (whatAmI == 2 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().showCircle(3);
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().hoverE = true;

                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().mouseClickOnSkill(3);
                        }
                    }
                    else if (whatAmI == 1 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().showCircle(3);
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().hoverE = true;
                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().mouseClickOnSkill(3);
                        }
                    }
                    else if (whatAmI == 3 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().showCircle(3);
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().hoverE = true;
                        if (Input.GetMouseButtonDown(0)) {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().mouseClickOnSkill(3);
                        }
                    }
                }
                else
                {
                    skillDescriptionBoxes[2].DrawBox(false);
                    if (whatAmI == 2) {
                        GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().hoverE = false;
                    }
                    else if (whatAmI == 1) {
                        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().hoverE = false;
                    }
                    else if (whatAmI == 3) {
                        GameObject.Find("Tank").GetComponent<TankSkillControlScript>().hoverE = false;
                    }
                }

                if (skill4_Rect.Contains(Event.current.mousePosition))
                {
                    skillDescriptionBoxes[3].DrawBox(true);
                }
                else
                {
                    skillDescriptionBoxes[3].DrawBox(false);
                }
            }
            else
            {
                if (skill1_Rect.Contains(Event.current.mousePosition))
                {
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 1)
                    {
                        monsterDescriptionBoxes[0].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(1);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverQ = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(1);
                            }
                        }
                    }
                    else if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 11)
                    {
                        monsterDescriptionBoxes[1].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(1);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverQ = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(1);
                            }
                        }
                    }
                    else if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 12)
                    {
                        monsterDescriptionBoxes[2].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(1);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverQ = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(1);
                            }
                        }
                    }
                }
                else
                {
                    monsterDescriptionBoxes[0].DrawBox(false);
                    monsterDescriptionBoxes[1].DrawBox(false);
                    monsterDescriptionBoxes[2].DrawBox(false);
                    if (whatAmI == 0) {
                        GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverQ = false;
                    }
                }

                if (skill2_Rect.Contains(Event.current.mousePosition))
                {
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 2)
                    {
                        monsterDescriptionBoxes[3].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(2);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverW = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(2);
                            }
                        }
                    }
                    else if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 21)
                    {
                        monsterDescriptionBoxes[4].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(2);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverW = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(2);
                            }
                        }

                    }
                    else if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 22)
                    {
                        monsterDescriptionBoxes[5].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(2);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverW = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(2);
                            }
                        }
                    }
                }
                else
                {
                    monsterDescriptionBoxes[3].DrawBox(false);
                    monsterDescriptionBoxes[4].DrawBox(false);
                    monsterDescriptionBoxes[5].DrawBox(false);
                    if (whatAmI == 0) {
                        GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverW = false;
                    }
                }

                if (skill3_Rect.Contains(Event.current.mousePosition))
                {
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 3)
                    {
                        monsterDescriptionBoxes[6].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(3);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverE = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(3);
                            }
                        }
                    }
                    else if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 31)
                    {
                        monsterDescriptionBoxes[7].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(3);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverE = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(3);
                            }
                        }
                    }
                    else if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 32)
                    {
                        monsterDescriptionBoxes[8].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(3);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverE = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(3);
                            }
                        }
                    }
                }
                else
                {
                    monsterDescriptionBoxes[6].DrawBox(false);
                    monsterDescriptionBoxes[7].DrawBox(false);
                    monsterDescriptionBoxes[8].DrawBox(false);
                    if (whatAmI == 0) {
                        GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverE = false;
                    }
                }

                if (skill4_Rect.Contains(Event.current.mousePosition))
                {
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill == 4)
                    {
                        monsterDescriptionBoxes[9].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(4);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverR = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(4);
                            }
                        }
                    }
                    else if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill == 41)
                    {
                        monsterDescriptionBoxes[10].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(4);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverR = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(4);
                            }
                        }
                    }
                }
                else
                {
                    monsterDescriptionBoxes[9].DrawBox(false);
                    monsterDescriptionBoxes[10].DrawBox(false);
                    if (whatAmI == 0) {
                        GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverR = false;
                    }
                }

                if (skill5_Rect.Contains(Event.current.mousePosition))
                {
                        monsterDescriptionBoxes[12].DrawBox(true);
                        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().showCircle(5);
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverT = true;
                            if (Input.GetMouseButtonDown(0)) {
                                GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().mouseClickOnSkill(5);
                            }
                        }
                }
                else
                {
                    monsterDescriptionBoxes[12].DrawBox(false);
                    if (whatAmI == 0) {
                        GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().hoverT = false;
                    }
                }

            }

            //


            //DaemonLife
            MonsterLifeFrame_Rect = new Rect(new Rect(1920 / 2f - daemonLife_Frame.width / 2f, 0, daemonLife_Frame.width, daemonLife_Frame.height));
            daemonLife_Rect = new Rect(MonsterLifeFrame_Rect.x + 106, MonsterLifeFrame_Rect.y + 58, 532 * daemonlifepercent, 24);

            //Teammates
            teammates_Window_Rect = new Rect(0, 1080 - teammates_Window.height, teammates_Window.width, teammates_Window.height);
            tankLife_Rect = new Rect(teammates_Window_Rect.x + 31, teammates_Window_Rect.y + 141, 124 * tankLifePercent, 18);
            supportLife_Rect = new Rect(teammates_Window_Rect.x + 207, teammates_Window_Rect.y + 141, 124 * supportLifePercent, 18);
            rangerLife_Rect = new Rect(teammates_Window_Rect.x + 207, teammates_Window_Rect.y + 141, 124 * rangerLifePercent, 18);

            //Monster Life

            if (whatAmI != 0)
            {

                Graphics.DrawTexture(MonsterLifeFrame_Rect, daemonLife_Frame);

                if (currentLife_Daemon > 0)
                {
                    Graphics.DrawTexture(daemonLife_Rect, daemonLife, 11, 11, 0, 0);
                }

                style.fontSize = 25;
                style.normal.textColor = Color.black;
                GUI.Label(new Rect(((MonsterLifeFrame_Rect.x + 106 + 532 / 2) - style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).x / 2) + 2, ((MonsterLifeFrame_Rect.y + 57 + 24 / 2) - style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).y / 2) + 2, style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).x, style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).y), "" + currentLife_Daemon + "/" + maxLife_Daemon, style);
                style.normal.textColor = Color.white;
                GUI.Label(new Rect((MonsterLifeFrame_Rect.x + 106 + 532 / 2) - style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).x / 2, (MonsterLifeFrame_Rect.y + 57 + 24 / 2) - style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).y / 2, style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).x, style.CalcSize(new GUIContent("" + currentLife_Daemon + "/" + maxLife_Daemon)).y), "" + currentLife_Daemon + "/" + maxLife_Daemon, style);

                if (whatAmI == 1)
                {
                    //UpgradeWindow
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getOfferForQSkill()&&!wUpgradeVisible&&!eUpgradeVisible)
                    {
                        qUpgradeVisible = true;
                        upgradeWindows[0].drawWindow();
                        if (upgradeWindows[0].skillImproved()&&!clickTimerActive)
                        {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().improveQSkill();
                            qUpgradeVisible = false;
                            activateTimer = true;
                            clickTimer();
                        }
                        else if (upgradeWindows[0].skillChanged() && !clickTimerActive)
                        {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().ChangeQSkill();
                            qUpgradeVisible = false;
                            activateTimer = true;
                            clickTimer();
                        }
                    }

                    else if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getOfferForWSkill()&&!qUpgradeVisible&&!eUpgradeVisible)
                    {
                        wUpgradeVisible = true;
                        upgradeWindows[1].drawWindow();
                        if (upgradeWindows[1].skillImproved() && !clickTimerActive)
                        {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().improveWSkill();
                            wUpgradeVisible = false;
                            activateTimer = true;
                            clickTimer();
                        }
                        else if (upgradeWindows[1].skillChanged() && !clickTimerActive)
                        {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().ChangeWSkill();
                            wUpgradeVisible = false;
                            activateTimer = true;
                            clickTimer();
                        }
                    }

                    else if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getOfferForESkill()&&!qUpgradeVisible&&!wUpgradeVisible)
                    {
                        eUpgradeVisible = true;
                        upgradeWindows[2].drawWindow();
                        if (upgradeWindows[2].skillImproved() && !clickTimerActive)
                        {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().improveESkill();
                            eUpgradeVisible = false;
                            activateTimer = true;
                            clickTimer();
                        }
                        else if (upgradeWindows[2].skillChanged() && !clickTimerActive)
                        {
                            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().ChangeESkill();
                            eUpgradeVisible = false;
                            activateTimer = true;
                            clickTimer();
                        }
                    }

                    clickTimer();
                    Graphics.DrawTexture(teammates_Window_Rect, teammates_Window);

                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), teammates_Frame);
                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), teammates_Frame);

                    if (GameObject.Find("Tank") != null)
                    {

                        if (tankLifePercent <= 1.0f && tankLifePercent >= 0.5f && GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), tank100);
                        }
                        else if (tankLifePercent < 0.5f && tankLifePercent > 0f && GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), tank50);
                        }
                        else if (!GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive)
                        {
                                Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), tank0);
                        }
                    }
                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 31, teammates_Window_Rect.y + 141, 124, 18), teammates_Life_Back, 9, 9, 0, 0);
                    if (tankCurrentLife > 0)
                    {
                        Graphics.DrawTexture(tankLife_Rect, teammates_Life, 9, 9, 0, 0);
                    }

                    if (GameObject.Find("Healer") != null)
                    {
                        if (supportLifePercent <= 1.0f && supportLifePercent >= 0.5f&&GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), support100);
                        }
                        else if (supportLifePercent < 0.5f && supportLifePercent > 0f && GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), support50);
                        }
                        else if (!GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), support0);
                        }
                    }
                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 31, teammates_Window_Rect.y + 141, 124, 18), teammates_Life_Back, 9, 9, 0, 0);
                    if (supportCurrentLife > 0)
                    {
                        Graphics.DrawTexture(supportLife_Rect, teammates_Life, 9, 9, 0, 0);
                    }
                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 207, teammates_Window_Rect.y + 141, 124, 18), teammates_Life_Back, 9, 9, 0, 0);
                    if (supportCurrentLife > 0)
                    {
                        Graphics.DrawTexture(supportLife_Rect, teammates_Life, 9, 9, 0, 0);
                    }

                }
                else if (whatAmI == 2)
                {

                    //UpgradeWindow
                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getOfferForQSkill())
                    {
                        upgradeWindows[0].drawWindow();
                        if (upgradeWindows[0].skillImproved())
                        {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().improveQSkill();
                        }
                        else if (upgradeWindows[0].skillChanged())
                        {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().ChangeQSkill();
                        }
                    }

                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getOfferForWSkill())
                    {
                        upgradeWindows[1].drawWindow();
                        if (upgradeWindows[1].skillImproved())
                        {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().improveWSkill();
                        }
                        else if (upgradeWindows[1].skillChanged())
                        {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().ChangeWSkill();
                        }
                    }

                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getOfferForESkill())
                    {
                        upgradeWindows[2].drawWindow();
                        if (upgradeWindows[2].skillImproved())
                        {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().improveESkill();
                        }
                        else if (upgradeWindows[2].skillChanged())
                        {
                            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().ChangeESkill();
                        }
                    }
                    Graphics.DrawTexture(teammates_Window_Rect, teammates_Window);

                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), teammates_Frame);
                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), teammates_Frame);

                    if (GameObject.Find("Tank") != null)
                    {
                        if (tankLifePercent <= 1.0f && tankLifePercent >= 0.5f && GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), tank100);
                        }
                        else if (tankLifePercent < 0.5f && tankLifePercent > 0f && GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), tank50);
                        }
                        else if (!GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), tank0);
                        }
                    }

                        Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 31, teammates_Window_Rect.y + 141, 124, 18), teammates_Life_Back, 9, 9, 0, 0);
                        if (tankCurrentLife > 0)
                        {
                            Graphics.DrawTexture(tankLife_Rect, teammates_Life, 9, 9, 0, 0);
                        }

                        if (GameObject.Find("Ranger") != null)
                        {
                            if (rangerLifePercent <= 1.0f && rangerLifePercent >= 0.5f && GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive)
                            {
                                Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), ranger100);
                            }
                            else if (rangerLifePercent < 0.5f && rangerLifePercent > 0f && GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive)
                            {
                                Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), ranger50);
                            }
                            else if (!GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive)
                            {
                                Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), ranger0);
                            }
                        }

                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 207, teammates_Window_Rect.y + 141, 124, 18), teammates_Life_Back, 9, 9, 0, 0);
                    if (rangerCurrentLife > 0)
                    {
                        Graphics.DrawTexture(rangerLife_Rect, teammates_Life, 9, 9, 0, 0);
                    }
                }
                else if (whatAmI == 3)
                {

                    //UpgradeWindow
                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getOfferForQSkill())
                    {
                        upgradeWindows[0].drawWindow();
                        if (upgradeWindows[0].skillImproved())
                        {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().improveQSkill();
                        }
                        else if (upgradeWindows[0].skillChanged())
                        {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().ChangeQSkill();
                        }



                    }

                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getOfferForWSkill())
                    {
                        upgradeWindows[1].drawWindow();
                        if (upgradeWindows[1].skillImproved())
                        {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().improveWSkill();
                        }
                        else if (upgradeWindows[1].skillChanged())
                        {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().ChangeWSkill();
                        }

                    }

                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getOfferForESkill())
                    {
                        upgradeWindows[2].drawWindow();
                        if (upgradeWindows[2].skillImproved())
                        {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().improveESkill();
                        }
                        else if (upgradeWindows[2].skillChanged())
                        {
                            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().ChangeESkill();
                        }
                    }

                    Graphics.DrawTexture(teammates_Window_Rect, teammates_Window);

                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), teammates_Frame);
                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), teammates_Frame);
                    supportLife_Rect.x = teammates_Window_Rect.x + 31;

                    if (GameObject.Find("Healer") != null)
                    {
                        if (supportLifePercent <= 1.0f && supportLifePercent >= 0.5f && GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), support100);
                        }
                        else if (supportLifePercent < 0.5f && supportLifePercent > 0f && GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), support50);
                        }
                        else if (!GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 40, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), support0);
                        }
                    }
                    Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 31, teammates_Window_Rect.y + 141, 124, 18), teammates_Life_Back, 9, 9, 0, 0);
                    if (supportCurrentLife > 0)
                    {
                        Graphics.DrawTexture(supportLife_Rect, teammates_Life, 9, 9, 0, 0);
                    }

                    if (GameObject.Find("Ranger") != null)
                    {
                        if (rangerLifePercent <= 1.0f && rangerLifePercent >= 0.5f&& GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), ranger100);
                        }
                        else if (rangerLifePercent < 0.5f && rangerLifePercent > 0f&& GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), ranger50);
                        }
                        else if (!GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive)
                        {
                            Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 215, teammates_Window_Rect.y + 34, teammates_Frame.width, teammates_Frame.height), ranger0);
                        }
                    }
                        Graphics.DrawTexture(new Rect(teammates_Window_Rect.x + 207, teammates_Window_Rect.y + 141, 124, 18), teammates_Life_Back, 9, 9, 0, 0);
                    
                    if (rangerCurrentLife > 0)
                    {
                        Graphics.DrawTexture(rangerLife_Rect, teammates_Life, 9, 9, 0, 0);
                    }
                }

                if (GameObject.Find("GameManager").GetComponent<GameStarter>().hunterCountdown)
                {
                    style.fontSize = 60;
                    style.normal.textColor = Color.black;
                    GUI.Label(new Rect((1920 / 2 - style.CalcSize(new GUIContent("Game starts in: ")).x / 2) + 4 - 45, 150 + 4, style.CalcSize(new GUIContent("Game starts in: ")).x, style.CalcSize(new GUIContent("Game starts in: ")).y), "Game starts in: ", style);
                    GUI.Label(new Rect(((1920 / 2 + style.CalcSize(new GUIContent("Game starts in: ")).x / 2) - 23) + 4, 150 + 4, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().hunterCountdown_Number)).x, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().hunterCountdown_Number)).y), "" + GameObject.Find("GameManager").GetComponent<GameStarter>().hunterCountdown_Number, style);
                    style.normal.textColor = Color.white;
                    GUI.Label(new Rect((1920 / 2 - style.CalcSize(new GUIContent("Game starts in: ")).x / 2) - 45, 150, style.CalcSize(new GUIContent("Game starts in: ")).x, style.CalcSize(new GUIContent("Game starts in: ")).y), "Game starts in: ", style);
                    GUI.Label(new Rect((1920 / 2 + style.CalcSize(new GUIContent("Game starts in: ")).x / 2) - 23, 150, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().hunterCountdown_Number)).x, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().hunterCountdown_Number)).y), "" + GameObject.Find("GameManager").GetComponent<GameStarter>().hunterCountdown_Number, style);
                }

                if(GameObject.Find("GameManager").GetComponent<Winconditions>().huntersWin){
                    Graphics.DrawTexture(new Rect(1920/2-win_Screen.width/2,1080/2-win_Screen.height/2,win_Screen.width,win_Screen.height),win_Screen);
                    if (GUI.Button(new Rect(((1920 / 2 - win_Screen.width / 2) + win_Screen.width / 2) - button_Continue.width / 2, (1080 / 2 - win_Screen.height / 2)+185, button_Continue.width, button_Continue.height), button_Continue, style))
                    {
                        GameObject.Find("Menu").GetComponent<Menu>().reset();
                    }
                }
                else if (GameObject.Find("GameManager").GetComponent<Winconditions>().monsterWin)
                {
                    Graphics.DrawTexture(new Rect(1920 / 2 - win_Screen.width / 2, 1080 / 2 - win_Screen.height / 2, win_Screen.width, win_Screen.height), lost_Screen);
                    if (GUI.Button(new Rect(((1920 / 2 - win_Screen.width / 2) + win_Screen.width / 2) - button_Continue.width / 2, (1080 / 2 - win_Screen.height / 2) + 185, button_Continue.width, button_Continue.height), button_Continue, style))
                    {
                        GameObject.Find("Menu").GetComponent<Menu>().reset();
                    }
                }

            }

            if (whatAmI == 0)
            {

                soulsplinters_Rect=new Rect(MonsterLifeFrame_Rect.x + 106, MonsterLifeFrame_Rect.y + 58, 532 * soulplinters_Percent, 24);

                Graphics.DrawTexture(MonsterLifeFrame_Rect, soulsplinters_Frame);
                Graphics.DrawTexture(new Rect(MonsterLifeFrame_Rect.x + 106, MonsterLifeFrame_Rect.y + 58, 532, 24), daemonLife_Back, 11, 11, 0, 0);

                if (current_Soulsplinters > 0)
                {
                    Graphics.DrawTexture(soulsplinters_Rect, soulsplinters_Count, 11, 11, 0, 0);
                }

                style.fontSize = 25;
                style.normal.textColor = Color.black;
                GUI.Label(new Rect(((MonsterLifeFrame_Rect.x + 106 + 532 / 2) - style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).x / 2) + 2, ((MonsterLifeFrame_Rect.y + 57 + 24 / 2) - style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).y / 2) + 2, style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).x, style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).y), "" + current_Soulsplinters + "/" + max_Soulsplinters, style);
                style.normal.textColor = Color.white;
                GUI.Label(new Rect((MonsterLifeFrame_Rect.x + 106 + 532 / 2) - style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).x / 2, (MonsterLifeFrame_Rect.y + 57 + 24 / 2) - style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).y / 2, style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).x, style.CalcSize(new GUIContent("" + current_Soulsplinters + "/" + max_Soulsplinters)).y), "" + current_Soulsplinters + "/" + max_Soulsplinters, style);


                if (monsterLoadout)
                {
                    Rect monsterLoadout_back_Rect = new Rect(1920 / 2 - (monsterLoadout_back.width / 2), 1080 / 2 - (monsterLoadout_back.height / 2), monsterLoadout_back.width, monsterLoadout_back.height);
                    Rect monsterLoadout_Spalte1_Rect = new Rect(monsterLoadout_back_Rect.x + 149, monsterLoadout_back_Rect.y + 100, monsterLoadout_Spalte_Q.width, monsterLoadout_Spalte_Q.height);
                    Rect monsterLoadout_Spalte2_Rect = new Rect(monsterLoadout_back_Rect.x + 339, monsterLoadout_back_Rect.y + 100, monsterLoadout_Spalte_Q.width, monsterLoadout_Spalte_Q.height);
                    Rect monsterLoadout_Spalte3_Rect = new Rect(monsterLoadout_back_Rect.x + 529, monsterLoadout_back_Rect.y + 100, monsterLoadout_Spalte_Q.width, monsterLoadout_Spalte_Q.height);
                    Rect monsterLoadout_Spalte4_Rect = new Rect(monsterLoadout_back_Rect.x + 719, monsterLoadout_back_Rect.y + 100, monsterLoadout_Spalte_Q.width, monsterLoadout_Spalte_Q.height);
                    Rect monsterLoadout_Button_Rect = new Rect((monsterLoadout_back_Rect.x + monsterLoadout_back_Rect.width / 2) - monsterLoadout_button.width / 2, monsterLoadout_back_Rect.y + 534, monsterLoadout_button.width, monsterLoadout_button.height);
                    Rect monsterLoadout_Spalte1_SkillFrame_Rect = new Rect((monsterLoadout_Spalte1_Rect.x + monsterLoadout_Spalte1_Rect.width / 2) - (skillBar_SkillFrame_Monster.width / 2), monsterLoadout_Spalte1_Rect.y + 100, skillBar_SkillFrame_Monster.width, skillBar_SkillFrame_Monster.height);
                    Rect monsterLoadout_Spalte2_SkillFrame_Rect = new Rect((monsterLoadout_Spalte2_Rect.x + monsterLoadout_Spalte2_Rect.width / 2) - (skillBar_SkillFrame_Monster.width / 2), monsterLoadout_Spalte2_Rect.y + 100, skillBar_SkillFrame_Monster.width, skillBar_SkillFrame_Monster.height);
                    Rect monsterLoadout_Spalte3_SkillFrame_Rect = new Rect((monsterLoadout_Spalte3_Rect.x + monsterLoadout_Spalte3_Rect.width / 2) - (skillBar_SkillFrame_Monster.width / 2), monsterLoadout_Spalte3_Rect.y + 100, skillBar_SkillFrame_Monster.width, skillBar_SkillFrame_Monster.height);
                    Rect monsterLoadout_Spalte4_SkillFrame_Rect = new Rect((monsterLoadout_Spalte4_Rect.x + monsterLoadout_Spalte4_Rect.width / 2) - (skillBar_SkillFrame_Monster.width / 2), monsterLoadout_Spalte4_Rect.y + 145, skillBar_SkillFrame_Monster.width, skillBar_SkillFrame_Monster.height);



                    GUI.DrawTexture(monsterLoadout_back_Rect, monsterLoadout_back);
                    GUI.DrawTexture(monsterLoadout_Spalte1_Rect, monsterLoadout_Spalte_Q);
                    GUI.DrawTexture(monsterLoadout_Spalte2_Rect, monsterLoadout_Spalte_W);
                    GUI.DrawTexture(monsterLoadout_Spalte3_Rect, monsterLoadout_Spalte_E);
                    GUI.DrawTexture(monsterLoadout_Spalte4_Rect, monsterLoadout_Spalte_R);



                    //Spalte1
                    GUI.DrawTexture(monsterLoadout_Spalte1_SkillFrame_Rect,skillBar_SkillFrame_Monster);

                    if (monsterLoadout_Spalte1_SkillFrame_Rect.Contains(Event.current.mousePosition)||GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill==1)
                    {
                        if (monsterLoadout_Spalte1_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[0].DrawBoxWithCoords(true,monsterLoadout_Spalte1_SkillFrame_Rect.x+monsterLoadout_Spalte1_SkillFrame_Rect.width,monsterLoadout_Spalte1_SkillFrame_Rect.y+10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte1_SkillFrame_Rect.y + monsterLoadout_Spalte1_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Fireblow);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill = 1;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte1_SkillFrame_Rect.y + monsterLoadout_Spalte1_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Fireblow_bw);
                        monsterDescriptionBoxes[0].DrawBoxWithCoords(false,0,0);
                    }

                    monsterLoadout_Spalte1_SkillFrame_Rect.y += 112;
                    GUI.DrawTexture(monsterLoadout_Spalte1_SkillFrame_Rect, skillBar_SkillFrame_Monster);

                    if (monsterLoadout_Spalte1_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 11)
                    {
                        if (monsterLoadout_Spalte1_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[1].DrawBoxWithCoords(true, monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width, monsterLoadout_Spalte1_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte1_SkillFrame_Rect.y + monsterLoadout_Spalte1_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Cleave);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill = 11;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte1_SkillFrame_Rect.y + monsterLoadout_Spalte1_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Cleave_bw);
                        monsterDescriptionBoxes[1].DrawBoxWithCoords(false, 0, 0);
                    }
                    monsterLoadout_Spalte1_SkillFrame_Rect.y += 112;
                    GUI.DrawTexture(monsterLoadout_Spalte1_SkillFrame_Rect, skillBar_SkillFrame_Monster);

                    if (monsterLoadout_Spalte1_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 12)
                    {
                        if (monsterLoadout_Spalte1_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[2].DrawBoxWithCoords(true, monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width, monsterLoadout_Spalte1_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte1_SkillFrame_Rect.y + monsterLoadout_Spalte1_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Swordstrike);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill = 12;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte1_SkillFrame_Rect.x + monsterLoadout_Spalte1_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte1_SkillFrame_Rect.y + monsterLoadout_Spalte1_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Swordstrike_bw);
                        monsterDescriptionBoxes[2].DrawBoxWithCoords(false, 0, 0);
                    }


                    //Spalte2
                    GUI.DrawTexture(monsterLoadout_Spalte2_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte2_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 2)
                    {

                        if (monsterLoadout_Spalte2_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[3].DrawBoxWithCoords(true, monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width, monsterLoadout_Spalte2_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte2_SkillFrame_Rect.y + monsterLoadout_Spalte2_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Hands);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill = 2;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte2_SkillFrame_Rect.y + monsterLoadout_Spalte2_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Hands_bw);
                        monsterDescriptionBoxes[3].DrawBoxWithCoords(false, 0, 0);
                    }
                    monsterLoadout_Spalte2_SkillFrame_Rect.y += 112;
                    GUI.DrawTexture(monsterLoadout_Spalte2_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte2_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 21)
                    {
                        if (monsterLoadout_Spalte2_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[4].DrawBoxWithCoords(true, monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width, monsterLoadout_Spalte2_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte2_SkillFrame_Rect.y + monsterLoadout_Spalte2_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Hook);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill = 21;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte2_SkillFrame_Rect.y + monsterLoadout_Spalte2_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Hook_bw);
                        monsterDescriptionBoxes[4].DrawBoxWithCoords(false, 0, 0);
                    }
                    monsterLoadout_Spalte2_SkillFrame_Rect.y += 112;
                    GUI.DrawTexture(monsterLoadout_Spalte2_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte2_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 22)
                    {
                        if (monsterLoadout_Spalte2_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[5].DrawBoxWithCoords(true, monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width, monsterLoadout_Spalte2_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte2_SkillFrame_Rect.y + monsterLoadout_Spalte2_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Silence);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill = 22;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte2_SkillFrame_Rect.x + monsterLoadout_Spalte2_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte2_SkillFrame_Rect.y + monsterLoadout_Spalte2_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Silence_bw);
                        monsterDescriptionBoxes[5].DrawBoxWithCoords(false, 0, 0);
                    }
                    //Spalte3
                    GUI.DrawTexture(monsterLoadout_Spalte3_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte3_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 3)
                    {

                        if (monsterLoadout_Spalte3_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[6].DrawBoxWithCoords(true, monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width, monsterLoadout_Spalte3_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte3_SkillFrame_Rect.y + monsterLoadout_Spalte3_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Flamecharge);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill = 3;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte3_SkillFrame_Rect.y + monsterLoadout_Spalte3_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Flamecharge_bw);
                        monsterDescriptionBoxes[6].DrawBoxWithCoords(false, 0, 0);
                    }
                    monsterLoadout_Spalte3_SkillFrame_Rect.y += 112;
                    GUI.DrawTexture(monsterLoadout_Spalte3_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte3_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 31)
                    {
                        if (monsterLoadout_Spalte3_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[7].DrawBoxWithCoords(true, monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width, monsterLoadout_Spalte3_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte3_SkillFrame_Rect.y + monsterLoadout_Spalte3_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Astral);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill = 31;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte3_SkillFrame_Rect.y + monsterLoadout_Spalte3_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Astral_bw);
                        monsterDescriptionBoxes[7].DrawBoxWithCoords(false, 0, 0);
                    }
                    monsterLoadout_Spalte3_SkillFrame_Rect.y += 112;
                    GUI.DrawTexture(monsterLoadout_Spalte3_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte3_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 32)
                    {
                        if (monsterLoadout_Spalte3_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[8].DrawBoxWithCoords(true, monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width, monsterLoadout_Spalte3_SkillFrame_Rect.y + 10);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte3_SkillFrame_Rect.y + monsterLoadout_Spalte3_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Jump);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill = 32;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte3_SkillFrame_Rect.x + monsterLoadout_Spalte3_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte3_SkillFrame_Rect.y + monsterLoadout_Spalte3_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Jump_bw);
                        monsterDescriptionBoxes[8].DrawBoxWithCoords(false, 0, 0);
                    }
                    //Spalte4
                    GUI.DrawTexture(monsterLoadout_Spalte4_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte4_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill == 4)
                    {
                        if (monsterLoadout_Spalte4_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[9].DrawBoxWithCoords(true, monsterLoadout_Spalte4_SkillFrame_Rect.x, monsterLoadout_Spalte4_SkillFrame_Rect.y + 85);
                        }

                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte4_SkillFrame_Rect.x + monsterLoadout_Spalte4_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte4_SkillFrame_Rect.y + monsterLoadout_Spalte4_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Soulvortex);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill = 4;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte4_SkillFrame_Rect.x + monsterLoadout_Spalte4_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte4_SkillFrame_Rect.y + monsterLoadout_Spalte4_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Soulvortex_bw);
                        monsterDescriptionBoxes[9].DrawBoxWithCoords(false, 0, 0);
                    }
                    monsterLoadout_Spalte4_SkillFrame_Rect.y += 112;
                    GUI.DrawTexture(monsterLoadout_Spalte4_SkillFrame_Rect, skillBar_SkillFrame_Monster);
                    if (monsterLoadout_Spalte4_SkillFrame_Rect.Contains(Event.current.mousePosition) || GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill == 41)
                    {
                        if (monsterLoadout_Spalte4_SkillFrame_Rect.Contains(Event.current.mousePosition))
                        {
                            monsterDescriptionBoxes[10].DrawBoxWithCoords(true, monsterLoadout_Spalte4_SkillFrame_Rect.x, monsterLoadout_Spalte4_SkillFrame_Rect.y -190);
                        }
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte4_SkillFrame_Rect.x + monsterLoadout_Spalte4_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte4_SkillFrame_Rect.y + monsterLoadout_Spalte4_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Soulvolcano);
                        if (Input.GetMouseButtonUp(0))
                        {
                            GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill = 41;
                        }
                    }
                    else
                    {
                        GUI.DrawTexture(new Rect((monsterLoadout_Spalte4_SkillFrame_Rect.x + monsterLoadout_Spalte4_SkillFrame_Rect.width / 2) - skill_Passiv_Ranger.width / 2, (monsterLoadout_Spalte4_SkillFrame_Rect.y + monsterLoadout_Spalte4_SkillFrame_Rect.height / 2) - skill_Passiv_Ranger.height / 2, skill_Passiv_Ranger.width, skill_Passiv_Ranger.height), skill_Soulvolcano_bw);
                        monsterDescriptionBoxes[10].DrawBoxWithCoords(false, 0, 0);
                    }

                    GUI.DrawTexture(monsterLoadout_Button_Rect, monsterLoadout_button);

                    if (monsterLoadout_Button_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonUp(0) && GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill != 0 && GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill != 0 && GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill != 0 && GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill != 0)
                    {
                        monsterLoadout = false;
                    }
                }

                //Skill1
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 1)
                {
                    GUI.DrawTexture(skill1_Rect, skill_Fireblow);

                    //Cooldown1
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill1.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill1.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill1.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 67 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }

                //Skill11
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 11)
                {
                    GUI.DrawTexture(skill1_Rect, skill_Cleave);

                    //Cooldown11
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill11.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill11.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill11.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 67 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }

                //Skill12
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().qSkill == 12)
                {
                    GUI.DrawTexture(skill1_Rect, skill_Swordstrike);

                    //Cooldown12
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill12.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill12.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill12.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 67 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }


                //Skill2
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 2)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Hands);

                    //Cooldown2
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill2.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill2.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill2.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 210 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }

                //Skill21
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 21)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Hook);

                    //Cooldown21
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill21.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill21.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill21.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 210 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }

                //Skill22
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().wSkill == 22)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Silence);

                    //Cooldown22
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill22.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill22.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill22.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 210 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }

                //Skill3
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 3)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Flamecharge);

                    //Cooldown3
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill3.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill3.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill3.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 353 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }

                //Skill31
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 31)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Astral);

                    //Cooldown3
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill31.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill31.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill31.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 353 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }

                //Skill32
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().eSkill == 32)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Jump);

                    //Cooldown3
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill32.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill32.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill32.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 353 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }

                //Skill4
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill == 4)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Soulvortex);

                    //Cooldown4
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill4.IsOnCooldown())
                    {
                        float skill4CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill4.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill4.GetCooldownLeft() / 100;
                        Rect skillCooldownRect4 = new Rect(skillBar_Back_Rect.x + 496 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill4CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect4, skill_Cooldown);
                    }
                }
                //Skill41
                if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().rSkill == 41)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Soulvolcano);

                    //Cooldown4
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill41.IsOnCooldown())
                    {
                        float skill4CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill41.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill41.GetCooldownLeft() / 100;
                        Rect skillCooldownRect4 = new Rect(skillBar_Back_Rect.x + 496 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 138 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill4CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect4, skill_Cooldown);
                    }
                }

                //Skill5
                    GUI.DrawTexture(skill5_Rect, skill_Lantern);

                    //Cooldown5
                    if (GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill5.IsOnCooldown())
                    {
                        float skill5CooldownPerc = 100 / GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill5.getCooldown() * GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().cdTSkill5.GetCooldownLeft() / 100;
                        Rect skillCooldownRect5 = new Rect(skillBar_Back_Rect.x + 625 + (skillBar_SkillFrame_Lantern.width / 2) - (skill_Cooldown_Lantern.width / 2), skillBar_Back_Rect.y + 160 + (skillBar_SkillFrame_Lantern.height / 2) + (skill_Cooldown_Lantern.height / 2), skill_Cooldown_Lantern.width, -skill_Cooldown_Lantern.height * skill5CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect5, skill_Cooldown);
                    }

                //style.fontSize = 25;
                //style.normal.textColor = Color.red;
                //if (altar1_attacked)
                //{
                //    GUI.Label(new Rect(500, 500, 300, 300), "Altar oben links wird attackiert!",style);
                //    if (!timerStarted)
                //    {
                //        currentTime = Time.time;
                //        timerStarted=true;
                        
                //    }
                //    if (timerStarted && Time.time>=(currentTime + 3))
                //    {
                //        timerStarted = false;
                //        altar1_attacked = false;
                //    }

                //}

                //if (altar2_attacked)
                //{
                //    GUI.Label(new Rect(500, 500, 300, 300), "Altar oben rechts wird attackiert!", style);
                //    if (!timerStarted)
                //    {
                //        currentTime = Time.time;
                //        timerStarted = true;
                //    }
                //    if (timerStarted && Time.time >= (currentTime + 3))
                //    {
                //        timerStarted = false;
                //        altar2_attacked = false;
                //    }
                //}

                //if (altar3_attacked)
                //{
                //    GUI.Label(new Rect(500, 500, 300, 300), "Altar unten links wird attackiert!", style);
                //    if (!timerStarted)
                //    {
                //        currentTime = Time.time;
                //        timerStarted = true;
                //    }
                //    if (timerStarted && Time.time >= (currentTime + 3))
                //    {
                //        timerStarted = false;
                //        altar3_attacked = false;
                //    }
                //}

                //if (altar4_attacked)
                //{
                //    GUI.Label(new Rect(500, 500, 300, 300), "Altar unten rechts wird attackiert!", style);
                //    if (!timerStarted)
                //    {
                //        currentTime = Time.time;
                //        timerStarted = true;
                //    }
                //    if (timerStarted && Time.time >= (currentTime + 3))
                //    {
                //        timerStarted = false;
                //        altar4_attacked = false;
                //    }
                //}

                if (GameObject.Find("GameManager").GetComponent<GameStarter>().monsterCountdown)
                {
                    style.fontSize=60;
                    style.normal.textColor=Color.black;
                    GUI.Label(new Rect((1920 / 2 - style.CalcSize(new GUIContent("Game starts in: ")).x / 2)+4-45, 150+4, style.CalcSize(new GUIContent("Game starts in: ")).x, style.CalcSize(new GUIContent("Game starts in: ")).y), "Game starts in: ", style);
                    GUI.Label(new Rect(((1920 / 2 + style.CalcSize(new GUIContent("Game starts in: ")).x / 2) -23) + 4, 150 + 4, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().monsterCountdown_Number)).x, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().monsterCountdown_Number)).y), "" + GameObject.Find("GameManager").GetComponent<GameStarter>().monsterCountdown_Number, style);
                    style.normal.textColor = Color.white;
                    GUI.Label(new Rect((1920 / 2 - style.CalcSize(new GUIContent("Game starts in: ")).x / 2)-45, 150, style.CalcSize(new GUIContent("Game starts in: ")).x, style.CalcSize(new GUIContent("Game starts in: ")).y), "Game starts in: ", style);
                    GUI.Label(new Rect((1920 / 2 + style.CalcSize(new GUIContent("Game starts in: ")).x / 2) -23, 150, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().monsterCountdown_Number)).x, style.CalcSize(new GUIContent("" + GameObject.Find("GameManager").GetComponent<GameStarter>().monsterCountdown_Number)).y), "" + GameObject.Find("GameManager").GetComponent<GameStarter>().monsterCountdown_Number, style);
                }

                for (int i = 0; i < monsterDescriptionBoxes.Length; ++i)
                {
                    monsterDescriptionBoxes[i].Draw();
                }

                if (GameObject.Find("GameManager").GetComponent<Winconditions>().huntersWin)
                {
                    Graphics.DrawTexture(new Rect(1920 / 2 - win_Screen.width / 2, 1080 / 2 - win_Screen.height / 2, win_Screen.width, win_Screen.height), lost_Screen);
                    if (GUI.Button(new Rect(((1920 / 2 - win_Screen.width / 2) + win_Screen.width / 2) - button_Continue.width / 2, (1080 / 2 - win_Screen.height / 2) + 185, button_Continue.width, button_Continue.height), button_Continue, style))
                    {
                        GameObject.Find("Menu").GetComponent<Menu>().reset();
                    }
                }
                else if (GameObject.Find("GameManager").GetComponent<Winconditions>().monsterWin)
                {
                    Graphics.DrawTexture(new Rect(1920 / 2 - win_Screen.width / 2, 1080 / 2 - win_Screen.height / 2, win_Screen.width, win_Screen.height), win_Screen);
                    if (GUI.Button(new Rect(((1920 / 2 - win_Screen.width / 2) + win_Screen.width / 2) - button_Continue.width / 2, (1080 / 2 - win_Screen.height / 2) + 185, button_Continue.width, button_Continue.height), button_Continue, style))
                    {
                        GameObject.Find("Menu").GetComponent<Menu>().reset();
                    }
                }

            }




            //ranger
            if (whatAmI == 1)
            {

                //Passive Frame
                if (!GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 550, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_P);
                }
                else
                {
                    GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 550, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_P_Active);
                }



                //Skil1
                if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getQSkill() == 0)
                {
                    GUI.DrawTexture(skill1_Rect, skill_Schnellschuss);

                    //Cooldown1
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill1.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill1.getCooldown() * GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill1.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 100 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }//Skill11
                else if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getQSkill() == 1)
                {
                    GUI.DrawTexture(skill1_Rect, skill_ChargeShot);
                    //Cooldown11
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill11.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill11.getCooldown() * GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill11.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 100 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }
                //Skill2
                if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getWSkill() == 0)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Anker);
                    //Cooldown2
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill2.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill2.getCooldown() * GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill2.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 242 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }//Skill21
                else if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getWSkill() == 1)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Trap);
                    //Cooldown21
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill21.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill21.getCooldown() * GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill21.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 242 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }

                //Skill3
                if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getESkill() == 0)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Backflip);
                    //Cooldown3
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill3.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill3.getCooldown() * GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill3.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 384 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }//Skill31
                else if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().getESkill() == 1)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Dash);
                    //Cooldown3
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill31.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill31.getCooldown() * GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().cdTSkill31.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 384 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }



                //Passive
                style.fontStyle = FontStyle.Bold;
                style.fontSize = 38;


                if (!GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Passiv_Ranger);
                } else if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Passiv_Ranger_active);

                    //Stacks
                    if (GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().passivStack > 0)
                    {
                        style.normal.textColor = Color.black;
                        string stack_Shadow = "" + GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().passivStack;

                        GUI.Label(new Rect(((skill4_Rect.x + skill4_Rect.width / 2) - (style.CalcSize(new GUIContent(stack_Shadow)).x / 2)) + 2, ((skill4_Rect.y + skill4_Rect.height / 2) - (style.CalcSize(new GUIContent(stack_Shadow)).y / 2)) + 2, style.CalcSize(new GUIContent(stack_Shadow)).x, style.CalcSize(new GUIContent(stack_Shadow)).y), stack_Shadow, style);

                        style.normal.textColor = Color.white;
                        string stack = "" + GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().passivStack;

                        GUI.Label(new Rect((skill4_Rect.x + skill4_Rect.width / 2) - (style.CalcSize(new GUIContent(stack)).x / 2), (skill4_Rect.y + skill4_Rect.height / 2) - (style.CalcSize(new GUIContent(stack)).y / 2), style.CalcSize(new GUIContent(stack)).x, style.CalcSize(new GUIContent(stack)).y), stack, style);
                    }
                   
                }



            }
            //Support
            else if (whatAmI == 2)
            {

                //Passive Frame
                if (!GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 550, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_P);
                }
                else
                {
                    GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 550, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_P_Active);
                }


                //Skill1
                if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getQSkill() == 0)
                {
                    GUI.DrawTexture(skill1_Rect, skill_HeiligesLicht);
                    //Cooldown1
                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill1.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill1.getCooldown() * GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill1.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 100 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }//Skill11
                else if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getQSkill() == 1)
                {
                    GUI.DrawTexture(skill1_Rect, skill_Absorb);
                    //Cooldown11
                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill11.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill11.getCooldown() * GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill11.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 100 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }

                if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getWSkill() == 0)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Schild);
                    //Cooldown2
                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill2.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill2.getCooldown() * GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill2.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 242 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }
                else if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getWSkill() == 1)
                {
                    GUI.DrawTexture(skill2_Rect, skill_SoulLink);
                    //Cooldown2
                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill21.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill21.getCooldown() * GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill21.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 242 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }

                }

                if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getESkill() == 0)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Blink);
                    //Cooldown3
                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill3.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill3.getCooldown() * GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill3.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 384 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }
                else if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().getESkill() == 1)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Port);
                    //Cooldown3
                    if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill31.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill31.getCooldown() * GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().cdTSkill31.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 384 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }


                if (!GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Passiv_Support);
                }
                else if (GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Passiv_Support_active);
                }
            }
            //Tank
            else if (whatAmI == 3)
            {

                //Passive Frame
                if (!GameObject.Find("Tank").GetComponent<TankSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 550, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_P);
                }
                else
                {
                    GUI.DrawTexture(new Rect(skillBar_Back_Rect.x + 550, skillBar_Back_Rect.y + 138, skillBar_SkillFrame_Q.width, skillBar_SkillFrame_Q.height), skillBar_SkillFrame_P_Active);
                }


                if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getQSkill() == 0)
                {
                    GUI.DrawTexture(skill1_Rect, skill_KnockBack);
                    //Cooldown1
                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill1.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill1.getCooldown() * GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill1.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 100 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }
                else if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getQSkill() == 1)
                {
                    GUI.DrawTexture(skill1_Rect, skill_Bash);
                    //Cooldown1
                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill11.IsOnCooldown())
                    {
                        float skill1CooldownPerc = 100 / GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill11.getCooldown() * GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill11.GetCooldownLeft() / 100;
                        Rect skillCooldownRect1 = new Rect(skillBar_Back_Rect.x + 100 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill1CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect1, skill_Cooldown);
                    }
                }

                if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getWSkill() == 0)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Barriere);
                    //Cooldown2
                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill2.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill2.getCooldown() * GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill2.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 242 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }
                else if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getWSkill() == 1)
                {
                    GUI.DrawTexture(skill2_Rect, skill_Taunt);
                    //Cooldown2
                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill21.IsOnCooldown())
                    {
                        float skill2CooldownPerc = 100 / GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill21.getCooldown() * GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill21.GetCooldownLeft() / 100;
                        Rect skillCooldownRect2 = new Rect(skillBar_Back_Rect.x + 242 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill2CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect2, skill_Cooldown);
                    }
                }

                if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getESkill() == 0)
                {
                    GUI.DrawTexture(skill3_Rect, skill_Charge);
                    //Cooldown3
                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill3.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill3.getCooldown() * GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill3.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 384 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }
                else if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().getESkill() == 1)
                {
                    GUI.DrawTexture(skill3_Rect, skill_WarCry);
                    //Cooldown3
                    if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill31.IsOnCooldown())
                    {
                        float skill3CooldownPerc = 100 / GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill31.getCooldown() * GameObject.Find("Tank").GetComponent<TankSkillControlScript>().cdTSkill31.GetCooldownLeft() / 100;
                        Rect skillCooldownRect3 = new Rect(skillBar_Back_Rect.x + 384 + (skillBar_SkillFrame_Q.width / 2) - (skill_Cooldown.width / 2), skillBar_Back_Rect.y + 139 + (skillBar_SkillFrame_Q.height / 2) + (skill_Cooldown.height / 2), skill_Cooldown.width, -skill_Cooldown.height * skill3CooldownPerc);
                        Graphics.DrawTexture(skillCooldownRect3, skill_Cooldown);
                    }
                }

                if (!GameObject.Find("Tank").GetComponent<TankSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Passiv_Tank);
                }
                else if (GameObject.Find("Tank").GetComponent<TankSkillControlScript>().passivActive)
                {
                    GUI.DrawTexture(skill4_Rect, skill_Passiv_Tank_active);
                }
            }


            if (showMenu)
            {
                Rect menu_Rect = new Rect(1920 / 2 - menu_Back.width / 2, 1080 / 2 - menu_Back.height/2, menu_Back.width, menu_Back.height);

                Graphics.DrawTexture(menu_Rect, menu_Back);

                if (GUI.Button(new Rect((menu_Rect.x + menu_Rect.width / 2) - menu_Button_Continue.width / 2, menu_Rect.y + 129, menu_Button_Continue.width, menu_Button_Continue.height), menu_Button_Continue,style))
                {
                    showMenu = false;
                }
                else if (GUI.Button(new Rect((menu_Rect.x + menu_Rect.width / 2) - menu_Button_Continue.width / 2, menu_Rect.y + 215, menu_Button_Continue.width, menu_Button_Continue.height), menu_Button_Options, style))
                {
                    showMenu = false;
                    showOptions = true;
                }
                else if (GUI.Button(new Rect((menu_Rect.x + menu_Rect.width / 2) - menu_Button_Continue.width / 2, menu_Rect.y + 301, menu_Button_Continue.width, menu_Button_Continue.height), menu_Button_Exit, style))
                {
                    GameObject.Find("Menu").GetComponent<Menu>().reset();
                }
            }

            if (showOptions)
            {
                volume_knob_x = ((GameObject.Find("Menu").GetComponent<Menu>().getVolume() * options_Volume_Rect.width) + options_Volume_Rect.x);
                Graphics.DrawTexture(options_Rect, menu_Options_Back);
                Graphics.DrawTexture(volume_Knob_Rect, menu_Options_Knob);
                Graphics.DrawTexture(scrollspeed_Knob_Rect, menu_Options_Knob);

                if (!dragVolume && options_Volume_Rect.Contains(Event.current.mousePosition)&&Input.GetMouseButtonUp(0))
                {
                    if (Event.current.mousePosition.x >= options_Volume_Rect.x && Event.current.mousePosition.x <= options_Volume_Rect.x + options_Volume_Rect.width)
                    {
                        volume_knob_x = Event.current.mousePosition.x - menu_Options_Knob.width / 2;
                        
                    }
                }

                if (volume_Knob_Rect.Contains(Event.current.mousePosition)&&Input.GetMouseButtonDown(0))
                {
                    dragVolume = true;
                }

                if (dragVolume)
                {
                    if (Event.current.mousePosition.x - menu_Options_Knob.width / 2 >= options_Volume_Rect.x && Event.current.mousePosition.x - menu_Options_Knob.width / 2 <= options_Volume_Rect.x + options_Volume_Rect.width)
                    {
                        volume_knob_x = Event.current.mousePosition.x-menu_Options_Knob.width/2;
                    }
                    else if (Event.current.mousePosition.x - menu_Options_Knob.width / 2 > options_Volume_Rect.x + options_Volume_Rect.width)
                    {
                        volume_knob_x = 1088;
                    }
                    else if (Event.current.mousePosition.x - menu_Options_Knob.width / 2 < options_Volume_Rect.x)
                    {
                        volume_knob_x = 854;
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        dragVolume = false;
                    }
                }
                GameObject.Find("Menu").GetComponent<Menu>().ChangeVolume((int)((100f / options_Volume_Rect.width) * (volume_knob_x-854)));

                scrollspeed_knob_x = (GameObject.Find("Main Camera").GetComponent<Scrolling>().getScaleFactor() * options_Scrollspeed_Rect.width) + options_Scrollspeed_Rect.x;
                Graphics.DrawTexture(scrollspeed_Knob_Rect, menu_Options_Knob);


                if (!dragScrollspeed && options_Scrollspeed_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonUp(0))
                {
                    if (Event.current.mousePosition.x >= options_Scrollspeed_Rect.x && Event.current.mousePosition.x <= options_Scrollspeed_Rect.x + options_Scrollspeed_Rect.width)
                    {
                        scrollspeed_knob_x = Event.current.mousePosition.x - menu_Options_Knob.width / 2;

                    }
                }

                if (scrollspeed_Knob_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonDown(0))
                {
                    dragScrollspeed = true;
                }

                if (dragScrollspeed)
                {
                    if (Event.current.mousePosition.x - menu_Options_Knob.width / 2 >= options_Scrollspeed_Rect.x && Event.current.mousePosition.x - menu_Options_Knob.width / 2 <= options_Scrollspeed_Rect.x + options_Scrollspeed_Rect.width)
                    {
                        scrollspeed_knob_x = Event.current.mousePosition.x - menu_Options_Knob.width / 2;
                    }
                    else if (Event.current.mousePosition.x - menu_Options_Knob.width / 2 > options_Scrollspeed_Rect.x + options_Scrollspeed_Rect.width)
                    {
                        scrollspeed_knob_x = 1088;
                    }
                    else if (Event.current.mousePosition.x - menu_Options_Knob.width / 2 < options_Scrollspeed_Rect.x)
                    {
                        scrollspeed_knob_x = 854;
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        dragScrollspeed = false;
                    }
                }

                GameObject.Find("Main Camera").GetComponent<Scrolling>().changeScrollspeed(((100f / options_Scrollspeed_Rect.width) * (scrollspeed_knob_x - 854)) / 100);

                if (GUI.Button(new Rect((options_Rect.x + options_Rect.width / 2) - menu_Options_Apply.width / 2, options_Rect.y + 180, menu_Options_Apply.width, menu_Options_Apply.height), menu_Options_Apply, style))
                {
                    reset();
                    showOptions = false;
                    showMenu = true;
                }


            }
            GL.PopMatrix();
        }
    }

}
