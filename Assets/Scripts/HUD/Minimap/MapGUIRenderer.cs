using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Map))]
public class MapGUIRenderer : MonoBehaviour
{
    Map map;
    GUIStyle iconStyle;

    [SerializeField]
    Texture zoomInIcon;

    [SerializeField]
    Texture zoomOutIcon;

    [SerializeField]
    int iconSize = 24;

    [SerializeField]
    bool displayCoordinates;

    [SerializeField]
    string label = "";

    public Texture minimap_Frame;

    public GameObject player;

    public int whatAmI;

    public Texture viewRect_Texture;

    void Start()
    {
        map = GetComponent<Map>();
    }

    void DrawZoomButtons()
    {
        float left = map.ScreenLeft + map.ScreenSize;
        bool rightAligned = Screen.width - (left + iconSize) < iconSize;

        left = rightAligned ? map.ScreenLeft - iconSize : left;

        float offset = rightAligned ? -4f : 4f;

        if (GUI.Button(new Rect(1553, 798, zoomInIcon.width, zoomInIcon.height), zoomInIcon, iconStyle))
        {
            map.ZoomIn();
        }

        if (GUI.Button(new Rect(1553, 844, zoomInIcon.width, zoomInIcon.height), zoomOutIcon, iconStyle))
        {
            map.ZoomOut();
        }
    }

    void DrawMapItems(ref bool labelDrawn)
    {
        foreach (MapItem item in map.MapItems.OrderBy(x => x.ZOrder))
        {
            if (item.GetComponentInParent<SpriteRenderer>().enabled||item.alwaysVisible)
            {
                labelDrawn = item.Draw(labelDrawn) || labelDrawn;
            }
        }
    }

    void DrawLabel()
    {
        float top = 0f;

        switch (map.Orientation)
        {
            case MapOrientation.TopLeft:
            case MapOrientation.TopRight:
            case MapOrientation.TopCenter:
                top = map.ScreenTop + map.ScreenSize + 4;
                break;

            default:
                top = map.ScreenTop - 24f;
                break;
        }

        GUI.Box(new Rect(map.ScreenLeft - 2, top, map.ScreenSize + 4, 20f), "");
        GUI.Label(new Rect(map.ScreenLeft, top, map.ScreenSize + 4, 20f), label);

        if (displayCoordinates)
        {
            float wTop;
            float wLeft;

            Vector3 mpos = map.NormalizedMousePosition;
            Vector3 world = map.TargetPosition;

            if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop))
            {
                world = map.MapToWorldCoords(wLeft, wTop);
            }

            GUIStyle style = new GUIStyle("Label");
            style.alignment = TextAnchor.MiddleRight;

            float x = (float)Math.Round(world.x, 2);
            float y = (float)Math.Round(world.y, 2);

            GUI.Label(new Rect(map.ScreenLeft, top, map.ScreenSize - 4, 20f), string.Format("Y: {0}", y), style);
            GUI.Label(new Rect(map.ScreenLeft, top, map.ScreenSize - 60f, 20f), string.Format("X: {0}", x), style);
        }
    }

    void DrawPingIcons(ref bool labelDrawn)
    {
        if (map.PingIcon != null)
        {
            float size = map.IconSizeScaled;
            Vector3 mpos = map.NormalizedMousePosition;

            foreach (MapActivePing ping in map.Pings)
            {
                float top;
                float left;

                map.WorldToMapCoords(ping.Position, out left, out top);

                top += map.ScreenTop - (size / 2f);
                left += map.ScreenLeft - (size / 2f);

                GUI.DrawTexture(new Rect(left, top, size, size), map.PingIcon, ScaleMode.ScaleAndCrop, map.PingIconAlphaBlend);

                if (!labelDrawn && !string.IsNullOrEmpty(ping.Label))
                {
                    if (mpos.x >= left && mpos.x <= left + size && mpos.y >= top && mpos.y <= top + size)
                    {
                        GUI.Box(new Rect(left + size, top, 150, 20), ping.Label);
                        labelDrawn = true;
                    }
                }
            }
        }
    }

    void clickToMove()
    {
        if (whatAmI == 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked)
        {
            float wTop;
            float wLeft;
            Vector3 mpos = map.NormalizedMousePosition;

            if (whatAmI == 0)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<MonsterAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
            else if (whatAmI == 1)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<RangerAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
            else if (whatAmI == 2)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<HealerAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
            else if (whatAmI == 3)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<TankAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
        }
        else if (whatAmI != 0 && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked)
        {
            float wTop;
            float wLeft;
            Vector3 mpos = map.NormalizedMousePosition;

            if (whatAmI == 0)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<MonsterAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
            else if (whatAmI == 1)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<RangerAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
            else if (whatAmI == 2)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<HealerAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
            else if (whatAmI == 3)
            {
                if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(1) && player.GetComponent<TankAttributesScript>().isAlive)
                {
                    player.GetComponent<Movement>().iMoved(map.MapToWorldCoords(wLeft, wTop));
                }
            }
        }
    }

    void clickToMoveCamera()
    {
            float wTop;
            float wLeft;
            Vector3 mpos = map.NormalizedMousePosition;

            if (map.ScreenToMapCoords(mpos.x, mpos.y, out wLeft, out wTop) && Input.GetMouseButtonDown(0))
            {
                Vector3 CameraPos = map.MapToWorldCoords(wLeft, wTop);
                CameraPos.z = -6;
                if (CameraPos.y < -45f)
                {
                    CameraPos.y = -45f;
                }
                else if (CameraPos.y > 30f)
                {
                    CameraPos.y = 30f;
                }

                if (CameraPos.x < -41f)
                {
                    CameraPos.x = -41f;
                }
                else if (CameraPos.x > 40f)
                {
                    CameraPos.x = 40f;
                }
                GameObject.Find("Main Camera").transform.position = CameraPos;
            }
    }




    void drawCameraRect()
    {
        float orthsize = GameObject.Find("Main Camera").GetComponent<Camera>().orthographicSize;
        float cameraY = 2 * orthsize;
        float cameraX = (cameraY / 9) * 16;
        float top;
        float left;

        map.WorldToMapCoords(GameObject.Find("Main Camera").transform.position, out left, out top);

        top += map.ScreenTop;

        left += map.ScreenLeft;

        Rect viewRect = new Rect(top, left, 100, 100);
        Graphics.DrawTexture(viewRect, viewRect_Texture);
    }


    float native_width = 1920;
    float native_height = 1080;


    void OnGUI()
    {

        float rx = Screen.width / native_width;
        float ry = Screen.height / native_height;
        GL.PushMatrix();
        GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));


        if (iconStyle == null)
        {
            iconStyle = new GUIStyle();
            //iconStyle.padding = new RectOffset(0, 0, -2, 0);
        }

        GUI.Box(new Rect(map.ScreenLeft - 2, map.ScreenTop - 2, map.ScreenSize + 4, map.ScreenSize  + 4), "");
        GUI.BeginGroup(new Rect(map.ScreenLeft, map.ScreenTop, map.ScreenSize, map.ScreenSize));

        GUI.DrawTexture(new Rect(map.TextureLeft, map.TextureTop, map.TextureSize, map.TextureSize), map.Texture, ScaleMode.StretchToFill, false);

        GUI.EndGroup();

        bool labelDrawn = false;

        DrawZoomButtons();
        DrawLabel();
        DrawMapItems(ref labelDrawn);
        DrawPingIcons(ref labelDrawn);
        clickToMove();
        clickToMoveCamera();

        GUI.DrawTexture(new Rect(1920 - minimap_Frame.width, 1080 - minimap_Frame.height, minimap_Frame.width, minimap_Frame.height),minimap_Frame);

        GL.PopMatrix();
    }
}
