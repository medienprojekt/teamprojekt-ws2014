﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
public class SkillUpgradeWindow {

    public Texture upgradeWindow;
    public Texture selection_Frame1;
    public Texture selection_Frame1_Highlighted;
    public Texture selection_Frame2;
    public Texture selection_Frame2_Highlighted;
    public Texture improveSkill;
    public Texture changeSkill;
    public Texture skillFrame;
    public Texture skillButton1;
    public Texture skillButton2;
    public Texture descriptionBox;
    public float x;
    public float y;

    private Rect upgradeWindow_Rect;
    private Rect selectionFrame_1_Rect;
    private Rect selectionFrame_2_Rect;
    private Rect improveSkill_Frame;
    private Rect changeSkill_Frame1;
    private Rect changeSkill_Frame2;

    string key;
    int whatAmI;
    private SkillDescriptionBox[] rangerDescriptions;
    private SkillDescriptionBox[] supportDescriptions;
    private SkillDescriptionBox[] tankDescriptions;


    public SkillUpgradeWindow(Texture upgradeWindow, Texture selection_Frame1, Texture selection_Frame1_Highlighted, Texture selection_Frame2, Texture selection_Frame2_Highlighted, Texture improveSkill, Texture changeSkill, Texture skillFrame, Texture skillButton1, Texture skillButton2, Texture descriptionBox,float y, string key, int whatAmI)
    {
        this.upgradeWindow=upgradeWindow;
        this.selection_Frame1=selection_Frame1;
        this.selection_Frame1_Highlighted=selection_Frame1_Highlighted;
        this.selection_Frame2 = selection_Frame2;
        this.selection_Frame2_Highlighted = selection_Frame2_Highlighted;
        this.improveSkill=improveSkill;
        this.changeSkill=changeSkill;
        this.skillFrame=skillFrame;
        this.skillButton1 = skillButton1;
        this.skillButton2 = skillButton2;
        this.descriptionBox = descriptionBox;
        x = 0;
        this.y = y;
        this.key = key;
        this.whatAmI = whatAmI;

        rangerDescriptions = new SkillDescriptionBox[6];

        rangerDescriptions[0] = new SkillDescriptionBox(descriptionBox);
        rangerDescriptions[0].setKey("Q");
        rangerDescriptions[0].setTitle("Fast Arrows");
        rangerDescriptions[0].setDescription("The Gecko shoots 3 fast and \n1 Charged Arrow at the Target");

        rangerDescriptions[1] = new SkillDescriptionBox(descriptionBox);
        rangerDescriptions[1].setKey("Q");
        rangerDescriptions[1].setTitle("Charge Shot");
        rangerDescriptions[1].setDescription("The Gecko charges a powerful arrow");

        rangerDescriptions[2] = new SkillDescriptionBox(descriptionBox);
        rangerDescriptions[2].setKey("W");
        rangerDescriptions[2].setTitle("Hook");
        rangerDescriptions[2].setDescription("The Gecko uses this skill to retain the Daemon.\nShoot the first Hook in the ground and the second on the Daemon.");

        rangerDescriptions[3] = new SkillDescriptionBox(descriptionBox);
        rangerDescriptions[3].setKey("W");
        rangerDescriptions[3].setTitle("Trap");
        rangerDescriptions[3].setDescription("The Gecko sets up a trap which damages\nthe target on activation.");

        rangerDescriptions[4] = new SkillDescriptionBox(descriptionBox);
        rangerDescriptions[4].setKey("E");
        rangerDescriptions[4].setTitle("Backflip");
        rangerDescriptions[4].setDescription("The Gecko performs a backflip to \nget more range to the target.");

        rangerDescriptions[5] = new SkillDescriptionBox(descriptionBox);
        rangerDescriptions[5].setKey("E");
        rangerDescriptions[5].setTitle("Dash");
        rangerDescriptions[5].setDescription("The Gecko moves very fast in the selected direction. Cloaks for a short moment.");


        supportDescriptions = new SkillDescriptionBox[6];

        supportDescriptions[0] = new SkillDescriptionBox(descriptionBox);
        supportDescriptions[0].setKey("Q");
        supportDescriptions[0].setTitle("Holy Light");
        supportDescriptions[0].setDescription("The Owl uses this skill to heal \nitself or its teammates");

        supportDescriptions[1] = new SkillDescriptionBox(descriptionBox);
        supportDescriptions[1].setKey("Q");
        supportDescriptions[1].setTitle("Absorb");
        supportDescriptions[1].setDescription("The Owl absorbs life from the enemy and links it to a teammate.");

        supportDescriptions[2] = new SkillDescriptionBox(descriptionBox);
        supportDescriptions[2].setKey("W");
        supportDescriptions[2].setTitle("Shield");
        supportDescriptions[2].setDescription("The Owl applies a shield to the target \nwhich absorbs a certain amount of damage");

        supportDescriptions[3] = new SkillDescriptionBox(descriptionBox);
        supportDescriptions[3].setKey("W");
        supportDescriptions[3].setTitle("Soul Link");
        supportDescriptions[3].setDescription("The Owl buffs the power of the selected teammate.\nThe lower the distance, the more effect does it have.");

        supportDescriptions[4] = new SkillDescriptionBox(descriptionBox);
        supportDescriptions[4].setKey("E");
        supportDescriptions[4].setTitle("Blink");
        supportDescriptions[4].setDescription("The Owl teleports itself to the pointed location.");

        supportDescriptions[5] = new SkillDescriptionBox(descriptionBox);
        supportDescriptions[5].setKey("E");
        supportDescriptions[5].setTitle("Port");
        supportDescriptions[5].setDescription("The Owl can teleport the selected teammate to its own position");


        tankDescriptions = new SkillDescriptionBox[6];

        tankDescriptions[0] = new SkillDescriptionBox(descriptionBox);
        tankDescriptions[0].setKey("Q");
        tankDescriptions[0].setTitle("Knockback");
        tankDescriptions[0].setDescription("The Armadillo uses charges at the target and knocks it back.");

        tankDescriptions[1] = new SkillDescriptionBox(descriptionBox);
        tankDescriptions[1].setKey("Q");
        tankDescriptions[1].setTitle("Bash");
        tankDescriptions[1].setDescription("The Armadillo performs a powerful strike,\nwhich slows the enemy");

        tankDescriptions[2] = new SkillDescriptionBox(descriptionBox);
        tankDescriptions[2].setKey("W");
        tankDescriptions[2].setTitle("Barrier");
        tankDescriptions[2].setDescription("The Armadillo summons a barrier in the picked direction.\nNothing can pierce the barrier while it's active.");

        tankDescriptions[3] = new SkillDescriptionBox(descriptionBox);
        tankDescriptions[3].setKey("W");
        tankDescriptions[3].setTitle("Taunt");
        tankDescriptions[3].setDescription("The Armadillo forces the enemy to attack him");

        tankDescriptions[4] = new SkillDescriptionBox(descriptionBox);
        tankDescriptions[4].setKey("E");
        tankDescriptions[4].setTitle("Charge");
        tankDescriptions[4].setDescription("The Armadillo charges in the picked direction.\nIf it hits an enemy while in charge, the enemy gets damaged.");

        tankDescriptions[5] = new SkillDescriptionBox(descriptionBox);
        tankDescriptions[5].setKey("E");
        tankDescriptions[5].setTitle("Warcry");
        tankDescriptions[5].setDescription("The Armadillo performs a war cry to buff all teammates in range");


    }

    float native_width = 1920;
    float native_height = 1080;

    public void drawWindow()
    {
        //float rx = Screen.width / native_width;
        //float ry = Screen.height / native_height;
        //GL.PushMatrix();
        //GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
        //GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));
            
        //Rects
        upgradeWindow_Rect=new Rect(0,y,upgradeWindow.width,upgradeWindow.height);
        selectionFrame_1_Rect = new Rect(upgradeWindow_Rect.x + 35, upgradeWindow_Rect.y + 80, selection_Frame1.width, selection_Frame1.height);
        selectionFrame_2_Rect = new Rect(upgradeWindow_Rect.x + 11, upgradeWindow_Rect.y + 200, selection_Frame2.width, selection_Frame2.height);
        improveSkill_Frame = new Rect(selectionFrame_1_Rect.x + 22, selectionFrame_1_Rect.y + 11, 91, 91);
        changeSkill_Frame1 = new Rect(selectionFrame_2_Rect.x + 22, selectionFrame_2_Rect.y + 11, 91, 91);
        changeSkill_Frame2 = new Rect(selectionFrame_2_Rect.x + 227, selectionFrame_2_Rect.y + 11, 91, 91);

        //Textures
        Graphics.DrawTexture(upgradeWindow_Rect, upgradeWindow);


        if (!selectionFrame_1_Rect.Contains(Event.current.mousePosition))
        {
            Graphics.DrawTexture(selectionFrame_1_Rect, selection_Frame1);
        }
        else
        {
            Graphics.DrawTexture(selectionFrame_1_Rect, selection_Frame1_Highlighted);
        }

        if (!selectionFrame_2_Rect.Contains(Event.current.mousePosition))
        {
            Graphics.DrawTexture(selectionFrame_2_Rect, selection_Frame2);
        }
        else
        {
            Graphics.DrawTexture(selectionFrame_2_Rect, selection_Frame2_Highlighted);
        }


        float buttonSize = 87;

        Graphics.DrawTexture(improveSkill_Frame, skillFrame);
        Graphics.DrawTexture(changeSkill_Frame1, skillFrame);
        Graphics.DrawTexture(changeSkill_Frame2, skillFrame);
        Graphics.DrawTexture(new Rect((improveSkill_Frame.x + (improveSkill_Frame.width / 2)) - buttonSize / 2, (improveSkill_Frame.y + (improveSkill_Frame.height / 2)) - buttonSize / 2, buttonSize, buttonSize), skillButton1);
        Graphics.DrawTexture(new Rect((changeSkill_Frame1.x + (changeSkill_Frame1.width / 2)) - buttonSize / 2, (changeSkill_Frame1.y + (changeSkill_Frame1.height / 2)) - buttonSize / 2, buttonSize, buttonSize), skillButton1);
        Graphics.DrawTexture(new Rect((changeSkill_Frame2.x + (changeSkill_Frame2.width / 2)) - buttonSize / 2, (changeSkill_Frame2.y + (changeSkill_Frame2.height / 2)) - buttonSize / 2, buttonSize, buttonSize), skillButton2);

        Graphics.DrawTexture(new Rect(selectionFrame_1_Rect.x + 123, (selectionFrame_1_Rect.y + (selectionFrame_1_Rect.height / 2)) - (improveSkill.height / 2), improveSkill.width, improveSkill.height), improveSkill);
        Graphics.DrawTexture(new Rect((selectionFrame_2_Rect.x + (selectionFrame_2_Rect.width / 2)) - (changeSkill.width / 2), (selectionFrame_2_Rect.y + (selectionFrame_2_Rect.height / 2)) - (changeSkill.height/ 2), changeSkill.width, changeSkill.height), changeSkill);


        if (improveSkill_Frame.Contains(Event.current.mousePosition))
        {
            if (key == "Q")
            {
                if (whatAmI == 1)
                {
                    rangerDescriptions[0].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[0].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[0].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }

            }

            if (key == "W")
                if (whatAmI == 1)
                {
                    rangerDescriptions[2].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[2].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[2].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }

            if (key == "E")
                if (whatAmI == 1)
                {
                    rangerDescriptions[4].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[4].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[4].DrawBoxWithCoords(true, improveSkill_Frame.x + improveSkill_Frame.width, improveSkill_Frame.y - improveSkill_Frame.height);
                }
        } 
        else  if (changeSkill_Frame1.Contains(Event.current.mousePosition))
        {
            if (key == "Q")
            {
                if (whatAmI == 1)
                {
                    rangerDescriptions[0].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[0].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[0].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }

            }

            if (key == "W")
                if (whatAmI == 1)
                {
                    rangerDescriptions[2].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[2].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[2].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }

            if (key == "E")
                if (whatAmI == 1)
                {
                    rangerDescriptions[4].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[4].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[4].DrawBoxWithCoords(true, changeSkill_Frame1.x + changeSkill_Frame1.width, changeSkill_Frame1.y - changeSkill_Frame1.height);
                }
        }
        else
        {
            if (key == "Q")
            {
                if (whatAmI == 1)
                {
                    rangerDescriptions[0].DrawBoxWithCoords(false,0,0);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[0].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[0].DrawBoxWithCoords(false, 0, 0);
                }

            }

            if (key == "W")
                if (whatAmI == 1)
                {
                    rangerDescriptions[2].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[2].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[2].DrawBoxWithCoords(false, 0, 0);
                }

            if (key == "E")
                if (whatAmI == 1)
                {
                    rangerDescriptions[4].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[4].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[4].DrawBoxWithCoords(false, 0, 0);
                }
        }

        if (changeSkill_Frame2.Contains(Event.current.mousePosition))
        {
            if (key == "Q")
            {
                if (whatAmI == 1)
                {
                    rangerDescriptions[1].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[1].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[1].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }

            }

            if (key == "W")
                if (whatAmI == 1)
                {
                    rangerDescriptions[3].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[3].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[3].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }

            if (key == "E")
                if (whatAmI == 1)
                {
                    rangerDescriptions[5].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[5].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[5].DrawBoxWithCoords(true, changeSkill_Frame2.x + changeSkill_Frame2.width, changeSkill_Frame2.y - changeSkill_Frame2.height);
                }
        }
        else
        {
            if (key == "Q")
            {
                if (whatAmI == 1)
                {
                    rangerDescriptions[1].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[1].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[1].DrawBoxWithCoords(false, 0, 0);
                }

            }

            if (key == "W")
                if (whatAmI == 1)
                {
                    rangerDescriptions[3].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[3].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[3].DrawBoxWithCoords(false, 0, 0);
                }

            if (key == "E")
                if (whatAmI == 1)
                {
                    rangerDescriptions[5].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[5].DrawBoxWithCoords(false, 0, 0);
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[5].DrawBoxWithCoords(false, 0, 0);
                }
        }


            for (int i = 0; i < 6; ++i)
            {
                if (whatAmI == 1)
                {
                    rangerDescriptions[i].Draw();
                }
                else if (whatAmI == 2)
                {
                    supportDescriptions[i].Draw();
                }
                else if (whatAmI == 3)
                {
                    tankDescriptions[i].Draw();
                }
            }
        //GL.PopMatrix();
    }

    public bool skillImproved()
    {
        return selectionFrame_1_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonUp(0);

    }

    public bool skillChanged()
    {
        return selectionFrame_2_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonUp(0);
    }

}
