﻿using UnityEngine;
using System.Collections;

public class SkillDescriptionBox
{

    private Texture box;
    private string Title;
    private string Key;
    private string Description;
    private bool draw;
    private bool drawWithCoords;
    private float width;
    private float height;
    private float x;
    private float y;
    private GUIStyle style;
    private Rect rect;


    // Use this for initialization

    public SkillDescriptionBox(Texture box)
    {
        this.box = box;
        draw = false;
        drawWithCoords = false;
        Title = "";
        Key = "";
        Description = "";
        style = new GUIStyle();
        style.fontSize = 35;
        style.normal.textColor = Color.white;
        width = 0;
        height = 0;
        Update();
    }

    // Update is called once per frame
    public void Update()
    {
        if (!drawWithCoords) { 
            if (Key == "Q")
            {
                x = 760;
                y = 927;
            }
            else if (Key == "W")
            {
                x = 921;
                y = 927;
             }
            else if (Key == "E")
            {
                x = 1058;
                y = 927;
            }
            else if (Key == "R"||Key=="P")
            {
                x = 1176;
                y = 927;
            }
            else if (Key == "T")
            {
                x = 1275;
                y = 927;
            }
    }
        style.fontStyle = FontStyle.Normal;
        style.fontSize = 30;
        width = style.CalcSize(new GUIContent(Description)).x+14;
        rect = new Rect(x, y, width, height);
        style.fontSize = 35;
        height = style.CalcSize(new GUIContent(Title)).y;
        style.fontStyle = FontStyle.Normal;
        style.fontSize = 30;
        height += style.CalcSize(new GUIContent(Description)).y+14;

        x -= width / 2;
        y -= height;

    }

    public void setTitle(string title)
    {
        Title = title;
        Update();
    }

    public void setKey(string key)
    {
        Key = key;
        Update();
    }

    public void setDescription(string desc)
    {
        Description = desc;
        Update();
    }

    public void DrawBox(bool draw)
    {
        this.draw = draw;
    }

    public void DrawBoxWithCoords(bool draw,float x, float y)
    {
        drawWithCoords = draw;
        this.x = x;
        this.y = y;
        
    }

    float native_width = 1920;
    float native_height = 1080;

    public void Draw()
    {

        if (draw||drawWithCoords)
        {
            //float rx = Screen.width / native_width;
            //float ry = Screen.height / native_height;
            //GL.PushMatrix();
            //GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
            //GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));
            
            if (box != null)
            {
                    Graphics.DrawTexture(new Rect(x, y, width, height), box, 14, 14, 12, 12);
                    rect = new Rect(x, y, width, height);
                    style.fontSize = 35;
                    style.fontStyle = FontStyle.Bold;
                    GUI.Label(new Rect(rect.x + 7, rect.y + 7, rect.width, rect.height), Title, style);
                    style.fontStyle = FontStyle.Normal;
                    style.fontSize = 30;
                    GUI.Label(new Rect(rect.x + 7, rect.y + style.CalcSize(new GUIContent(Title)).y + 10, rect.width, rect.height), Description, style);
                    
                    if(Key=="Q"||Key=="W"||Key=="E"||Key=="R"||Key=="T"){
                        GUI.Label(new Rect(((rect.x + rect.width) - style.CalcSize(new GUIContent("[" + Key + "]")).x) - 7, rect.y + 7, style.CalcSize(new GUIContent("[" + Key + "]")).x, style.CalcSize(new GUIContent("[" + Key + "]")).y), "[" + Key + "]", style);
                    }


            }
            //
            // GL.PopMatrix();
       }
    }
}
