﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTimeScript : MonoBehaviour {

    public float destroyTime;
    private float time;
    private Color curColor;
    public GameObject target;
    public float zLayer;

    // Use this for initialization
    void Start() {
        time = Time.time + destroyTime;
        curColor = GetComponent<SpriteRenderer>().color;
        Vector3 v3 = transform.position;
        v3.z += zLayer;
        transform.position = v3;
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (Time.time > time - destroyTime/2) {
            curColor.a -= 0.02f;
        }
         GetComponent<SpriteRenderer>().color = curColor;
    }


    // Update is called once per frame
    void Update() {

        if (target != null) {
            Vector3 v3 = target.transform.position;
            v3.z += zLayer;
            transform.position = v3;
        }

        if (curColor.a <= 0.0f) {
            Destroy(this.gameObject);

        }


    }
}
