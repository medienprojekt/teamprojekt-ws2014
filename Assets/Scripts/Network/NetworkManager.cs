﻿using UnityEngine;
using System.Collections;

public class NetworkManager : Photon.MonoBehaviour {

    public string monsterPlayerName;
    public string rangerPlayerName;
    public string supportPlayerName;
    public string tankPlayerName;


    void Start()
    {
        monsterPlayerName = "";
        rangerPlayerName = "";
        supportPlayerName = "";
        tankPlayerName = "";
    }

    public string getPlayerName(int role)
    {
        if (role == 0)
        {
            return monsterPlayerName;
        }
        else if (role == 1)
        {
            return rangerPlayerName;
        } else if (role == 2)
        {
            return supportPlayerName;
        } else if (role == 3)
        {
            return tankPlayerName;
        }

        return "";
    }

    public void setUpPlayerName(string name, int role)
    {
        if (role == 0)
        {
            monsterPlayerName=name;
        }
        else if (role == 1)
        {
           rangerPlayerName=name;
        }
        else if (role == 2)
        {
            supportPlayerName=name;
        }
        else if (role == 3)
        {
            tankPlayerName=name;
        }
    }


    public void sendNameOnJoin(string name, int role)
    {
        if (role == 0)
        {
            photonView.RPC("sendName", PhotonTargets.Others, name, role);
        }
        else if (role == 1)
        {
            photonView.RPC("sendName", PhotonTargets.Others, name, role);
        } else if (role == 2)
        {
            photonView.RPC("sendName", PhotonTargets.Others, name, role);
        }
        else if (role == 3)
        {
            photonView.RPC("sendName", PhotonTargets.Others, name, role);
        }

    }


    void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("sendName", player, monsterPlayerName, 0);
            photonView.RPC("sendName", player, rangerPlayerName, 1);
            photonView.RPC("sendName", player, supportPlayerName, 2);
            photonView.RPC("sendName", player, tankPlayerName, 3);
        }
    }

    [RPC]
    void sendName(string playerName, int role)
    {
        setUpPlayerName(playerName, role);
    }
}
