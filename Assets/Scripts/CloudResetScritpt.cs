﻿using UnityEngine;
using System.Collections;

public class CloudResetScritpt : MonoBehaviour {

    public GameObject resetPoint;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider coll) {

        if (coll.name == "cloud") {
            coll.transform.position = new Vector3(resetPoint.transform.position.x, coll.transform.position.y, coll.transform.position.z);

        }
    }


}
