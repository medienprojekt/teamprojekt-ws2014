﻿using UnityEngine;
using System.Collections;

public class WardScript : Photon.MonoBehaviour {

	private Menu menuScript;

    // 1 jäger, 2 monster
    public int iAmCapturedByTeam;

    private Menu menu;

    private FogOfWarCircleRevealer fowC;

    public Sprite sprite;

	public AudioClip tookWardSound;
	private bool playedSoundTeam1;
	private bool playedSoundTeam2;

    private Animator animator;

    public GameObject effect;

	// Use this for initialization
	void Start () {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		playedSoundTeam1 = false;
		playedSoundTeam2 = false;

        iAmCapturedByTeam = 0;
        menu = GameObject.Find("Menu").GetComponent<Menu>();
        fowC = this.GetComponent<FogOfWarCircleRevealer>();

        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        if (iAmCapturedByTeam != 0) {
            animator.SetBool("Active", true);
        }

        if (iAmCapturedByTeam == 1 && menu.iAmOnTeam == 1) {
            fowC.enabled = true;
			if(playedSoundTeam1 == false || playedSoundTeam2 == true)
			{
                Vector3 v3 = transform.position;
                v3.x += 0.2f;
                v3.y += 0.25f;
                Instantiate(effect, v3, transform.rotation);
                PlayTookWardSound ();
				playedSoundTeam1 = true;
				playedSoundTeam2 = false;
			}
        }
        else if (iAmCapturedByTeam == 2 && menu.iAmOnTeam == 2) {
            fowC.enabled = true;
			if(playedSoundTeam2 == false || playedSoundTeam1 == true)
			{
                Vector3 v3 = transform.position;
                v3.x += 0.2f;
                v3.y += 0.25f;
                Instantiate(effect, v3, transform.rotation);
                PlayTookWardSound ();
				playedSoundTeam2 = true;
				playedSoundTeam1 = false;
			}
        }
        else {
            fowC.enabled = false;
        }



	}

	public void PlayTookWardSound (){
		AudioSource.PlayClipAtPoint (tookWardSound,transform.position,1.00f*menuScript.scaleVolumeFactor);
	}

    public void reset() {
        iAmCapturedByTeam = 0;
        fowC.enabled = false;
        animator.SetBool("Active", false);
    }
}
