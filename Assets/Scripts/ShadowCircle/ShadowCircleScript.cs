﻿using UnityEngine;
using System.Collections;

public class ShadowCircleScript : MonoBehaviour {

	private Menu menuScript;

	public float startRadius;
	private float actualRadius;
	public float radiusReductionPerCooldown;
	public float endRadius;
	public float coolDown;
	private float coolDownTime;
	private CapsuleCollider capsuleCollider;

	public int damagePerSecond;
	private float damageCoolDown;
	private float damageCoolDownTimeMonster;
	private float damageCoolDownTimeRanger;
	private float damageCoolDownTimeHealer;
	private float damageCoolDownTimeTank;

	public bool monsterOutOfArena;
	public bool rangerOutOfArena;
	public bool healerOutOfArena;
	public bool tankOutOfArena;
	
	public AudioSource asound;
	public AudioClip shadowStartingSound;
	public bool shadowStartingSoundPlayed;

    private bool startCircle;

	// Use this for initialization
	void Start () {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.Stop ();
		shadowStartingSoundPlayed = false;

		startRadius = 100.0f;
		actualRadius = startRadius;
		capsuleCollider = transform.GetComponent<CapsuleCollider> ();
		capsuleCollider.radius = actualRadius;
		endRadius = 13.0f;

		radiusReductionPerCooldown = 0.3f;//Diesen Wert muss man einstellen
		coolDown = 1.0f;
		//coolDownTime = 0.0f + Time.time + coolDown;
		coolDownTime = 0.0f + (float) PhotonNetwork.time + coolDown;


		damageCoolDown = 1.0f;
		damageCoolDownTimeMonster = -1.0f;
		damageCoolDownTimeTank = damageCoolDownTimeHealer = damageCoolDownTimeRanger = damageCoolDownTimeMonster;

		damagePerSecond = 50;

		monsterOutOfArena = false;
		rangerOutOfArena = false;
		healerOutOfArena = false;
		tankOutOfArena = false;

        startCircle = false;
	}

	void OnTriggerExit(Collider collider/*CapsuleCollider capsuleCollider*/)
	{

		if (/*menuScript.iAmOnTeam == 1 && */collider.tag == "Monster") {
			//asound.Stop ();
			monsterOutOfArena = true;
			//Debug.Log ("Monster out of Arena");
		}
		
		//Hunter im Busch
		if (/*menuScript.iAmOnTeam == 2 && */collider.tag == "Team") {
			
			if (collider.name == "Ranger") {
				//asound.Stop ();
				rangerOutOfArena = true;
			}
			else if (collider.name == "Healer") {
				//asound.Stop ();
				healerOutOfArena = true;
			}
			else if (collider.name == "Tank") {
				//asound.Stop ();
				tankOutOfArena = true;
			}
			//Debug.Log ("Hunters out of Arena");
		}
	}

	void OnTriggerEnter(Collider collider/*CapsuleCollider capsuleCollider*/)
	{
		if (/*menuScript.iAmOnTeam == 1 && */collider.tag == "Monster") {
			//asound.Play ();
			monsterOutOfArena = false;
			//Debug.Log ("Monster back in of Arena");
		}
		
		//Hunter im Busch
		if (/*menuScript.iAmOnTeam == 2 && */collider.tag == "Team") {
			
			if (collider.name == "Ranger") {
				//asound.Play ();
				rangerOutOfArena = false;
			}
			else if (collider.name == "Healer") {
				//asound.Play ();
				healerOutOfArena = false;
			}
			else if (collider.name == "Tank") {
				//asound.Play ();
				tankOutOfArena = false;
			}
			//Debug.Log ("Hunters back in of Arena");
		}
	}
	[RPC]
	void PlayShadowStartingSound()
	{
		AudioSource.PlayClipAtPoint (shadowStartingSound, transform.position, 0.70f * menuScript.scaleVolumeFactor);
		shadowStartingSoundPlayed = true;
	}
	// Update is called once per frame
	void Update () {

        if (!startCircle && GameObject.Find("AltarPrefab1").GetComponent<AltarsScript>().health <= 0 || 
            GameObject.Find("AltarPrefab2").GetComponent<AltarsScript>().health <= 0 ||
            GameObject.Find("AltarPrefab3").GetComponent<AltarsScript>().health <= 0 ||
            GameObject.Find("AltarPrefab4").GetComponent<AltarsScript>().health <= 0) {
                startCircle = true;

        }

		if (!shadowStartingSoundPlayed && actualRadius <= 99.0f) 
		{
			PlayShadowStartingSound();
		}



        if (startCircle && actualRadius > endRadius) 
		{
			if(coolDownTime < (float) PhotonNetwork.time){
				actualRadius -= radiusReductionPerCooldown;
				coolDownTime = (float) PhotonNetwork.time + coolDown;
                capsuleCollider.radius = actualRadius;
            }
			
		}

		//Damage Dealen beim sich außerhalb Befinden
        if (PhotonNetwork.isMasterClient) {
            dealShadowDamage();
        }
	}

	void dealShadowDamage()
	{
		asound = GetComponent<AudioSource>();
		asound.loop = true;
		asound.volume = 0.6f * menuScript.scaleVolumeFactor;
		if (healerOutOfArena) 
		{
			if(menuScript.iAm == "Healer" && !asound.isPlaying)
			{
				asound.Play ();
			} 
			if (GameObject.Find("Healer") != null) 
			{
				GameObject healer = GameObject.Find("Healer");
				if(damageCoolDownTimeHealer < (float) PhotonNetwork.time)
				{
                    healer.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damagePerSecond, true);
					damageCoolDownTimeHealer = (float) PhotonNetwork.time + damageCoolDown;
				}
			}

		}
		else 
		{
			if(menuScript.iAm == "Healer" && asound.isPlaying)
			{
				asound.Stop ();
			}
		} 
		
		if (rangerOutOfArena) {
			if(menuScript.iAm == "Ranger" && !asound.isPlaying)
			{
				asound.Play ();
			} 
						
						if (GameObject.Find ("Ranger") != null) {
								GameObject ranger = GameObject.Find ("Ranger");
								if (damageCoolDownTimeRanger < (float)PhotonNetwork.time) {
                                    ranger.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damagePerSecond, true);
										damageCoolDownTimeRanger = (float)PhotonNetwork.time + damageCoolDown;
								}
						}
				} 
		else 
		{
			if(menuScript.iAm == "Ranger" && asound.isPlaying)
			{
				asound.Stop ();
			}
		} 
		
		if (tankOutOfArena) 
		{
			if(menuScript.iAm == "Tank" && !asound.isPlaying)
			{
				asound.Play ();
			} 
			if (GameObject.Find("Tank") != null) 
			{
				GameObject tank = GameObject.Find("Tank");
				if(damageCoolDownTimeTank < (float) PhotonNetwork.time)
				{
                    tank.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damagePerSecond, true);
					damageCoolDownTimeTank = (float) PhotonNetwork.time + damageCoolDown;
				}
			}
		}
		else 
		{
			if(menuScript.iAm == "Tank" && asound.isPlaying)
			{
				asound.Stop ();
			}
		} 
		
		if (monsterOutOfArena) 
		{
			if(menuScript.iAm == "Monster" && !asound.isPlaying)
			{
				asound.Play ();
			} 
			if (GameObject.Find("Monster") != null) 
			{
				GameObject monster = GameObject.Find("Monster");
				if(damageCoolDownTimeMonster < (float) PhotonNetwork.time)
				{
					monster.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(-(damagePerSecond*6));
					damageCoolDownTimeMonster = (float) PhotonNetwork.time + damageCoolDown;
				}
			}
		}
		else 
		{
			if(menuScript.iAm == "Monster" && asound.isPlaying)
			{
				asound.Stop ();
			}
		} 
	}


    public void reset() {
        startCircle = false;
        actualRadius = startRadius;
        capsuleCollider.radius = startRadius;
    }

}
