﻿using UnityEngine;
using System.Collections;

public class ShadowVisualsScript : Photon.MonoBehaviour {

	private GameObject shadowCircle;

	// Use this for initialization
	void Start () {
		shadowCircle = GameObject.Find("ShadowCircle");
	}
	
	// Update is called once per frame
	void Update () {

        this.renderer.material.SetVector("_Circle_Pos", shadowCircle.transform.position);

        this.renderer.material.SetFloat("_FogRadius", shadowCircle.GetComponent<CapsuleCollider>().radius+1.0f);


	}
}
