﻿using UnityEngine;
using System.Collections;

public class RangerSkillControlScript : Photon.MonoBehaviour {

	private Menu menuScript;

    private RangerControlScript rangerControlScript;
    private int skillNumer; //0 nichts, 1-4 q-r
    private RangerAttributesScript rangerAttributesScript;
    public GameObject skill1Prefab;
    public bool skillMode;
    public GameObject skillCirclePrefab;
    private GameObject skillCircle;

    public CooldownTimer cdTSkill1;
    public CooldownTimer cdTSkill2;
    public CooldownTimer cdTSkill3;
    public CooldownTimer cdTSkill11;
    public CooldownTimer cdTSkill21;
    public CooldownTimer cdTSkill31;

    public bool attackSkill11Active;
    private float curRangeSkill11;
    private float curDamageSkill11;

    private Vector3 clickPoint;

    private bool attackSkill21Active;

    private Movement movementScript;

    public int currentTrapCounter = 0;
    private GameObject[] traps = new GameObject[3];

    private MouseHitboxScript mouseHitboxScript;

    private float oldMovmentSpeed;

    public bool attackSkill1Active;

    private GameObject targetGO;

    private float timeStempSkill1;
    private bool skill1FirstShot;
    private bool skill1SecondShot;
    private bool skill1ThirdShot;

    public bool attackSkill2Active;

    private bool skill2FirstShot;

    private GameObject skill2Shot1;
    private GameObject skill2Shot2;

    public Transform rangerTrapAni1;

    public Transform rangerChargeAni1;

    public Transform rangerSchnellschussAni;

    private Transform schnellschussAniTransform;

    private Animator animator;

    private bool blockInput;

    public Transform dashAni1;
    public Transform dashAni2;
    public Transform hookAni;

    private AnimaitonScript animationScript;

    private bool offerForQSkill;
    private bool offerForWSkill;
    private bool offerForESkill;

    private int qSkill;
    private int wSkill;
    private int eSkill;

	public AudioClip[] sounds;
	public AudioSource asound;
	private bool q1SkillPlayOnce;
	public AudioClip winSound;
	public AudioClip loseSound;
	private bool winSoundPlayed;
	private bool loseSoundPlayed;

    public bool iAmSilenced;
    public float silenceEnd;

    public bool passivActive;
    public int passivStack;
    private float cdPassivStack;

    private float hookDestroyAutoTimer;

    public bool hoverQ;
    public bool hoverW;
    public bool hoverE;

    private bool mouseClickSkill;

    // Use this for initialization
    void Start() {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		winSoundPlayed = false;
		loseSoundPlayed = false;

		q1SkillPlayOnce = false;

        rangerControlScript = transform.GetComponent<RangerControlScript>();
        rangerAttributesScript = transform.GetComponent<RangerAttributesScript>();
        if (photonView.isMine) {


            skill2FirstShot = false;

            skill1FirstShot = false;
            skill1SecondShot = false;
            skill1ThirdShot = false;

            mouseHitboxScript = GameObject.Find("MouseHitbox").GetComponent<MouseHitboxScript>();

            animationScript = transform.GetComponent<AnimaitonScript>();

            attackSkill11Active = false;

            skillCircle = Instantiate(skillCirclePrefab, transform.position, transform.rotation) as GameObject;
            skillCircle.transform.parent = transform;
            Vector3 sCPos = new Vector3(transform.position.x, transform.position.y, 3);
            skillCircle.transform.position = sCPos;
            skillCircle.renderer.enabled = false;

            movementScript = transform.GetComponent<Movement>();

            animator = this.GetComponent<Animator>();

            skillMode = false;
            skillNumer = 0;

            cdTSkill1 = new CooldownTimer(rangerAttributesScript.cdSkill1);
            cdTSkill2 = new CooldownTimer(rangerAttributesScript.cdSkill2);
            cdTSkill3 = new CooldownTimer(rangerAttributesScript.cdSkill3);
            cdTSkill11 = new CooldownTimer(rangerAttributesScript.cdSkill11);
            cdTSkill21 = new CooldownTimer(rangerAttributesScript.cdSkill21);
            cdTSkill31 = new CooldownTimer(rangerAttributesScript.cdSkill31);
        } else {
            enabled = false;
        }


    }

    // Update is called once per frame
    void Update() {
        if (!blockInput && rangerAttributesScript.isAlive && !iAmSilenced && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked){
            checkUpKeyPressed();
        }
        skillActive();
        if (!hoverQ && !hoverW && !hoverE) {
            skillCircle.renderer.enabled = false;
        }
        if (skillMode && !blockInput) {

            if (skillNumer == 1) {
                skillCircle.renderer.enabled = true;
                float x = (rangerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (rangerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }

            if (skillNumer == 11) {
                skillCircle.renderer.enabled = true;
                float x = (curRangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (curRangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }

            if (skillNumer == 2) {
                skillCircle.renderer.enabled = true;
                float x = (rangerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (rangerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }

            if (skillNumer == 21) {
                skillCircle.renderer.enabled = true;
                float x = (rangerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (rangerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }


            if (skillNumer == 3) {
                Vector3 newPos = calculateNewPos(transform.position, animator.GetInteger("Direction") ,rangerAttributesScript.rangeSkill3);
                movementScript.iBlinked(newPos);
                cdTSkill3.StartCooldown();
                skillMode = false;
                photonView.RPC("RangerStartBackflipAni", PhotonTargets.All);
                //SOUND HIER backflip
				photonView.RPC ("PlayRangerBackflipSound", PhotonTargets.All, null);
				revealMeRanger();
            }

            if (skillNumer == 31) {
                skillCircle.renderer.enabled = true;
                float x = (rangerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (rangerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }


            if (Input.GetMouseButtonDown(0) && !mouseClickSkill) {
                //fire skill
                fireSkill(skillNumer);
            }
            else if (mouseClickSkill) {
                mouseClickSkill = false;
            }

        }

        if (iAmSilenced && Time.time > silenceEnd) {
            iAmSilenced = false;
        }

        if (passivStack >= 4) {
            passivStack = 4;
        }


        if (passivActive && Time.time > cdPassivStack) {
            passivStack = 0;
        }

        if (attackSkill2Active && Time.time > hookDestroyAutoTimer) {
            disableAllSkills();
        }

		//WIN | LOSE
		if (GameObject.Find ("GameManager") != null) {
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().huntersWin == true && winSoundPlayed == false) {
				PlayWinSound ();
				winSoundPlayed = true;
			}
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().monsterWin == true && loseSoundPlayed == false) {
				PlayLoseSound ();
				loseSoundPlayed = true;
			}
		}

    }


    void fireSkill(int number) {
        GetComponent<AnimaitonScript>().lookAt(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z));
        rangerControlScript.attackMode = false;

        //schnellschuss
        if (number == 1) {
            if (mouseHitboxScript.mouseOverEnemy) {
                photonView.RPC("RangerStartSchnellschussAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                targetGO = mouseHitboxScript.mouseOverLastEnemyGO;
                timeStempSkill1 = Time.time;
                skill1FirstShot = false;
                skill1SecondShot = false;
                skill1ThirdShot = false;
                attackSkill1Active = true;
                cdTSkill1.StartCooldown();
            }
            skillMode = false;
        }
        //chargeschuss
        else if (number == 11) {
            //ziel
            Vector3 tempV3 = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z);
            photonView.RPC("RangerSkill11", PhotonTargets.MasterClient, 0, transform.name, "Team", tempV3, curDamageSkill11, curRangeSkill11);

            revealMeRanger();
            photonView.RPC("RangerStopChargeAni", PhotonTargets.All);
            animationScript.startAttackAnimationOne();
            attackSkill11Active = false;
            skillMode = false;
            cdTSkill11.StartCooldown();
            rangerAttributesScript.movementSpeed = oldMovmentSpeed;
            //SOUND HIER chargeschuss abschuss
            q1SkillPlayOnce = false;
            photonView.RPC("PlayRangerChargeSchussSound", PhotonTargets.All, null);
            revealMeRanger();
            movementScript.stopMove();
        }
        //hook
        else if (number == 2) {
            //erster schuss
            //ziel berechnen
            Vector3 hookTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            hookTarget.z = transform.position.z;
            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            if (Vector3.Distance(hookTarget, transform.position) > rangerAttributesScript.rangeSkill2) {
                hookTarget = hookTarget - transform.position;
                hookTarget.Normalize();
                hookTarget = rangerAttributesScript.rangeSkill2 * hookTarget;
                hookTarget = hookTarget + transform.position;
            }


            //2. schuss
            if (skill2FirstShot) {
                photonView.RPC("RangerSkill2", PhotonTargets.MasterClient, "Team", hookTarget, rangerAttributesScript.attackDamage, skill2Shot1.GetPhotonView().viewID, 0, 2, rangerAttributesScript.timeSkill2);
                skillMode = false;
                skill2FirstShot = false;
                attackSkill2Active = false;
                cdTSkill2.StartCooldown();
                photonView.RPC("RangerStartHookCastAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                animationScript.startAttackAnimationOne();

                revealMeRanger();
                //SOUND HIER hook
                photonView.RPC("PlayRangerHook1Sound", PhotonTargets.All, null);
                revealMeRanger();
                return;

            }

            //1. schuss
            if (!skill2FirstShot) {
                photonView.RPC("RangerSkill2", PhotonTargets.MasterClient, "Team", hookTarget, rangerAttributesScript.attackDamage, 0, 0, 1, rangerAttributesScript.timeSkill2);
                skill2FirstShot = true;
                attackSkill2Active = true;
                photonView.RPC("RangerStartHookCastAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                animationScript.startAttackAnimationOne();

                revealMeRanger();
                //SOUND HIER hook
                photonView.RPC("PlayRangerHook1Sound", PhotonTargets.All, null);
                revealMeRanger();
                hookDestroyAutoTimer = Time.time + 4.0f;
            }
        }



            //trap
        else if (number == 21) {
            clickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            clickPoint.z = transform.position.z;
            attackSkill21Active = true;
            //cdTSkill21.StartCooldown();
        }

             //backflip
        else if (number == 3) {
            //nichts zu tun

        }



    //dash mit unsichtbarkeit
        else if (number == 31) {

            //ziel berechnen
            Vector3 pointToMoveSkill3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pointToMoveSkill3.z = transform.position.z;
            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            //berechnung falsch
            if (Vector3.Distance(pointToMoveSkill3, transform.position) > rangerAttributesScript.rangeSkill31) {
                pointToMoveSkill3 = pointToMoveSkill3 - transform.position;
                pointToMoveSkill3.Normalize();
                pointToMoveSkill3 = pointToMoveSkill3 * rangerAttributesScript.rangeSkill31;
                pointToMoveSkill3 = pointToMoveSkill3 + transform.position;
            }

            //print(Vector3.Distance(pointToMoveSkill3, transform.position));

            movementScript.iCharged(pointToMoveSkill3);

            oldMovmentSpeed = rangerAttributesScript.movementSpeed;
            rangerAttributesScript.movementSpeed = rangerAttributesScript.movementSpeed * 2.5f;
            cdTSkill31.StartCooldown();
            skillMode = false;
            blockInput = true;
            rangerControlScript.blockInput = true;
            photonView.RPC("RangerStartDashAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
            //SOUND HIER dash
            PlayRangerDashSound();
            revealMeRanger();
        }


    }

    public void skillActive() {

        //schnellschuss
        if (attackSkill1Active) {
            if (targetGO != null) {
                if (skill1FirstShot || Vector3.Distance(transform.position, targetGO.transform.position) < rangerAttributesScript.rangeSkill1) {
                    //erster schuss
                    if (!skill1FirstShot) {
                        rangerControlScript.spawnAutoAttackFromSkill(targetGO);
                        timeStempSkill1 = Time.time;
                        skill1FirstShot = true;
                        animationScript.startAttackAnimationOne();
                        //SOUND HIER schnellschuss 1
						photonView.RPC ("PlayRangerAutoAttackSound", PhotonTargets.All, null);
                        revealMeRanger();
                    }
                    if (skill1FirstShot && !skill1SecondShot && Time.time > timeStempSkill1 + 0.5f) {
                        rangerControlScript.spawnAutoAttackFromSkill(targetGO);
                        timeStempSkill1 = Time.time;
                        skill1SecondShot = true;
                        photonView.RPC("RangerStartSchnellschussNextShot", PhotonTargets.All);
                        animationScript.startAttackAnimationOne();
                        //SOUND HIER schnellschuss 2
						photonView.RPC ("PlayRangerAutoAttackSound", PhotonTargets.All, null);
                        revealMeRanger();
                    }
                    if (skill1SecondShot && !skill1ThirdShot && Time.time > timeStempSkill1 + 0.5f) {
                        rangerControlScript.spawnAutoAttackFromSkill(targetGO);
                        timeStempSkill1 = Time.time;
                        skill1ThirdShot = true;
                        photonView.RPC("RangerStartSchnellschussNextShot", PhotonTargets.All);
                        animationScript.startAttackAnimationOne();
                        //SOUND HIER schnellschuss 3
						photonView.RPC ("PlayRangerAutoAttackSound", PhotonTargets.All, null);
                        revealMeRanger();
                    }
                    //aufladeanimation starten
                    if (skill1ThirdShot) {
						//photonView.RPC ("PlayRangerSchnellSchussChargeSound", PhotonTargets.All, null);
                    }
                    //großer schuss
                    if (skill1ThirdShot && Time.time > timeStempSkill1 + 1.2f) {
                        photonView.RPC("RangerStartSchnellschussNextShot", PhotonTargets.All);
                        photonView.RPC("RangerSkill1", PhotonTargets.MasterClient, transform.name, "Team", targetGO.transform.position,
                                 rangerAttributesScript.damageSkill1, targetGO.GetPhotonView().viewID, 0);
                        attackSkill1Active = false;
                        animationScript.startAttackAnimationOne();
                        //SOUND HIER schnellschuss großer schuss
						q1SkillPlayOnce = false;
						photonView.RPC ("PlayRangerSchnellSchuss3Sound", PhotonTargets.All, null);
                        revealMeRanger();
                        movementScript.stopMove();
                    }
                } else {
                    //zu ihm hin laufen
                    if (!movementScript.moving) {
                        movementScript.moveTowardsEnemy(targetGO, rangerAttributesScript.rangeSkill1);
                    }
                }
            }
        }


        //aufladeschuss
        if (attackSkill11Active) {
            //SOUND HIER chargeschuss aufladesound

			// _____________________________________________________

            //aufladen
            if (curRangeSkill11 < rangerAttributesScript.rangeSkill11) {
                curRangeSkill11 += (2.0f * Time.deltaTime);
            }else {
                photonView.RPC("RangerChargeAniDone", PhotonTargets.All);
                //attackSkill11Active = false;
            }

            if (curDamageSkill11 < rangerAttributesScript.damageSkill11) {
                curDamageSkill11 = (curRangeSkill11 / rangerAttributesScript.rangeSkill11 * rangerAttributesScript.damageSkill11);
            } else {
                curDamageSkill11 = rangerAttributesScript.damageSkill11;
            }
            //auflade animation
        }



        if (attackSkill2Active) {
            //shot 1 abgeschossen -> zeit laufen lassen bis 2 gefeuert werden muss -> cd sonst
        }

        if (attackSkill21Active) {
            //in range?
            if (Vector3.Distance(transform.position, clickPoint) < rangerAttributesScript.rangeSkill21) {
                if ((currentTrapCounter + 1) > 3) {
                    //letzte trap löschen
                    photonView.RPC("deleteTrap", PhotonTargets.MasterClient, traps[0].GetPhotonView().viewID);
                    //shuffle array
                    trapDestroyed(0);
                }


                //Objekt erstellen und alle sollen ihr objekt bearbeiten
                photonView.RPC("RangerSkill21", PhotonTargets.MasterClient, 0,
                    rangerAttributesScript.damageSkill21, rangerAttributesScript.slowTimeSkill21, currentTrapCounter, PhotonNetwork.player.ID, clickPoint);
                attackSkill21Active = false;
                cdTSkill21.StartCooldown();
                skillMode = false;
                //animation starten
                photonView.RPC("RangerTrapAni1", PhotonTargets.All, clickPoint);

                //SOUND HIER falle legen
				photonView.RPC ("PlayRangerFalleLegenSound", PhotonTargets.All, null);

				revealMeRanger();
                movementScript.stopMove();
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsPoint(clickPoint, rangerAttributesScript.rangeSkill21);
                }
            }

        }

    }

    public void disableAllSkills() {
        if (attackSkill1Active) {
            photonView.RPC("RangerStopSchnellschussAni", PhotonTargets.All);
            cdTSkill1.StartCooldown();
        }
        attackSkill1Active = false;

        if (attackSkill11Active) {
            photonView.RPC("RangerStopChargeAni", PhotonTargets.All);
            rangerAttributesScript.movementSpeed = oldMovmentSpeed;
            cdTSkill11.StartCooldown();
        }
        attackSkill11Active = false;

        if (attackSkill2Active) {
            //hooks zerstören falls verhanden
            if (GameObject.Find("RangerHook1") != null) {
                photonView.RPC("DestroyThis", PhotonTargets.MasterClient, GameObject.Find("RangerHook1").GetPhotonView().viewID);
            }
            cdTSkill2.StartCooldown();
        }
        attackSkill2Active = false;
    }

    public void interruptSkillsFromWalking() {
        if (attackSkill1Active) {
            photonView.RPC("RangerStopSchnellschussAni", PhotonTargets.All);
            cdTSkill1.StartCooldown();
        }
        attackSkill1Active = false;
    }

    [RPC]
    public void DestroyThis(int viewID) {
        if(PhotonView.Find(viewID) != null)
        PhotonNetwork.Destroy(PhotonView.Find(viewID));
    }

    public void trapDestroyed(int arrayNumber) {
        if (photonView.isMine) {

            currentTrapCounter--;
            if (arrayNumber == 0) {
                //counter bei traps selber runter setzen
                if (traps[1] != null) {
                    traps[1].GetComponent<Skill21Script>().arrayNumber--;
                }
                if (traps[2] != null) {
                    traps[2].GetComponent<Skill21Script>().arrayNumber--;
                }

                //shuffle array
                traps[0] = traps[1];
                traps[1] = traps[2];
                traps[2] = null;

            }

            if (arrayNumber == 1) {
                if (traps[2] != null) {
                    traps[2].GetComponent<Skill21Script>().arrayNumber--;
                }
                traps[1] = traps[2];
                traps[2] = null;

            }

            if (arrayNumber == 2) {
                traps[2] = null;
            }

        }
    }

    private void checkUpKeyPressed() {
        if (!offerForQSkill && qSkill == 0 && Input.GetKeyDown(KeyCode.Q) && !cdTSkill1.IsOnCooldown() && !cdTSkill11.IsOnCooldown()) {
			PlaySkillActivatedSound();

            skillMode = true;
            skillNumer = 1;
            disableAllSkills();
        }
        else if (qSkill == 1 && Input.GetKeyDown(KeyCode.Q) && !cdTSkill11.IsOnCooldown() && !attackSkill11Active) {
			PlaySkillActivatedSound();

            disableAllSkills();
            curRangeSkill11 = 0;
            curDamageSkill11 = 0;
            attackSkill11Active = true;
            //animation starten
            photonView.RPC("RangerStartChargeAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
            skillMode = true;
            skillNumer = 11;
              //langsamer machen
            oldMovmentSpeed = rangerAttributesScript.movementSpeed;
            rangerAttributesScript.movementSpeed = rangerAttributesScript.movementSpeed / 2;
        }
        else if (!offerForWSkill && wSkill == 0 && Input.GetKeyDown(KeyCode.W) && !cdTSkill2.IsOnCooldown() && !attackSkill2Active) {
			PlaySkillActivatedSound();

            disableAllSkills();
            skillMode = true;
            skillNumer = 2;
            skill2FirstShot = false;

        }
        else if(wSkill == 1 && Input.GetKeyDown(KeyCode.W) && !cdTSkill21.IsOnCooldown()){
			PlaySkillActivatedSound();

            disableAllSkills();
            skillMode = true;
            skillNumer = 21;
        }

        else if (!offerForESkill && eSkill == 0 && Input.GetKeyDown(KeyCode.E) && !cdTSkill3.IsOnCooldown()) {
			PlaySkillActivatedSound();

            disableAllSkills();
            skillMode = true;
            skillNumer = 3;
        } 
        else if (eSkill == 1 && Input.GetKeyDown(KeyCode.E) && !cdTSkill31.IsOnCooldown()) {
			PlaySkillActivatedSound();

            disableAllSkills();
            skillMode = true;
            skillNumer = 31;
        }
          
        
        
        
        
        
        
        
        //LÖSCHEN
        /*
        if (Input.GetKeyDown(KeyCode.A) && !cdTSkill11.IsOnCooldown()) {
			PlaySkillActivatedSound();
			photonView.RPC ("PlayRangerChargeSchussChargingSound", PhotonTargets.All, null);


            disableAllSkills();
            curRangeSkill11 = 0;
            curDamageSkill11 = 0;
            attackSkill11Active = true;
            //animation starten
            photonView.RPC("RangerStartChargeAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
            skillMode = true;
            skillNumer = 11;
              //langsamer machen
            oldMovmentSpeed = rangerAttributesScript.movementSpeed;
            rangerAttributesScript.movementSpeed = rangerAttributesScript.movementSpeed / 2;
        } else if (Input.GetKeyDown(KeyCode.S) && !cdTSkill21.IsOnCooldown()) {
			PlaySkillActivatedSound();

            disableAllSkills();
            skillMode = true;
            skillNumer = 21;
        } else if (Input.GetKeyDown(KeyCode.D) && !cdTSkill31.IsOnCooldown()) {
			PlaySkillActivatedSound();

            disableAllSkills();
            skillMode = true;
            skillNumer = 31;
        }
         */
    }

    public void mouseClickOnSkill(int skill) {
        if (!blockInput && rangerAttributesScript.isAlive && !iAmSilenced && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {

            mouseClickSkill = true;

            if (!offerForQSkill && qSkill == 0 && skill == 1 && !cdTSkill1.IsOnCooldown() && !cdTSkill11.IsOnCooldown()) {
                PlaySkillActivatedSound();

                skillMode = true;
                skillNumer = 1;
                disableAllSkills();
            }
            else if (qSkill == 1 && skill == 1 && !cdTSkill11.IsOnCooldown() && !attackSkill11Active) {
                PlaySkillActivatedSound();

                disableAllSkills();
                curRangeSkill11 = 0;
                curDamageSkill11 = 0;
                attackSkill11Active = true;
                //animation starten
                photonView.RPC("RangerStartChargeAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                skillMode = true;
                skillNumer = 11;
                //langsamer machen
                oldMovmentSpeed = rangerAttributesScript.movementSpeed;
                rangerAttributesScript.movementSpeed = rangerAttributesScript.movementSpeed / 2;
            }
            else if (!offerForWSkill && wSkill == 0 && skill == 2 && !cdTSkill2.IsOnCooldown() && !attackSkill2Active) {
                PlaySkillActivatedSound();

                disableAllSkills();
                skillMode = true;
                skillNumer = 2;
                skill2FirstShot = false;

            }
            else if (wSkill == 1 && skill == 2 && !cdTSkill21.IsOnCooldown()) {
                PlaySkillActivatedSound();

                disableAllSkills();
                skillMode = true;
                skillNumer = 21;
            }

            else if (!offerForESkill && eSkill == 0 && skill == 3 && !cdTSkill3.IsOnCooldown()) {
                PlaySkillActivatedSound();

                disableAllSkills();
                skillMode = true;
                skillNumer = 3;
            }
            else if (eSkill == 1 && skill == 3 && !cdTSkill31.IsOnCooldown()) {
                PlaySkillActivatedSound();

                disableAllSkills();
                skillMode = true;
                skillNumer = 31;
            }
        }
    }





    public int getQSkill()
    {
        return qSkill;
    }

    public int getWSkill()
    {
        return wSkill;
    }

    public int getESkill()
    {
        return eSkill;
    }


    [RPC]
    void RangerSkill11(int viewID, string originatorName, string team, Vector3 target, float damage, float range) {

        GameObject go;

        if (PhotonNetwork.isMasterClient) {
            go = PhotonNetwork.Instantiate("Ranger/Skills/Skill11", transform.position, transform.rotation, 0) as GameObject;
            photonView.RPC("RangerSkill11", PhotonTargets.Others, go.GetPhotonView().viewID, originatorName, team, target, damage, range);
        } else {
            go = PhotonView.Find(viewID).gameObject;
        }

        go.GetComponent<SkillScript1>().myOriginator = originatorName;
        go.GetComponent<SkillScript1>().target = target;
        go.GetComponent<SkillScript1>().team = team;
        go.GetComponent<SkillScript1>().range = range;
        go.GetComponent<SkillScript1>().damage = damage;
    }

    [RPC]
    void RangerSkill21(int viewID, float damage, float time, int arrayNumber, int rangerID, Vector3 clickP) {

        GameObject go;

        if (PhotonNetwork.isMasterClient) {
            go = PhotonNetwork.Instantiate("Ranger/Skills/Skill21", clickP, transform.rotation, 0) as GameObject;
            photonView.RPC("RangerSkill21", PhotonTargets.Others, go.GetPhotonView().viewID,
                    damage, time, arrayNumber, rangerID, clickP);
        } else {
            go = PhotonView.Find(viewID).gameObject;
        }

        if (photonView.isMine) {
            traps[currentTrapCounter] = go;
            currentTrapCounter++;
            cdTSkill21.StartCooldown();
        }
        go.GetComponent<Skill21Script>().damage = damage;
        go.GetComponent<Skill21Script>().slowTime = time;
        go.GetComponent<Skill21Script>().slowFaktor = 2.0f;
        go.GetComponent<Skill21Script>().arrayNumber = arrayNumber;
        go.GetComponent<Skill21Script>().rangerID = rangerID;
    }

    [RPC]
    void deleteTrap(int viewID) {
        PhotonNetwork.Destroy(PhotonView.Find(viewID).gameObject);
    }

    [RPC]
    void RangerSkill1(string originatorName, string team, Vector3 target, float attackDamage, int viewID, int goViewID) {
        GameObject go;

        if (PhotonNetwork.isMasterClient) {
            go = PhotonNetwork.Instantiate("Ranger/Skills/Skill1", transform.position, transform.rotation, 0) as GameObject;
            photonView.RPC("RangerSkill1", PhotonTargets.Others, originatorName, team, target,
                                attackDamage, viewID, go.GetPhotonView().viewID);

        } else {
            go = PhotonView.Find(goViewID).gameObject;
        }

        AutoAttackScript aAS = go.GetComponent<AutoAttackScript>();
        aAS.targetViewID = viewID;
        aAS.myOriginator = originatorName;
        aAS.target = target;
        aAS.team = team;
        aAS.damage = attackDamage;

    }

    [RPC]
    void RangerSkill2(string team, Vector3 target, float attackDamage, int shot1ViewID, int shot2ViewID, int shotNumber, float skillTime) {

        if (shotNumber == 1) {
            if (PhotonNetwork.isMasterClient) {
                skill2Shot1 = PhotonNetwork.Instantiate("Ranger/Skills/Skill2", transform.position, transform.rotation, 0) as GameObject;
                photonView.RPC("RangerSkill2", PhotonTargets.Others, team, target, attackDamage, skill2Shot1.GetPhotonView().viewID, 0, 1, skillTime);
            } else {
                skill2Shot1 = PhotonView.Find(shot1ViewID).gameObject;
            }

            Skill2Script aAS = skill2Shot1.GetComponent<Skill2Script>();
            aAS.target = target;
            aAS.team = team;
            aAS.damage = attackDamage;
            aAS.shotNumber = 1;
            aAS.rootTime = skillTime;

        }

        if (shotNumber == 2) {
            if (PhotonNetwork.isMasterClient) {
                skill2Shot2 = PhotonNetwork.Instantiate("Ranger/Skills/Skill2", transform.position, transform.rotation, 0) as GameObject;
                photonView.RPC("RangerSkill2", PhotonTargets.Others, team, target, attackDamage, shot1ViewID, skill2Shot2.GetPhotonView().viewID, 2, skillTime);
            } else {
                skill2Shot2 = PhotonView.Find(shot2ViewID).gameObject;

            }

            Skill2Script aAS = skill2Shot2.GetComponent<Skill2Script>();
            aAS.target = target;
            aAS.team = team;
            aAS.damage = attackDamage;
            aAS.shotNumber = 2;
            aAS.rootTime = skillTime;

            aAS.otherShotViewID = shot1ViewID;

            Skill2Script aAS1 = skill2Shot1.GetComponent<Skill2Script>();
            aAS1.otherShotViewID = skill2Shot2.GetPhotonView().viewID;

        }

        

    }

    private Vector3 calculateNewPos(Vector3 target, int direction, float length) {
        Vector3 newPos = target;

        if (direction == 0) {
            newPos.y += length;
        }
        else if (direction == 1) {
            newPos.x += length * Mathf.Cos(Mathf.Deg2Rad * 45);
            newPos.y += length * Mathf.Sin(Mathf.Deg2Rad * 45);
        }
        else if (direction == 2) {
            newPos.x += length;
        }
        else if (direction == 3) {
            newPos.x += length * Mathf.Cos(Mathf.Deg2Rad * 45);
            newPos.y -= length * Mathf.Sin(Mathf.Deg2Rad * 45);
        }
        else if (direction == 4) {
            newPos.y -= length;
        }
        else if (direction == 5) {
            newPos.x -= length * Mathf.Cos(Mathf.Deg2Rad * 45);
            newPos.y -= length * Mathf.Sin(Mathf.Deg2Rad * 45);
        }
        else if (direction == 6) {
            newPos.x -= length;
        }
        else if (direction == 7) {
            newPos.x -= length * Mathf.Cos(Mathf.Deg2Rad * 45);
            newPos.y += length * Mathf.Sin(Mathf.Deg2Rad * 45);
        }

        return newPos;
    }

    [RPC]
    void RangerTrapAni1(Vector3 pos) {
        Instantiate(rangerTrapAni1, pos, transform.rotation);
    }

    [RPC]
    void RangerStartChargeAni(int viewID) {
        Transform go = Instantiate(rangerChargeAni1, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        go.GetComponent<RangerChargeAni1Script>().target = PhotonView.Find(viewID).gameObject;
        //go.GetComponent<RangerChargeAni1Script>().startChargeTimer(3);
    }

    [RPC]
    void RangerChargeAniDone() {
        if(GameObject.Find("RangerChargeAni1") != null)
        GameObject.Find("RangerChargeAni1").GetComponent<RangerChargeAni1Script>().chargeDone();
    }

    [RPC]
    void RangerStopChargeAni() {
        Destroy(GameObject.Find("RangerChargeAni1"));
    }

    [RPC]
    void RangerStartSchnellschussAni(int viewID) {
        schnellschussAniTransform = Instantiate(rangerSchnellschussAni, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        schnellschussAniTransform.GetComponent<RangerSchnellschussScript>().target = PhotonView.Find(viewID).gameObject;
    }

    [RPC]
    void RangerStartSchnellschussNextShot() {
        schnellschussAniTransform.GetComponent<RangerSchnellschussScript>().nextShot();
    }

    [RPC]
    void RangerStopSchnellschussAni() {
        Destroy(GameObject.Find("RangerSchnellschussAni"));
    }

    [RPC]
    void RangerStartBackflipAni() {
        this.gameObject.GetComponent<RangerBackflipAni>().startAnimation();
    }

    public void setOldMovementSpeed() {
        rangerAttributesScript.movementSpeed = rangerAttributesScript.oldMovementSpeed;
        blockInput = false;
        rangerControlScript.blockInput = false;
    }

    [RPC]
    void RangerStartDashAni(int viewID) {

        this.GetComponent<RangerInvisibleAfterSkill>().startInvi();

        Transform go = Instantiate(dashAni1, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        go.GetComponent<RangerDashAni1>().target = PhotonView.Find(viewID).transform;

        Transform go2 = Instantiate(dashAni2, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        go2.GetComponent<RangerDashAni2>().target = PhotonView.Find(viewID).transform;
    }

    [RPC]
    void RangerStartHookCastAni(int viewID) {
        Transform go = Instantiate(hookAni, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        go.GetComponent<RangerHookCastAni>().target = PhotonView.Find(viewID).transform;
    }

    public void offerToChangeSkill(int number) {
        if (photonView.isMine) {
            if (number == 1) {
                offerForQSkill = true;
            }
            if (number == 2) {
                offerForWSkill = true;
            }
            if (number == 3) {
                offerForESkill = true;
            }
        }
    }

    public bool getOfferForQSkill()
    {
        return offerForQSkill;
    }

    public bool getOfferForWSkill()
    {
        return offerForWSkill;
    }
    public bool getOfferForESkill()
    {
        return offerForESkill;
    }

    public void ChangeQSkill()
    {
        PlaySkillActivatedSound();

        offerForQSkill = false;
        qSkill = 1;
    }
    public void ChangeWSkill()
    {
        PlaySkillActivatedSound();

        offerForWSkill = false;
        wSkill = 1;
    }
    public void ChangeESkill()
    {
        PlaySkillActivatedSound();

        offerForESkill = false;
        eSkill = 1;
    }

    public void improveQSkill()
    {
        PlaySkillActivatedSound();
        //Q Skill buff

        rangerAttributesScript.rangeSkill1 = rangerAttributesScript.rangeSkill1 + rangerAttributesScript.rangeSkill1 * 0.1f;
        rangerAttributesScript.damageSkill1 = rangerAttributesScript.damageSkill1 + rangerAttributesScript.damageSkill1 * 0.1f;

        offerForQSkill = false;
    }

    public void improveWSkill()
    {
        PlaySkillActivatedSound();

        rangerAttributesScript.rangeSkill2 = rangerAttributesScript.rangeSkill2 + rangerAttributesScript.rangeSkill2 * 0.1f;
        rangerAttributesScript.damageSkill2 = rangerAttributesScript.damageSkill2 + rangerAttributesScript.damageSkill2 * 0.1f;
        rangerAttributesScript.timeSkill2 = rangerAttributesScript.timeSkill2 + rangerAttributesScript.timeSkill2 * 0.1f;

        offerForWSkill = false;
    }
    public void improveESkill()
    {
        PlaySkillActivatedSound();

        rangerAttributesScript.rangeSkill3 = rangerAttributesScript.rangeSkill3 + rangerAttributesScript.rangeSkill3 * 0.1f;

        offerForESkill = false;
    }

    public void silenceMe(float time) {
        if (photonView.isMine) {
            iAmSilenced = true;
            silenceEnd = Time.time + time;
        }
    }

    public void increasePassivStack() {
        if (photonView.isMine) {
            passivStack++;
            cdPassivStack = Time.time + 3.0f;
        }
    }

    public void revealMeRanger() {
        if (GameObject.Find("Monster") != null)
            photonView.RPC("revealMeRangerRPC", GameObject.Find("Monster").GetPhotonView().owner);
    }


    [RPC]
    public void revealMeRangerRPC() {
        if (GameObject.Find("Monster") != null) {
            GameObject.Find("Monster").GetComponent<InviChecker>().revealRanger();
        }
    }

    // Sound Methods
    /*[RPC]
    public void PlayRangerAutoAttackSound()
    {
        asound.clip = sounds [1];
        asound.Play();
    } */
    [RPC]
    public void PlayRangerSchnellSchuss3Sound() 
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound(2,0.65f);
    }
    [RPC]
    public void PlayRangerSchnellSchussChargeSound() 
	{
        if (GetComponent<SpriteRenderer>().enabled)
        if (q1SkillPlayOnce == false) {
            PlaySound(3);
            q1SkillPlayOnce = true;
        }
    }
    [RPC]
    public void PlayRangerFalleLegenSound() 
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound(9);
    }
	[RPC]
	public void PlayRangerChargeSchussChargingSound() 
	{
		//if (q1SkillPlayOnce == false) {
		//PlaySound(8);
        if (GetComponent<SpriteRenderer>().enabled) {
            asound.clip = sounds[8];
            asound.volume = 1.00f * menuScript.scaleVolumeFactor;
            asound.Play();
        }
		//    q1SkillPlayOnce = true;
		//}
	}

    [RPC]
    public void PlayRangerChargeSchussSound() 
	{
        //if (q1SkillPlayOnce == false) {
            //PlaySound(2);
        if (GetComponent<SpriteRenderer>().enabled){
		asound.clip = sounds [2];
		asound.volume = 0.65f * menuScript.scaleVolumeFactor;
		asound.Play();
        }
        //    q1SkillPlayOnce = true;
        //}
    }
    [RPC]
    public void PlayRangerHook1Sound() 
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound(4);
    }
    [RPC]
    public void PlayRangerHook2Sound() 
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound(5);
    }
    public void PlayRangerDashSound() 
	{
        PlaySound(6);
    }
    [RPC]
    public void PlayRangerBackflipSound() 
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound(7);
    }

    private void PlaySkillActivatedSound() 
	{
        PlaySound(0,0.45f);
    }


	private void PlaySound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}

	private void PlaySound(int Number, float volume)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, volume*menuScript.scaleVolumeFactor);
	}

	private void PlayWinSound()
	{
		AudioSource.PlayClipAtPoint(winSound, transform.position, 0.75f*menuScript.scaleVolumeFactor);
	}
	
	private void PlayLoseSound()
	{
		AudioSource.PlayClipAtPoint(loseSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}

    public void showCircle(int which) {
        if (which == 1 && qSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (rangerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (rangerAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 1 && qSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (rangerAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (rangerAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 2 && wSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (rangerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (rangerAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 2 && wSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (rangerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (rangerAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 3 && eSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (rangerAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (rangerAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 3 && eSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (rangerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (rangerAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
    }
}

