﻿using UnityEngine;
using System.Collections;

public class RangerInvisibleAfterSkill : Photon.MonoBehaviour {

    public bool iAmInvisible;
    private bool inviTimerb;
    private float inviTimerf;
    public float inviTime;
    //private Color oldColor;
    //private Color newColor;

	// Use this for initialization
	void Start () {
        iAmInvisible = false;
        inviTimerb = false;
        inviTime = 2.0f;
        //oldColor = renderer.material.color;
        //newColor = renderer.material.color;
        //newColor.a = 0.6f;
	}
	
	// Update is called once per frame
	void Update () {

        if (inviTimerb && inviTimerf < Time.time) {
            iAmInvisible = false;
            inviTimerb = false;
            //renderer.material.color = oldColor;
        }
	}

    public void startInvi() {
        //renderer.material.color = newColor;
        iAmInvisible = true;
        inviTimerb = true;
        inviTimerf = Time.time + inviTime;
    }
}
