﻿using UnityEngine;
using System.Collections;

public class SkillScript1 : Photon.MonoBehaviour {


    public float projectileSpeed = 0.2f;
    public string team;
    public string myOriginator;
    private Vector3 richtungsVektor;
    public Vector3 target;
    private Vector3 startPosition;
    public float range;
    public float damage;
    public bool destroyed;

    public Transform rangerChargeExplosionAni;

    // Use this for initialization
    void Start() {
        richtungsVektor = (target - transform.position) / Vector3.Distance(target, transform.position);
        startPosition = transform.position;
        destroyed = false;

        Vector3 direction = target - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        angle += 180.0f;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

    }

    // Update is called once per frame
    void FixedUpdate() {


        if (!destroyed) {
            transform.position = transform.position + richtungsVektor * projectileSpeed;
            Vector3 bla = transform.position;
            bla.z = 2;
            transform.position = bla;

        } else {
            transform.renderer.enabled = false;
        }
    }

    void Update() {
        checkDestroyingYourself();
    }

    void checkDestroyingYourself() {

        if (PhotonNetwork.isMasterClient) {
            if (Vector3.Distance(startPosition, transform.position) > range) {
                PhotonNetwork.Destroy(this.gameObject);
            }
        }


    }


    void OnTriggerEnter(Collider collider) {



        if (team.Equals("Team") && collider.transform.tag.Equals("Monster")) {

            if (PhotonNetwork.isMasterClient) {
                //leben anpassen im master
                collider.gameObject.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(-damage);
                //print(damage);


                //zerstören
                if (!destroyed) {
                    photonView.RPC("RangerStartExplosionAni", PhotonTargets.All, collider.gameObject.transform.position);
                    PhotonNetwork.Destroy(this.gameObject);
                }
            } else {
                transform.renderer.enabled = false;
            }
            destroyed = true;
        }

        if (team.Equals("Monster") && collider.transform.tag.Equals("Team")) {
            if (PhotonNetwork.isMasterClient) {
                if (collider.gameObject.GetComponent<RangerAttributesScript>() != null) {
                    collider.gameObject.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, true);
                } else if (collider.gameObject.GetComponent<HealerAttributesScript>() != null) {
                    collider.gameObject.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, true);
                } else if (collider.gameObject.GetComponent<TankAttributesScript>() != null) {
                    collider.gameObject.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, true);
                }

                //zerstören
                if (!destroyed) {
                    photonView.RPC("RangerStartExplosionAni", PhotonTargets.All, collider.gameObject.transform.position);
                    PhotonNetwork.Destroy(this.gameObject);
                }
            } else {
                transform.renderer.enabled = false;
            }
            destroyed = true;

        }


    }

    void OnDestroy() {
        transform.renderer.enabled = false;
    }



    [RPC]
    void RangerStartExplosionAni(Vector3 pos) {
        Instantiate(rangerChargeExplosionAni, pos, transform.rotation);
    }



}


