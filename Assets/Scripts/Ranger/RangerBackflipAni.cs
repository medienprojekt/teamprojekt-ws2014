﻿using UnityEngine;
using System.Collections;

public class RangerBackflipAni : Photon.MonoBehaviour {

    private Color oldColor;
    private Color newColor;
    private bool activeB;

	// Use this for initialization
	void Start () {
        oldColor = this.GetComponent<SpriteRenderer>().material.color;
        newColor = new Color(0.5f, 0.5f, 0.5f, 0.0f);
        activeB = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (activeB) {
            newColor.a += 0.05f;
            this.GetComponent<SpriteRenderer>().color = newColor;
            renderer.material.color = newColor;

            if (newColor.a >= 1.0f) {
                this.GetComponent<SpriteRenderer>().color = oldColor;
                renderer.material.color = oldColor;
                activeB = false;
            }
        }
	}

    public void startAnimation() {
        activeB = true;
        newColor = new Color(0.5f, 0.5f, 0.5f, 0.0f);
        this.GetComponent<SpriteRenderer>().color = newColor;
        renderer.material.color = newColor;
    }
}
