﻿using UnityEngine;
using System.Collections;

public class Skill21Script : Photon.MonoBehaviour {

    public float damage;
    public float slowTime;
    public float slowFaktor;
    private bool destroyed;
    public int arrayNumber;
    public int rangerID;
    Color curColor;
    public Transform rangerTrapAni2;
    private Menu menu;

    public GameObject dizzyAni;

    // Use this for initialization
    void Start() {
        destroyed = false;
        curColor = renderer.material.color;
        curColor.a = 1.0f;
        menu = GameObject.Find("Menu").GetComponent<Menu>();
    }

    // Update is called once per frame
    void Update() {

        if (destroyed) {
            transform.renderer.enabled = false;
        }

    }


    void FixedUpdate() {
        //fade in & out
        if (curColor.a > 0.6f) {
            curColor.a -= 0.01f;
        }

        if (menu.iAmOnTeam == 2 && curColor.a <= 0.6f && curColor.a >= 0.0f) {
            curColor.a -= 0.01f;
        }
        renderer.material.color = curColor;
    }
    void OnTriggerEnter(Collider collider) {

        if (collider.transform.tag.Equals("Monster")) {

            if (PhotonNetwork.isMasterClient) {
                //leben anpassen im master
                collider.gameObject.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(-damage);
                //slow
                photonView.RPC("skill21SlowMonster", PhotonTargets.All, collider.gameObject.GetPhotonView().viewID,
                    damage, slowTime);

                //zerstören
                if (!destroyed) {
                    photonView.RPC("ArrayShuffle", PhotonTargets.All);
                    photonView.RPC("RangerTrapAni2", PhotonTargets.All, transform.position);
                    photonView.RPC("startStunAni", PhotonTargets.All, collider.gameObject.GetPhotonView().viewID, slowTime);
                    PhotonNetwork.Destroy(this.gameObject);
                }
            } else {
                transform.renderer.enabled = false;
            }
            destroyed = true;
        }
    }

    void OnDestroy() {
        transform.renderer.enabled = false;
    }

    [RPC]
    void ArrayShuffle() {
        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().trapDestroyed(arrayNumber);
    }

    [RPC]
    void skill21SlowMonster(int viewID, float damage, float time) {
        Transform trans = PhotonView.Find(viewID).transform;
        trans.GetComponent<MonsterControlScript>().makeMeDizzy(time);
    }

    [RPC]
    void RangerTrapAni2(Vector3 pos) {
        Instantiate(rangerTrapAni2, pos, transform.rotation);
    }

    [RPC]
    public void startStunAni(int viewID, float time) {
        GameObject go = Instantiate(dizzyAni, PhotonView.Find(viewID).transform.position, PhotonView.Find(viewID).transform.rotation) as GameObject;

        go.GetComponent<StunAniScript>().target = PhotonView.Find(viewID).gameObject;
        go.GetComponent<StunAniScript>().time = time + Time.time;
    }

}
