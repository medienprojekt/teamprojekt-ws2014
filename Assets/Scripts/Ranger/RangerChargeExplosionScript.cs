﻿using UnityEngine;
using System.Collections;

public class RangerChargeExplosionScript : Photon.MonoBehaviour {

    float time;
    Color curColor;

    // Use this for initialization
    void Start() {
        Vector3 pos = transform.position;
        pos.z = pos.z - 1;
        transform.position = pos;
        curColor = GetComponent<SpriteRenderer>().material.color;
        time = Time.time + 0.5f;
        curColor.a = 0.5f;
    }

    // Update is called once per frame
    void FixedUpdate() {
        //fade in & out
        if (curColor.a < 1.0f && Time.time < time - 0.3) {
            curColor.a += 0.05f;
        }
        if (Time.time > time - 0.2) {
            curColor.a -= 0.05f;
        }
        GetComponent<SpriteRenderer>().material.color = curColor;
    }

    void Update() {


        if (!GameObject.Find("Monster").renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }


        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }
}
