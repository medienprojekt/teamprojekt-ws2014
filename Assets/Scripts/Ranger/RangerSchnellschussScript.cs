﻿using UnityEngine;
using System.Collections;

public class RangerSchnellschussScript : Photon.MonoBehaviour {

    public GameObject target;
    Color curColor;
    private bool timerActive;
    private float destroyTime;
    private Animator animator;
    private int state;

    // Use this for initialization
    void Start() {
        name = "RangerSchnellschussAni";
        animator = this.GetComponent<Animator>();
        state = 0;
    }

    void Update() {


        if (!GameObject.Find("Ranger").renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }

        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.z -= 1;
            transform.position = pos;
        }

        if (timerActive && Time.time > destroyTime) {
            Destroy(this.gameObject);
        }
    }

    public void nextShot() {
        state++;
        if (animator == null) {
            animator = this.GetComponent<Animator>();
        }
        animator.SetInteger("State", state);

        if (state >= 3) {
            timerActive = true;
            destroyTime = Time.time + 0.8f;
        }
    }
}
