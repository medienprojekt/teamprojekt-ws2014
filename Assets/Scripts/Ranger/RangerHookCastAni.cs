﻿using UnityEngine;
using System.Collections;

public class RangerHookCastAni : Photon.MonoBehaviour {

    float time;
    Color curColor;
    public Transform target;

    // Use this for initialization
    void Start() {
        Vector3 pos = transform.position;
        pos.z = 1;
        transform.position = pos;
        curColor = renderer.material.color;
        time = Time.time + 0.5f;
        curColor.a = 0.5f;
    }

    // Update is called once per frame
    void FixedUpdate() {
        //fade in & out
        if (Time.time > time - 0.2) {
            curColor.a -= 0.05f;
        }
        renderer.material.color = curColor;
    }

    void Update() {


        if (!GameObject.Find("Ranger").renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }

        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.z = 1;
            transform.position = pos;
        }


        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }
}
