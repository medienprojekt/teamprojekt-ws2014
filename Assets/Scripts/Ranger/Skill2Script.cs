﻿using UnityEngine;
using System.Collections;

public class Skill2Script : Photon.MonoBehaviour {

    public Vector3 target;
    public string team;
    public float damage;
    public int shotNumber;
    private float speed = 10.0f;
    public bool hitEnemy;
    public int otherShotViewID;
    private GameObject hittedEnemy;
    private bool infoMonster;
    public float rootTime;
    private bool end;
    public bool disableCollider;
    private Animator animator;
    private float destroyTimer;

    private DrawLineScript drawLine;

    // Use this for initialization
    void Start() {
        hitEnemy = false;
        infoMonster = true;
        end = false;

        drawLine = GameObject.Find("LineRenderer2").GetComponent<DrawLineScript>();

        if (GameObject.Find("RangerHook1") == null) {
            name = "RangerHook1";
        }
        else {
            name = "RangerHook2";
        }

        Vector3 direction = target - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        angle += 180.0f;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        Vector3 newPos = transform.position;
        newPos.z = 3;
        transform.position = newPos;

        target.z = 3;

        animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {

        if (!hitEnemy) {
            float step = speed * Time.deltaTime;
            Vector3 moveTowards;
            if (Vector3.Distance(transform.position, target) >= 0.1f) {
                moveTowards = Vector3.MoveTowards(transform.position, target, step);
                transform.position = moveTowards;
            }
            else {
                transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
                animator.SetBool("Ground", true);
            }
            Vector3 newPos = transform.position;
            newPos.z = 3;
            transform.position = newPos;
        } else {
            transform.position = hittedEnemy.transform.position;
            animator.SetBool("Ready", true);
            Vector3 newPos = transform.position;
            newPos.z = 3;
            transform.position = newPos;
            if (otherShotViewID != 0) {
                PhotonView.Find(otherShotViewID).gameObject.GetComponent<Skill2Script>().hitEnemy = false;
                PhotonView.Find(otherShotViewID).gameObject.GetComponent<Skill2Script>().disableCollider = true;
            }
        }

        if (PhotonNetwork.isMasterClient && shotNumber == 2 && (hitEnemy || PhotonView.Find(otherShotViewID).gameObject.GetComponent<Skill2Script>().hitEnemy) && destroyTimer != 0 && Time.time > destroyTimer) {
            photonView.RPC("stopLineRenderer2", PhotonTargets.All);
            
            if (hittedEnemy != null) {
                PhotonView.Find(hittedEnemy.GetPhotonView().viewID).gameObject.GetComponent<MonsterControlScript>().noLongerHookedByRanger();
            }
            else {
                PhotonView.Find(PhotonView.Find(otherShotViewID).gameObject.GetComponent<Skill2Script>().hittedEnemy.GetPhotonView().viewID).gameObject.GetComponent<MonsterControlScript>().noLongerHookedByRanger();
            }

            PhotonNetwork.Destroy(PhotonView.Find(otherShotViewID));
            PhotonNetwork.Destroy(this.gameObject);
        }


        //ablauf des skills
        if (PhotonNetwork.isMasterClient && shotNumber == 2 && !end) {

            //check ob was getroffen
            if (hitEnemy || (PhotonView.Find(otherShotViewID).gameObject.GetComponent<Skill2Script>().hitEnemy && Vector3.Distance(transform.position, target) <= 0.2f)) {
                //Monster sagen das getroffen wurde
                if (hitEnemy) {
                    photonView.RPC("informMonster", PhotonTargets.All,
                        Vector3.Distance(transform.position, PhotonView.Find(otherShotViewID).gameObject.transform.position),
                        rootTime, PhotonView.Find(otherShotViewID).transform.position, hittedEnemy.GetPhotonView().viewID);
                } else {
                    photonView.RPC("informMonster", PhotonTargets.All,
                        Vector3.Distance(transform.position, PhotonView.Find(otherShotViewID).gameObject.transform.position),
                        rootTime, transform.position, PhotonView.Find(otherShotViewID).GetComponent<Skill2Script>().hittedEnemy.GetPhotonView().viewID);
                }
                //Timer starten
                destroyTimer = Time.time + rootTime;

                //line renderer starten bei allen
                photonView.RPC("startLineRenderer2", PhotonTargets.All, this.gameObject.GetPhotonView().viewID, otherShotViewID);

                end = true;
            }
            else if (Vector3.Distance(transform.position, target) <= 0.2f && !PhotonView.Find(otherShotViewID).gameObject.GetComponent<Skill2Script>().hitEnemy && !hitEnemy) {
                //nichts getroffen -> zerstören
                PhotonNetwork.Destroy(PhotonView.Find(otherShotViewID).gameObject);
                PhotonNetwork.Destroy(this.gameObject);
                end = true;
            }
        }

    }

    void OnTriggerEnter(Collider collider) {

        if (PhotonNetwork.isMasterClient) {
            if (!disableCollider && !hitEnemy && collider.transform.tag.Equals("Monster")) {
                hitEnemy = true;
                hittedEnemy = collider.gameObject;
                target = collider.transform.position;
                photonView.RPC("masterInfromOthersOnHit", PhotonTargets.Others, target, collider.gameObject.GetPhotonView().viewID);
            }
        }
    }

    [RPC]
    void masterInfromOthersOnHit(Vector3 pos, int viewID) {
        hitEnemy = true;
        target = pos;
        hittedEnemy = PhotonView.Find(viewID).gameObject;
    }


    void OnDestroy() {
        transform.renderer.enabled = false;
    }


    [RPC]
    void informMonster(float range, float rootTime, Vector3 pos, int viewID) {
        PhotonView.Find(viewID).gameObject.GetComponent<MonsterControlScript>().hitByRangerHook(range, rootTime, pos);
    }

                    
        
    [RPC]
    void startLineRenderer2(int viewID, int viewID2) {
        drawLine.startDrawLine(PhotonView.Find(viewID).transform, PhotonView.Find(viewID2).transform);
    }


    [RPC]
    void stopLineRenderer2() {
        drawLine.stopDrawLine();
    }
    
}
