﻿using UnityEngine;
using System.Collections;

public class RangerChargeAni1Script : Photon.MonoBehaviour {

    public GameObject target;
    private float chargeUpTime;
    Color curColor;
    private bool timerActive;
    private Animator animator;
    private Menu menu;

    // Use this for initialization
    void Start() {
        name = "RangerChargeAni1";
        animator = this.GetComponent<Animator>();
        curColor = renderer.material.color;
        curColor.a = 0.0f;
        menu = GameObject.Find("Menu").GetComponent<Menu>();
    }

    // Update is called once per frame
    void FixedUpdate() {
        //fade in & out
        if (curColor.a < GameObject.Find("Ranger").renderer.material.color.a) {
            curColor.a += 0.05f;
        }
        renderer.material.color = curColor;
    }

    void Update() {

        if (!GameObject.Find("Ranger").renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }


        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.z -= 1;
            transform.position = pos;

        }
    }

    public void chargeDone() {
        animator.SetBool("ChargeDone", true);
    }
}