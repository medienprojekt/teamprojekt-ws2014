﻿using UnityEngine;
using System.Collections;

public class RangerControlScript : Photon.MonoBehaviour {

	private Menu menuScript;

    public float speed = 1.5f;
    public Vector3 target;
    private GameObject multiplayerManger;
    public GameObject cam;
    public GameObject cursor;
    public CooldownTimer autoAttackCD;
    public GameObject autoAttackProjectileGO;
    public bool attackMode;
    private Vector3 distanceBetweenPlayerAndTarget;
    private bool runTowardsForAutoAttack;
    private RangerSkillControlScript rangerSkillControlScript;
    private Movement movementScript;
    private RangerAttributesScript rangerAttributesScript;
    private GameObject mouseHitbox;
    private MouseHitboxScript mouseHitboxScript;
    private GameObject targetGO;
    private bool walkToPortal;
    private GameObject walkToPortalGO;

    public bool isDizzy;
    private CooldownTimer dizzyTime;
    private float oldMovementSpeed;

    private AnimaitonScript animationScript;
    private Animator animator;

    public bool blockInput;

    public bool reanimateMode;
    private GameObject reanimateGO;

    public float reanimateCharge;

    public bool captureWard;
    private GameObject captrueWardGO;
    public float captureWardCharge;

    private bool attackAltar;
    private GameObject altarGO;

	public AudioClip[] sounds;
	public AudioSource asound;
	public AudioClip mouseClickSound;
	public AudioClip autoAttackSound;
	public AudioClip reanimationSound;
	private int counter1;

    public Texture tex1;
    public Texture tex2;

    public GameObject mouseClick;
    public GameObject reanimateAni;
    void Awake() {

		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];
		counter1 = 3;

        rangerSkillControlScript = transform.GetComponent<RangerSkillControlScript>();
        rangerAttributesScript = transform.GetComponent<RangerAttributesScript>();

        if (photonView.isMine) {
            animator = this.GetComponent<Animator>();

            target = transform.position;
            walkToPortal = false;
            autoAttackCD = new CooldownTimer(rangerAttributesScript.attackSpeed);
            attackMode = false;

            dizzyTime = new CooldownTimer(0);
            isDizzy = false;

            runTowardsForAutoAttack = false;


            movementScript = transform.GetComponent<Movement>();

            animationScript = transform.GetComponent<AnimaitonScript>();

            mouseHitbox = GameObject.Find("MouseHitbox");
            mouseHitboxScript = mouseHitbox.GetComponent<MouseHitboxScript>();


        } else {
            enabled = false;
        }

        Vector3 bla = transform.position;
        bla.z = 2;
        transform.position = bla;
        target = transform.position;


    }


    void Update() {
        if (!blockInput && rangerAttributesScript.isAlive && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked)
        {
            handleMouseInteractions();
            if (!rangerSkillControlScript.attackSkill1Active && !rangerSkillControlScript.attackSkill11Active) {
                autoattack();
            }
        }

        if (rangerAttributesScript.isAlive && attackAltar && altarGO != null && altarGO.GetComponent<AltarsScript>().health > 0) {
            //wenn in range und autoAttackMode an ist
            if (Vector3.Distance(transform.position, altarGO.transform.position) < rangerAttributesScript.attackRange+4.0f) {
                //gegner angreifen
                if (!autoAttackCD.IsOnCooldown()) {
                    animationScript.startAttackAnimation();
                    photonView.RPC("SpawnAutoAttackRangerMasterAltar", PhotonTargets.MasterClient, altarGO.GetPhotonView().viewID, 0);
                    autoAttackCD.StartCooldown();

                    //SOUND HIER autoattack
					PlayRPCRangerAutoAttackSound ();
                }

            }
            else {
                if (animationScript.isAttacking && !attackMode) {
                    animationScript.stopAttackAnimation();
                }
            }
        }

        if (rangerAttributesScript.isAlive && reanimateMode && Vector3.Distance(reanimateGO.transform.position, transform.position) <= 1.5f) {
            //animation
            reanimateCharge += 25*Time.deltaTime;

            if (reanimateCharge >= 100.0f) {
                if (reanimateGO.GetComponent<HealerAttributesScript>() != null) {
                    reanimateGO.GetComponent<HealerAttributesScript>().reanimateMe();
                    photonView.RPC("StartReanimateAniRanger", PhotonTargets.All, reanimateGO.transform.position);
                }
                else if (reanimateGO.GetComponent<TankAttributesScript>() != null) {
                    reanimateGO.GetComponent<TankAttributesScript>().reanimateMe();
                    photonView.RPC("StartReanimateAniRanger", PhotonTargets.All, reanimateGO.transform.position);
                }
                //SOUND HIER wiederbelebt
				PlayRPCRangerReanimationSound();

                reanimateCharge = 0.0f;
                reanimateMode = false;
            }
        }

        //ward
        if (rangerAttributesScript.isAlive && captureWard && Vector3.Distance(captrueWardGO.transform.position, transform.position) <= 2.0f) {
            captureWardCharge += 25 * Time.deltaTime;
            movementScript.stopMove();
            if (captureWardCharge >= 100.0f) {
                //ward captured
                photonView.RPC("captureWardRanger", PhotonTargets.All, captrueWardGO.GetPhotonView().viewID);
                captureWardCharge = 0.0f;
                captureWard = false;
                //SOUND HIER ward captured(steht im Ward selbst)
            }
        }

        //portal behandlung
        if (walkToPortalGO == null) {
            walkToPortal = false;
        }
        if (walkToPortal) {

            if (walkToPortalGO != null && Vector3.Distance(transform.position, walkToPortalGO.transform.position) < 2.0f) {

                if (walkToPortalGO.name.Equals("Portal1")) {
                    GameObject portal2 = GameObject.Find("Portal2");
                    Vector3 portalTarget = portal2.transform.position;
                    portalTarget.z = transform.position.z;
                    movementScript.iBlinked(portalTarget);
                } else if (walkToPortalGO.name.Equals("Portal2")) {
                    GameObject portal2 = GameObject.Find("Portal1");
                    Vector3 portalTarget = portal2.transform.position;
                    portalTarget.z = transform.position.z;
                    movementScript.iBlinked(portalTarget);
                }
                walkToPortal = false;
            }
        }

        //dizzy checkup
        if (isDizzy && !dizzyTime.IsOnCooldown()) {
            isDizzy = false;
            rangerAttributesScript.movementSpeed = oldMovementSpeed;
        }
    }

    void autoattack() {


        //wenn in range und autoAttackMode an ist
        if (targetGO != null && attackMode && Vector3.Distance(transform.position, targetGO.transform.position) < rangerAttributesScript.attackRange) {

            if (!targetGO.renderer.enabled) {
                targetGO = null;
                attackMode = false;
                movementScript.iMoved(transform.position);
                return;
            }
            
            
            //gegner angreifen
            if (!autoAttackCD.IsOnCooldown()) {
                animationScript.startAttackAnimation();

                float dmg = rangerAttributesScript.attackDamage;
                //damage berechnen falls passiv
                if (rangerSkillControlScript.passivActive) {
                    dmg = dmg + ((dmg / 8) * rangerSkillControlScript.passivStack);
                }
                photonView.RPC("SpawnAutoAttackRangerMaster", PhotonTargets.MasterClient, transform.name, "Team", targetGO.transform.position, dmg, targetGO.GetPhotonView().viewID);
                autoAttackCD.StartCooldown();

                rangerSkillControlScript.revealMeRanger();

                //SOUND HIER autoattack
				PlayRPCRangerAutoAttackSound ();
            }

        }
        else {
            if (animationScript.isAttacking && !attackAltar) {
                animationScript.stopAttackAnimation();
            }
        }
    }

    float native_width = 1920;
    float native_height = 1080;

    void handleMouseInteractions() {
        //handle mouse interactions
        //right click

        float rx = Screen.width / native_width;
        float ry = Screen.height / native_height;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

        Vector3 mpos;
        mpos = GameObject.Find("Minimap").GetComponent<Map>().NormalizedMousePosition;


        if (Input.GetMouseButtonDown(1) && !GameObject.Find("Minimap").GetComponent<Map>().mouseOnMap(mpos.x, mpos.y))
        {
			PlayMouseClickSound();

            walkToPortal = false;
            reanimateMode = false;
            captureWard = false;
            attackAltar = false;

            rangerSkillControlScript.interruptSkillsFromWalking();

            if (!rangerSkillControlScript.attackSkill11Active && !rangerSkillControlScript.attackSkill2Active) {
                rangerSkillControlScript.skillMode = false;
            }

            //run or attack or other stuff...
            //run
            if (!mouseHitboxScript.mouseOverEnemy && !mouseHitboxScript.mouseOverTeammate && !mouseHitboxScript.mouseOverPortal && !mouseHitboxScript.mouseOverWard && !mouseHitboxScript.mouseOverAltar) {
                //maus portion holen
                target.x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
                target.y = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
                target.z = transform.position.z;
                attackMode = false;
                //movementScript bescheid sagen
                movementScript.iMoved(target);

                //SOUND HIER bewegungsbestätigung (lol - ezreal - over here! :D )
				PlayMaybeMoveVoiceSound ();

                Instantiate(mouseClick, target, transform.rotation);
            }
            //attack
            else if (mouseHitboxScript.mouseOverEnemy) {
                if (mouseHitboxScript.mouseOverLastEnemyGO != null) {
                    attackMode = true;
                    targetGO = mouseHitboxScript.mouseOverLastEnemyGO;
                    //laufe zum target hin
                    movementScript.moveTowardsEnemy(mouseHitboxScript.mouseOverLastEnemyGO, rangerAttributesScript.attackRange);

                    //SOUND HIER angriffsbestätigung
					PlayAttackVoiceSound ();
                }
            }

            //teammate wiederbeleben
            else if (mouseHitboxScript.mouseOverTeammate) {
                if (mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<HealerAttributesScript>() != null &&
                    !mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<HealerAttributesScript>().isAlive) {
                        movementScript.moveTowardsPoint(mouseHitboxScript.mouseOverLastTeammateGO.transform.position, 1.0f);
                    reanimateMode = true;
                    reanimateGO = mouseHitboxScript.mouseOverLastTeammateGO;
                    reanimateCharge = 0;

                    //SOUND HIER bewegungsbestätigung
					PlayMoveVoiceSound ();
                }
                else if (mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<TankAttributesScript>() != null &&
                    !mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<TankAttributesScript>().isAlive) {
                        movementScript.moveTowardsPoint(mouseHitboxScript.mouseOverLastTeammateGO.transform.position, 1.0f);
                    reanimateMode = true;
                    reanimateGO = mouseHitboxScript.mouseOverLastTeammateGO;
                    reanimateCharge = 0;

                    //SOUND HIER bewegungsbestätigung
					PlayMoveVoiceSound ();
                }
            }


            //auf portal geklickt
            else if (mouseHitboxScript.mouseOverPortal) {
                walkToPortal = true;
                walkToPortalGO = mouseHitboxScript.mouseOverPortalGO;
                movementScript.moveTowardsEnemy(mouseHitboxScript.mouseOverPortalGO, 1.8f);
            }


            //ward
            else if (mouseHitboxScript.mouseOverWard && mouseHitboxScript.mouseOverWardGO.GetComponent<WardScript>().iAmCapturedByTeam != 1) {
                captureWard = true;
                movementScript.moveTowardsPoint(mouseHitboxScript.mouseOverWardGO.transform.position, 0.1f);
                captureWardCharge = 0.0f;
                captrueWardGO = mouseHitboxScript.mouseOverWardGO;

                //SOUND HIER bewegungsbestätigung
				PlayMoveVoiceSound ();
            }


            //altar
            else if (mouseHitboxScript.mouseOverAltar) {
                attackAltar = true;
                movementScript.moveTowardsEnemy(mouseHitboxScript.mouseOverAltarGO, rangerAttributesScript.attackRange);
                altarGO = mouseHitboxScript.mouseOverAltarGO;

                //SOUND HIER bewegungsbestätigung
				PlayAttackVoiceSound ();
            }

        }
    }

    public void spawnAutoAttackFromSkill(GameObject aTarget) {
        photonView.RPC("SpawnAutoAttackRangerMaster", PhotonTargets.MasterClient,
            transform.name, "Team", aTarget.transform.position, rangerAttributesScript.attackDamage, aTarget.GetPhotonView().viewID);
    }

    [RPC]
    void SpawnAutoAttackRangerMaster(string originatorName, string team, Vector3 target, float attackDamage, int viewID) {
        GameObject go = PhotonNetwork.Instantiate("Ranger/RangerAutoAttackPrefab", transform.position, transform.rotation, 0) as GameObject;
        photonView.RPC("SpawnAutoAttackRanger", PhotonTargets.All, originatorName, team, target, attackDamage, viewID, go.GetPhotonView().viewID);
    }

    [RPC]
    void SpawnAutoAttackRanger(string originatorName, string team, Vector3 target, float attackDamage, int viewID, int viewIDGO) {
        GameObject go = PhotonView.Find(viewIDGO).gameObject;

        AutoAttackScript aAS = go.GetComponent<AutoAttackScript>();
        aAS.targetViewID = viewID;
        aAS.myOriginator = originatorName;
        aAS.target = target;
        aAS.team = team;
        aAS.damage = attackDamage;
        aAS.rangerStack = true;
    }


    public void makeMeDizzy(float time, float slowFactor) {
        if (photonView.isMine) {
            isDizzy = true;
            dizzyTime = new CooldownTimer(time);
            dizzyTime.StartCooldown();
            oldMovementSpeed = rangerAttributesScript.oldMovementSpeed;
            rangerAttributesScript.movementSpeed = rangerAttributesScript.oldMovementSpeed * slowFactor;
        }
    }

    void OnGUI() {

        float rx = Screen.width / native_width;
        float ry = Screen.height / native_height;
        GL.PushMatrix();
        GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));


        if (reanimateMode && Vector3.Distance(reanimateGO.transform.position, transform.position) <= 1.5f) {
            GUI.DrawTexture(new Rect(1920 / 2 - tex1.width / 2, 900 - tex1.height, tex1.width, tex1.height), tex1);

            GUI.BeginGroup(new Rect(1920 / 2 - tex2.width / 2, 900 - tex2.height, tex2.width * (reanimateCharge / 100), tex2.height));
            GUI.DrawTexture(new Rect(0, 0, tex2.width, tex2.height), tex2);
            GUI.EndGroup();
        }

        //ward
        if (captureWard && Vector3.Distance(captrueWardGO.transform.position, transform.position) <= 2.0f) {
            GUI.DrawTexture(new Rect(1920 / 2 - tex1.width / 2, 900 - tex1.height, tex1.width, tex1.height), tex1);

            GUI.BeginGroup(new Rect(1920 / 2 - tex2.width / 2, 900 - tex2.height, tex2.width * (captureWardCharge / 100), tex2.height));
            GUI.DrawTexture(new Rect(0, 0, tex2.width, tex2.height), tex2);
            GUI.EndGroup();
        }




        GL.PopMatrix();

    }

    [RPC]
    private void captureWardRanger(int viewID) {
        Transform trans = PhotonView.Find(viewID).transform;
        trans.GetComponent<WardScript>().iAmCapturedByTeam = 1;
    }


    
    [RPC]
    void SpawnAutoAttackRangerMasterAltar(int viewIDGO, int viewID) {
        GameObject go = null;
        if (PhotonNetwork.isMasterClient) {
            go = PhotonNetwork.Instantiate("Ranger/RangerAutoAttackPrefab", transform.position, transform.rotation, 0) as GameObject;
            photonView.RPC("SpawnAutoAttackRangerMasterAltar", PhotonTargets.Others, viewIDGO, go.GetPhotonView().viewID);
        }
        else {
            if (PhotonView.Find(viewID) != null)
            go = PhotonView.Find(viewID).gameObject;
        }
        if (go != null) {
            AutoAttackScript aAS = go.GetComponent<AutoAttackScript>();
            aAS.targetViewID = viewIDGO;
            aAS.target = target;
            aAS.altar = true;
            aAS.damage = 10;
        }
    }

	//Sound Methods
	[RPC]
	public void PlayRangerAutoAttackSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        AudioSource.PlayClipAtPoint(autoAttackSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	public void PlayRPCRangerAutoAttackSound()
	{
		photonView.RPC ("PlayRangerAutoAttackSound", PhotonTargets.All, null);
	}
	
	[RPC]
	public void PlayRangerReanimationSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        AudioSource.PlayClipAtPoint(reanimationSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	public void PlayRPCRangerReanimationSound()
	{
		photonView.RPC ("PlayRangerReanimationSound", PhotonTargets.All, null);
	}
	
	public void PlayMouseClickSound()
	{
		AudioSource.PlayClipAtPoint(mouseClickSound, transform.position, 0.12f*menuScript.scaleVolumeFactor);
	}
	
	private void PlaySound(int Number)
	{
		asound.clip = sounds[Number];
		asound.volume = 0.3f*menuScript.scaleVolumeFactor;
		asound.Play ();
	}
	
	private void PlayMaybeMoveVoiceSound()
	{
		if(counter1 == 6)
		{
			int r = Random.Range(0,3);
			PlaySound (r);
			counter1 = 0;
		}
		
		counter1++;
	}
	
	private void PlayMoveVoiceSound()
	{
		int r = Random.Range(0,3);
		PlaySound (r);
	}
	
	private void PlayAttackVoiceSound()
	{
		int r = Random.Range(3,6);
		PlaySound (r);
	}


    [RPC]
    public void StartReanimateAniRanger(Vector3 pos) {
        Instantiate(reanimateAni, pos, transform.rotation);
    }

    public void lostMonsterSight() {
        if (targetGO != null) {
            targetGO = null;
            attackMode = false;
            movementScript.lostMonsterSight();
        }
    }
}

