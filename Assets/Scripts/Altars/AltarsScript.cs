﻿using UnityEngine;
using System.Collections;

public class AltarsScript : Photon.MonoBehaviour{

	private Menu menuScript;

	private Vector3 richtungsVektor;
	public Vector3 target;
	public int health = 200;
    private int maxHealth;

	public int damagePerSecond;
	private float damageCoolDown;
	private float damageCoolDownTimeRanger;
	private float damageCoolDownTimeHealer;
	private float damageCoolDownTimeTank;

	public bool rangerInRange;
	public bool healerInRange;
	public bool tankInRange;

    public int kindOfAltar;

    private bool buffDone;

	public AudioClip[] sounds;
	public AudioSource asound;
	public AudioClip destroySound;

    public Animator animator;

    public GameObject altarExplosionAni;
    public GameObject altarBuffAni;

	// Use this for initialization
	void Start () {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];

        animator = this.GetComponent<Animator>();

        if (kindOfAltar == 1) {
            animator.SetInteger("Color", 1);
        }else  if (kindOfAltar == 2) {
            animator.SetInteger("Color", 2);
        }else  if (kindOfAltar == 3) {
            animator.SetInteger("Color", 0);
        }else  if (kindOfAltar == 4) {
            animator.SetInteger("Color", 3);
        }

        maxHealth = health;
	}

	void PlaySound (int Number, float pitch)
	{
		asound.clip = sounds[Number];
		asound.pitch = pitch;
		asound.volume = 0.60f * menuScript.scaleVolumeFactor;
		asound.Play ();
	}

	void PlaySound (int Number)
	{
		asound.clip = sounds[Number];
		asound.pitch = 1.0f;
		asound.volume = 1.0f * menuScript.scaleVolumeFactor;
		asound.Play ();
	}

	void PlayDestroySound()
	{
		//PlaySound (1);
		AudioSource.PlayClipAtPoint(destroySound, transform.position, 0.75f*menuScript.scaleVolumeFactor);
	}

	void GrantBuff(){
		//Kaputtsound


        if (kindOfAltar == 1) {
            photonView.RPC("buffStats", PhotonTargets.All, 0.05f);
            photonView.RPC("offerToChangeQSkill", PhotonTargets.All);
        }

        if (kindOfAltar == 2) {
            photonView.RPC("buffStats", PhotonTargets.All, 0.05f);
            photonView.RPC("offerToChangeWSkill", PhotonTargets.All);
        }

        if (kindOfAltar == 3) {
            photonView.RPC("buffStats", PhotonTargets.All, 0.05f);
            photonView.RPC("offerToChangeESkill", PhotonTargets.All);
        }

        if (kindOfAltar == 4) {
            photonView.RPC("buffStats", PhotonTargets.All, 0.05f);
            photonView.RPC("givePassivSkills", PhotonTargets.All);
        }



	}

    [RPC]
    void buffStats(float percent) {

        Vector3 v = transform.position;

        if (kindOfAltar == 4) {
            v.x += 0.2f;
        }
        else if (kindOfAltar == 2) {
            v.x += 0.2f;
        }


        //animationen starten
        Instantiate(altarExplosionAni, v, transform.rotation);



        if (menuScript.iAm == "Healer" && Vector3.Distance(GameObject.Find("Healer").transform.position, transform.position) <= 13.0f) {
            PlayDestroySound();
        }
        else if (menuScript.iAm == "Ranger" && Vector3.Distance(GameObject.Find("Ranger").transform.position, transform.position) <= 13.0f) {
            PlayDestroySound();
        }
        else if (menuScript.iAm == "Tank" && Vector3.Distance(GameObject.Find("Tank").transform.position, transform.position) <= 13.0f) {
            PlayDestroySound();
        }
        else if (menuScript.iAm == "Monster" && Vector3.Distance(GameObject.Find("Monster").transform.position, transform.position) <= 13.0f) {
            PlayDestroySound();
        }


        if (GameObject.Find("Ranger") != null) {
            GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().buffMe(percent);

            Vector3 v3 = GameObject.Find("Ranger").transform.position;
            v3.y += 0.3f;

            GameObject go =  Instantiate(altarBuffAni, v3, GameObject.Find("Ranger").transform.rotation) as GameObject;
            go.GetComponent<AltarBuffScript>().target = GameObject.Find("Ranger");


			//Buffsound
        }
        if (GameObject.Find("Healer") != null) {
            GameObject.Find("Healer").GetComponent<HealerAttributesScript>().buffMe(percent);
            Vector3 v3 = GameObject.Find("Healer").transform.position;
            v3.y += 0.3f;
            GameObject go = Instantiate(altarBuffAni, v3, GameObject.Find("Healer").transform.rotation) as GameObject;
            go.GetComponent<AltarBuffScript>().target = GameObject.Find("Healer");
			//Buffsound
        } 
        if (GameObject.Find("Tank") != null) {
            GameObject.Find("Tank").GetComponent<TankAttributesScript>().buffMe(percent);
            Vector3 v3 = GameObject.Find("Tank").transform.position;
            v3.y += 0.3f;
            GameObject go = Instantiate(altarBuffAni, v3, GameObject.Find("Tank").transform.rotation) as GameObject;
            go.GetComponent<AltarBuffScript>().target = GameObject.Find("Tank");

			//Buffsound
        }

    }




	// Update is called once per frame
	void Update () {

        //animation
        if (animator.GetInteger("Life") != 1 && health <= maxHealth / 2 && health > 0) {
            animator.SetInteger("Life", 1);
        }
        else if (animator.GetInteger("Life") != 2 && health <= 0) {
            animator.SetInteger("Life", 2);
        }else if(animator.GetInteger("Life") != 0 && health == maxHealth){
            animator.SetInteger("Life", 0);
        }



        if (PhotonNetwork.isMasterClient && health <= 0 && !buffDone) {
            buffDone = true;
            GrantBuff();
		}
	}

    public void getDamage() {
		//Hitsound
		float r = Random.Range (1.0f, 1.4f);
		PlaySound (0, r);

        health -= 5;

        //wenn vom master gecallt
        if (PhotonNetwork.isMasterClient) {
            //bei allen anderen das leben anpassen
            photonView.RPC("makeDmgAltar", PhotonTargets.Others);
        }
    }

    [RPC]
    void makeDmgAltar() {
        gameObject.GetComponent<AltarsScript>().getDamage();
    }

    
    [RPC]
    void offerToChangeQSkill() {
        if (GameObject.Find("Ranger") != null) {
            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().offerToChangeSkill(1);
        }
        if (GameObject.Find("Healer") != null) {
            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().offerToChangeSkill(1);
        }
        if (GameObject.Find("Tank") != null) {
            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().offerToChangeSkill(1);
        }
    }


    [RPC]
    void offerToChangeWSkill() {
        if (GameObject.Find("Ranger") != null) {
            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().offerToChangeSkill(2);
        }
        if (GameObject.Find("Healer") != null) {
            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().offerToChangeSkill(2);
        }
        if (GameObject.Find("Tank") != null) {
            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().offerToChangeSkill(2);
        }
    }

    [RPC]
    void offerToChangeESkill() {
        if (GameObject.Find("Ranger") != null) {
            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().offerToChangeSkill(3);
        }
        if (GameObject.Find("Healer") != null) {
            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().offerToChangeSkill(3);
        }
        if (GameObject.Find("Tank") != null) {
            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().offerToChangeSkill(3);
        }
    }

    [RPC]
    void givePassivSkills() {
        if (GameObject.Find("Ranger") != null) {
            GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().passivActive = true;
        }
        if (GameObject.Find("Healer") != null) {
            GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().passivActive = true;
        }
        if (GameObject.Find("Tank") != null) {
            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().passivActive = true;
        }
    }

    public void reset() {
        health = maxHealth;
        buffDone = false;
    }
    
}
