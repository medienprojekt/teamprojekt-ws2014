﻿using UnityEngine;
using System.Collections;

public class AltarBuffScript : MonoBehaviour {

    public GameObject target;
    private float time;

    // Use this for initialization
    void Start() {
        time = Time.time + 0.84f;
    }

    // Update is called once per frame
    void Update() {

        if (target != null) {
            Vector3 v3 = target.transform.position;
            v3.y += 1.2f;

            transform.position = v3;
        }
        if (Time.time > time) {
            Destroy(this.gameObject);

        }


    }
}

