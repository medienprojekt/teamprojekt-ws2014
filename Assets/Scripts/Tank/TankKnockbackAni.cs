﻿using UnityEngine;
using System.Collections;

public class TankKnockbackAni : Photon.MonoBehaviour {

    public GameObject target;
    float time;
    Color curColor;

    // Use this for initialization
    void Start() {
        curColor = renderer.material.color;
        time = Time.time + 0.5f;
        curColor.a = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate() {
        //fade in & out
        if (curColor.a < 1.0f && Time.time < time - 0.3) {
            curColor.a += 0.05f;
        }
        if (Time.time > time - 0.2) {
            curColor.a -= 0.05f;
        }
        renderer.material.color = curColor;
    }

    void Update() {
        if (target != null) {
            transform.position = target.transform.position;
        }
        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }
}
