﻿using UnityEngine;
using System.Collections;

public class tank_warcry_script : Photon.MonoBehaviour {

    public GameObject target;
    float time;
    Color curColor;

    // Use this for initialization
    void Start() {
        curColor = renderer.material.color;
        time = Time.time + 0.7f;
        curColor.a = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate() {
        //fade in & out
        if (curColor.a < 1.0f && Time.time < time - 0.3) {
            curColor.a += 0.05f;
        }
        if (Time.time > time - 0.2) {
            curColor.a -= 0.05f;
        }
        renderer.material.color = curColor;
    }

    void Update() {

        if (!target.renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }

        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.z = 1;
            transform.position = pos;
        }
        if (Time.time > time) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }
}
