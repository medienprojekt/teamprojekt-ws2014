﻿using UnityEngine;
using System.Collections;

public class tank_charge_script : Photon.MonoBehaviour {


    public GameObject target;

    // Use this for initialization
    void Start() {
        name = "TankChargeAni";
    }

    // Update is called once per frame
    void FixedUpdate() {
        
    }

    void Update() {

        if (!target.renderer.enabled) {
            this.renderer.enabled = false;

        }
        else {
            this.renderer.enabled = true;
        }

        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.z = 1;
            transform.position = pos;
        }
    }

}
