﻿using UnityEngine;
using System.Collections;

public class TankBarriereScript : Photon.MonoBehaviour {

    public int direction;
    private Animator animator;
    public float destroyTimer;
    public Vector3 offset;
    public int tankViewID;
    private bool destroyed;

	// Use this for initialization
	void Start () {
        name = "TankBarriere";
        animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        //position
        Vector3 pos = PhotonView.Find(tankViewID).transform.position + offset;

        if (direction == 1 || direction == 2) {
            pos.z = 3;

        }else if(direction == 3 || direction == 4) {
            pos.z = 1;
        }

        transform.position = pos;


        if (direction != 0 && animator.GetInteger("direction") == 0) {
            animator.SetInteger("direction", direction);
        }


        if (!destroyed && PhotonNetwork.isMasterClient && destroyTimer != 0 && Time.time >= destroyTimer) {
            GameObject.Find("Tank").GetComponent<TankSkillControlScript>().resetAttackSkill2();
            photonView.RPC("DestroyTankBarriere", PhotonTargets.MasterClient, this.gameObject.GetPhotonView().viewID);
            destroyed = true;
        }
	}

    public void destroyInstant() {
        photonView.RPC("DestroyTankBarriere", PhotonTargets.MasterClient, this.gameObject.GetPhotonView().viewID);
    }

    [RPC]
    public void DestroyTankBarriere(int viewID) {
        PhotonNetwork.Destroy(PhotonView.Find(viewID));
    }

    

    void OnTriggerEnter(Collider coll) {
        bool dest = false;
        if (PhotonNetwork.isMasterClient) {
            if (coll.tag == "MonsterSkill") {
                if (coll.gameObject != null) {
                    if (!dest) {
                        PhotonNetwork.Destroy(coll.gameObject);
                        dest = true;
                    }
                }
            }
        }
    }

}
