﻿using UnityEngine;
using System.Collections;

public class tank_taunt_script2 : Photon.MonoBehaviour {

    public GameObject target;
    Color curColor;
    bool kill;
    float killTime;

    // Use this for initialization
    void Start() {
        name = "TankTauntAni";
        curColor = renderer.material.color;
        curColor.a = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (curColor.a > 0.0f && kill) {
            curColor.a -= 0.1f;
            renderer.material.color = curColor;
        }

    }

    void Update() {
        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.z = 1;
            transform.position = pos;
        }

        if (kill && Time.time > killTime) {
            transform.renderer.enabled = false;
            Destroy(this.gameObject);
        }
    }

    public void killMe() {
        kill = true;
        killTime = Time.time + 0.1f;
    }
}
