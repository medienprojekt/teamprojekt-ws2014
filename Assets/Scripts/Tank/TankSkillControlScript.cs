﻿using UnityEngine;
using System.Collections;

public class TankSkillControlScript : Photon.MonoBehaviour {

	private Menu menuScript;

    private TankControlScript tankControlScript;
    private TankAttributesScript tankAttributesScript;
    private GameObject mouseHitbox;
    private MouseHitboxScript mouseHitboxScript;
    private Movement movementScript;

    private int skillNumer; //0 nichts, 1-4 q-r
    public GameObject skill1Prefab;
    public bool skillMode;
    public GameObject skillCirclePrefab;
    private GameObject skillCircle;
    private GameObject curTarget;

    public bool attackSkill1Active;
    public bool attackSkill11Active;
    public bool attackSkill21Active;
    public bool attackSkill3Active;
    public Vector3 pointToMoveSkill3;
    public float oldMovmentSpeed;

    public CooldownTimer cdTSkill1;
    public CooldownTimer cdTSkill2;
    public CooldownTimer cdTSkill3;
    public CooldownTimer cdTSkill11;
    public CooldownTimer cdTSkill21;
    public CooldownTimer cdTSkill31;

    public Transform warcryAni;
    public Transform chargeAni;
    private bool currentCharging;
    public Transform tauntAni1;
    public Transform tauntAni2;

    public Transform bashAni1;
    public Transform bashAni2;
    public Transform bashAni3;
    public Transform bashAni4;
    public Transform bashHit;

    private Animator animator;

    public Transform knockbackAni;

    private bool offerForQSkill;
    private bool offerForWSkill;
    private bool offerForESkill;

    private int qSkill;
    private int wSkill;
    private int eSkill;

    public bool iAmSilenced;
    private float silenceEnd;

    public bool passivActive;

    public bool attackSkill2Active;

	public AudioClip[] sounds;
	public AudioSource asound;
	public AudioClip winSound;
	public AudioClip loseSound;
	private bool winSoundPlayed;
	private bool loseSoundPlayed;

    public GameObject stunAni;

    private AnimaitonScript animationScript;

    public bool hoverQ;
    public bool hoverW;
    public bool hoverE;

    private bool mouseClickSkill;

    // Use this for initialization
    void Start() {

		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];
		winSoundPlayed = false;
		loseSoundPlayed = false;

        animationScript = GetComponent<AnimaitonScript>();

        if (photonView.isMine) {
            tankControlScript = transform.GetComponent<TankControlScript>();
            tankAttributesScript = transform.GetComponent<TankAttributesScript>();

            skillCircle = Instantiate(skillCirclePrefab, transform.position, transform.rotation) as GameObject;
            skillCircle.transform.parent = transform;
            Vector3 sCPos = new Vector3(transform.position.x, transform.position.y, 3);
            skillCircle.transform.position = sCPos;
            skillCircle.renderer.enabled = false;

            animator = transform.GetComponent<Animator>();

            skillMode = false;
            skillNumer = 0;

            movementScript = transform.GetComponent<Movement>();

            mouseHitbox = GameObject.Find("MouseHitbox");
            mouseHitboxScript = mouseHitbox.GetComponent<MouseHitboxScript>();


            cdTSkill1 = new CooldownTimer(tankAttributesScript.cdSkill1);
            cdTSkill2 = new CooldownTimer(tankAttributesScript.cdSkill2);
            cdTSkill3 = new CooldownTimer(tankAttributesScript.cdSkill3);
            cdTSkill11 = new CooldownTimer(tankAttributesScript.cdSkill11);
            cdTSkill21 = new CooldownTimer(tankAttributesScript.cdSkill21);
            cdTSkill31 = new CooldownTimer(tankAttributesScript.cdSkill31);
        } else {
            enabled = false;
        }


    }

    // Update is called once per frame
    void Update() {
        if (!movementScript.charge && tankAttributesScript.isAlive && !iAmSilenced && !attackSkill2Active && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked){
            checkUpKeyPressed();
        }

        skillActive();
        if (!hoverQ && !hoverW && !hoverE) {
            skillCircle.renderer.enabled = false;
        }
        if (skillMode) {
            if (skillNumer == 1) {
                /*
                skillCircle.renderer.enabled = true;
                float x = (tankAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (tankAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);

                Vector3 scale = new Vector3(x, y, 1);

                //double cause of circle...
                skillCircle.transform.localScale = scale * 2;
                */
                fireSkill(skillNumer);
            }
            if (skillNumer == 11) {
                skillCircle.renderer.enabled = true;
                float x = (tankAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (tankAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);

                Vector3 scale = new Vector3(x, y, 1);

                //double cause of circle...
                skillCircle.transform.localScale = scale * 2;


            }
            if (skillNumer == 2) {
                skillCircle.renderer.enabled = true;
                float x = (tankAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (tankAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                //double cause of circle...
                skillCircle.transform.localScale = scale * 2;


            }
            if (skillNumer == 21) {
                skillCircle.renderer.enabled = true;
                float x = (tankAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (tankAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                //double cause of circle...
                skillCircle.transform.localScale = scale * 2;


            }


            if (skillNumer == 3) {
                skillCircle.renderer.enabled = true;
                float x = (tankAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (tankAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);

                Vector3 scale = new Vector3(x, y, 1);

                //double cause of circle...
                skillCircle.transform.localScale = scale * 2;


            }
            //buff sofort zünden und nicht auf klick warten
            if (skillNumer == 31) {
                photonView.RPC("TankAttackSkill31", PhotonTargets.MasterClient, tankAttributesScript.buffPercentSkill31,
                    tankAttributesScript.timeSkill31, tankAttributesScript.rangeSkill31, transform.position);
                photonView.RPC("TankWarcryAnimation", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);

                //autoattack unterbrechen
                tankControlScript.attackMode = false;
                //skillmode beenden
                skillMode = false;

				//SOUND HIER WarCry
				photonView.RPC ("PlayTankWarCrySound", PhotonTargets.All, null);

                cdTSkill31.StartCooldown();

                return;
            }
            //Skill ausführen wenn linke maustaste
            if (Input.GetMouseButtonDown(0) && !mouseClickSkill) {
                //autoattack unterbrechen
                tankControlScript.attackMode = false;
                //fire skill
                fireSkill(skillNumer);
            }
            else if (mouseClickSkill) {
                mouseClickSkill = false;

            }

        }


        //checkup charge animation
        if (currentCharging && !movementScript.charge) {
            currentCharging = false;
            //stop animation
            photonView.RPC("TankKillChargeAnimation", PhotonTargets.All);
        }

        if (iAmSilenced && Time.time > silenceEnd) {
            iAmSilenced = false;
        }

		//WIN | LOSE
		if (GameObject.Find ("GameManager") != null) {
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().huntersWin == true && winSoundPlayed == false) {
				PlayWinSound ();
				winSoundPlayed = true;
			}
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().monsterWin == true && loseSoundPlayed == false) {
				PlayLoseSound ();
				loseSoundPlayed = true;
			}
		}
    }

    void fireSkill(int number) {
        GetComponent<AnimaitonScript>().lookAt(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z));
        tankControlScript.attackMode = false;

        //knockback
        if (number == 1) {

            if (GameObject.Find("Monster") != null && Vector3.Distance(transform.position, GameObject.Find("Monster").transform.position) <= tankAttributesScript.rangeSkill1) {

                //skill ausführen
                //winkelBerechnung
                //punkt vom gegner aus -> in welchem winkel steht der tank
                //gegen winkel = flugrichtung
                float angle = calculateAngle(GameObject.Find("Monster").transform.position, transform.position);
                photonView.RPC("TankAttackSkill1", PhotonTargets.All, GameObject.Find("Monster").GetPhotonView().viewID, -tankAttributesScript.damageSkill1, angle);
                revealMeTank();

                //SOUND HIER Knockback
                photonView.RPC("PlayTankKnockbackSound", PhotonTargets.All, null);
            }

            attackSkill1Active = false;
            movementScript.stopMove();
            animationScript.startAttackAnimationOne();
            photonView.RPC("TankKnockbackAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
            cdTSkill1.StartCooldown();

            /*
            //check ob über einen gegner als geklickt wurde
            if (mouseHitboxScript.mouseOverEnemy) {
                attackSkill1Active = true;
                curTarget = mouseHitboxScript.mouseOverLastEnemyGO;

				//SOUND HIER Knockback
				//photonView.RPC ("PlayTankKnockbackSound", PhotonTargets.All, null);
            }
            */
        }
            //bash
        else if (number == 11) {
            //check ob über einen gegner als geklickt wurde
            if (mouseHitboxScript.mouseOverEnemy) {
                attackSkill11Active = true;
                curTarget = mouseHitboxScript.mouseOverLastEnemyGO;

				//SOUND HIER Bash
				//photonView.RPC ("PlayTankBashSound", PhotonTargets.All, null);
            }

        }

        //Barriere
        else if (number == 2) {
            
            //ziel berechnen
            Vector3 pointToMoveSkill3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pointToMoveSkill3.z = transform.position.z;
            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            //berechnung falsch
            if (Vector3.Distance(pointToMoveSkill3, transform.position) > tankAttributesScript.rangeSkill2) {
                pointToMoveSkill3 = pointToMoveSkill3 - transform.position;
                pointToMoveSkill3.Normalize();
                pointToMoveSkill3 = pointToMoveSkill3 * tankAttributesScript.rangeSkill2;
                pointToMoveSkill3 = pointToMoveSkill3 + transform.position;
            }

            Vector3 offset = pointToMoveSkill3 - transform.position;

            int direction = computeDirection(pointToMoveSkill3);


            photonView.RPC("TankAttackSkill2", PhotonTargets.MasterClient, pointToMoveSkill3, this.gameObject.GetPhotonView().viewID, offset, tankAttributesScript.timeSkill2, direction);
                   
            cdTSkill2.StartCooldown();
            attackSkill2Active = true;
            skillMode = false;

			//SOUND HIER Barrier
			photonView.RPC ("PlayTankBarrierSound", PhotonTargets.All, null);
        }

        //taunt
        else if (number == 21) {
            //check ob über einen gegner als geklickt wurde
            if (mouseHitboxScript.mouseOverEnemy) {
                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScript.mouseOverLastEnemyGO.transform.position) < tankAttributesScript.rangeSkill21) {
                    photonView.RPC("TankAttackSkill21", PhotonTargets.All, mouseHitboxScript.mouseOverLastEnemyGO.GetPhotonView().viewID, tankAttributesScript.tauntTimeSkill21);
                    cdTSkill21.StartCooldown();
                    photonView.RPC("TankTauntAnimation", PhotonTargets.All, this.gameObject.GetPhotonView().viewID, mouseHitboxScript.mouseOverLastEnemyGO.GetPhotonView().viewID);
                    revealMeTank();

					//SOUND HIER Taunt
					photonView.RPC ("PlayTankTauntSound", PhotonTargets.All, null);
                } else {
                    attackSkill21Active = true;
                    curTarget = mouseHitboxScript.mouseOverLastEnemyGO;

					//SOUND HIER Taunt
					//photonView.RPC ("PlayTankTauntSound", PhotonTargets.All, null);
                }
                skillMode = false;


            }
        }



        //charge
        else if (number == 3) {
            //abchecken ob aushalb der reichweite geklickt wurde
            //ziel berechnen
            Vector3 pointToMoveSkill3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pointToMoveSkill3.z = transform.position.z;

            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            if (Vector3.Distance(pointToMoveSkill3, transform.position) > tankAttributesScript.rangeSkill3) {
                pointToMoveSkill3 = pointToMoveSkill3 - transform.position;
                pointToMoveSkill3.Normalize();
                pointToMoveSkill3 = pointToMoveSkill3 * tankAttributesScript.rangeSkill3;
                pointToMoveSkill3 = pointToMoveSkill3 + transform.position;
            }
            movementScript.iCharged(pointToMoveSkill3);

            oldMovmentSpeed = tankAttributesScript.movementSpeed;
            tankAttributesScript.movementSpeed = tankAttributesScript.movementSpeed * tankAttributesScript.speedMultiplierSkill3;
            attackSkill3Active = true;
            currentCharging = true;
            photonView.RPC("TankChargeAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
            cdTSkill3.StartCooldown();

			//SOUND HIER Charge
			photonView.RPC ("PlayTankChargeSound", PhotonTargets.All, null);
        }
            //battlecry
        else if (number == 31) {
            //nothing to do
			//SOUND HIER WarCry
			//photonView.RPC ("PlayTankWarCrySound", PhotonTargets.All, null);
        }

        skillMode = false;
    }

    private void checkUpKeyPressed() {

        if (!offerForQSkill && qSkill == 0 && Input.GetKeyDown(KeyCode.Q) && !cdTSkill1.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 1;
        }
        else if (!offerForWSkill && wSkill == 0 && Input.GetKeyDown(KeyCode.W) && !cdTSkill2.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 2;
        }
        else if (!offerForESkill && eSkill == 0 && Input.GetKeyDown(KeyCode.E) && !cdTSkill3.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 3;
        } 
        else if (qSkill == 1 && Input.GetKeyDown(KeyCode.Q) && !cdTSkill11.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 11;
        } 
        else if (wSkill == 1 && Input.GetKeyDown(KeyCode.W) && !cdTSkill21.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 21;
        } 
        else if (eSkill == 1 && Input.GetKeyDown(KeyCode.E) && !cdTSkill31.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 31;
        }




          //LÖSCHEN
        /*
        if (Input.GetKeyDown(KeyCode.A) && !cdTSkill11.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 11;
        } else if (Input.GetKeyDown(KeyCode.S) && !cdTSkill21.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 21;
        } else if (Input.GetKeyDown(KeyCode.D) && !cdTSkill31.IsOnCooldown()) {
			PlaySkillActivatedSound ();
            skillMode = true;
            skillNumer = 31;
        }
        */
    }

    public void mouseClickOnSkill(int skill) {

        if (!movementScript.charge && tankAttributesScript.isAlive && !iAmSilenced && !attackSkill2Active && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked) {

            mouseClickSkill = true;


            if (!offerForQSkill && qSkill == 0 && skill == 1 && !cdTSkill1.IsOnCooldown()) {
                PlaySkillActivatedSound();
                skillMode = true;
                skillNumer = 1;
            }
            else if (!offerForWSkill && wSkill == 0 && skill == 2 && !cdTSkill2.IsOnCooldown()) {
                PlaySkillActivatedSound();
                skillMode = true;
                skillNumer = 2;
            }
            else if (!offerForESkill && eSkill == 0 && skill == 3 && !cdTSkill3.IsOnCooldown()) {
                PlaySkillActivatedSound();
                skillMode = true;
                skillNumer = 3;
            }
            else if (qSkill == 1 && skill == 1 && !cdTSkill11.IsOnCooldown()) {
                PlaySkillActivatedSound();
                skillMode = true;
                skillNumer = 11;
            }
            else if (wSkill == 1 && skill == 2 && !cdTSkill21.IsOnCooldown()) {
                PlaySkillActivatedSound();
                skillMode = true;
                skillNumer = 21;
            }
            else if (eSkill == 1 && skill == 3 && !cdTSkill31.IsOnCooldown()) {
                PlaySkillActivatedSound();
                skillMode = true;
                skillNumer = 31;
            }

        }
    }



    private void skillActive() {

        //knockback
        /*
        if (attackSkill1Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) <= tankAttributesScript.rangeSkill1) {

                //skill ausführen
                //winkelBerechnung
                //punkt vom gegner aus -> in welchem winkel steht der tank
                //gegen winkel = flugrichtung
                float angle = calculateAngle(curTarget.transform.position, transform.position);
                photonView.RPC("TankAttackSkill1", PhotonTargets.All, curTarget.GetPhotonView().viewID, -tankAttributesScript.damageSkill1, angle);
                attackSkill1Active = false;
                movementScript.stopMove();
                cdTSkill1.StartCooldown();
                photonView.RPC("TankKnockbackAni", PhotonTargets.All, this.gameObject.GetPhotonView().viewID);
                revealMeTank();
                animationScript.startAttackAnimationOne();

				//SOUND HIER Knockback
				photonView.RPC ("PlayTankKnockbackSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, tankAttributesScript.rangeSkill1);
                }
            }
        }
        */

        //bash
        if (attackSkill11Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) <= tankAttributesScript.rangeSkill11) {
                //skill ausführen
                photonView.RPC("TankAttackSkill11", PhotonTargets.All,
                    curTarget.GetPhotonView().viewID, -tankAttributesScript.damageSkill11, tankAttributesScript.timeSkill11);
                cdTSkill11.StartCooldown();
                attackSkill11Active = false;
                movementScript.stopMove();
                int pick = animator.GetInteger("Direction");
                revealMeTank();
                photonView.RPC("TankStartBashAni", PhotonTargets.All,
                    this.gameObject.GetPhotonView().viewID, pick, curTarget.GetPhotonView().viewID);
                photonView.RPC("startStunAni", PhotonTargets.All,
                  curTarget.GetPhotonView().viewID, tankAttributesScript.timeSkill11);
                animationScript.startAttackAnimationOne();

				//SOUND HIER Bash
				photonView.RPC ("PlayTankBashSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, tankAttributesScript.rangeSkill11);
                }
            }
        }

        //barriere
        else if (attackSkill2Active) {
            if (Input.GetKeyDown(KeyCode.W)){
                if(GameObject.Find("TankBarriere") != null){
                    GameObject.Find("TankBarriere").GetComponent<TankBarriereScript>().destroyInstant();
                }
                resetAttackSkill2();
            }
        }

        //taunt
        else if (attackSkill21Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) < tankAttributesScript.rangeSkill21) {
                photonView.RPC("TankAttackSkill21", PhotonTargets.All, curTarget.GetPhotonView().viewID, tankAttributesScript.tauntTimeSkill21);
                cdTSkill21.StartCooldown();
                attackSkill21Active = false;
                movementScript.stopMove();
                photonView.RPC("TankTauntAnimation", PhotonTargets.All, this.gameObject.GetPhotonView().viewID, curTarget.GetPhotonView().viewID);
                revealMeTank();
                animationScript.startAttackAnimationOne();

				//SOUND HIER Taunt
				photonView.RPC ("PlayTankTauntSound", PhotonTargets.All, null);
            }
            else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, tankAttributesScript.rangeSkill11);
                }
            }
        }

        //charge
        if (attackSkill3Active) {
            //nothing to do

			//SOUND HIER Charge
			//photonView.RPC ("PlayTankChargeSound", PhotonTargets.All, null);
        }
        //warcry
        //nothing to do

		//SOUND HIER WarCry
		//photonView.RPC ("PlayTankWarCrySound", PhotonTargets.All, null);

    }

    public void setOldMovementSpeed() {
        tankAttributesScript.movementSpeed = tankAttributesScript.oldMovementSpeed;
        attackSkill3Active = false;
    }

    private float calculateAngle(Vector3 target, Vector3 myPos) {

        //animationszeug winkel berechnung
        float laengeA = target.x - myPos.x;
        if (laengeA < 0) {
            laengeA = laengeA * -1;
        }

        float laengeC = target.y - myPos.y;
        if (laengeC < 0) {
            laengeC = laengeC * -1;
        }

        float laengeB = Mathf.Sqrt(laengeA * laengeA + laengeC * laengeC);

        float winkelAlpha = Mathf.Acos(laengeA / laengeB);
        winkelAlpha = winkelAlpha * Mathf.Rad2Deg;

        if (target.x > transform.position.x && target.y > myPos.y) {
            //nichts machen
        } else if (target.x < myPos.x && target.y > myPos.y) {
            winkelAlpha = 180 - winkelAlpha;
        } else if (target.x < myPos.x && target.y < myPos.y) {
            winkelAlpha = 180 + winkelAlpha;
        } else if (target.x > myPos.x && target.y < myPos.y) {
            winkelAlpha = 360 - winkelAlpha;
        } else if (target.x > myPos.x && target.y == myPos.y) {
            winkelAlpha = 0;
        } else if (target.x < myPos.x && target.y == myPos.y) {
            winkelAlpha = 180;
        } else if (target.x == myPos.x && target.y > myPos.y) {
            winkelAlpha = 90;
        } else if (target.x == myPos.x && target.y < myPos.y) {
            winkelAlpha = 270;
        }

        return winkelAlpha;
    }

    void OnTriggerEnter(Collider coll) {

        if (coll.tag.Equals("Monster")) {
            //charge -> slow und schaden
            if (attackSkill3Active) {
                //stehen bleiben
                tankAttributesScript.movementSpeed = oldMovmentSpeed;
                movementScript.endCharge();

                //gegner slowen und damage
                photonView.RPC("TankAttackSkill3", PhotonTargets.All, coll.gameObject.GetPhotonView().viewID, -tankAttributesScript.damageSkill3, tankAttributesScript.slowTimeSkill3);

                photonView.RPC("startStunAni", PhotonTargets.All,
                 coll.gameObject.GetPhotonView().viewID, tankAttributesScript.slowTimeSkill3);
                
                
                attackSkill3Active = false;
            }
        }
    }

    public int getQSkill()
    {
        return qSkill;
    }

    public int getWSkill()
    {
        return wSkill;
    }

    public int getESkill()
    {
        return eSkill;
    }


    [RPC]
    public void TankAttackSkill3(int viewID, float damage, float slowTime) {
        Transform trans = PhotonView.Find(viewID).transform;
        if (PhotonNetwork.isMasterClient) {
            trans.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(damage);
        }
        trans.GetComponent<MonsterAttributesScript>().SlowMe(slowTime);
    }



    //Knockback
    [RPC]
    void TankAttackSkill1(int viewID, float damage, float knockbackAngle) {
        Transform trans = PhotonView.Find(viewID).transform;
        if(PhotonNetwork.isMasterClient)
        trans.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(damage);
        trans.GetComponent<MonsterControlScript>().knockback(knockbackAngle);
    }


    [RPC]
    public void TankAttackSkill11(int viewID, float damage, float dizzyTime) {
        Transform trans = PhotonView.Find(viewID).transform;
        if (PhotonNetwork.isMasterClient) {
            trans.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(damage);
        }
        trans.GetComponent<MonsterControlScript>().makeMeDizzy(dizzyTime);
    }


    [RPC]
    public void TankAttackSkill31(float percent, float time, float range, Vector3 tankPos) {
        //tank
        this.GetComponent<TankAttributesScript>().battleCryBuffMe(percent, time);

        //healer
        if (GameObject.Find("Healer") != null) {
            GameObject trans2 = GameObject.Find("Healer");
            trans2.GetComponent<HealerAttributesScript>().battleCryBuffMeCheck(percent, time, range, tankPos);
        }
        //ranger
        if (GameObject.Find("Ranger") != null) {
            GameObject trans3 = GameObject.Find("Ranger");
            trans3.GetComponent<RangerAttributesScript>().battleCryBuffMeCheck(percent, time, range, tankPos);
        }
    }
     
    [RPC]
    public void TankAttackSkill21(int viewID, float time) {
        Transform trans = PhotonView.Find(viewID).transform;
        trans.GetComponent<MonsterControlScript>().makeMeTaunted(time, this.photonView.viewID);

    }

    [RPC]
    public void TankWarcryAnimation(int viewID) {
        Transform go = Instantiate(warcryAni, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        go.GetComponent<tank_warcry_script>().target = PhotonView.Find(viewID).gameObject;
    }

    [RPC]
    public void TankChargeAni(int viewID) {
        Transform go = Instantiate(chargeAni, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        go.GetComponent<tank_charge_script>().target = PhotonView.Find(viewID).gameObject;
    }
    
    [RPC]
    public void TankKillChargeAnimation() {
        if (GameObject.Find("TankChargeAni") != null) {
            Destroy(GameObject.Find("TankChargeAni"));

        }
    }

    [RPC]
    public void TankTauntAnimation(int viewID1, int viewID2) {
        Transform go = Instantiate(tauntAni1, PhotonView.Find(viewID1).transform.position, transform.rotation) as Transform;
        go.GetComponent<tank_taunt_script1>().target = PhotonView.Find(viewID1).gameObject;

        Transform go2 = Instantiate(tauntAni2, PhotonView.Find(viewID2).transform.position, transform.rotation) as Transform;
        go2.GetComponent<tank_taunt_script2>().target = PhotonView.Find(viewID2).gameObject;
    }

    [RPC]
    public void TankStartBashAni(int viewID1, int pick, int viewID2) {
        if (pick == 0 || pick == 1 || pick == 2) {
            Transform go = Instantiate(bashAni1, PhotonView.Find(viewID1).transform.position, transform.rotation) as Transform;
            go.GetComponent<TankBashScript>().target = PhotonView.Find(viewID1).gameObject;
            go.GetComponent<TankBashScript>().z = 1;
        }
        else if (pick == 3 || pick == 4) {
            Transform go = Instantiate(bashAni2, PhotonView.Find(viewID1).transform.position, transform.rotation) as Transform;
            go.GetComponent<TankBashScript>().target = PhotonView.Find(viewID1).gameObject;
            go.GetComponent<TankBashScript>().z = 3;
        }
        else if (pick == 6 || pick == 7) {
            Transform go = Instantiate(bashAni3, PhotonView.Find(viewID1).transform.position, transform.rotation) as Transform;
            go.GetComponent<TankBashScript>().target = PhotonView.Find(viewID1).gameObject;
            go.GetComponent<TankBashScript>().z = 1;
        }
        else if (pick == 5) {
            Transform go = Instantiate(bashAni4, PhotonView.Find(viewID1).transform.position, transform.rotation) as Transform;
            go.GetComponent<TankBashScript>().target = PhotonView.Find(viewID1).gameObject;
            go.GetComponent<TankBashScript>().z = 3;
        }


        Transform go2 = Instantiate(bashHit, PhotonView.Find(viewID2).transform.position, transform.rotation) as Transform;
        go2.GetComponent<TankBashHitScript>().target = PhotonView.Find(viewID2).gameObject;

    }

    
    [RPC]
    public void TankKnockbackAni(int viewID) {
        Transform go = Instantiate(knockbackAni, PhotonView.Find(viewID).transform.position, transform.rotation) as Transform;
        go.GetComponent<TankKnockbackAni>().target = PhotonView.Find(viewID).gameObject;
    }

    public void offerToChangeSkill(int number) {
        if (photonView.isMine) {
            if (number == 1) {
                offerForQSkill = true;
            }
            if (number == 2) {
                offerForWSkill = true;
            }
            if (number == 3) {
                offerForESkill = true;
            }
        }
    }

    public bool getOfferForQSkill()
    {
        return offerForQSkill;
    }

    public bool getOfferForWSkill()
    {
        return offerForWSkill;
    }
    public bool getOfferForESkill()
    {
        return offerForESkill;
    }

    public void ChangeQSkill()
    {
        PlaySkillActivatedSound();

        offerForQSkill = false;
        qSkill = 1;
    }
    public void ChangeWSkill()
    {
        PlaySkillActivatedSound();

        offerForWSkill = false;
        wSkill = 1;
    }
    public void ChangeESkill()
    {
        PlaySkillActivatedSound();

        offerForESkill = false;
        eSkill = 1;
    }

    public void improveQSkill()
    {
			PlaySkillActivatedSound ();
            tankAttributesScript.rangeSkill1 = tankAttributesScript.rangeSkill1 + tankAttributesScript.rangeSkill1 * 0.1f;
            tankAttributesScript.damageSkill1 = tankAttributesScript.damageSkill1 + tankAttributesScript.damageSkill1 * 0.1f;

            offerForQSkill = false;
    }

    public void improveWSkill()
    {
			PlaySkillActivatedSound ();
            //tankAttributesScript.rangeSkill2 = tankAttributesScript.rangeSkill2 + tankAttributesScript.rangeSkill2 * 0.1f;
            tankAttributesScript.timeSkill2 = tankAttributesScript.timeSkill2 + tankAttributesScript.timeSkill2 * 0.1f;

            offerForWSkill = false;
    }
    public void improveESkill()
    {
			PlaySkillActivatedSound ();
            tankAttributesScript.damageSkill3 = tankAttributesScript.damageSkill3 + tankAttributesScript.damageSkill3 * 0.1f;
            tankAttributesScript.rangeSkill3 = tankAttributesScript.rangeSkill3 + tankAttributesScript.rangeSkill3 * 0.1f;
            tankAttributesScript.slowTimeSkill3 = tankAttributesScript.slowTimeSkill3 + tankAttributesScript.slowTimeSkill3 * 0.1f;
            tankAttributesScript.speedMultiplierSkill3 = tankAttributesScript.speedMultiplierSkill3 + tankAttributesScript.speedMultiplierSkill3 * 0.1f;


            offerForESkill = false;
    }

    public void silenceMe(float time) {
        if (photonView.isMine) {
            iAmSilenced = true;
            silenceEnd = Time.time + time;
        }
    }

    [RPC]
    public void TankAttackSkill2(Vector3 pos, int viewID, Vector3 offset, float time, int direction) {
        GameObject go = PhotonNetwork.Instantiate("Tank/TankBarriere", pos, transform.rotation, 0) as GameObject;

        photonView.RPC("TankAttackSkill2Set", PhotonTargets.All, go.GetPhotonView().viewID, pos, viewID, offset, time, direction);
    }

    [RPC]
    public void TankAttackSkill2Set(int goViewID, Vector3 pos, int viewID, Vector3 offset, float time, int direction) {
        GameObject go = PhotonView.Find(goViewID).gameObject;
        go.GetComponent<TankBarriereScript>().tankViewID = viewID;
        go.GetComponent<TankBarriereScript>().offset = offset;
        go.GetComponent<TankBarriereScript>().destroyTimer = Time.time + time;
        go.GetComponent<TankBarriereScript>().direction = direction;
    }

    public int computeDirection(Vector3 pos) {
        //animationszeug winkel berechnung
        float laengeA = pos.x - transform.position.x;
        if (laengeA < 0) {
            laengeA = laengeA * -1;
        }

        float laengeC = pos.y - transform.position.y;
        if (laengeC < 0) {
            laengeC = laengeC * -1;
        }

        float laengeB = Mathf.Sqrt(laengeA * laengeA + laengeC * laengeC);

        float winkelAlpha = Mathf.Acos(laengeA / laengeB);
        winkelAlpha = winkelAlpha * Mathf.Rad2Deg;

        if (pos.x > transform.position.x && pos.y > transform.position.y) {
            //nichts machen
        }
        else if (pos.x < transform.position.x && pos.y > transform.position.y) {
            winkelAlpha = 180 - winkelAlpha;
        }
        else if (pos.x < transform.position.x && pos.y < transform.position.y) {
            winkelAlpha = 180 + winkelAlpha;
        }
        else if (pos.x > transform.position.x && pos.y < transform.position.y) {
            winkelAlpha = 360 - winkelAlpha;
        }
        else if (pos.x > transform.position.x && pos.y == transform.position.y) {
            winkelAlpha = 0;
        }
        else if (pos.x < transform.position.x && pos.y == transform.position.y) {
            winkelAlpha = 180;
        }
        else if (pos.x == transform.position.x && pos.y > transform.position.y) {
            winkelAlpha = 90;
        }
        else if (pos.x == transform.position.x && pos.y < transform.position.y) {
            winkelAlpha = 270;
        }

        if (winkelAlpha >= 0 && winkelAlpha <= 90) {
            return 2;
        }
        else if (winkelAlpha >= 90 && winkelAlpha <= 180) {
            return 1;
        }
        else if (winkelAlpha >= 180 && winkelAlpha <= 270) {
            return 4;
        }
        else{
            return 3;
        }
    }

    public void resetAttackSkill2() {
        attackSkill2Active = false;
        if (cdTSkill2 != null) {
            cdTSkill2.StartCooldown();
        }
        photonView.RPC("resetAttackSkill2RPC", PhotonTargets.Others);
    }

    [RPC]
    public void resetAttackSkill2RPC() {
        attackSkill2Active = false;
        if (cdTSkill2 != null) {
            cdTSkill2.StartCooldown();
        }
    }

    public void revealMeTank() {
        if (GameObject.Find("Monster") != null)
            photonView.RPC("revealMeTankRPC", GameObject.Find("Monster").GetPhotonView().owner);
    }


    [RPC]
    public void revealMeTankRPC() {
        if (GameObject.Find("Monster") != null) {
            GameObject.Find("Monster").GetComponent<InviChecker>().revealTank();
        }
    }

    [RPC]
    public void startStunAni(int viewID, float time) {
        GameObject go = Instantiate(stunAni, PhotonView.Find(viewID).transform.position, PhotonView.Find(viewID).transform.rotation) as GameObject;

        go.GetComponent<StunAniScript>().target = PhotonView.Find(viewID).gameObject;
        go.GetComponent<StunAniScript>().time = time + Time.time;
    }

	// Sound Methods
	private void PlaySkillActivatedSound() 
	{
		PlaySound(0,0.45f);
	}
	
	[RPC]
	public void PlayTankKnockbackSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (1);
	}
	
	[RPC]
	public void PlayTankBashSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (2);
	}
	
	[RPC]
	public void PlayTankBarrierSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (3);
	}
	
	[RPC]
	public void PlayTankTauntSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (4);
	}
	
	[RPC]
	public void PlayTankChargeSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (5);
	}
	
	[RPC]
	public void PlayTankWarCrySound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (6);
	}
	
	
	private void PlaySound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	private void PlaySound(int Number, float volume)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, volume*menuScript.scaleVolumeFactor);
	}

	private void PlayWinSound()
	{
		AudioSource.PlayClipAtPoint(winSound, transform.position, 0.75f*menuScript.scaleVolumeFactor);
	}
	
	private void PlayLoseSound()
	{
		AudioSource.PlayClipAtPoint(loseSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}

    public void showCircle(int which) {
        if (which == 1 && qSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (tankAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (tankAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 1 && qSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (tankAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (tankAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 2 && wSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (tankAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (tankAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 2 && wSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (tankAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (tankAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 3 && eSkill == 0) {
            skillCircle.renderer.enabled = true;
            float x = (tankAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (tankAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
        else if (which == 3 && eSkill == 1) {
            skillCircle.renderer.enabled = true;
            float x = (tankAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
            float y = (tankAttributesScript.rangeSkill31 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
            Vector3 scale = new Vector3(x, y, 1);
            skillCircle.transform.localScale = scale * 2;
        }
    }

}

