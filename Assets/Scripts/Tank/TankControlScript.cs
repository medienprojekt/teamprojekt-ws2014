﻿using UnityEngine;
using System.Collections;

public class TankControlScript : Photon.MonoBehaviour {

	private Menu menuScript;

    public float speed = 1.5f;
    public Vector3 target;
    private GameObject multiplayerManger;
    public GameObject cam;
    public CooldownTimer autoAttackCD;
    public GameObject autoAttackProjectileGO;
    public bool attackMode;
    private Vector3 distanceBetweenPlayerAndTarget;
    private bool runTowardsForAutoAttack;
    private TankSkillControlScript tankSkillControlScript;
    private Movement movementScript;
    private TankAttributesScript tankAttributesScript;
    private GameObject mouseHitbox;
    private MouseHitboxScript mouseHitboxScript;
    private GameObject targetGO;
    private bool walkToPortal;
    private GameObject walkToPortalGO;

    public bool isDizzy;
    private CooldownTimer dizzyTime;
    private float oldMovementSpeed;

    public bool reanimateMode;
    private GameObject reanimateGO;

    public float reanimateCharge;

    private AnimaitonScript animationScript;
    private Animator animator;

    public bool captureWard;
    private GameObject captrueWardGO;
    private float captureWardCharge;

    private bool attackAltar;
    private GameObject altarGO;

	public AudioClip[] sounds;
	public AudioSource asound;
	public AudioClip mouseClickSound;
	public AudioClip autoAttackSound;
	public AudioClip reanimationSound;
	private int counter1;

    public Texture tex1;
    public Texture tex2;

    public GameObject mouseClick;
    public GameObject reanimateAni;
    void Awake() {

		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];
		counter1 = 3;

        tankSkillControlScript = transform.GetComponent<TankSkillControlScript>();
        tankAttributesScript = transform.GetComponent<TankAttributesScript>();

        if (photonView.isMine) {
            animator = this.GetComponent<Animator>();
            animationScript = transform.GetComponent<AnimaitonScript>();

            target = transform.position;
            walkToPortal = false;
            autoAttackCD = new CooldownTimer(tankAttributesScript.attackSpeed);
            attackMode = false;

            dizzyTime = new CooldownTimer(0);
            isDizzy = false;

            runTowardsForAutoAttack = false;


            movementScript = transform.GetComponent<Movement>();

            mouseHitbox = GameObject.Find("MouseHitbox");
            mouseHitboxScript = mouseHitbox.GetComponent<MouseHitboxScript>();




        }
        else {
            enabled = false;
        }

        Vector3 bla = transform.position;
        bla.z = 2;
        transform.position = bla;
        target = transform.position;


    }


    void Update() {
        if (tankAttributesScript.isAlive && !movementScript.charge && !GameObject.Find("GameManager").GetComponent<GameStarter>().HuntersLocked)
        {
            handleMouseInteractions();
            if (!tankSkillControlScript.attackSkill2Active && !tankSkillControlScript.attackSkill3Active) {
                autoattack();
            }
        }

        if (tankAttributesScript.isAlive && attackAltar && altarGO != null && altarGO.GetComponent<AltarsScript>().health > 0) {
            //wenn in range und autoAttackMode an ist
            if (Vector3.Distance(transform.position, altarGO.transform.position) < 4.0f) {
                movementScript.stopMove();
                //gegner angreifen
                if (!autoAttackCD.IsOnCooldown()) {
                    //TODO animation
                    animationScript.startAttackAnimation();
                    //leben abziehen
                    photonView.RPC("tankAutoAttackAltarRPC", PhotonTargets.MasterClient, altarGO.GetPhotonView().viewID);
                    autoAttackCD.StartCooldown();

					//SOUND HIER autoattack
					PlayRPCTankAutoAttackSound ();
                }
            }
            else {
                if (animationScript.isAttacking && !attackMode) {
                    animationScript.stopAttackAnimation();
                }
            }
        }


        if (tankAttributesScript.isAlive && reanimateMode && Vector3.Distance(reanimateGO.transform.position, transform.position) <= 1.5f) {
            //animation
            reanimateCharge += 25 * Time.deltaTime;

            if (reanimateCharge >= 100.0f) {
                if (reanimateGO.GetComponent<HealerAttributesScript>() != null) {
                    reanimateGO.GetComponent<HealerAttributesScript>().reanimateMe();
                    photonView.RPC("StartReanimateAniTank", PhotonTargets.All, reanimateGO.transform.position);
                }
                else if (reanimateGO.GetComponent<RangerAttributesScript>() != null) {
                    reanimateGO.GetComponent<RangerAttributesScript>().reanimateMe();
                    photonView.RPC("StartReanimateAniTank", PhotonTargets.All, reanimateGO.transform.position);
                }
				//SOUND HIER wiederbelebt
				PlayRPCTankReanimationSound();

                reanimateCharge = 0.0f;
                reanimateMode = false;

            }
        }

        //ward
        if (tankAttributesScript.isAlive && captureWard && Vector3.Distance(captrueWardGO.transform.position, transform.position) <= 2.0f) {
            captureWardCharge += 25 * Time.deltaTime;
            movementScript.stopMove();

            if (captureWardCharge >= 100.0f) {
                //ward captured
                photonView.RPC("captureWardTank", PhotonTargets.All, captrueWardGO.GetPhotonView().viewID);
                captureWardCharge = 0.0f;
                captureWard = false;

				//SOUND HIER ward captured(steht im Ward selbst)
            }
        }


        //portal behandlung
        if (walkToPortalGO == null) {
            walkToPortal = false;
        }
        if (walkToPortal) {

            if (walkToPortalGO != null && Vector3.Distance(transform.position, walkToPortalGO.transform.position) < 2.0f) {

                if (walkToPortalGO.name.Equals("Portal1")) {
                    GameObject portal2 = GameObject.Find("Portal2");
                    Vector3 portalTarget = portal2.transform.position;
                    portalTarget.z = transform.position.z;
                    movementScript.iBlinked(portalTarget);
                }
                else if (walkToPortalGO.name.Equals("Portal2")) {
                    GameObject portal2 = GameObject.Find("Portal1");
                    Vector3 portalTarget = portal2.transform.position;
                    portalTarget.z = transform.position.z;
                    movementScript.iBlinked(portalTarget);
                }
                walkToPortal = false;
            }
        }

        //dizzy checkup
        if (isDizzy && !dizzyTime.IsOnCooldown()) {
            isDizzy = false;
            tankAttributesScript.movementSpeed = oldMovementSpeed;
        }
    }

    void autoattack() {

        //wenn in range und autoAttackMode an ist
        if (targetGO != null && attackMode && Vector3.Distance(transform.position, targetGO.transform.position) < tankAttributesScript.attackRange) {

            if (!targetGO.renderer.enabled) {
                targetGO = null;
                attackMode = false;
                movementScript.iMoved(transform.position);
                return;
            }

            //gegner angreifen
            if (!autoAttackCD.IsOnCooldown()) {
                //TODO animation
                animationScript.startAttackAnimation();
                //leben abziehen
                photonView.RPC("tankAutoAttackRPC", PhotonTargets.MasterClient, targetGO.GetPhotonView().viewID, -tankAttributesScript.attackDamage);
                autoAttackCD.StartCooldown();
                tankSkillControlScript.revealMeTank();

				//SOUND HIER tank autoattack
				PlayRPCTankAutoAttackSound ();

            }

        }
        else {
            if (animationScript.isAttacking && !attackAltar) {
                animationScript.stopAttackAnimation();
            }
        }

    }

    float native_width = 1920;
    float native_height = 1080;

    void handleMouseInteractions() {
        //handle mouse interactions
        //right click

        float rx = Screen.width / native_width;
        float ry = Screen.height / native_height;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

        Vector3 mpos;
        mpos = GameObject.Find("Minimap").GetComponent<Map>().NormalizedMousePosition;

        if (Input.GetMouseButtonDown(1) && !tankSkillControlScript.attackSkill3Active && !movementScript.charge && !GameObject.Find("Minimap").GetComponent<Map>().mouseOnMap(mpos.x, mpos.y))
        {

			PlayMouseClickSound();

            walkToPortal = false;
            tankSkillControlScript.skillMode = false;
            reanimateMode = false;
            captureWard = false;
            attackAltar = false;
            //run or attack or other stuff...
            //run
            if (!mouseHitboxScript.mouseOverEnemy && !mouseHitboxScript.mouseOverTeammate && !mouseHitboxScript.mouseOverPortal && !mouseHitboxScript.mouseOverWard && !mouseHitboxScript.mouseOverAltar) {
                //maus portion holen
                target.x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
                target.y = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
                target.z = transform.position.z;
                attackMode = false;
                //movementScript bescheid sagen
                movementScript.iMoved(target);
                tankSkillControlScript.attackSkill1Active = false;
                Instantiate(mouseClick, target, transform.rotation);

				//SOUND HIER bewegungsbestätigung (lol - ezreal - over here! :D )
				PlayMaybeMoveVoiceSound ();
            }

            //auto-attack
            else if (mouseHitboxScript.mouseOverEnemy) {
                if (mouseHitboxScript.mouseOverLastEnemyGO != null) {
                    attackMode = true;
                    tankSkillControlScript.attackSkill1Active = false;
                    targetGO = mouseHitboxScript.mouseOverLastEnemyGO;
                    //laufe zum target hin
                    movementScript.moveTowardsEnemy(mouseHitboxScript.mouseOverLastEnemyGO, tankAttributesScript.attackRange);

					//SOUND HIER angriffsbestätigung
					PlayAttackVoiceSound ();
                }
            }

            //teammate wiederbeleben
            else if (mouseHitboxScript.mouseOverTeammate) {
                if (mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<RangerAttributesScript>() != null &&
                    !mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<RangerAttributesScript>().isAlive) {
                        movementScript.moveTowardsPoint(mouseHitboxScript.mouseOverLastTeammateGO.transform.position, 1.0f);
                    reanimateMode = true;
                    reanimateGO = mouseHitboxScript.mouseOverLastTeammateGO;
                    reanimateCharge = 0;

					//SOUND HIER bewegungsbestätigung
					PlayMoveVoiceSound ();
                }
                else if (mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<HealerAttributesScript>() != null &&
                    !mouseHitboxScript.mouseOverLastTeammateGO.GetComponent<HealerAttributesScript>().isAlive) {
                        movementScript.moveTowardsPoint(mouseHitboxScript.mouseOverLastTeammateGO.transform.position, 1.0f);
                    reanimateMode = true;
                    reanimateGO = mouseHitboxScript.mouseOverLastTeammateGO;
                    reanimateCharge = 0;

					//SOUND HIER bewegungsbestätigung
					PlayMoveVoiceSound ();
                }
            }


            //auf portal geklickt
            else if (mouseHitboxScript.mouseOverPortal) {
                walkToPortal = true;
                walkToPortalGO = mouseHitboxScript.mouseOverPortalGO;
                movementScript.moveTowardsEnemy(mouseHitboxScript.mouseOverPortalGO, 1.8f);
            }


            //ward
            else if (mouseHitboxScript.mouseOverWard && mouseHitboxScript.mouseOverWardGO.GetComponent<WardScript>().iAmCapturedByTeam != 1) {
                captureWard = true;
                movementScript.moveTowardsPoint(mouseHitboxScript.mouseOverWardGO.transform.position, 1.0f);
                captureWardCharge = 0.0f;
                captrueWardGO = mouseHitboxScript.mouseOverWardGO;

				//SOUND HIER bewegungsbestätigung
				PlayMoveVoiceSound ();
            }

            //altar
            else if (mouseHitboxScript.mouseOverAltar) {
                attackAltar = true;
                movementScript.moveTowardsEnemy(mouseHitboxScript.mouseOverAltarGO, 3.0f);
                altarGO = mouseHitboxScript.mouseOverAltarGO;

				//SOUND HIER bewegungsbestätigung
				PlayAttackVoiceSound ();
            }

        }

    }

    [RPC]
    private void tankAutoAttackRPC(int viewID, float adjustHealth) {
        Transform trans = PhotonView.Find(viewID).transform;
        if(PhotonNetwork.isMasterClient)
        trans.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(adjustHealth);
    }

    public void makeMeDizzy(float time, float slowFactor) {
        if (photonView.isMine) {
            isDizzy = true;
            dizzyTime = new CooldownTimer(time);
            dizzyTime.StartCooldown();
            oldMovementSpeed = tankAttributesScript.oldMovementSpeed;
            tankAttributesScript.movementSpeed = tankAttributesScript.oldMovementSpeed * slowFactor;
        }
    }

    [RPC]
    private void captureWardTank(int viewID) {
        Transform trans = PhotonView.Find(viewID).transform;
        trans.GetComponent<WardScript>().iAmCapturedByTeam = 1;
    }


    void OnGUI() {


        float rx = Screen.width / native_width;
        float ry = Screen.height / native_height;
        GL.PushMatrix();
        GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));


        if (reanimateMode && Vector3.Distance(reanimateGO.transform.position, transform.position) <= 1.5f) {
            GUI.DrawTexture(new Rect(1920 / 2 - tex1.width / 2, 900 - tex1.height, tex1.width, tex1.height), tex1);

            GUI.BeginGroup(new Rect(1920 / 2 - tex2.width / 2, 900 - tex2.height, tex2.width * (reanimateCharge / 100), tex2.height));
            GUI.DrawTexture(new Rect(0, 0, tex2.width, tex2.height), tex2);
            GUI.EndGroup();
        }

        //ward
        if (captureWard && Vector3.Distance(captrueWardGO.transform.position, transform.position) <= 2.0f) {
            GUI.DrawTexture(new Rect(1920 / 2 - tex1.width / 2, 900 - tex1.height, tex1.width, tex1.height), tex1);

            GUI.BeginGroup(new Rect(1920 / 2 - tex2.width / 2, 900 - tex2.height, tex2.width * (captureWardCharge / 100), tex2.height));
            GUI.DrawTexture(new Rect(0, 0, tex2.width, tex2.height), tex2);
            GUI.EndGroup();
        }




        GL.PopMatrix();

    }

    [RPC]
    void tankAutoAttackAltarRPC(int viewID) {
        Transform trans = PhotonView.Find(viewID).transform;
        trans.GetComponent<AltarsScript>().getDamage();
    }


	//Sound Methods
	[RPC]
	public void PlayTankAutoAttackSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        AudioSource.PlayClipAtPoint(autoAttackSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	public void PlayRPCTankAutoAttackSound()
	{
		photonView.RPC ("PlayTankAutoAttackSound", PhotonTargets.All, null);
	}
	
	[RPC]
	public void PlayTankReanimationSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        AudioSource.PlayClipAtPoint(reanimationSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	public void PlayRPCTankReanimationSound()
	{
		photonView.RPC ("PlayTankReanimationSound", PhotonTargets.All, null);
	}
	
	public void PlayMouseClickSound()
	{
		AudioSource.PlayClipAtPoint(mouseClickSound, transform.position, 0.12f*menuScript.scaleVolumeFactor);
	}
	
	private void PlaySound(int Number)
	{
		asound.clip = sounds[Number];
		asound.volume = 0.3f*menuScript.scaleVolumeFactor;
		asound.Play ();
	}
	
	private void PlayMaybeMoveVoiceSound()
	{
		if(counter1 == 6)
		{
			int r = Random.Range(0,3);
			PlaySound (r);
			counter1 = 0;
		}
		
		counter1++;
	}
	
	private void PlayMoveVoiceSound()
	{
		int r = Random.Range(0,3);
		PlaySound (r);
	}
	
	private void PlayAttackVoiceSound()
	{
		int r = Random.Range(3,6);
		PlaySound (r);
	}


    [RPC]
    public void StartReanimateAniTank(Vector3 pos) {
        Instantiate(reanimateAni, pos, transform.rotation);
    }

    public void lostMonsterSight() {
        if (targetGO != null) {
            targetGO = null;
            attackMode = false;
            movementScript.lostMonsterSight();
        }
    }
}

