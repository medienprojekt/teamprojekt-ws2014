﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Menu;
using System.Collections.Generic;

public class Menu : Photon.MonoBehaviour {

	//sounds
	public AudioClip blipAudio;
	public AudioClip blipAudio2;
	public AudioClip[] songs;
	private int songNumber;
	public AudioSource backgroundMusic;
	public bool switchMusicToGame;
	public bool switchMusicToMenu;
	public float scaleVolumeFactor;
	public float scaleVolumeFactorBefore;
	private bool scaleVolumeFactorGotChanged;

    //bools
    public bool inMenu;
    public bool mainMenu;
    public bool serverBrowser;
    public bool options;
    public bool joinGame;
    public bool createGame;

    //Role Selection
    public bool daemon;
    public bool ranger;
    public bool support;
    public bool tank;

    public bool daemon_Blocked;
    public bool ranger_Blocked;
    public bool support_Blocked;
    public bool tank_Blocked;



    private bool nameTaken;

    private string roomName;
    private string playerName;
    private RoomInfo[] rooms;

    private GameObject hudManager;

    //1 = jäger, 2 = monster
    public int iAmOnTeam;
    //name
    public string iAm;


    //Textures
    public Texture background;
    public Texture button_Play;
    public Texture button_Options;
    public Texture button_Exit;

    public Texture window_ServerBrowser;
    public Texture window_ServerBrowserBack;
    public Texture serverBrowser_Entry;
    public Texture serverBrowser_Entry_Selected;
    public Texture button_Join;
    public Texture button_Create;
    public Texture button_Back;
    public Texture button_Ready;
    public Texture button_Leave;
    public Texture ready_checked;
    public Texture ready_unchecked;



    public Texture window_Join;
    public Texture button_Okay;
    public Texture button_Role_Deamon;
    public Texture button_Role_Deamon_Selected;
    public Texture button_Role_Ranger;
    public Texture button_Role_Ranger_Selected;
    public Texture button_Role_Support;
    public Texture button_Role_Support_Selected;
    public Texture button_Role_Tank;
    public Texture button_Role_Tank_Selected;

    public Texture window_Create;

    public Texture options_back;
    public Texture options_apply;
    public Texture options_knob;

    public Texture playerDisconnected;
    public Texture button_Continue;


    public ServerBrowser serverManager;
    public List<GameEntry> servers;
    public GameEntry currentSelection;
    public GUIStyle gameEntryFont;
    public GUIStyle textFieldFont;

    private bool rolePickLocked;
    private bool readyLock;
    private bool daemon_ready;
    private bool ranger_ready;
    private bool support_ready;
    private bool tank_ready;

    private bool noNick;

    public bool dragVolume;
    public bool dragScrollspeed;

    public bool playerDisc;


    private float volume_knob_x;
    private float scrollspeed_knob_x;

    private Rect options_Rect;
    private Rect options_Volume_Rect;
    private Rect options_Scrollspeed_Rect;
    private Rect volume_Knob_Rect;
    private Rect scrollspeed_Knob_Rect;
	// Use this for initialization
	void Start () {
        PhotonNetwork.ConnectUsingSettings("0.1");
        Screen.showCursor = true;
		//Audio
		backgroundMusic = GetComponent<AudioSource>();
		backgroundMusic.clip = songs [0];
		scaleVolumeFactor = 1.0f;
		scaleVolumeFactorBefore = scaleVolumeFactor;
		scaleVolumeFactorGotChanged = false;
		songNumber = 1;

		backgroundMusic.Play ();
		switchMusicToGame = true;
		switchMusicToMenu = false;
		//AudioSource.PlayClipAtPoint (menuBackgroundMusic, transform.position);


        inMenu = true;
        mainMenu = true;
        serverBrowser = false;
        options = false;
        joinGame = false;
        createGame = false;
        
        daemon = false;
        daemon_Blocked = false;

        ranger = false;
        ranger_Blocked = false;

        support = false;
        support_Blocked = false;

        tank = false;
        tank_Blocked = false;

        playerDisc = false;

        servers = serverManager.getServers();
        gameEntryFont = new GUIStyle();
        textFieldFont = new GUIStyle();
        roomName = "";
        hudManager = GameObject.Find("HUDManager");
        iAmOnTeam = 0;
        playerName = "";
        roomName = "";

        rolePickLocked = true;
        readyLock = false;

        daemon_ready = false;
        ranger_ready = false;
        support_ready = false;
        tank_ready = false;

        noNick = false;

        options_Rect = new Rect(1920 / 2 - options_back.width / 2, 1080 / 2 - options_back.height / 2, options_back.width, options_back.height);
        options_Volume_Rect = new Rect(options_Rect.x + 137, options_Rect.y + 111, 292, 35);
        options_Scrollspeed_Rect = new Rect(options_Rect.x + 137, options_Rect.y + 175, 292, 35);
        volume_knob_x = getVolume() * options_Volume_Rect.width + options_Volume_Rect.x;
        scrollspeed_knob_x = GameObject.Find("Main Camera").GetComponent<Scrolling>().getScaleFactor() * options_Scrollspeed_Rect.width + options_Scrollspeed_Rect.x;
        //Anschalten für OfflineMode (kein locales netzwerk)
        //PhotonNetwork.offlineMode = true;
	}
    void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        playerDisc = true;
        PhotonNetwork.LeaveRoom();
        reset();
    }

    public void reset()
    {
        if (daemon)
        {
            photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", false);
            photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Daemon", false);
        }
        else if (ranger)
        {
            photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", false);
            photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Ranger", false);
        }
        else if (support)
        {
            photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", false);
            photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Support", false);
        }
        else if (tank)
        {
            photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", false);
            photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Tank", false);
        }

        daemon_Blocked = false;
        daemon_ready = false;
        daemon = false;
        ranger_Blocked = false;
        ranger_ready = false;
        ranger = false;
        support_Blocked = false;
        support_ready = false;
        support = false;
        tank_Blocked = false;
        tank_ready = false;
        tank = false;
        rolePickLocked = true;
        readyLock = false;
        noNick = false;
        PhotonNetwork.LeaveRoom();
        inMenu = true;
        joinGame = false;
        serverBrowser = true;
        nameTaken = false;
        iAmOnTeam = 0;
        GameObject.Find("GameManager").GetComponent<GameStarter>().reset();
        GameObject.Find("GameManager").GetComponent<Winconditions>().reset();
        GameObject.Find("Main Camera").GetComponent<Transform>().position = new Vector3(1.337f, -3.43f, -6f);

    }


	// Update is called once per frame
	void Update () {


        volume_Knob_Rect = new Rect(volume_knob_x, (options_Volume_Rect.y + options_Volume_Rect.height / 2) - options_knob.height / 2, options_knob.width, options_knob.height);
        scrollspeed_Knob_Rect = new Rect(scrollspeed_knob_x, (options_Scrollspeed_Rect.y + options_Scrollspeed_Rect.height / 2) - options_knob.height / 2, options_knob.width, options_knob.height);

        if (inMenu)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (inMenu)
                {
                    if (serverBrowser)
                    {
                        serverBrowser = false;
                        mainMenu = true;
                    }

                    if (options)
                    {
                        options = false;
                        mainMenu = true;
                    }

                    if (createGame)
                    {
                        createGame = false;
                        serverBrowser = true;
                    }

                    if (joinGame)
                    {
                        reset();
                    }

                    if (playerDisc)
                    {
                        playerDisc = false;
                    }
                }
            }
        }
		//Music switchen
		if (!inMenu && switchMusicToGame) 
		{
			songNumber = 2;
			switchMusicToGame = false;
			switchMusicToMenu = true;
			backgroundMusic.Stop ();
			backgroundMusic.clip = songs[1];
			backgroundMusic.volume = 0.18f*scaleVolumeFactor;
			backgroundMusic.Play ();
		}

		if (inMenu && switchMusicToMenu) 
		{
			songNumber = 1;
			switchMusicToGame = true;
			switchMusicToMenu = false;
			backgroundMusic.Stop ();
			backgroundMusic.clip = songs[0];
			backgroundMusic.volume = 0.8f*scaleVolumeFactor;
			backgroundMusic.Play ();
		}

		if (scaleVolumeFactorGotChanged == true && songNumber == 1)
		{
			backgroundMusic.volume = 1.0f*scaleVolumeFactor;
		} else if (scaleVolumeFactorGotChanged == true && songNumber == 2) 
		{
			backgroundMusic.volume = 0.25f*scaleVolumeFactor;
		}

		if (scaleVolumeFactor != scaleVolumeFactorBefore) 
		{
			scaleVolumeFactorGotChanged = true;
			scaleVolumeFactorBefore = scaleVolumeFactor;
		} else 
		{
			scaleVolumeFactorGotChanged = false;
		}

        if (PhotonNetwork.isMasterClient)
        {
            rolePickLocked = false;
        }

        if (inMenu && daemon_ready && ranger_ready && support_ready && tank_ready)
        //if (inMenu&&((daemon&&daemon_ready)||(ranger&&ranger_ready)||(support&&support_ready)||(tank&&tank_ready)))
        {
            GameObject.Find("GameManager").GetComponent<Winconditions>().reset();
            if (daemon)
            {
                iAmOnTeam = 2;
                iAm = "Monster";
                GameObject player = PhotonNetwork.Instantiate("Chars/MonsterPrefab", GameObject.Find("MonsterSpawnPoint").transform.position, GameObject.Find("MonsterSpawnPoint").transform.rotation, 0);
                hudManager.GetComponent<HUDScript>().setUp(0, player);
                hudManager.GetComponent<HUDScript>().monsterLoadout = true;
                GameObject.Find("MouseHitbox").GetComponent<MouseHitboxScriptMonster>().enabled = true;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().player = player;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().whatAmI = 0;

                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().setUpPlayerName(playerName, 0);
                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().sendNameOnJoin(playerName, 0);

                GameObject.Find("GameManager").GetComponent<GameStarter>().gameStarted = true;
                GameObject.Find("GameManager").GetComponent<GameStarter>().whatAmI = 0;
            }
            else if (ranger)
            {
                iAmOnTeam = 1;
                iAm = "Ranger";
                GameObject player = PhotonNetwork.Instantiate("Chars/RangerPrefab", GameObject.Find("Klasse1SpawnPoint").transform.position, GameObject.Find("Klasse1SpawnPoint").transform.rotation, 0);
                hudManager.GetComponent<HUDScript>().setUp(1, player);
                GameObject.Find("MouseHitbox").GetComponent<MouseHitboxScript>().enabled = true;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().player = player;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().whatAmI = 1;
                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().setUpPlayerName(playerName, 1);
                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().sendNameOnJoin(playerName, 1);
                GameObject.Find("GameManager").GetComponent<GameStarter>().gameStarted = true;
                GameObject.Find("GameManager").GetComponent<GameStarter>().whatAmI =1;
            }
            else if (support)
            {
                iAmOnTeam = 1;
                iAm = "Healer";
                GameObject player = PhotonNetwork.Instantiate("Chars/HealerPrefab", GameObject.Find("Klasse2SpawnPoint").transform.position, GameObject.Find("Klasse2SpawnPoint").transform.rotation, 0);
                hudManager.GetComponent<HUDScript>().setUp(2, player);
                GameObject.Find("MouseHitbox").GetComponent<MouseHitboxScript>().enabled = true;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().player = player;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().whatAmI = 2;
                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().setUpPlayerName(playerName, 2);
                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().sendNameOnJoin(playerName, 2);
                GameObject.Find("GameManager").GetComponent<GameStarter>().gameStarted = true;
                GameObject.Find("GameManager").GetComponent<GameStarter>().whatAmI = 2;
            }
            else if (tank)
            {
                iAmOnTeam = 1;
                iAm = "Tank";
                GameObject player = PhotonNetwork.Instantiate("Chars/TankPrefab", GameObject.Find("Klasse3SpawnPoint").transform.position, GameObject.Find("Klasse3SpawnPoint").transform.rotation, 0);
                hudManager.GetComponent<HUDScript>().setUp(3, player);
                GameObject.Find("MouseHitbox").GetComponent<MouseHitboxScript>().enabled = true;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().player = player;
                GameObject.Find("Minimap").GetComponent<MapGUIRenderer>().whatAmI = 3;
                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().setUpPlayerName(playerName, 3);
                GameObject.Find("NetworkManager").GetComponent<NetworkManager>().sendNameOnJoin(playerName, 3);
                GameObject.Find("GameManager").GetComponent<GameStarter>().gameStarted = true;
                GameObject.Find("GameManager").GetComponent<GameStarter>().whatAmI = 3;
            }
            PhotonNetwork.playerName = playerName;

            inMenu = false;
        }
        servers = serverManager.getServers();
	}



    float native_width = 1920;
    float native_height = 1080;

    GUIStyle buttonStyle = new GUIStyle();
    void OnGUI()
    {

        float rx = Screen.width / native_width;
        float ry = Screen.height / native_height;
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

        if (inMenu)
        {
            GUI.DrawTexture(new Rect(0, 0, background.width, background.height), background);


            //Hauptmenü
            if (mainMenu)
            {
                if (GUI.Button(new Rect(335, 670, button_Play.width, button_Play.height), button_Play,buttonStyle))
                {
					PlayButton1Sound();
                    mainMenu = false;
                    serverBrowser = true;

                }

                if (GUI.Button(new Rect(670, 730, button_Options.width, button_Options.height), button_Options, buttonStyle))
                {
					PlayButton1Sound();
                    mainMenu = false;
                    options = true;

                }


                if (GUI.Button(new Rect(1068, 718, button_Exit.width, button_Exit.height), button_Exit, buttonStyle))
                {
					PlayButton2Sound();
                    Application.Quit();
                }
            }

            //Options
            if (options)
            {
                volume_knob_x = getVolume() * options_Volume_Rect.width + options_Volume_Rect.x;
                Graphics.DrawTexture(options_Rect, options_back);
                Graphics.DrawTexture(volume_Knob_Rect, options_knob);

                if (!dragVolume && options_Volume_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonUp(0))
                {
                    if (Event.current.mousePosition.x >= options_Volume_Rect.x && Event.current.mousePosition.x <= options_Volume_Rect.x + options_Volume_Rect.width)
                    {
                        volume_knob_x = Event.current.mousePosition.x - options_knob.width / 2;
                    }
                }

                if (volume_Knob_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonDown(0))
                {
                    dragVolume = true;
                }

                if (dragVolume)
                {
                    if (Event.current.mousePosition.x - options_knob.width / 2 >= options_Volume_Rect.x && Event.current.mousePosition.x - options_knob.width / 2 <= options_Volume_Rect.x + options_Volume_Rect.width)
                    {
                        volume_knob_x = Event.current.mousePosition.x - options_knob.width / 2;
                    }
                    else if (Event.current.mousePosition.x - options_knob.width / 2 > options_Volume_Rect.x + options_Volume_Rect.width)
                    {
                        volume_knob_x = 1124;
                    }
                    else if (Event.current.mousePosition.x - options_knob.width / 2 < options_Volume_Rect.x)
                    {
                        volume_knob_x = 832;
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        dragVolume = false;
                    }
                }

                ChangeVolume((int)((100f / options_Volume_Rect.width) * (volume_knob_x - 832)));


                scrollspeed_knob_x = (GameObject.Find("Main Camera").GetComponent<Scrolling>().getScaleFactor() * options_Scrollspeed_Rect.width) + options_Scrollspeed_Rect.x;
                Graphics.DrawTexture(scrollspeed_Knob_Rect, options_knob);


                if (!dragScrollspeed && options_Scrollspeed_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonUp(0))
                {
                    if (Event.current.mousePosition.x >= options_Scrollspeed_Rect.x && Event.current.mousePosition.x <= options_Scrollspeed_Rect.x + options_Scrollspeed_Rect.width)
                    {
                        scrollspeed_knob_x = Event.current.mousePosition.x - options_knob.width / 2;

                    }
                }

                if (scrollspeed_Knob_Rect.Contains(Event.current.mousePosition) && Input.GetMouseButtonDown(0))
                {
                    dragScrollspeed = true;
                }

                if (dragScrollspeed)
                {
                    if (Event.current.mousePosition.x - options_knob.width / 2 >= options_Scrollspeed_Rect.x && Event.current.mousePosition.x - options_knob.width / 2 <= options_Scrollspeed_Rect.x + options_Scrollspeed_Rect.width)
                    {
                        scrollspeed_knob_x = Event.current.mousePosition.x - options_knob.width / 2;
                    }
                    else if (Event.current.mousePosition.x - options_knob.width / 2 > options_Scrollspeed_Rect.x + options_Scrollspeed_Rect.width)
                    {
                        scrollspeed_knob_x = 1124;
                    }
                    else if (Event.current.mousePosition.x - options_knob.width / 2 < options_Scrollspeed_Rect.x)
                    {
                        scrollspeed_knob_x = 832;
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        dragScrollspeed = false;
                    }
                }

                GameObject.Find("Main Camera").GetComponent<Scrolling>().changeScrollspeed(((100f / options_Scrollspeed_Rect.width) * (scrollspeed_knob_x - 832))/100);

                if(GUI.Button(new Rect((options_Rect.x+options_Rect.width/2)-options_apply.width/2,options_Rect.y+200,options_apply.width,options_apply.height),options_apply,buttonStyle)){
                    options=false;
                    mainMenu=true;
                }
            }


            //ServerBrowser
            if (serverBrowser)
            {
                //Hintergrund
                GUI.DrawTexture(new Rect(285, 215, window_ServerBrowserBack.width, window_ServerBrowserBack.height), window_ServerBrowserBack);


                float gameEntry_x=341;
                float gameEntry_y=312;


                foreach (GameEntry entry in servers)
                {
                    entry.setRectangle(new Rect(gameEntry_x, gameEntry_y, serverBrowser_Entry.width, serverBrowser_Entry.height));

                    if (entry.getRectangle().Contains(Event.current.mousePosition)&&Input.GetMouseButton(0))
                    {
                        if (currentSelection != null&&entry!=currentSelection)
                        {
                            currentSelection.deselectEntry();
                        }
                        currentSelection = entry;
                        entry.selectEntry();
                    }

                    gameEntryFont.fontSize = 50;
                    gameEntryFont.normal.textColor = Color.black;


                    if (entry.getSelected())
                    {
                        GUI.DrawTexture(entry.getRectangle(), serverBrowser_Entry_Selected);
                        GUI.Label(new Rect(entry.getRectangle().x + 20, entry.getRectangle().y + 10, 100, 40), entry.getRoom().name,gameEntryFont);
                        GUI.Label(new Rect(entry.getRectangle().x + 1040, entry.getRectangle().y + 10, 100, 40), entry.getRoom().playerCount+"/4", gameEntryFont);
                    }
                    else
                    {                     
                        GUI.DrawTexture(entry.getRectangle(), serverBrowser_Entry);
                        GUI.Label(new Rect(entry.getRectangle().x + 20, entry.getRectangle().y + 10, 100, 40), entry.getRoom().name, gameEntryFont);
                        GUI.Label(new Rect(entry.getRectangle().x + 1040, entry.getRectangle().y + 10, 100, 40), entry.getRoom().playerCount + "/4", gameEntryFont);
                        
                    }

                    gameEntry_y += 72;
                }


                //Vordergrund
                GUI.DrawTexture(new Rect(285, 215, window_ServerBrowser.width, window_ServerBrowser.height), window_ServerBrowser);



                if (GUI.Button(new Rect(483, 802, button_Join.width, button_Join.height), button_Join, buttonStyle))
                {
					PlayButton1Sound();
                    if (currentSelection != null)
                    {
                        if (currentSelection.getRoom().playerCount < 4)
                        {
                            PhotonNetwork.JoinRoom(currentSelection.getRoom().name);
                            serverBrowser = false;
                            joinGame = true;
                        }
                    }
                }

                if (GUI.Button(new Rect(760, 833, button_Create.width, button_Create.height), button_Create, buttonStyle))
                {
					PlayButton1Sound();
                    serverBrowser = false;
                    createGame = true;
                }

                if (GUI.Button(new Rect(1100, 822, button_Back.width, button_Back.height), button_Back, buttonStyle))
                {
					PlayButton2Sound();
                    serverBrowser = false;
                    mainMenu = true;
                }
            }

            //JoinGame
            if (joinGame)
            {
                GUI.DrawTexture(new Rect(667, 208, window_Join.width, window_Join.height), window_Join);

                float roleButtons_X=805;
                float roleButtons_width=button_Role_Deamon.width;
                float roleButtons_height=button_Role_Deamon.height;


                //Textfield
                textFieldFont.fontSize = 50;
                textFieldFont.normal.textColor = Color.white;
                playerName = GUI.TextArea(new Rect(938,321,260,52), playerName,10,textFieldFont);
                if (noNick)
                {   textFieldFont.fontSize = 35;
                    textFieldFont.normal.textColor = Color.red;
                    GUI.Label(new Rect(938, (321 + 52 / 2) - textFieldFont.CalcSize(new GUIContent("No Nickname!")).y / 2, textFieldFont.CalcSize(new GUIContent("No Nickname!")).x, textFieldFont.CalcSize(new GUIContent("No Nickname!")).y), "No Nickname!", textFieldFont);
                    if (!playerName.Equals(""))
                    {
                        noNick = false;
                    }
                }

                //Rollen

                    if (daemon || daemon_Blocked)
                    {

                        if (GUI.Button(new Rect(roleButtons_X, 455, roleButtons_width, roleButtons_height), button_Role_Deamon_Selected, buttonStyle) && !daemon_Blocked)
                        {
                            if (!rolePickLocked&&!readyLock)
                            {
                                PlayButton1Sound();
                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", true);

                                if (ranger)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", false);
                                }
                                else if (support)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", false);
                                }
                                else if (tank)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", false);
                                }
                                daemon = true;
                                ranger = false;
                                support = false;
                                tank = false;
                            }
                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(roleButtons_X, 455, roleButtons_width, roleButtons_height), button_Role_Deamon, buttonStyle) && !daemon_Blocked)
                        {
                            if (!rolePickLocked && !readyLock)
                            {
                                PlayButton1Sound();
                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", true);

                                if (ranger)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", false);
                                }
                                else if (support)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", false);
                                }
                                else if (tank)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", false);
                                }
                                daemon = true;
                                ranger = false;
                                support = false;
                                tank = false;
                            }
                        }
                    }

                    if (daemon_ready)
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X - 48, ((455 + roleButtons_height / 2) - ready_unchecked.height / 2) - 10, ready_unchecked.width, ready_unchecked.height), ready_checked);

                    }
                    else
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X-48,((455+roleButtons_height/2)-ready_unchecked.height/2)-10,ready_unchecked.width,ready_unchecked.height), ready_unchecked);
                    }


                    if (ranger || ranger_Blocked)
                    {
                        if (GUI.Button(new Rect(roleButtons_X, 595, roleButtons_width, roleButtons_height), button_Role_Ranger_Selected, buttonStyle) && !ranger_Blocked)
                        {
                            if (!rolePickLocked && !readyLock)
                            {
                                PlayButton1Sound();
                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", true);
                                if (daemon)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", false);
                                }
                                else if (support)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", false);
                                }
                                else if (tank)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", false);
                                }

                                daemon = false;
                                ranger = true;
                                support = false;
                                tank = false;
                            }

                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(roleButtons_X, 595, roleButtons_width, roleButtons_height), button_Role_Ranger, buttonStyle) && !ranger_Blocked)
                        {
                            if (!rolePickLocked && !readyLock)
                            {
                                PlayButton1Sound();

                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", true);

                                if (daemon)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", false);
                                }
                                else if (support)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", false);
                                }
                                else if (tank)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", false);
                                }
                                daemon = false;
                                ranger = true;
                                support = false;
                                tank = false;
                            }
                        }
                    }

                    if (ranger_ready)
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X - 48, ((595 + roleButtons_height / 2) - ready_unchecked.height / 2) - 10, ready_unchecked.width, ready_unchecked.height), ready_checked);

                    }
                    else
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X - 48, ((595 + roleButtons_height / 2) - ready_unchecked.height / 2) - 10, ready_unchecked.width, ready_unchecked.height), ready_unchecked);
                    }


                    if (support || support_Blocked)
                    {
                        if (GUI.Button(new Rect(roleButtons_X, 675, roleButtons_width, roleButtons_height), button_Role_Support_Selected, buttonStyle) && !support_Blocked)
                        {
                            if (!rolePickLocked && !readyLock)
                            {
                                PlayButton1Sound();
                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", true);

                                if (daemon)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", false);
                                }
                                else if (ranger)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", false);
                                }
                                else if (support)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", false);
                                }
                                daemon = false;
                                ranger = false;
                                support = true;
                                tank = false;
                            }
                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(roleButtons_X, 675, roleButtons_width, roleButtons_height), button_Role_Support, buttonStyle) && !support_Blocked)
                        {
                            if (!rolePickLocked && !readyLock)
                            {
                                PlayButton1Sound();
                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", true);

                                if (daemon)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", false);
                                }
                                else if (ranger)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", false);
                                }
                                else if (tank)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", false);
                                }
                                daemon = false;
                                ranger = false;
                                support = true;
                                tank = false;
                            }
                        }
                    }

                    if (support_ready)
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X - 48, ((675 + roleButtons_height / 2) - ready_unchecked.height / 2) - 10, ready_unchecked.width, ready_unchecked.height), ready_checked);

                    }
                    else
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X - 48, ((675 + roleButtons_height / 2) - ready_unchecked.height / 2) - 10, ready_unchecked.width, ready_unchecked.height), ready_unchecked);
                    }

                    if (tank || tank_Blocked)
                    {
                        if (GUI.Button(new Rect(roleButtons_X, 755, roleButtons_width, roleButtons_height), button_Role_Tank_Selected, buttonStyle) && !tank_Blocked)
                        {
                            if (!rolePickLocked&&!readyLock)
                            {
                                PlayButton1Sound();
                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", true);

                                if (daemon)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", false);
                                }
                                else if (ranger)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", false);
                                }
                                else if (support)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", false);
                                }
                                daemon = false;
                                ranger = false;
                                support = false;
                                tank = true;
                            }
                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(roleButtons_X, 755, roleButtons_width, roleButtons_height), button_Role_Tank, buttonStyle) && !tank_Blocked)
                        {
                            if (!rolePickLocked && !readyLock)
                            {
                                PlayButton1Sound();
                                photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Tank", true);

                                if (daemon)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Daemon", false);
                                }
                                else if (ranger)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Ranger", false);
                                }
                                else if (support)
                                {
                                    photonView.RPC("setRoleBlocked", PhotonTargets.MasterClient, "Support", false);
                                }

                                daemon = false;
                                ranger = false;
                                support = false;
                                tank = true;
                            }            
                        }
                    }

                    if (tank_ready)
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X - 48, ((755 + roleButtons_height / 2) - ready_unchecked.height / 2) - 10, ready_unchecked.width, ready_unchecked.height), ready_checked);

                    }
                    else
                    {
                        Graphics.DrawTexture(new Rect(roleButtons_X - 48, ((755 + roleButtons_height / 2) - ready_unchecked.height / 2) - 10, ready_unchecked.width, ready_unchecked.height), ready_unchecked);
                    }

                //Menü
                if (GUI.Button(new Rect(658, 820, button_Ready.width, button_Ready.height), button_Ready, buttonStyle))
                {
					PlayButton1Sound();
                    if (!playerName.Equals(""))
                    {
                        if (daemon)
                        {
                            if(daemon_ready)
                            {
                                readyLock = false;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Daemon", false);
                            }
                            else
                            {
                                readyLock = true;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Daemon", true);
                            }
                        }
                        else if (ranger)
                        {
                            if (ranger_ready)
                            {
                                readyLock = false;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Ranger", false);
                            }
                            else
                            {
                                readyLock = true;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Ranger", true);
                            }
                        }
                        else if (support)
                        {
                            if (support_ready)
                            {
                                readyLock = false;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Support", false);
                            }
                            else
                            {
                                readyLock = true;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Support", true);
                            }
                        }
                        else if (tank)
                        {
                            if (tank_ready)
                            {
                                readyLock = false;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Tank", false);
                            }
                            else
                            {
                                readyLock = true;
                                photonView.RPC("setRoleReady", PhotonTargets.MasterClient, "Tank", true);
                            }
                        }
                        noNick = false;
                    }
                    else
                    {
                        noNick = true;
                    }
                }

                if (GUI.Button(new Rect(898, 823, button_Leave.width, button_Leave.height), button_Leave, buttonStyle))
                {
                    reset();
                }
            }

            //CreateGame

            if (createGame)
            {
                GUI.DrawTexture(new Rect(670, 406, window_Create.width, window_Create.height), window_Create);

                textFieldFont.fontSize = 50;
                textFieldFont.normal.textColor = Color.white;
                roomName = GUI.TextArea(new Rect(936, 520, 260, 52), roomName,10,textFieldFont);
                if (noNick)
                {
                    textFieldFont.fontSize = 35;
                    textFieldFont.normal.textColor = Color.red;
                    GUI.Label(new Rect(939, (520 + 52 / 2) - textFieldFont.CalcSize(new GUIContent("No Game name!")).y / 2, textFieldFont.CalcSize(new GUIContent("No Game name!")).x, textFieldFont.CalcSize(new GUIContent("No Game name!")).y), "No Game name!", textFieldFont);
                    if (!roomName.Equals(""))
                    {
                        noNick = false;
                    }
                }

                if (GUI.Button(new Rect(658, 800, button_Okay.width, button_Okay.height), button_Okay, buttonStyle))
                {
					PlayButton1Sound();
                    if (!roomName.Equals(""))
                    {
                        PhotonNetwork.CreateRoom(roomName);
                        PhotonNetwork.SetMasterClient(PhotonNetwork.player);
                        createGame = false;
                        joinGame = true;
                        noNick = false;
                    }
                    else
                    {
                        noNick = true;
                    }
                }

                if (GUI.Button(new Rect(898, 803, button_Back.width, button_Back.height), button_Back, buttonStyle))
                {
					PlayButton2Sound();
                    createGame = false;
                    serverBrowser = true;
                    noNick = false;
                }
            }

            if (playerDisc)
            {
                Rect playerDisc_Rect = new Rect(1920 / 2 - playerDisconnected.width / 2, 1080 / 2 - playerDisconnected.height / 2, playerDisconnected.width, playerDisconnected.height);
                Graphics.DrawTexture(playerDisc_Rect,playerDisconnected);
                if(GUI.Button(new Rect((playerDisc_Rect.x+playerDisc_Rect.width/2)-button_Continue.width/2,playerDisc_Rect.y+185,button_Continue.width,button_Continue.height),button_Continue,buttonStyle)){
                    playerDisc = false;
                }
            }
        }
    }

    void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("initRoles", player,daemon_Blocked,ranger_Blocked,support_Blocked,tank_Blocked,daemon_ready,ranger_ready,support_ready,tank_ready);
        }
    }




    [RPC]
    void initRoles(bool daemon, bool ranger, bool support, bool tank,bool daemon_ready, bool ranger_ready,bool support_ready,bool tank_ready)
    {
        if (!PhotonNetwork.isMasterClient)
        {
            daemon_Blocked = daemon;
            ranger_Blocked = ranger;
            support_Blocked = support;
            tank_Blocked = tank;
            this.daemon_ready = daemon_ready;
            this.ranger_ready = ranger_ready;
            this.support_ready = support_ready;
            this.tank_ready = tank_ready;
            rolePickLocked = false;
        }
    }


    [RPC]
    void setRoleBlocked(string role, bool state)
    {
        if (PhotonNetwork.isMasterClient)
        {
            if (role.Equals("Daemon"))
            {

                if (!daemon_Blocked&&state)
                {
                    daemon_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others,"Daemon",state);
                }
                else if (daemon_Blocked&&!state)
                {
                    daemon_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others, "Daemon", state);
                }
            }
            else if (role.Equals("Ranger"))
            {
                if (!ranger_Blocked)
                {
                    ranger_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others, "Ranger", state);
                }
                else if (ranger_Blocked && !state)
                {
                    ranger_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others, "Ranger", state);
                }
            }
            else if (role.Equals("Support"))
            {
                if (!support_Blocked)
                {
                    support_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others, "Support", state);
                }
                else if (support_Blocked && !state)
                {
                    support_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others, "Support", state);
                }
            }
            else if (role.Equals("Tank"))
            {
                if (!tank_Blocked)
                {
                    tank_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others, "Tank", state);
                }
                else if (tank_Blocked && !state)
                {
                    tank_Blocked = state;
                    photonView.RPC("setRoleBlocked", PhotonTargets.Others, "Tank", state);
                }
            }
        }
        else
        {
            if (role.Equals("Daemon"))
            {         
                daemon_Blocked = state;
            }
            else if (role.Equals("Ranger"))
            {
                ranger_Blocked = state;
            }
            else if (role.Equals("Support"))
            {
                support_Blocked = state;
            }
            else if (role.Equals("Tank"))
            {
                tank_Blocked = state;
            }
        }
    }

    [RPC]
    void setRoleReady(string role,bool state)
    {
        if (PhotonNetwork.isMasterClient)
        {
            if (role.Equals("Daemon"))
            {

                if (state)
                {
                    daemon_ready= state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Daemon", state);
                }
                else if (!state)
                {
                    daemon_ready = state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Daemon", state);
                }
            }else if (role.Equals("Ranger"))
            {

                if (state)
                {
                    ranger_ready = state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Ranger", state);
                }
                else if (!state)
                {
                    ranger_ready = state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Ranger", state);
                }
            }
            else if (role.Equals("Support"))
            {

                if (state)
                {
                    support_ready = state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Support", state);
                }
                else if (!state)
                {
                    support_ready = state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Support", state);
                }
            }
            else if (role.Equals("Tank"))
            {

                if (state)
                {
                    tank_ready = state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Tank", state);
                }
                else if (!state)
                {
                    tank_ready = state;
                    photonView.RPC("setRoleReady", PhotonTargets.Others, "Tank", state);
                }
            }
        }
        else
        {
            if (role.Equals("Daemon"))
            {
                daemon_ready = state;
            }
            else if (role.Equals("Ranger"))
            {
                ranger_ready = state;
            }
            else if (role.Equals("Support"))
            {
                support_ready = state;
            }
            else if (role.Equals("Tank"))
            {
                tank_ready = state;
            }
        }
    }



	public void ChangeVolume(int percentageVolume)
	{
        int temp_Volume = percentageVolume;

        if (percentageVolume > 100)
        {
            temp_Volume = 100;
        }
        else if (percentageVolume < 0)
        {
            temp_Volume = 0;
        }
        scaleVolumeFactor = (float)temp_Volume / 100;
	}

    public float getVolume()
    {
        return scaleVolumeFactor;
    }

	void PlayButton1Sound()
	{
		AudioSource.PlayClipAtPoint(blipAudio, transform.position, 0.25f*scaleVolumeFactor);
	}
	void PlayButton2Sound()
	{
		AudioSource.PlayClipAtPoint(blipAudio2, transform.position, 0.25f*scaleVolumeFactor);
	}
}
