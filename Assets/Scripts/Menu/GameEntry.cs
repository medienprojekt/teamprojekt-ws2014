﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Menu
{
    public class GameEntry
    {
        public RoomInfo room;
        public Rect rectangle;
        public bool selected;

        public GameEntry(RoomInfo room)
        {
            this.room = room;
            selected = false;
            rectangle = new Rect(0, 0, 0, 0);
        }

        public void setRectangle(Rect rectangle)
        {
            this.rectangle = rectangle;
        }

        public Rect getRectangle()
        {
            return rectangle;
        }

        public void selectEntry()
        {
            selected = true;
        }

        public void deselectEntry()
        {
            selected = false;
        }

        public bool getSelected()
        {
            return selected;
        }

        public RoomInfo getRoom()
        {
            return room;
        }
    }
}
