﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Menu;
using System.Collections.Generic;



namespace Assets.Scripts.Menu
{
   public class ServerBrowser : Photon.MonoBehaviour
    {
        public RoomInfo[] rooms;
        public List<GameEntry> servers;

        public bool newServer;
        public bool serverExists;


        void Start()
        {
            rooms = PhotonNetwork.GetRoomList();
            servers = new List<GameEntry>();          
        }


        public List<GameEntry> getServers()
        {
            return servers;
        }


        void Update()
        {

            rooms = PhotonNetwork.GetRoomList();
            //Neue Server finden
            for (int i = 0; i < rooms.Length; ++i)
            {
                if (rooms[i] != null)
                {
                    newServer = true;
                    foreach (GameEntry entry in servers) {
                        if (rooms[i] == entry.room)
                        {
                            newServer = false;
                        }
                    }

                    if (newServer)
                    {
                        servers.Add(new GameEntry(rooms[i]));
                    }
                }
            }


            //Geschlossene Server löschen

            //temporäre Liste erstellen
            List<GameEntry> temp_Servers=new List<GameEntry>();

            foreach (GameEntry entry in servers)
            {
                temp_Servers.Add(entry);
            }

            foreach (GameEntry entry in servers)
            {
                serverExists = false;
                for (int i = 0; i < rooms.Length; ++i)
                {
                    if (entry.room == rooms[i])
                    {
                        serverExists = true;
                    }
                }
                if (!serverExists)
                {
                    temp_Servers.Remove(entry);
                }
            }

            //aus temporärer Liste zurück kopieren
            servers.Clear();
            foreach (GameEntry entry in temp_Servers)
            {
                servers.Add(entry);
            }

        }
    }
}
