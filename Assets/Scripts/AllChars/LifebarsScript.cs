﻿using UnityEngine;
using System.Collections;

public class LifebarsScript : MonoBehaviour {

    public string playerName;
    public Texture lifebar;
    public Texture lifebar_Neutral;
    public Texture lifebar_Back;
    public Texture lifebar_shield;
    private Rect lifebar_Rect;
    private Rect shield_Rect;
    private Rect playerName_Rect;
    private Rect lifeNumber_Rect;
    private Rect lifeNumber_Rect_shadow;
    private int whatAmI;
    private float maxWidth;
    private float maxHeight;
    public float lifePercent;
    private float shieldPercent;
    private int maxLife;
    private int currentLife;
    Vector3 position;
    private float yOffset;
    GUIStyle style;


    public bool drawShield;
    private bool drawLife;
	// Use this for initialization
	void Start () {
        maxWidth = 200;
        maxHeight = 40;
        yOffset = 0;
        style = new GUIStyle();
        drawLife = true;
	}

    float native_width = 1920;
    float native_height = 1080;
    float rx;
    float ry;
	// Update is called once per frame
	void Update () {
        rx = Screen.width / native_width;
        ry = Screen.height / native_height;
        maxWidth = 200;
        maxHeight = 40;
        maxWidth *= rx;
        maxHeight *= ry;

        position = Camera.main.WorldToScreenPoint(transform.position);

        position.y = Screen.height - position.y;
        if (name == "Ranger")
        {
            if (GetComponent<RangerAttributesScript>().curHealth <= 260)
            {
                lifePercent = (100 / GetComponent<RangerAttributesScript>().maxHealth * 260) / 100;
            }
            else
            {
                lifePercent = (100 / GetComponent<RangerAttributesScript>().maxHealth * GetComponent<RangerAttributesScript>().curHealth) / 100;
            }

            if (GetComponent<RangerAttributesScript>().curHealth <= 0)
            {
                drawLife = false;
            }
            else
            {
                drawLife = true;
            }

            if (GetComponent<RangerAttributesScript>().shieldAmount <= 285)
            {
                shieldPercent = (100 / GetComponent<RangerAttributesScript>().maxHealth * 285) / 100;
            }
            else
            {
                shieldPercent = (100 / GetComponent<RangerAttributesScript>().maxHealth * GetComponent<RangerAttributesScript>().shieldAmount) / 100;             
            }

            if (GetComponent<RangerAttributesScript>().shieldAmount <= 0)
            {
                drawShield = false;
            }
            else
            {
                drawShield = true;
            }

            yOffset = 100*ry;
            playerName = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().getPlayerName(1);
            maxLife = Mathf.CeilToInt(GetComponent<RangerAttributesScript>().maxHealth);
            currentLife = Mathf.CeilToInt(GetComponent<RangerAttributesScript>().curHealth);
        }
        else if (name == "Healer")
        {
            if (GetComponent<HealerAttributesScript>().curHealth <= 220)
            {
                lifePercent = (100 / GetComponent<HealerAttributesScript>().maxHealth * 220) / 100;
            }
            else if (GetComponent<HealerAttributesScript>().curHealth <= 0)
            {
                lifePercent = 0f;
            }
            else
            {
                lifePercent = (100 / GetComponent<HealerAttributesScript>().maxHealth * GetComponent<HealerAttributesScript>().curHealth) / 100;
            }

            if (GetComponent<HealerAttributesScript>().curHealth <= 0)
            {
                drawLife = false;
            }
            else
            {
                drawLife = true;
            }

            if (GetComponent<HealerAttributesScript>().shieldAmount <= 240)
            {
                shieldPercent = (100 / GetComponent<HealerAttributesScript>().maxHealth * 240) / 100;
            }
            else
            {
                shieldPercent = (100 / GetComponent<HealerAttributesScript>().maxHealth * GetComponent<HealerAttributesScript>().shieldAmount) / 100;
            }

            if (GetComponent<HealerAttributesScript>().shieldAmount <= 0)
            {
                drawShield = false;
            }
            else
            {
                drawShield = true;
            }

            yOffset = 100 * ry;
            playerName = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().getPlayerName(2);

            maxLife = Mathf.CeilToInt(GetComponent<HealerAttributesScript>().maxHealth);
            currentLife = Mathf.CeilToInt(GetComponent<HealerAttributesScript>().curHealth);

        }
        else if (name == "Tank")
        {
            if (GetComponent<TankAttributesScript>().curHealth <= 490)
            {
                lifePercent = (100 / GetComponent<TankAttributesScript>().maxHealth * 490) / 100;
            }
            else
            {
                lifePercent = (100 / GetComponent<TankAttributesScript>().maxHealth * GetComponent<TankAttributesScript>().curHealth) / 100;
            }

            if (GetComponent<TankAttributesScript>().curHealth <= 0)
            {
                drawLife = false;
            }
            else
            {
                drawLife = true;
            }

            if (GetComponent<TankAttributesScript>().shieldAmount <= 535)
            {
                shieldPercent = (100 / GetComponent<TankAttributesScript>().maxHealth * 535) / 100;
            }
            else
            {
                shieldPercent = (100 / GetComponent<TankAttributesScript>().maxHealth * GetComponent<TankAttributesScript>().shieldAmount) / 100;
            }

            if (GetComponent<TankAttributesScript>().shieldAmount <= 0)
            {
                drawShield = false;
            }
            else
            {
                drawShield = true;
            }


            yOffset = 100 * ry;
            playerName = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().getPlayerName(3);
            maxLife = Mathf.CeilToInt(GetComponent<TankAttributesScript>().maxHealth);
            currentLife = Mathf.CeilToInt(GetComponent<TankAttributesScript>().curHealth);

        }
        else if (name == "Monster")
        {
            if (GetComponent<MonsterAttributesScript>().curHealth <= 2500)
            {
                lifePercent = (100 / GetComponent<MonsterAttributesScript>().maxHealth * 2500) / 100;
            }
            else
            {
                lifePercent = (100 / GetComponent<MonsterAttributesScript>().maxHealth * GetComponent<MonsterAttributesScript>().curHealth) / 100;
            }

            if (GetComponent<MonsterAttributesScript>().curHealth <= 0)
            {
                drawLife = false;
            }
            else
            {
                drawLife = true;
            }

            yOffset = 140 * ry;
            playerName = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().getPlayerName(0);
            maxLife = Mathf.CeilToInt(GetComponent<MonsterAttributesScript>().maxHealth);
            currentLife = Mathf.CeilToInt(GetComponent<MonsterAttributesScript>().curHealth);
        }

        lifebar_Rect = new Rect(position.x - (maxWidth / 2), position.y - yOffset, maxWidth * lifePercent, maxHeight);
        shield_Rect = new Rect(position.x - (maxWidth / 2), position.y - yOffset, maxWidth * shieldPercent, maxHeight);
        lifeNumber_Rect = new Rect();
 

        style.fontSize = (int)(35 * rx);
        style.normal.textColor = Color.white;
        //style.fontStyle = FontStyle.Bold;
        playerName_Rect = new Rect((lifebar_Rect.x + lifebar_Rect.width / 2) - style.CalcSize(new GUIContent(playerName)).x / 2, lifebar_Rect.y - style.CalcSize(new GUIContent(playerName)).y, style.CalcSize(new GUIContent(playerName)).x*rx, style.CalcSize(new GUIContent(playerName)).y*ry);


        style.fontSize = (int)(25* rx);
        style.normal.textColor = Color.white;
        lifeNumber_Rect = new Rect((lifebar_Rect.x + maxWidth / 2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x / 2, (lifebar_Rect.y + maxHeight / 2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y / 2, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y);

        lifeNumber_Rect_shadow = new Rect(((lifebar_Rect.x + maxWidth / 2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x / 2)+2, ((lifebar_Rect.y + maxHeight / 2) - style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y / 2)+2, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).x, style.CalcSize(new GUIContent("" + currentLife + "/" + maxLife)).y);


	}

    public void setPlayerName(string name)
    {
        this.playerName = name;
    }

    void OnGUI()
    {
        if (GetComponent<SpriteRenderer>().enabled)
        {
            GUI.Label(playerName_Rect, playerName, style);
            Graphics.DrawTexture(new Rect(lifebar_Rect.x, lifebar_Rect.y, maxWidth, maxHeight), lifebar_Back, 12, 12, 0, 0);
            Graphics.DrawTexture(new Rect(lifebar_Rect.x, lifebar_Rect.y, maxWidth, maxHeight), lifebar_Neutral, 14, 14, 0, 0);
            if (drawLife)
            {
                Graphics.DrawTexture(lifebar_Rect, lifebar, 14, 14, 0, 0);
            }

            if (drawShield)
            {
                Graphics.DrawTexture(shield_Rect, lifebar_shield, 14, 14, 0, 0);

            }
            style.normal.textColor = Color.black;
            GUI.Label(lifeNumber_Rect_shadow, "" + currentLife + "/" + maxLife, style);
            style.normal.textColor = Color.white;
            GUI.Label(lifeNumber_Rect, "" + currentLife + "/" + maxLife, style);
        }
    }
}
