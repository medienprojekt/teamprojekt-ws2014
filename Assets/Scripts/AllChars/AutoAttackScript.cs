﻿using UnityEngine;
using System.Collections;

public class AutoAttackScript : Photon.MonoBehaviour {

    public float projectileSpeed = 0.2f;
    public string team;
    public string myOriginator;
    private Vector3 richtungsVektor;
    public Vector3 target;
    public float damage;
    public int targetViewID;
    public bool altar;
    public bool rangerStack;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void FixedUpdate() {

        //position updaten
        if (PhotonView.Find(targetViewID) != null) {
            target = PhotonView.Find(targetViewID).transform.position;


            richtungsVektor = (target - transform.position) / Vector3.Distance(target, transform.position);

            transform.position = transform.position + richtungsVektor * projectileSpeed;
            Vector3 bla = transform.position;
            bla.z = 2;
            transform.position = bla;

            Vector3 direction = target - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            angle += 180.0f;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        else {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider collider) {

        if (!altar) {

            // lokal für clienten
            if (team.Equals("Team") && collider.transform.tag.Equals("Monster")) {
                transform.renderer.enabled = false;
            }
            else if (team.Equals("Monster") && collider.transform.tag.Equals("Team")) {
                transform.renderer.enabled = false;
            }

            //Master
            if (PhotonNetwork.isMasterClient) {

                if (team.Equals("Team") && collider.transform.tag.Equals("Monster")) {


                    //leben anpassen im master
                    collider.gameObject.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(-damage);

                    if (rangerStack) {
                        photonView.RPC("rangerStackIncrease", PhotonTargets.All);
                    }

                    //zerstören
                    PhotonNetwork.Destroy(this.gameObject);
                }

                if (team.Equals("Monster") && collider.transform.tag.Equals("Team")) {

                    if (collider.gameObject.GetComponent<RangerAttributesScript>() != null) {
                        collider.gameObject.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, true);
                    }
                    else if (collider.gameObject.GetComponent<TankAttributesScript>() != null) {
                        collider.gameObject.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, true);
                    }
                    else if (collider.gameObject.GetComponent<HealerAttributesScript>() != null) {
                        collider.gameObject.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, true);
                    }

                    //zerstören
                    PhotonNetwork.Destroy(this.gameObject);
                }
            }

        }
        else {

            //Master
            if (PhotonNetwork.isMasterClient) {
                if (collider.transform.tag.Equals("Altar")) {
                    collider.GetComponent<AltarsScript>().getDamage();
                    PhotonNetwork.Destroy(this.gameObject);
                }
            }
        }
    }

    void OnDestroy() {
        transform.renderer.enabled = false;
    }

    [RPC]
    void rangerStackIncrease() {
        GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().increasePassivStack();
    }

}
