﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InviChecker : Photon.MonoBehaviour {

    private FogOfWar fogOfWar;
    private List<GameObject> bushList;
    private Menu menu;
    private Color saveColor;

    private bool attackRevealRanger;
    private bool attackRevealHealer;
    private bool attackRevealTank;
    private float attackRevealRangerTimer;
    private float attackRevealHealerTimer;
    private float attackRevealTankTimer;
    private bool attackRevealMonster;
    private float attackRevealMonsterTimer;

	// Use this for initialization
	void Start () {
        if (photonView.isMine) {

            menu = GameObject.Find("Menu").GetComponent<Menu>();

            fogOfWar = GameObject.Find("FogOfWar").GetComponent<FogOfWar>();

            bushList = new List<GameObject>();

            //bush liste füllen
            //bushList.Add(GameObject.Find("Bush"));
            GameObject[] gos =  GameObject.FindGameObjectsWithTag("Bush");

            foreach (GameObject go in gos) {
                bushList.Add(go);
            }

            //bushList.Count (um länge zu bekommen)
            saveColor = renderer.material.color;

        }
        else {
            enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {

        //monster
        if (menu.iAmOnTeam == 2) {
            //unsichtbarkeit für Ranger
            if (GameObject.Find("Ranger") != null) {
                GameObject ranger = GameObject.Find("Ranger");

                //FOW checkup
                ranger.renderer.enabled = fogOfWar.IsRevealed(ranger.transform.position);

                //ranger skill unsichtbarkeit zuende
                if (ranger.renderer.enabled && !ranger.GetComponent<RangerInvisibleAfterSkill>().iAmInvisible
                            && !ranger.renderer.enabled) {
                    ranger.renderer.enabled = true;
                }

                //bushList checkup
                if (ranger.renderer.enabled && !ranger.GetComponent<RangerInvisibleAfterSkill>().iAmInvisible) {
                    foreach (GameObject bush in bushList) {
                        if (bush.GetComponent<BrushScript>().rangerInBrush) {
                            ranger.renderer.enabled = false;
                            if (bush.GetComponent<BrushScript>().monsterInBrush) {
                                ranger.renderer.enabled = true;
                            }
                            if (attackRevealRanger) {
                                ranger.renderer.enabled = true;
                            }
                            break;
                        }
                        else {
                            ranger.renderer.enabled = true;
                        }
                    }
                }


                //ranger skill unsichtbarkeit
                if (ranger.GetComponent<RangerInvisibleAfterSkill>().iAmInvisible) {
                    ranger.renderer.enabled = false;
                }

            }


            //unsichtbarkeit für Healer
            if (GameObject.Find("Healer") != null) {
                GameObject healer = GameObject.Find("Healer");

                //FOW checkup
                healer.renderer.enabled = fogOfWar.IsRevealed(healer.transform.position);

                //bushList checkup
                if (healer.renderer.enabled) {
                    foreach (GameObject bush in bushList) {
                        if (bush.GetComponent<BrushScript>().healerInBrush) {
                            healer.renderer.enabled = false;
                            if (bush.GetComponent<BrushScript>().monsterInBrush) {
                                healer.renderer.enabled = true;
                            }
                            if (attackRevealHealer) {
                                healer.renderer.enabled = true;
                            }
                            break;
                        }
                        else {
                            healer.renderer.enabled = true;
                        }
                    }
                }

            }

            //unsichtbarkeit für Tank
            if (GameObject.Find("Tank") != null) {
                GameObject tank = GameObject.Find("Tank");

                //FOW checkup
                tank.renderer.enabled = fogOfWar.IsRevealed(tank.transform.position);

                //bushList checkup
                if (tank.renderer.enabled) {
                    foreach (GameObject bush in bushList) {
                        if (bush.GetComponent<BrushScript>().tankInBrush) {
                            tank.renderer.enabled = false;
                            if (bush.GetComponent<BrushScript>().monsterInBrush) {
                                tank.renderer.enabled = true;
                            }
                            if (attackRevealTank) {
                                tank.renderer.enabled = true;
                            }
                            break;
                        }
                        else {
                            tank.renderer.enabled = true;
                        }
                    }
                }
            }

        }



        if (menu.iAmOnTeam == 1) {


            if (GameObject.Find("Monster") != null) {
                GameObject monster = GameObject.Find("Monster");

                //FOW checkup
                monster.renderer.enabled = fogOfWar.IsRevealed(monster.transform.position);

                //bushList checkup
                if (monster.renderer.enabled) {
                    foreach (GameObject bush in bushList) {
                        if (bush.GetComponent<BrushScript>().monsterInBrush) {
                            monster.renderer.enabled = false;
                            if (bush.GetComponent<BrushScript>().rangerInBrush || bush.GetComponent<BrushScript>().healerInBrush || bush.GetComponent<BrushScript>().tankInBrush) {
                                monster.renderer.enabled = true;
                            }
                            if (attackRevealMonster) {
                                monster.renderer.enabled = true;
                            }
                            break;
                        }
                        else {
                            monster.renderer.enabled = true;
                        }
                    }
                }

                if (!monster.renderer.enabled) {

                    if (menu.iAm == "Healer") {
                        GetComponent<HealerControlScript>().lostMonsterSight();
                    }
                    if (menu.iAm == "Tank") {
                        GetComponent<TankControlScript>().lostMonsterSight();
                    }
                    if (menu.iAm == "Ranger") {
                        GetComponent<RangerControlScript>().lostMonsterSight();
                    }

                }


            }

           
        }



        //alpha
        foreach (GameObject bush in bushList) {
            if (GameObject.Find("Healer") != null) {
                GameObject healer = GameObject.Find("Healer");
                if (bush.GetComponent<BrushScript>().healerInBrush) {
                    healer.renderer.material.color = new Color(saveColor.r, saveColor.g, saveColor.b, 0.6f);
                    break;
                }
                else {
                    healer.renderer.material.color = saveColor;
                }
            }
        }

        foreach (GameObject bush in bushList) {
            if (GameObject.Find("Tank") != null) {
                GameObject tank = GameObject.Find("Tank");
                if (bush.GetComponent<BrushScript>().tankInBrush) {
                    tank.renderer.material.color = new Color(saveColor.r, saveColor.g, saveColor.b, 0.6f);
                    break;
                }
                else {
                    tank.renderer.material.color = saveColor;
                }
            }
        }

        foreach (GameObject bush in bushList) {
            if (GameObject.Find("Ranger") != null) {
                GameObject ranger = GameObject.Find("Ranger");
                if (bush.GetComponent<BrushScript>().rangerInBrush || ranger.GetComponent<RangerInvisibleAfterSkill>().iAmInvisible) {
                    ranger.renderer.material.color = new Color(saveColor.r, saveColor.g, saveColor.b, 0.6f);
                    break;
                }
                else {
                    ranger.renderer.material.color = saveColor;
                }
            }
        }


        //alpha
        foreach (GameObject bush in bushList) {
            if (GameObject.Find("Monster") != null) {
                GameObject monster = GameObject.Find("Monster");
                if (bush.GetComponent<BrushScript>().monsterInBrush) {
                    monster.renderer.material.color = new Color(saveColor.r, saveColor.g, saveColor.b, 0.6f);
                    break;
                }
                else {
                    monster.renderer.material.color = saveColor;
                }
            }
        }



        if (attackRevealRanger && Time.time > attackRevealRangerTimer) {
            attackRevealRanger = false;
        }
        if (attackRevealTank && Time.time > attackRevealTankTimer) {
            attackRevealTank = false;
        }
        if (attackRevealHealer && Time.time > attackRevealHealerTimer) {
            attackRevealHealer = false;
        }
        if (attackRevealMonster && Time.time > attackRevealMonsterTimer) {
            attackRevealMonster = false;
        }
	}

    public void revealRanger() {
        attackRevealRanger = true;
        attackRevealRangerTimer = Time.time + 1.5f;
    }
    public void revealTank() {
        attackRevealTank = true;
        attackRevealTankTimer = Time.time + 1.5f;
    }
    public void revealHealer() {
        attackRevealHealer = true;
        attackRevealHealerTimer = Time.time + 1.5f;
    }
    public void revealMonster() {
        attackRevealMonster = true;
        attackRevealMonsterTimer = Time.time + 1.5f;
    }

}
