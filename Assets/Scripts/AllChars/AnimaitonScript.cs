﻿using UnityEngine;
using System.Collections;

public class AnimaitonScript : Photon.MonoBehaviour {

    private Animator animator;
    private Movement movementScript;
    private float winkelAlpha;
    float laengeA;
    float laengeB;
    float laengeC;
    private int iAm; //1 ranger, 2 healer, 3 tank, 4 monster

    public bool isAttacking;

    public Sprite deathSprite;

    public Vector3 target;

    // Use this for initialization
    void Start() {
        isAttacking = false;
        animator = this.GetComponent<Animator>();
        movementScript = this.GetComponent<Movement>();

        if (this.GetComponent<RangerAttributesScript>() != null) {
            iAm = 1;
        }
        else if (this.GetComponent<HealerAttributesScript>() != null) {
            iAm = 2;
        }
        else if (this.GetComponent<TankAttributesScript>() != null) {
            iAm = 3;
        }
        else if (this.GetComponent<MonsterAttributesScript>() != null) {
            iAm = 4;
        }

        if (photonView.isMine) {
 



        } else {
            //enabled = false;
        }

    }

    // Update is called once per frame
    void Update() {
        if (photonView.isMine) {
            //if (!isAttacking) {
            winkelBerechnung();

            if (movementScript.moving) {
                animator.SetBool("Idle", false);
                target = movementScript.target;
            }
            else {
                animator.SetBool("Idle", true);
            }

            setDirection();
        }

        //chechup tod
        if (iAm == 1 && this.GetComponent<Animator>().enabled) {
            if (!this.GetComponent<RangerAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = false;
                this.GetComponent<SpriteRenderer>().sprite = deathSprite;
            }
        }
        else if (iAm == 2 && this.GetComponent<Animator>().enabled) {
            if (!this.GetComponent<HealerAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = false;
                this.GetComponent<SpriteRenderer>().sprite = deathSprite;
            }
        }
        else if (iAm == 3 && this.GetComponent<Animator>().enabled) {
            if (!this.GetComponent<TankAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = false;
                this.GetComponent<SpriteRenderer>().sprite = deathSprite;
            }
        }
        else if (iAm == 4 && this.GetComponent<Animator>().enabled) {
            if (!this.GetComponent<MonsterAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = false;
                this.GetComponent<SpriteRenderer>().sprite = deathSprite;
            }
        }
        

        //chechup tod
        if (iAm == 1 && !this.GetComponent<Animator>().enabled) {
            if (this.GetComponent<RangerAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = true;
            }
        }
        else if (iAm == 2 && !this.GetComponent<Animator>().enabled) {
            if (this.GetComponent<HealerAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = true;
            }
        }
        else if (iAm == 3 && !this.GetComponent<Animator>().enabled) {
            if (this.GetComponent<TankAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = true;
            }
        }
        else if (iAm == 4 && !this.GetComponent<Animator>().enabled) {
            if (this.GetComponent<MonsterAttributesScript>().isAlive) {
                this.GetComponent<Animator>().enabled = true;
            }
        }
    }

    public void lookAt(Vector3 v3) {
        target = v3;
    }

    public void startAttackAnimation() {
        if (photonView.isMine) {
            animator.SetBool("BreakoutAttack", false);
            if (isAttacking) {
                photonView.RPC("AnimatorAttackTrigger", PhotonTargets.All);
            }

            isAttacking = true;
            animator.SetBool("Attacking", true);
        }
    }

    public void stopAttackAnimation() {
        if (photonView.isMine) {
            isAttacking = false;
            this.GetComponent<Animator>().SetBool("Attacking", false);
            this.GetComponent<Animator>().SetBool("BreakoutAttack", true);
            setDirection();
            animator.SetBool("Idle", true);
        }
    }

    public void startAttackAnimationOne() {
        if (photonView.isMine) {
            photonView.RPC("AnimatorAttackOne", PhotonTargets.All);
        }
    }

    public void setDirection() {
        if (winkelAlpha <= 23 || winkelAlpha >= 338) {
            animator.SetInteger("Direction", 6);
        }
        else if (winkelAlpha >= 23 && winkelAlpha <= 68) {
            animator.SetInteger("Direction", 5);
        }
        else if (winkelAlpha >= 68 && winkelAlpha <= 113) {
            animator.SetInteger("Direction", 4);
        }
        else if (winkelAlpha >= 113 && winkelAlpha <= 158) {
            animator.SetInteger("Direction", 3);
        }
        else if (winkelAlpha >= 158 && winkelAlpha <= 203) {
            animator.SetInteger("Direction", 2);
        }
        else if (winkelAlpha >= 203 && winkelAlpha <= 248) {
            animator.SetInteger("Direction", 1);
        }
        else if (winkelAlpha >= 248 && winkelAlpha <= 293) {
            animator.SetInteger("Direction", 0);
        }
        else if (winkelAlpha >= 293 && winkelAlpha <= 338) {
            animator.SetInteger("Direction", 7);
        }
    }



    

    [RPC]
    void AnimatorAttackOne() {
        isAttacking = true;
        animator.SetBool("BreakoutAttack", true);
        animator.SetBool("Attacking", true);
    }

    [RPC]
    void AnimatorAttackTrigger() {
        this.GetComponent<Animator>().SetTrigger("AttackIdleRelease");
    }


    void winkelBerechnung() {

        if (target != null) {
            //animationszeug winkel berechnung
            laengeA = target.x - transform.position.x;
            if (laengeA < 0) {
                laengeA = laengeA * -1;
            }

            laengeC = target.y - transform.position.y;
            if (laengeC < 0) {
                laengeC = laengeC * -1;
            }

            laengeB = Mathf.Sqrt(laengeA * laengeA + laengeC * laengeC);

            winkelAlpha = Mathf.Acos(laengeA / laengeB);
            winkelAlpha = winkelAlpha * Mathf.Rad2Deg;

            if (target.x > transform.position.x && target.y > transform.position.y) {
                //nichts machen
            }
            else if (target.x < transform.position.x && target.y > transform.position.y) {
                winkelAlpha = 180 - winkelAlpha;
            }
            else if (target.x < transform.position.x && target.y < transform.position.y) {
                winkelAlpha = 180 + winkelAlpha;
            }
            else if (target.x > transform.position.x && target.y < transform.position.y) {
                winkelAlpha = 360 - winkelAlpha;
            }
            else if (target.x > transform.position.x && target.y == transform.position.y) {
                winkelAlpha = 0;
            }
            else if (target.x < transform.position.x && target.y == transform.position.y) {
                winkelAlpha = 180;
            }
            else if (target.x == transform.position.x && target.y > transform.position.y) {
                winkelAlpha = 90;
            }
            else if (target.x == transform.position.x && target.y < transform.position.y) {
                winkelAlpha = 270;
            }

        }
        

    }
}
