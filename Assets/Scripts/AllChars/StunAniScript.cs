﻿using UnityEngine;
using System.Collections;

public class StunAniScript : MonoBehaviour {

    public float time;
    public GameObject target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (target != null) {
            Vector3 pos = target.transform.position;
            pos.y += 0.1f;
            transform.position = pos;


            if (target.renderer.enabled) {
                renderer.enabled = true;
            }
            else {
                renderer.enabled = false;
            }
        }

        if (time != 0 && Time.time >= time) {
            Destroy(this.gameObject);
        }


	}
}
