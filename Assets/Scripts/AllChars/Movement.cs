﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class Movement : Photon.MonoBehaviour {

    public float speed;
    public Vector3 target;
    public bool normalMove;
    public bool autoAttackMode;
    private float attackRange;
    private float lastSynchronizationTime = 0f;
    private float syncDelay = 0f;
    private float syncTime = 0f;
    private Vector3 syncStartPosition;
    private Vector3 syncEndPosition;
    private Vector3 syncVelocity=Vector3.zero;
    private Vector3 syncPosition;
    private Vector3 syncLastPosition=Vector3.zero;
    private Seeker pathfinder;
    private float nextWaypointDistance = 1;

    //A*
    private Path path;
    private int currentWaypoint;
    private GameObject targetGO;
    private Vector3 oldEnemyPos;
    public bool moving;
    public bool moveTowards;
    private float moveTowardsRange;

    //alle positionen anpassen am anfang
    private bool setAllPosAtStart;
    private float startTimer;

    public bool charge;
    private bool chargePathComplete;

    public bool moveTowardsP;

    public bool hooked;
    private float hookRange;
    private Vector3 hookPosition;

    private Animator animator;

    private bool hookedByMonster;

    private GameObject monster;

    private float timer;

    private bool blinked;
    private Vector3 blinkPos;

    private int lineRenderer;

    // Use this for initialization
    void Start() {
        speed = 5f;
        moveTowardsP = false;
        moveTowards = false;
        normalMove = false;
        autoAttackMode = false;
        charge = false;
        chargePathComplete = false;
        syncStartPosition = rigidbody.position;
        syncEndPosition = rigidbody.position;
        syncVelocity = rigidbody.velocity;
        syncPosition = rigidbody.position;
        pathfinder = this.GetComponent<Seeker>();
        currentWaypoint = 0;
        moving = false;
        setAllPosAtStart = true;
        startTimer = 0;
        hooked = false;
        animator = this.GetComponent<Animator>();
        hookedByMonster = false;



        if (GetComponent<MonsterAttributesScript>() != null) {
            speed = GetComponent<MonsterAttributesScript>().movementSpeed;
        } else if (GetComponent<HealerAttributesScript>() != null) {
            speed = GetComponent<HealerAttributesScript>().movementSpeed;
        } else if (GetComponent<RangerAttributesScript>() != null) {
            speed = GetComponent<RangerAttributesScript>().movementSpeed;
        } else if (GetComponent<TankAttributesScript>() != null) {
            speed = GetComponent<TankAttributesScript>().movementSpeed;
        }

    }

    public void setTarget(Vector3 target) {
        this.target = target;
    }

    // Update is called once per frame
    void Update() {
        //jeweilige movementspeed updaten (falls es veränderungen gibt)
        if (GetComponent<MonsterAttributesScript>() != null) {
            speed = GetComponent<MonsterAttributesScript>().movementSpeed;
        } else if (GetComponent<HealerAttributesScript>() != null) {
            speed = GetComponent<HealerAttributesScript>().movementSpeed;
        } else if (GetComponent<RangerAttributesScript>() != null) {
            speed = GetComponent<RangerAttributesScript>().movementSpeed;
        } else if (GetComponent<TankAttributesScript>() != null) {
            speed = GetComponent<TankAttributesScript>().movementSpeed;
        }

        if (photonView.isMine) {
            updateMovement();
        } else {
            SyncedMovement();
        }
    }

    public void iMoved(Vector3 newTarget) {
        moving = true;
        target = newTarget;
        target.z = 2;
        normalMove = true;
        moveTowards = false;
        moveTowardsP = false;
        targetGO = null;

        if (!hooked)
            pathfinder.StartPath(transform.position, target, OnPathComplete);
        else {
            //ziel berechnen
            Vector3 pointToMoveSkill3 = target;
            if (Vector3.Distance(pointToMoveSkill3, hookPosition) > hookRange) {
                pointToMoveSkill3 = pointToMoveSkill3 - hookPosition;
                pointToMoveSkill3.Normalize();
                pointToMoveSkill3 = pointToMoveSkill3 * hookRange;
                pointToMoveSkill3 = pointToMoveSkill3 + hookPosition;
            }
            pathfinder.StartPath(transform.position, pointToMoveSkill3, OnPathComplete);


        }

        
    }

    public void iCharged(Vector3 newTarget) {
        moving = true;
        target = newTarget;
        normalMove = true;
        moveTowards = false;
        moveTowardsP = false;
        chargePathComplete = false;
        charge = true;
        targetGO = null;

        if (!hooked)
            pathfinder.StartPath(transform.position, target, OnPathComplete);
        else {
            //ziel berechnen
            Vector3 pointToMoveSkill3 = target;
            pointToMoveSkill3.z = transform.position.z;
            pointToMoveSkill3 = pointToMoveSkill3 - hookPosition;
            pointToMoveSkill3.Normalize();
            pointToMoveSkill3 = pointToMoveSkill3 * hookRange;
            pointToMoveSkill3 = pointToMoveSkill3 + hookPosition;
            pathfinder.StartPath(transform.position, pointToMoveSkill3, OnPathComplete);
        }
    }

    public void moveTowardsEnemy(GameObject newTarget, float range) {
        target = newTarget.transform.position;
        targetGO = newTarget;
        normalMove = false;
        moveTowards = true;
        moveTowardsP = false;
        moveTowardsRange = range;
        moving = true;
        oldEnemyPos = newTarget.transform.position;

        if (!hooked)
            pathfinder.StartPath(transform.position, target, OnPathComplete);
        else {
            //ziel berechnen
            Vector3 pointToMoveSkill3 = target;
            pointToMoveSkill3.z = transform.position.z;
            pointToMoveSkill3 = pointToMoveSkill3 - hookPosition;
            pointToMoveSkill3.Normalize();
            pointToMoveSkill3 = pointToMoveSkill3 * hookRange;
            pointToMoveSkill3 = pointToMoveSkill3 + hookPosition;
            pathfinder.StartPath(transform.position, pointToMoveSkill3, OnPathComplete);
        }

    }

    public void moveTowardsPoint(Vector3 newTarget, float range) {
        target = newTarget;
        normalMove = false;
        moveTowards = false;
        moveTowardsP = true;
        moveTowardsRange = range;
        moving = true;
        targetGO = null;
        if (!hooked)
            pathfinder.StartPath(transform.position, target, OnPathComplete);
        else {
            //ziel berechnen
            Vector3 pointToMoveSkill3 = target;
            pointToMoveSkill3.z = transform.position.z;
            pointToMoveSkill3 = pointToMoveSkill3 - hookPosition;
            pointToMoveSkill3.Normalize();
            pointToMoveSkill3 = pointToMoveSkill3 * hookRange;
            pointToMoveSkill3 = pointToMoveSkill3 + hookPosition;
            pathfinder.StartPath(transform.position, pointToMoveSkill3, OnPathComplete);
        }
    }

    public void iBlinked(Vector3 targetPos) {
        if (photonView.isMine) {
            blinked = true;
            pathfinder.StartPath(transform.position, targetPos, OnPathComplete);
            moving = false;
        }
    }

    public void endCharge() {
        currentWaypoint = path.vectorPath.Count;
        moving = false;

    }

    public void iAmHooked(float range, Vector3 hookPos) {
        if (photonView.isMine) {
            hooked = true;
            hookRange = range;
            hookPosition = hookPos;
        }
    }

    public void iAmHookedByMonster(int monsterViewID, int line) {
        if (photonView.isMine) {
            hookedByMonster = true;
            monster = PhotonView.Find(monsterViewID).gameObject;
            lineRenderer = line;
        }
    }

    public void stopMove() {
        targetGO = null;
        normalMove = false;
        moveTowards = false;
        moveTowardsP = false;
        moving = false;
        if(path != null)
        currentWaypoint = path.vectorPath.Count;
    }

    void updateMovement() {

        //astral abfrage
        if (this.GetComponent<MonsterSkillControlScript>() != null) {
            if (this.GetComponent<MonsterSkillControlScript>().iAmAstral) {
                hooked = false;
            }
        }


        //Falls autoAttackMode an und gegner sich bewegt
        if (targetGO != null && moveTowards && oldEnemyPos != targetGO.transform.position && timer < Time.time) {
            oldEnemyPos = targetGO.transform.position;
            moving = true;
            timer = Time.time + 0.2f;

            if (!hooked)
                pathfinder.StartPath(transform.position, targetGO.transform.position, OnPathComplete);
            else {
                targetGO = null;
                moveTowards = false;
                //ziel berechnen
                Vector3 pointToMoveSkill3 = target;
                pointToMoveSkill3.z = transform.position.z;
                pointToMoveSkill3 = pointToMoveSkill3 - hookPosition;
                pointToMoveSkill3.Normalize();
                pointToMoveSkill3 = pointToMoveSkill3 * hookRange;
                pointToMoveSkill3 = pointToMoveSkill3 + hookPosition;
                moveTowardsP = true;
                moveTowardsRange = 0.1f;
                pathfinder.StartPath(transform.position, pointToMoveSkill3, OnPathComplete);
            }
        }

        if (targetGO != null && moveTowards) {
            target = targetGO.transform.position;
        }


        if (targetGO != null && !targetGO.GetComponent<SpriteRenderer>().enabled) {
            targetGO = null;
            moveTowards = false;
            moving = false;
        }

        if (hookedByMonster) {
            Vector3 moveto = Vector3.MoveTowards(transform.position, monster.transform.position, 10.0f * Time.deltaTime);
            transform.position = moveto;

            if (Vector3.Distance(transform.position, monster.transform.position) <= 0.1f) {
                hookedByMonster = false;

                photonView.RPC("diableLineRenderer", PhotonTargets.All, lineRenderer);

                if(GameObject.Find("MonsterHook") != null)
                photonView.RPC("destroyMonsterHook2", PhotonTargets.MasterClient, GameObject.Find("MonsterHook").GetPhotonView().viewID);
            }

        }



        if (path == null || hookedByMonster) {
            return;
        }

        if (currentWaypoint >= path.vectorPath.Count && !moveTowards) {
            moving = false;
        } else {
            moving = true;
        }

        if (normalMove) {
            if (moving) {
                if (currentWaypoint < path.vectorPath.Count) {
                    Vector3 moveto = Vector3.MoveTowards(transform.position, path.vectorPath[currentWaypoint], speed * Time.deltaTime);
                    moveto.z = 2;
                    rigidbody.transform.position = moveto;

                    if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
                        currentWaypoint++;
                        return;
                    }
                }
            }
        }

        //charge resetten (tank)
        if (!moving && charge && chargePathComplete) {
            charge = false;
            if (GetComponent<TankSkillControlScript>() != null) {
                GetComponent<TankSkillControlScript>().setOldMovementSpeed();

            }
            if (GetComponent<RangerSkillControlScript>() != null) {
                GetComponent<RangerSkillControlScript>().setOldMovementSpeed();
            }
            if (GetComponent<MonsterSkillControlScript>() != null) {
                GetComponent<MonsterSkillControlScript>().setOldMovementSpeed();
            }

        }

        if (moveTowardsP) {
            if (Vector3.Distance(transform.position, target) < moveTowardsRange) {
                moving = false;
                currentWaypoint = path.vectorPath.Count;
            }

            if (moving) {
                if (currentWaypoint < path.vectorPath.Count) {
                    Vector3 moveto = Vector3.MoveTowards(transform.position, path.vectorPath[currentWaypoint], speed * Time.deltaTime);
                    moveto.z = 2;
                    rigidbody.transform.position = moveto;

                    if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
                        currentWaypoint++;
                        return;
                    }
                }
            }

        }


        if (moveTowards) {
            if (targetGO != null && Vector3.Distance(transform.position, targetGO.transform.position) < moveTowardsRange) {
                moving = false;
                currentWaypoint = path.vectorPath.Count;
            }

            if (moving) {
                if (currentWaypoint < path.vectorPath.Count) {
                    Vector3 moveto = Vector3.MoveTowards(transform.position, path.vectorPath[currentWaypoint], speed * Time.deltaTime);
                    moveto.z = 2;
                    rigidbody.transform.position = moveto;

                    if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
                        currentWaypoint++;
                        return;
                    }
                }
            }

        }


    }

    public void OnPathComplete(Path p) {
        if (!p.error) {
            path = p;
            currentWaypoint = 0;
        }
        if (charge) {
            chargePathComplete = true;
        }
        if (blinked) {
            Vector3 v3 = path.vectorPath[path.vectorPath.Count - 1];
            v3.z = 2;
            photonView.RPC("BlinkRPC", PhotonTargets.All, v3);
            blinked = false;
        }
    }

    void SyncedMovement() {
        if (setAllPosAtStart) {
            syncStartPosition = syncPosition;
            syncEndPosition = syncPosition;
            syncTime = 1;
            syncDelay = 1;
            transform.position = syncPosition;
            animator.SetBool("Idle", true);
        }

        startTimer += Time.deltaTime;
        //einmal ausführen reicht nicht...
        if (startTimer > 2.0f) {
            setAllPosAtStart = false;
        }

        syncTime += Time.deltaTime;
        transform.position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);

        
        if (blinked) {
            transform.position = blinkPos;
            //blinked = false;
        }
        

    }
    public void OnDisable() {
        pathfinder.pathCallback -= OnPathComplete;
    }

    [RPC]
    void BlinkRPC(Vector3 targetPos) {
            syncStartPosition = targetPos;
            syncEndPosition = targetPos;
            syncTime = 1;
            syncDelay = 1;
            transform.position = targetPos;
            blinked = true;
            blinkPos = targetPos;
            if (photonView.isMine) {
                currentWaypoint = path.vectorPath.Count;
            }
    }

    [RPC]
    void destroyMonsterHook2(int viewID) {
        if(PhotonView.Find(viewID) != null)
        PhotonNetwork.Destroy(PhotonView.Find(viewID).gameObject);
    }
    [RPC]
    void diableLineRenderer(int line) {

        if (line == 0) {
            GameObject.Find("LineRenderer3").GetComponent<DrawLineScript>().stopDrawLine();
        }
        else if (line == 1) {
            GameObject.Find("LineRenderer4").GetComponent<DrawLineScript>().stopDrawLine();
        }
        else if (line == 2) {
            GameObject.Find("LineRenderer5").GetComponent<DrawLineScript>().stopDrawLine();
        }
        else if (line == 3) {
            GameObject.Find("LineRenderer6").GetComponent<DrawLineScript>().stopDrawLine();
        }
        
    }

    //void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {

    //    if (stream.isWriting) {
    //        stream.SendNext(transform.position);
    //        //animation
    //        if (animator != null) {
    //            //print(animator.GetBool("AttackIdleRelease"));

    //            stream.SendNext(animator.GetInteger("Direction"));
    //            stream.SendNext(animator.GetBool("Idle"));
    //            stream.SendNext(animator.GetBool("Attacking"));
    //            stream.SendNext(animator.GetBool("BreakoutAttack"));
    //        }
    //    } else {
    //        //syncEndPosition = (Vector3)stream.ReceiveNext();
    //        //syncStartPosition = rigidbody.position;

    //        syncPosition = (Vector3)stream.ReceiveNext();
    //        //animation
    //        if (animator != null) {
    //            animator.SetInteger("Direction", (int)stream.ReceiveNext());
    //            animator.SetBool("Idle", (bool)stream.ReceiveNext());
    //            animator.SetBool("Attacking", (bool)stream.ReceiveNext());
    //            animator.SetBool("BreakoutAttack", (bool)stream.ReceiveNext());
    //        }
    //    }
    //}







    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {

        if (stream.isWriting)
        {
            stream.SendNext(transform.position);

            //animation
            if (animator != null)
            {
                //print(animator.GetBool("AttackIdleRelease"));

                stream.SendNext(animator.GetInteger("Direction"));
                stream.SendNext(animator.GetBool("Idle"));
                stream.SendNext(animator.GetBool("Attacking"));
                stream.SendNext(animator.GetBool("BreakoutAttack"));
            }
        }
        else
        {
           syncPosition = (Vector3)stream.ReceiveNext();
           syncPosition.z = 2;

            if (syncLastPosition.Equals(Vector3.zero))
            {
                syncVelocity = Vector3.zero;
                syncLastPosition = syncPosition;
            }
            else
            {
                syncVelocity = (syncPosition - syncLastPosition);
                syncLastPosition = syncPosition;
            }

            //animation
            if (animator != null)
            {
                animator.SetInteger("Direction", (int)stream.ReceiveNext());
                animator.SetBool("Idle", (bool)stream.ReceiveNext());
                animator.SetBool("Attacking", (bool)stream.ReceiveNext());
                animator.SetBool("BreakoutAttack", (bool)stream.ReceiveNext());
            }

            if (blinked) {
                syncVelocity = Vector3.zero;
                syncStartPosition = blinkPos;
                syncEndPosition = blinkPos;
                blinked = false;
            }

            syncTime = 0f;
            syncDelay = Time.time - lastSynchronizationTime;
            lastSynchronizationTime = Time.time;

            syncEndPosition = syncPosition + syncVelocity*syncDelay;
            syncStartPosition = transform.position;
        }
    }

    public void lostMonsterSight() {
        
        if (targetGO != null) {
            targetGO = null;
            moving = false;
            animator.SetBool("Idle", true);
            currentWaypoint = path.vectorPath.Count;
            GetComponent<AnimaitonScript>().isAttacking = false;
        }

    }


}
