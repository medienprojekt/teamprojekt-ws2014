﻿using UnityEngine;
using System.Collections;

public class Scrolling : MonoBehaviour {

    private float maxCamSpeed;
    public float CamSpeed;
    private int GUIsize = Screen.width/50;
    private Rect recdown;
    private Rect recup;
    private Rect recleft;
    private Rect recright;
    private bool actualFocus;
    private float oldScreenWidth;
    private float timeForDouble;
    private bool lockCam;
    private int clickCount;
    public float scaleFactor;

    // Use this for initialization
    void Start() {
        actualFocus = true;
        recdown = new Rect(0, -GUIsize, Screen.width, GUIsize*2);
        recup = new Rect(0, Screen.height - GUIsize, Screen.width, GUIsize * 2);
        recleft = new Rect(-GUIsize, 0, GUIsize * 2, Screen.height);
        recright = new Rect(Screen.width - GUIsize, 0, GUIsize * 2, Screen.height);
        oldScreenWidth = Screen.width;
        scaleFactor = 0.5f;
        maxCamSpeed = 40f;
        CamSpeed = maxCamSpeed*scaleFactor;
    }

    // Update is called once per frame
    void Update() {
        if (!GameObject.Find("Menu").GetComponent<Menu>().inMenu && actualFocus && !lockCam) {

            if (recdown.Contains(Input.mousePosition) && transform.position.y > -45.0f)
                    transform.Translate(0, -CamSpeed*Time.deltaTime, 0, Space.World);

            if (recup.Contains(Input.mousePosition) && transform.position.y < 30.0f)
                    transform.Translate(0, CamSpeed * Time.deltaTime, 0, Space.World);

                if (recleft.Contains(Input.mousePosition) && transform.position.x > -41.0f)
                    transform.Translate(-CamSpeed * Time.deltaTime, 0, 0, Space.World);

                if (recright.Contains(Input.mousePosition) && transform.position.x < 40.0f)
                    transform.Translate(CamSpeed * Time.deltaTime, 0, 0, Space.World);
        }


        if (timeForDouble < Time.time) {
            clickCount = 0;
        }


        if (Input.GetKeyDown(KeyCode.Space)) {

            if (!lockCam) {
                timeForDouble = Time.time + 1.0f;
                clickCount += 1;
            }

            if (lockCam) {
                lockCam = false;
                clickCount = 0;
            }

            if (clickCount >= 2 && !lockCam) {
                lockCam = true;
            }

        }


        if (Input.GetKey(KeyCode.Space)) {

            if (GameObject.Find("Menu").GetComponent<Menu>().iAmOnTeam != 0) {

                if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Monster" && GameObject.Find("Monster") != null) {
                    Vector3 v3 = GameObject.Find("Monster").transform.position;
                    if (!(GameObject.Find("Monster").transform.position.y > -45.0f)) {
                        v3.y = -45.0f;
                    }
                    if (!(GameObject.Find("Monster").transform.position.y < 30.0f)) {
                        v3.y = 30.0f;
                    }
                    if (!(GameObject.Find("Monster").transform.position.x > -41.0f)) {
                        v3.x = -41.0f;
                    }
                    if (!(GameObject.Find("Monster").transform.position.x < 40.0f)) {
                        v3.x = 40.0f;
                    }
                    v3.z = this.transform.position.z;
                    this.transform.position = v3;
                }
                else if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Ranger" && GameObject.Find("Ranger") != null) {
                    Vector3 v3 = GameObject.Find("Ranger").transform.position;
                    if (!(GameObject.Find("Ranger").transform.position.y > -45.0f)) {
                        v3.y = -45.0f;
                    }
                    if (!(GameObject.Find("Ranger").transform.position.y < 30.0f)) {
                        v3.y = 30.0f;
                    }
                    if (!(GameObject.Find("Ranger").transform.position.x > -41.0f)) {
                        v3.x = -41.0f;
                    }
                    if (!(GameObject.Find("Ranger").transform.position.x < 40.0f)) {
                        v3.x = 40.0f;
                    }
                    v3.z = this.transform.position.z;
                    this.transform.position = v3;
                }
                else if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Healer" && GameObject.Find("Healer") != null) {
                    Vector3 v3 = GameObject.Find("Healer").transform.position;
                    if (!(GameObject.Find("Healer").transform.position.y > -45.0f)) {
                        v3.y = -45.0f;
                    }
                    if (!(GameObject.Find("Healer").transform.position.y < 30.0f)) {
                        v3.y = 30.0f;
                    }
                    if (!(GameObject.Find("Healer").transform.position.x > -41.0f)) {
                        v3.x = -41.0f;
                    }
                    if (!(GameObject.Find("Healer").transform.position.x < 40.0f)) {
                        v3.x = 40.0f;
                    }
                    v3.z = this.transform.position.z;
                    this.transform.position = v3;
                }
                else if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Tank" && GameObject.Find("Tank") != null) {
                    Vector3 v3 = GameObject.Find("Tank").transform.position;
                    if (!(GameObject.Find("Tank").transform.position.y > -45.0f)) {
                        v3.y = -45.0f;
                    }
                    if (!(GameObject.Find("Tank").transform.position.y < 30.0f)) {
                        v3.y = 30.0f;
                    }
                    if (!(GameObject.Find("Tank").transform.position.x > -41.0f)) {
                        v3.x = -41.0f;
                    }
                    if (!(GameObject.Find("Tank").transform.position.x < 40.0f)) {
                        v3.x = 40.0f;
                    }
                    v3.z = this.transform.position.z;
                    this.transform.position = v3;
                }
            }
        }

        if (lockCam) {
            if (GameObject.Find("Menu").GetComponent<Menu>().iAmOnTeam != 0) {

                if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Monster" && GameObject.Find("Monster") != null) {
                    Vector3 v3 = GameObject.Find("Monster").transform.position;
                    if (!(GameObject.Find("Monster").transform.position.y > -45.0f)) {
                        v3.y = -45.0f;
                    }
                    if (!(GameObject.Find("Monster").transform.position.y < 30.0f)) {
                        v3.y = 30.0f;
                    }
                    if (!(GameObject.Find("Monster").transform.position.x > -41.0f)) {
                        v3.x = -41.0f;
                    }
                    if (!(GameObject.Find("Monster").transform.position.x < 40.0f)) {
                        v3.x = 40.0f;
                    }
                    v3.z = this.transform.position.z;
                    this.transform.position = v3;
                }
                else if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Ranger" && GameObject.Find("Ranger") != null) {
                        Vector3 v3 = GameObject.Find("Ranger").transform.position;
                        if (!(GameObject.Find("Ranger").transform.position.y > -45.0f)) {
                            v3.y = -45.0f;
                        }
                        if (!(GameObject.Find("Ranger").transform.position.y < 30.0f)) {
                            v3.y = 30.0f;
                        }
                        if (!(GameObject.Find("Ranger").transform.position.x > -41.0f) ) {
                            v3.x = -41.0f;
                        }
                        if (!(GameObject.Find("Ranger").transform.position.x < 40.0f)) {
                            v3.x = 40.0f;
                        }
                        v3.z = this.transform.position.z;
                        this.transform.position = v3;

                }
                else if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Healer" && GameObject.Find("Healer") != null) {
                    Vector3 v3 = GameObject.Find("Healer").transform.position;
                    if (!(GameObject.Find("Healer").transform.position.y > -45.0f)) {
                        v3.y = -45.0f;
                    }
                    if (!(GameObject.Find("Healer").transform.position.y < 30.0f)) {
                        v3.y = 30.0f;
                    }
                    if (!(GameObject.Find("Healer").transform.position.x > -41.0f)) {
                        v3.x = -41.0f;
                    }
                    if (!(GameObject.Find("Healer").transform.position.x < 40.0f)) {
                        v3.x = 40.0f;
                    }
                    v3.z = this.transform.position.z;
                    this.transform.position = v3;
                }
                else if (GameObject.Find("Menu").GetComponent<Menu>().iAm == "Tank" && GameObject.Find("Tank") != null) {
                    Vector3 v3 = GameObject.Find("Tank").transform.position;
                    if (!(GameObject.Find("Tank").transform.position.y > -45.0f)) {
                        v3.y = -45.0f;
                    }
                    if (!(GameObject.Find("Tank").transform.position.y < 30.0f)) {
                        v3.y = 30.0f;
                    }
                    if (!(GameObject.Find("Tank").transform.position.x > -41.0f)) {
                        v3.x = -41.0f;
                    }
                    if (!(GameObject.Find("Tank").transform.position.x < 40.0f)) {
                        v3.x = 40.0f;
                    }
                    v3.z = this.transform.position.z;
                    this.transform.position = v3;
                }
            }


        }
     }

    void OnApplicationFocus(bool focusStatus) {
        actualFocus = focusStatus;
    }

    public void changeScrollspeed(float scaleFactor)
    {
        float temp_Factor=scaleFactor;

        if (scaleFactor > 1.0f)
        {
            temp_Factor = 1.0f;
        }
        else if (scaleFactor <0.0f)

        {
            temp_Factor = 0.0f;
        }

        this.scaleFactor = temp_Factor;
        CamSpeed = maxCamSpeed *temp_Factor;
    }

    public float getScaleFactor()
    {
        return scaleFactor;
    }
}