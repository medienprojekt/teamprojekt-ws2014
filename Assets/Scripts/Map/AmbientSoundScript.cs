﻿using UnityEngine;
using System.Collections;

public class AmbientSoundScript : MonoBehaviour {

	private Menu menuScript;
	public AudioSource asound;
	private bool switchMusicToGame;
	private bool switchMusicToMenu;
	private bool inMenu;

	// Use this for initialization
	void Start () 
	{
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();
		asound = GetComponent<AudioSource>();

		switchMusicToGame = true;
		switchMusicToMenu = false;
		inMenu = menuScript.inMenu;
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		inMenu = GameObject.Find("Menu").GetComponent<Menu>().inMenu;
		asound.volume = 0.06f * menuScript.scaleVolumeFactor;
		if (!inMenu && switchMusicToGame) 
		{
			switchMusicToGame = false;
			switchMusicToMenu = true;
			asound.Play ();
		}
		
		if (inMenu && switchMusicToMenu) 
		{
			switchMusicToGame = true;
			switchMusicToMenu = false;
			asound.Stop ();
		}
	}
}
