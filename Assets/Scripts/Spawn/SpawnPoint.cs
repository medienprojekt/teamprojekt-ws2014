﻿using UnityEngine;
using System.Collections;

public class SpawnPoint : MonoBehaviour
{

    public Vector3 position;
    public bool taken;

    // Use this for initialization
    void Start()
    {
        position = transform.position;
        position.z = 2;
        taken = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void spawnTaken(bool taken)
    {
        this.taken = taken;
    }
}