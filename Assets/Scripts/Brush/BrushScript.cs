﻿using UnityEngine;
using System.Collections;

public class BrushScript : MonoBehaviour {

	private Menu menuScript;
	
	public bool monsterInBrush;
	
	private bool initLater;
	
	public bool rangerInBrush;
	public bool tankInBrush;
	public bool healerInBrush;

	public AudioClip[] sounds;
	public AudioSource asound;
	
	// Use this for initialization
	void Start () {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];

		monsterInBrush = false;
        rangerInBrush = false;
        tankInBrush = false;
        healerInBrush = false;

        Vector3 pos = transform.position;
        pos.z = 3;
        transform.position = pos;

		
	}

	private void PlaySound(int Number)
	{
		asound.clip = sounds[Number];
		asound.volume = menuScript.scaleVolumeFactor;
		asound.Play ();
	}

	//[RPC]
	public void PlayHeartBeatSound()
	{
		PlaySound (0);
	}
	//[RPC]
	public void StopHeartBeatSound()
	{
		asound.Stop ();
	}
	// Update is called once per frame
	void Update () {

	}
	
	void OnTriggerEnter(Collider collider) {
		//Monster im Busch
        if (collider.tag == "Monster") {
			PlayHeartBeatSound();
            monsterInBrush = true;
            //Debug.Log("Monster entered the Brush");
        }
		
			if (collider.name == "Ranger") {
			PlayHeartBeatSound();
				rangerInBrush = true;
                //Debug.Log("Hunter entered the Brush");
			}
			else if (collider.name == "Healer") {
			PlayHeartBeatSound();
				healerInBrush = true;
                //Debug.Log("Hunter entered the Brush");
			}
			else if (collider.name == "Tank") {
			PlayHeartBeatSound();
				tankInBrush = true;
                //Debug.Log("Hunter entered the Brush");
			}
			



	}
	
	void OnTriggerExit(Collider collider) {
		//Monster aus dem Busch
		if (collider.tag == "Monster") {
			StopHeartBeatSound();
			monsterInBrush = false;
			//Debug.Log ("Monster left the Brush");
		}

			if (collider.name == "Ranger") {
			StopHeartBeatSound();
				rangerInBrush = false;
                //Debug.Log("Hunter left the Brush");
			}
			else if (collider.name == "Healer") {
			StopHeartBeatSound();
				healerInBrush = false;
                //Debug.Log("Hunter left the Brush");
			}
			else if (collider.name == "Tank") {
			StopHeartBeatSound();
				tankInBrush = false;
                //Debug.Log("Hunter left the Brush");
			}
			


	}
	

}
