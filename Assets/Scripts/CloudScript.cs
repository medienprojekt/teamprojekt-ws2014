﻿using UnityEngine;
using System.Collections;

public class CloudScript : MonoBehaviour {

    private float speed = 0.01f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float newX = speed;

        Vector3 v3 = new Vector3(transform.position.x + newX, transform.position.y, transform.position.z);
        transform.position = v3;
	}
}
