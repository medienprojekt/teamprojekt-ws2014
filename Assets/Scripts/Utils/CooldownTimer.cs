﻿using UnityEngine;
using System.Collections;

public class CooldownTimer
{

		private float LastActionDone;
		private float CooldownSeconds;

		public CooldownTimer (float seconds)
		{
				CooldownSeconds = seconds;
                LastActionDone = -seconds;
		}

		public void StartCooldown ()
		{
				LastActionDone = Time.time;
		}

		public bool IsOnCooldown ()
		{
				if (LastActionDone + CooldownSeconds <= Time.time) {
						return false;
				}
				return true;

		}

        public float GetCooldownLeft() {

            if(IsOnCooldown()){
                return (LastActionDone + CooldownSeconds) - Time.time;
            }

            return 0;
        }

        public float getCooldown()
        {
            return CooldownSeconds;
        }

}
