﻿using UnityEngine;
using System.Collections;

public class MonsterAOEFlameAniScript : MonoBehaviour {

    public float destroyTime;
    private float time;
    private Color curColor;
    public GameObject target;
    public float zLayer;
    public Vector3 mousePos;
    public Vector3 offset;

    // Use this for initialization
    void Start() {
        time = Time.time + destroyTime;
        curColor = GetComponent<SpriteRenderer>().color;
        Vector3 v3 = transform.position;
        v3.z += zLayer;
        v3.y += -0.8f;
        transform.position = v3;

        Vector3 direction = mousePos - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (Time.time > time - destroyTime / 2) {
            curColor.a -= 0.02f;
        }
        GetComponent<SpriteRenderer>().color = curColor;
    }


    // Update is called once per frame
    void Update() {
        Vector3 direction = mousePos - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        GetComponent<Animator>().SetFloat("Angle", angle);

        if (angle > 90.0f || angle < -90.0f) {
            angle += 180.0f;
        }

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);


        if (curColor.a <= 0.0f) {
            Destroy(this.gameObject);

        }


    }
}
