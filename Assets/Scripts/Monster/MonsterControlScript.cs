﻿using UnityEngine;
using System.Collections;

public class MonsterControlScript : Photon.MonoBehaviour {

	private Menu menuScript;

    public float speed = 1.5f;
    private Vector3 target;
    private GameObject multiplayerManger;
    public GameObject cam;
    private CooldownTimer autoAttackCD;
    public GameObject autoAttackProjectileGO;
    private Vector3 distanceBetweenPlayerAndTarget;
    private bool runTowardsForAutoAttack;
    private MonsterSkillControlScript monsterSkillControlScript;
    private Movement movementScript;
    private MonsterAttributesScript monsterAttributesScript;
    private GameObject mouseHitbox;
    private MouseHitboxScriptMonster mouseHitboxScriptMonster;
    public bool attackMode;
    private float knockbackAngle;
    private bool startKnockback;
    public bool isDizzy;
    private float dizzyTime;

    private GameObject targetGO;

    private bool iAmHitByHook;
    private float hookRange;
    private float hookReleaseTime;
    private Vector3 hookPoint;

    public bool iAmTaunted;
    private CooldownTimer tauntTime;

    public bool captureWard;
    private GameObject captrueWardGO;
    private float captureWardCharge; 

	public AudioClip[] sounds;
	public AudioSource asound;
	public AudioClip mouseClickSound;
	public AudioClip autoAttackSound;
	private int counter1;

    private float oldMovementspeed;

    public Texture tex1;
    public Texture tex2;
    public GameObject mouseClick;

    private AnimaitonScript animationScript;

    void Awake() {

		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];
		counter1 = 3;

        animationScript = this.GetComponent<AnimaitonScript>();

        monsterSkillControlScript = transform.GetComponent<MonsterSkillControlScript>();
        monsterAttributesScript = transform.GetComponent<MonsterAttributesScript>();

        if (photonView.isMine) {
            startKnockback = false;
            target = transform.position;
            autoAttackCD = new CooldownTimer(monsterAttributesScript.attackSpeed);
            tauntTime = new CooldownTimer(0);
            attackMode = false;

            isDizzy = false;
            iAmTaunted = false;

            runTowardsForAutoAttack = false;


            movementScript = transform.GetComponent<Movement>();

            mouseHitbox = GameObject.Find("MouseHitbox");
            mouseHitboxScriptMonster = mouseHitbox.GetComponent<MouseHitboxScriptMonster>();


        } else {
            enabled = false;
        }

        Vector3 bla = transform.position;
        bla.z = 2;
        transform.position = bla;
        target = transform.position;


    }

    void Update() {

        if (monsterAttributesScript.isAlive && !monsterSkillControlScript.attackSkill41Active 
            && !movementScript.charge) {
                if (!iAmTaunted) {
                    handleMouseInteractions();
                }
                if (!monsterSkillControlScript.iAmAstral && !monsterSkillControlScript.attackSkill3Active) {
                    autoattack();
                }
        }

        if (!monsterAttributesScript.isAlive) {
            movementScript.iMoved(transform.position);
        }


        if (startKnockback) {

            Vector3 test = transform.position;

            test.x += (Mathf.Cos((knockbackAngle * Mathf.PI) / 180)) * 4.0f;
            test.y += (Mathf.Sin((knockbackAngle * Mathf.PI) / 180)) * 4.0f;

            movementScript.iCharged(test);

            startKnockback = false;
        }

        //dizzy checkup
        if ((monsterSkillControlScript.iAmAstral && isDizzy) || (isDizzy && Time.time > dizzyTime)) {
            isDizzy = false;
            monsterAttributesScript.movementSpeed = oldMovementspeed;
        }

        //taunt
        if ((monsterSkillControlScript.iAmAstral && iAmTaunted) || (iAmTaunted && !tauntTime.IsOnCooldown())) {
            iAmTaunted = false;
            attackMode = false;
            //animation zerstören
            photonView.RPC("monsterKillTauntAni", PhotonTargets.All);
            movementScript.moveTowardsPoint(transform.position, 0.1f);
   
        }


        //ward
        if (monsterAttributesScript.isAlive && captureWard && Vector3.Distance(captrueWardGO.transform.position, transform.position) <= 2.0f) {
            captureWardCharge += 25 * Time.deltaTime;
            movementScript.stopMove();
            if (captureWardCharge >= 100.0f) {
                //ward captured
                photonView.RPC("captureWardMonster", PhotonTargets.All, captrueWardGO.GetPhotonView().viewID);
                captureWardCharge = 0.0f;
                captureWard = false;
            }
        }


        //hookCheckUp
        if (iAmHitByHook && Time.time > hookReleaseTime) {
            iAmHitByHook = false;

            photonView.RPC("noLongerHookedByRangerRPC", PhotonTargets.All);
            if(GameObject.Find("RangerHook1") != null)
            photonView.RPC("DestroyThis1", PhotonTargets.MasterClient, GameObject.Find("RangerHook1").GetPhotonView().viewID);
            if (GameObject.Find("RangerHook2") != null)
                photonView.RPC("DestroyThis1", PhotonTargets.MasterClient, GameObject.Find("RangerHook2").GetPhotonView().viewID);
        }
    }

    [RPC]
    public void DestroyThis1(int viewID) {
        if (PhotonView.Find(viewID) != null)
            PhotonNetwork.Destroy(PhotonView.Find(viewID));
    }


    void autoattack() {

        if (targetGO != null && !targetGO.GetComponent<SpriteRenderer>().enabled) {
            attackMode = false;
            targetGO = null;
            movementScript.lostMonsterSight();
        }


        //wenn in range und autoAttackMode an ist
        if (targetGO != null && attackMode && Vector3.Distance(transform.position, targetGO.transform.position) < monsterAttributesScript.attackRange) {

            if (!targetGO.renderer.enabled) {
                targetGO = null;
                attackMode = false;
                movementScript.iMoved(transform.position);
                return;
            }


            //gegner angreifen
            if (!autoAttackCD.IsOnCooldown()) {

                //animation starten
                animationScript.startAttackAnimation();
                //leben abziehen
                photonView.RPC("monsterAutoAttackRPC", PhotonTargets.MasterClient, targetGO.GetPhotonView().viewID, -monsterAttributesScript.attackDamage);
				//SOUND HIER
				photonView.RPC ("PlayMonsterAutoAttackSound", PhotonTargets.All, null);
                
                //schaden zurückwerfen bei passiv
                if (targetGO.GetComponent<TankAttributesScript>() != null && targetGO.GetComponent<TankSkillControlScript>().passivActive) {
                    //selbst schaden machen
                    photonView.RPC("reflectDMG", PhotonTargets.MasterClient, this.gameObject.GetPhotonView().viewID, -monsterAttributesScript.attackDamage * targetGO.GetComponent<TankAttributesScript>().passivReflectPercent);
                
                }
                monsterSkillControlScript.revealMeMonster();
                autoAttackCD.StartCooldown();

				//SOUND HIER autoattack
				PlayRPCMonsterAutoAttackSound ();
            }

        }
        else {
            if (animationScript.isAttacking) {
                animationScript.stopAttackAnimation();
            }
        }

    }

    float native_width = 1920;
    float native_height = 1080;

    void handleMouseInteractions() {
        //handle mouse interactions
        //right click
        if (!GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked)
        {
            float rx = Screen.width / native_width;
            float ry = Screen.height / native_height;
            GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

            Vector3 mpos;
            mpos = GameObject.Find("Minimap").GetComponent<Map>().NormalizedMousePosition;


            if (Input.GetMouseButtonDown(1) && !iAmTaunted && !GameObject.Find("Minimap").GetComponent<Map>().mouseOnMap(mpos.x, mpos.y) && !monsterSkillControlScript.attackSkill41Active)
            {
                PlayMouseClickSound();

                monsterSkillControlScript.interruptChargeLamp();
                monsterSkillControlScript.skillMode = false;

                monsterSkillControlScript.disableSkills();



                captureWard = false;
                //run or attack or other stuff...
                //run
                if (!mouseHitboxScriptMonster.mouseOverEnemy && !mouseHitboxScriptMonster.mouseOverWard)
                {
                    //maus portion holen
                    target.x = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
                    target.y = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
                    target.z = transform.position.z;
                    attackMode = false;
                    //movementScript bescheid sagen
                    movementScript.iMoved(target);
                    Instantiate(mouseClick, target, transform.rotation);

                    //SOUND HIER bewegungsbestätigung (lol - ezreal - over here! :D )
                    PlayMaybeMoveVoiceSound();
                }
                //attack
                else if (mouseHitboxScriptMonster.mouseOverEnemy)
                {
                    if (mouseHitboxScriptMonster.mouseOverLastEnemyGO != null)
                    {
                        attackMode = true;
                        targetGO = mouseHitboxScriptMonster.mouseOverLastEnemyGO;
                        //laufe zum target hin
                        movementScript.moveTowardsEnemy(mouseHitboxScriptMonster.mouseOverLastEnemyGO, monsterAttributesScript.attackRange);

                        //SOUND HIER angriffsbestätigung
                        PlayAttackVoiceSound();
                    }
                }

                //ward
                else if (mouseHitboxScriptMonster.mouseOverWard && mouseHitboxScriptMonster.mouseOverWardGO.GetComponent<WardScript>().iAmCapturedByTeam != 2)
                {
                    captureWard = true;
                    movementScript.moveTowardsPoint(mouseHitboxScriptMonster.mouseOverWardGO.transform.position, 2.0f);
                    captureWardCharge = 0.0f;
                    captrueWardGO = mouseHitboxScriptMonster.mouseOverWardGO;
                }

            }

        }
    }

    [RPC]
    void monsterAutoAttackRPC(int viewID, float adjustHealth) {
        Transform trans = PhotonView.Find(viewID).transform;
        if (PhotonNetwork.isMasterClient) {
            if (trans.GetComponent<RangerAttributesScript>() != null) {
                trans.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(adjustHealth, true);
            }
            else if (trans.GetComponent<HealerAttributesScript>() != null) {
                trans.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(adjustHealth, true);
            }
            else if (trans.GetComponent<TankAttributesScript>() != null) {
                trans.GetComponent<TankAttributesScript>().AddjustCurrentHealth(adjustHealth, true);
            }
        }
    }


    public void knockback(float angle) {
        knockbackAngle = angle;
        startKnockback = true;
    }

    public void makeMeDizzy(float time) {
        if (photonView.isMine && !monsterSkillControlScript.iAmAstral) {
            isDizzy = true;
            dizzyTime = Time.time + time;
            oldMovementspeed = monsterAttributesScript.oldMovementSpeed;
            monsterAttributesScript.movementSpeed = monsterAttributesScript.oldMovementSpeed / 2;
        }
    }

    public void hitByRangerHook(float range, float rootTime, Vector3 rootPoint) {
        if (photonView.isMine && !monsterSkillControlScript.iAmAstral) {
            iAmHitByHook = true;
            hookRange = range;
            hookReleaseTime = Time.time + rootTime;
            hookPoint = rootPoint;

            movementScript.iAmHooked(range, rootPoint);
        }
    }

    public void makeMeTaunted(float time, int viewIDEnemy) {
        if (photonView.isMine && !monsterSkillControlScript.iAmAstral) {
            iAmTaunted = true;
            tauntTime = new CooldownTimer(time);
            tauntTime.StartCooldown();

            targetGO = PhotonView.Find(viewIDEnemy).gameObject;
            attackMode = true;
            movementScript.moveTowardsEnemy(targetGO, monsterAttributesScript.attackRange);
        }
    }

    void OnGUI() {

        //ward
        if (captureWard && Vector3.Distance(captrueWardGO.transform.position, transform.position) <= 2.0f) {
        float rx = Screen.width / native_width;
        float ry = Screen.height / native_height;
        GL.PushMatrix();
        GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));


            GUI.DrawTexture(new Rect(1920 / 2 - tex1.width / 2, 900 - tex1.height, tex1.width, tex1.height), tex1);

            GUI.BeginGroup(new Rect(1920 / 2 - tex2.width / 2, 900 - tex2.height, tex2.width * (captureWardCharge / 100), tex2.height));
            GUI.DrawTexture(new Rect(0, 0, tex2.width, tex2.height), tex2);
            GUI.EndGroup();

        GL.PopMatrix();
        }
    }

    [RPC]
    public void monsterKillTauntAni() {
        Transform go = GameObject.Find("TankTauntAni").transform;
        go.GetComponent<tank_taunt_script2>().killMe();
    }

    [RPC]
    private void captureWardMonster(int viewID) {
        Transform trans = PhotonView.Find(viewID).transform;
        trans.GetComponent<WardScript>().iAmCapturedByTeam = 2;
    }

    
    [RPC]
    public void reflectDMG(int viewID, float dmg) {
        Transform trans = PhotonView.Find(viewID).transform;
        if(PhotonNetwork.isMasterClient)
        trans.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(dmg);
    }

    public void noLongerHookedByRanger() {
        if (PhotonNetwork.isMasterClient) {
            photonView.RPC("noLongerHookedByRangerRPC", PhotonTargets.Others); 
        }

        if (photonView.isMine) {
            movementScript.hooked = false;
            movementScript.iMoved(transform.position);
        }
    }

    [RPC]
    public void noLongerHookedByRangerRPC() {
        noLongerHookedByRanger();
    }

	//Sound Methods
	[RPC]
	public void PlayMonsterAutoAttackSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        AudioSource.PlayClipAtPoint(autoAttackSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	public void PlayRPCMonsterAutoAttackSound()
	{
		photonView.RPC ("PlayMonsterAutoAttackSound", PhotonTargets.All, null);
	}
	
	public void PlayMouseClickSound()
	{
		AudioSource.PlayClipAtPoint(mouseClickSound, transform.position, 0.12f*menuScript.scaleVolumeFactor);
	}
	
	private void PlaySound(int Number)
	{
		asound.clip = sounds[Number];
		asound.volume = 0.3f*menuScript.scaleVolumeFactor;
		asound.Play ();
	}
	
	private void PlayMaybeMoveVoiceSound()
	{
		if(counter1 == 6)
		{
			int r = Random.Range(0,3);
			PlaySound (r);
			counter1 = 0;
		}
		
		counter1++;
	}
	
	private void PlayMoveVoiceSound()
	{
		int r = Random.Range(0,3);
		PlaySound (r);
	}
	
	private void PlayAttackVoiceSound()
	{
		int r = Random.Range(3,6);
		PlaySound (r);
	}

}
