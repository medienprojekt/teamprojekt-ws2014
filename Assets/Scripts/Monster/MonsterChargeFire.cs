﻿using UnityEngine;
using System.Collections;

public class MonsterChargeFire : Photon.MonoBehaviour {

    private Menu menu;

    private GameObject targetR;
    private GameObject targetH;
    private GameObject targetT;

    private float aLiveTimer;

	// Use this for initialization
	void Start () {
        menu = GameObject.Find("Menu").GetComponent<Menu>();
        aLiveTimer = Time.time + 3.0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (targetR != null) {
            targetR.GetComponent<RangerAttributesScript>().iAmInFire(true);
        }
        if (targetH != null) {
            targetH.GetComponent<HealerAttributesScript>().iAmInFire(true);
        }
        if (targetT != null) {
            targetT.GetComponent<TankAttributesScript>().iAmInFire(true);
        }

        if (aLiveTimer <= Time.time) {
            Destroy(this.gameObject);
        }
	}

    void OnTriggerEnter(Collider collider) {

        if (collider.tag == "Team") {
            if (menu.iAm == "Ranger" && collider.GetComponent<RangerAttributesScript>() != null) {
                collider.GetComponent<RangerAttributesScript>().iAmInFire(true);
                targetR = collider.gameObject;
            }
            if (menu.iAm == "Healer" && collider.GetComponent<HealerAttributesScript>() != null) {
                collider.GetComponent<HealerAttributesScript>().iAmInFire(true);
                targetH = collider.gameObject;
            }
            if (menu.iAm == "Tank" && collider.GetComponent<TankAttributesScript>() != null) {
                collider.GetComponent<TankAttributesScript>().iAmInFire(true);
                targetT = collider.gameObject;
            }
        }
    }

    void OnTriggerExit(Collider collider) {

        if (collider.tag == "Team") {
            if (menu.iAm == "Ranger" && collider.GetComponent<RangerAttributesScript>() != null) {
                collider.GetComponent<RangerAttributesScript>().iAmInFire(false);
                targetR = null;
            }
            if (menu.iAm == "Healer" && collider.GetComponent<HealerAttributesScript>() != null) {
                collider.GetComponent<HealerAttributesScript>().iAmInFire(false);
                targetH = null;
            }
            if (menu.iAm == "Tank" && collider.GetComponent<TankAttributesScript>() != null) {
                collider.GetComponent<TankAttributesScript>().iAmInFire(false);
                targetT = null;
            }
        }
    }

    void OnDestroy() {
        if (targetR != null) {
            targetR.GetComponent<RangerAttributesScript>().iAmInFire(false);
        }
        if (targetH != null) {
            targetH.GetComponent<HealerAttributesScript>().iAmInFire(false);
        }
        if (targetT != null) {
            targetT.GetComponent<TankAttributesScript>().iAmInFire(false);
        }
    }

}
