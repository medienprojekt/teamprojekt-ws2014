﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterSkillControlScript : Photon.MonoBehaviour {

	private Menu menuScript;

    private Movement movementScript;
    private MouseHitboxScriptMonster mouseHitboxScriptMonster;
    private MonsterControlScript monsterControlScript;
    private MonsterAttributesScript monsterAttributesScript;
    private int skillNumer; //0 nichts, 1-4 q-r
    public GameObject skill1Prefab;
    public bool skillMode;
    public GameObject skillCirclePrefab;
    public GameObject skillCircle;
    public GameObject skillConePrefab;
    private GameObject skillCone;

    public CooldownTimer cdTSkill1;
    public CooldownTimer cdTSkill2;
    public CooldownTimer cdTSkill3;
    public CooldownTimer cdTSkill4;
    public CooldownTimer cdTSkill11;
    public CooldownTimer cdTSkill21;
    public CooldownTimer cdTSkill31;
    public CooldownTimer cdTSkill41;
    public CooldownTimer cdTSkill12;
    public CooldownTimer cdTSkill22;
    public CooldownTimer cdTSkill32;
    public CooldownTimer cdTSkill5;

    private bool attackSkill12Active;
    private bool attackSkill2Active;
    public bool attackSkill3Active;

    private GameObject curTarget;
    private Vector3 mousePos;

    private float oldMovmentSpeed;
    private bool currentCharging;

    public Transform chargeFire;
    public float createFire;

    public bool iAmAstral;
    private float astralTimer;

    private Color oldColor;

    private GameObject monsterLamp;

    public float soulsplinter;

    public bool attackSkill41Active;

    public float skill41EndTime;
    public float skill41ShootTime;

    public GameObject silcenedAni;

	public AudioClip[] sounds;
	public AudioSource asound;
	public AudioClip winSound;
	public AudioClip loseSound;
	private bool winSoundPlayed;
	private bool loseSoundPlayed;

    private bool chargeLampe;
    private float chargeLampeTick;

    float native_width = 1920;
    float native_height = 1080;

    public Texture tex1;
    public Texture tex2;

    private float skill32Charge;
    public bool attackSkill32Active;

    public int qSkill;
    public int wSkill;
    public int eSkill;
    public int rSkill;

    public GameObject cleaveAni1;
    public GameObject cleaveAni2;
    public GameObject swordStrikeAni;
    public GameObject chargeAni;
    public GameObject jumpAni1;
    public GameObject jumpAni2;
    public GameObject aoeFlameAni;
    public GameObject silenceAni;

    private DrawLineScript drawLine;

    private AnimaitonScript animationScript;

    public bool hoverQ;
    public bool hoverW;
    public bool hoverE;
    public bool hoverR;
    public bool hoverT;

    private bool mouseClickSkill;

    // Use this for initialization
    void Start() {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

        monsterControlScript = transform.GetComponent<MonsterControlScript>();
        monsterAttributesScript = transform.GetComponent<MonsterAttributesScript>();

        oldMovmentSpeed = monsterAttributesScript.movementSpeed;
        animationScript = GetComponent<AnimaitonScript>();
		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];
		winSoundPlayed = false;
		loseSoundPlayed = false;

        oldColor = this.GetComponent<SpriteRenderer>().material.color;

        drawLine = GameObject.Find("LineRenderer3").GetComponent<DrawLineScript>();

        if (photonView.isMine) {
            mouseHitboxScriptMonster = GameObject.Find("MouseHitbox").GetComponent<MouseHitboxScriptMonster>();


            skillCircle = Instantiate(skillCirclePrefab, transform.position, transform.rotation) as GameObject;
            skillCircle.GetComponent<SkillCircleScript>().target = this.gameObject;
            Vector3 sCPos = new Vector3(transform.position.x, transform.position.y, 3);
            skillCircle.transform.position = sCPos;
            skillCircle.renderer.enabled = false;


            skillCone = Instantiate(skillConePrefab, transform.position, transform.rotation) as GameObject;
            skillCone.GetComponent<SkillConeScript>().target = this.gameObject;
            sCPos = new Vector3(transform.position.x, transform.position.y, 3);
            skillCone.transform.position = sCPos;
            skillCone.renderer.enabled = false;


            movementScript = transform.GetComponent<Movement>();

            skillMode = false;
            skillNumer = 0;


            attackSkill12Active = false;


            cdTSkill1 = new CooldownTimer(monsterAttributesScript.cdSkill1);
            cdTSkill2 = new CooldownTimer(monsterAttributesScript.cdSkill2);
            cdTSkill3 = new CooldownTimer(monsterAttributesScript.cdSkill3);
            cdTSkill4 = new CooldownTimer(monsterAttributesScript.cdSkill4);
            cdTSkill11 = new CooldownTimer(monsterAttributesScript.cdSkill11);
            cdTSkill21 = new CooldownTimer(monsterAttributesScript.cdSkill21);
            cdTSkill31 = new CooldownTimer(monsterAttributesScript.cdSkill31);
            cdTSkill41 = new CooldownTimer(monsterAttributesScript.cdSkill41);
            cdTSkill12 = new CooldownTimer(monsterAttributesScript.cdSkill12);
            cdTSkill22 = new CooldownTimer(monsterAttributesScript.cdSkill22);
            cdTSkill32 = new CooldownTimer(monsterAttributesScript.cdSkill32);
            cdTSkill5 = new CooldownTimer(monsterAttributesScript.cdSkill5);

        } else {
            enabled = false;
        }

        qSkill = 0;
        wSkill = 0;
        eSkill = 0;
        rSkill = 0;
    }


    // Update is called once per frame
    void Update() {

        if (!currentCharging && monsterAttributesScript.isAlive&&!GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {
            checkUpKeyPressed();
        }
        skillActive();
        if (!hoverQ && !hoverW && !hoverE && !hoverR && !hoverT) {
            skillCircle.renderer.enabled = false;
        }
        skillCone.renderer.enabled = false;
        if (soulsplinter > 100) {
            soulsplinter = 100;

        }


        if (skillMode) {

            //aoe flammen
            if (skillNumer == 1) {
                skillCone.renderer.enabled = true;
            }

            //cleave
            else if (skillNumer == 11) {
                //instant
                revealMeMonster();
                fireSkill(skillNumer);
            }
                //swordstrike
            else if (skillNumer == 12) {
                skillCircle.renderer.enabled = true;
                float x = (monsterAttributesScript.rangeSkill12 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (monsterAttributesScript.rangeSkill12 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }

            //slow hände
            else if (skillNumer == 2) {
                skillCircle.renderer.enabled = true;
                float x = (monsterAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (monsterAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }

            //hook
            else if (skillNumer == 21) {
                skillCircle.renderer.enabled = true;
                float x = (monsterAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (monsterAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }

            //silence
            else if (skillNumer == 22) {
                //instant
                revealMeMonster();
                fireSkill(skillNumer);
            }


           //flame charge
            else if (skillNumer == 3) {
                skillCircle.renderer.enabled = true;
                float x = (monsterAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (monsterAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }

            //astral
            else if (skillNumer == 31) {
                photonView.RPC("MonsterSkill31", PhotonTargets.All, true);
                astralTimer = Time.time + monsterAttributesScript.astralTimeSkill31;
                skillMode = false;
				//SOUND HIER astral
				photonView.RPC ("PlayMonsterAstralSound", PhotonTargets.All, null);
                cdTSkill31.StartCooldown();
                return;
            }


            //blink explosion
            else if (skillNumer == 32) {
                skillCircle.renderer.enabled = true;
                float x = (monsterAttributesScript.rangeSkill32 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                float y = (monsterAttributesScript.rangeSkill32 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                Vector3 scale = new Vector3(x, y, 1);
                skillCircle.transform.localScale = scale * 2;
            }


            //ulti soul vertex
            else if (skillNumer == 4) {

                if (GameObject.Find("Ranger") != null && GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive) {
                    GameObject go = GameObject.Find("Ranger");
                    if (Vector3.Distance(transform.position, go.transform.position) <= monsterAttributesScript.rangeSkill4) {
                        photonView.RPC("MonsterSkill4", PhotonTargets.All, go.GetPhotonView().viewID, monsterAttributesScript.attackDamage, 1);
                    }
                }

                if (GameObject.Find("Healer") != null && GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive) {
                    GameObject go = GameObject.Find("Healer");
                    if (Vector3.Distance(transform.position, go.transform.position) <= monsterAttributesScript.rangeSkill4) {
                        photonView.RPC("MonsterSkill4", PhotonTargets.All, go.GetPhotonView().viewID, monsterAttributesScript.attackDamage, 2);
                    }
                }

                if (GameObject.Find("Tank") != null && GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive) {
                    GameObject go = GameObject.Find("Tank");
                    if (Vector3.Distance(transform.position, go.transform.position) <= monsterAttributesScript.rangeSkill4) {
                        photonView.RPC("MonsterSkill4", PhotonTargets.All, go.GetPhotonView().viewID, monsterAttributesScript.attackDamage,3);
                    }
                }
                movementScript.stopMove();
                revealMeMonster();
                cdTSkill4.StartCooldown();
                skillMode = false;
            }


            //ulti soul vulcano
            else if (skillNumer == 41) {
                attackSkill41Active = true;
                skill41EndTime = Time.time + monsterAttributesScript.timeSkill41;
                skillMode = false;
                skill41ShootTime = 0.0f;
				photonView.RPC ("PlayMonsterSoulVulkanoSound", PhotonTargets.All, null);
                movementScript.stopMove();
            }



            //lamp
            else if (skillNumer == 5) {
                if (monsterLamp == null) {
                    skillCircle.renderer.enabled = true;
                    float x = (monsterAttributesScript.rangeSkill5 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
                    float y = (monsterAttributesScript.rangeSkill5 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
                    Vector3 scale = new Vector3(x, y, 1);
                    skillCircle.transform.localScale = scale * 2;
                    movementScript.stopMove();
                }            
                //port
                else {
                    movementScript.stopMove();
                    chargeLampe = true;
                    chargeLampeTick = 0;
                    skillMode = false;
					photonView.RPC ("PlayMonsterLampPortSound", PhotonTargets.All, null);
                    return;
                }
            }


            if (Input.GetMouseButtonDown(0) && !mouseClickSkill) {
                //fire skill
                fireSkill(skillNumer);
            }
            else if (mouseClickSkill) {
                mouseClickSkill = false;
            }

        }

        if (iAmAstral && astralTimer <= Time.time) {
            photonView.RPC("MonsterSkill31", PhotonTargets.All, false);
            astralTimer = 0.0f;
        }

        if (chargeLampe) {
            chargeLampeTick += 25 * Time.deltaTime;


            if (chargeLampeTick >= 100) {
                movementScript.iBlinked(monsterLamp.transform.position);
                cdTSkill5.StartCooldown();

                //destroy lamp
                photonView.RPC("MonsterDestrySkill5", PhotonTargets.MasterClient, monsterLamp.GetPhotonView().viewID);

                chargeLampe = false;
                chargeLampeTick = 0;
            }
        }


		//WIN | LOSE
		if (GameObject.Find ("GameManager") != null) {
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().monsterWin == true && winSoundPlayed == false) {
								PlayWinSound ();
								winSoundPlayed = true;
						}
			if (GameObject.Find ("GameManager").GetComponent<Winconditions> ().huntersWin == true && loseSoundPlayed == false) {
								PlayLoseSound ();
								loseSoundPlayed = true;
						}
				}

    }

    void fireSkill(int number) {
        if(GetComponent<AnimaitonScript>() != null)
        GetComponent<AnimaitonScript>().lookAt(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z));
        monsterControlScript.attackMode = false;

        //aoe flammen
        if (number == 1) {

            //liste holen
            List<GameObject> list =  skillCone.GetComponent<SkillConeScript>().insideList;
            mousePos = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z);
            //schaden machen
            foreach (GameObject go in list) {
                photonView.RPC("MonsterSkill11", PhotonTargets.MasterClient, go.GetPhotonView().viewID, monsterAttributesScript.damageSkill1);
            }

            photonView.RPC("StartMonsterAOEFlameAni", PhotonTargets.All, mousePos);
            animationScript.startAttackAnimationOne();
            revealMeMonster();
            cdTSkill1.StartCooldown();
            skillMode = false;
            movementScript.stopMove();
            
			photonView.RPC ("PlayMonsterAOEFlameSound", PhotonTargets.All, null);
        }
            //cleave
        else if (number == 11) {

            for (int i = 0; i < 3; i++) {
                string name = "";
                if (i == 0)
                    name = "Ranger";
                else if (i == 1)
                    name = "Tank";
                else if (i == 2)
                    name = "Healer";

                if (GameObject.Find(name) != null) {

                    //barriere checkup 
                    if (GameObject.Find("TankBarriere") != null) {

                        Vector3 direction = GameObject.Find(name).transform.position - transform.position;
                        int layerMask = 1 << 16;
                        RaycastHit hit;
                        if (Physics.Raycast(transform.position, direction, out hit, 1000f, layerMask)) {
                            if (hit.collider.gameObject.tag == "TankBarriere") {
                                continue;
                            }
                        }


                    }

                    if (Vector3.Distance(GameObject.Find(name).transform.position, transform.position) < monsterAttributesScript.rangeSkill11) {
                        photonView.RPC("MonsterSkill11", PhotonTargets.MasterClient, GameObject.Find(name).GetPhotonView().viewID, monsterAttributesScript.damageSkill11);
                    }
                }
            }
            animationScript.startAttackAnimationOne();
            cdTSkill11.StartCooldown();
            skillMode = false;
            photonView.RPC("StartCleaveAni", PhotonTargets.All);
			photonView.RPC ("PlayMonsterCleaveSound", PhotonTargets.All, null);
            movementScript.stopMove();
        }

       //swordstrike
        else if (number == 12) {

            if (mouseHitboxScriptMonster.mouseOverEnemy) {

                //in range?
                if (Vector3.Distance(transform.position, mouseHitboxScriptMonster.mouseOverLastEnemyGO.transform.position) < monsterAttributesScript.rangeSkill12) {
                   
                    Vector3 direction = mouseHitboxScriptMonster.mouseOverLastEnemyGO.transform.position - transform.position;
                    RaycastHit hit;
                    int layerMask = 1 << 16;
                    if (Physics.Raycast(transform.position, direction, out hit, 1000f, layerMask)) {
                        if (hit.collider.gameObject.tag == "TankBarriere") {
                        }
                        else {
                            photonView.RPC("MonsterSkill11", PhotonTargets.MasterClient, mouseHitboxScriptMonster.mouseOverLastEnemyGO.GetPhotonView().viewID, monsterAttributesScript.damageSkill12);
                        }
                    }
                    else {
                        photonView.RPC("MonsterSkill11", PhotonTargets.MasterClient, mouseHitboxScriptMonster.mouseOverLastEnemyGO.GetPhotonView().viewID, monsterAttributesScript.damageSkill12);
                    }

                    revealMeMonster();
                    cdTSkill12.StartCooldown();
                    movementScript.stopMove();
                    photonView.RPC("StartMonsterSwordStrikeAni", PhotonTargets.All, mouseHitboxScriptMonster.mouseOverLastEnemyGO.GetPhotonView().viewID);
					photonView.RPC ("PlayMonsterSwordStrikeSound", PhotonTargets.All, null);
                } else {
                    attackSkill12Active = true;
                    curTarget = mouseHitboxScriptMonster.mouseOverLastEnemyGO;
                }
            }
            skillMode = false;
            animationScript.startAttackAnimationOne();

        }

        //slow hände
        else if (number == 2) {
            //maus pos
            mousePos = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y, transform.position.z);

            //in range?
            if (Vector3.Distance(transform.position, mousePos) < monsterAttributesScript.rangeSkill2) {
                photonView.RPC("MonsterSkill2", PhotonTargets.MasterClient, mousePos, monsterAttributesScript.damageSkill2, monsterAttributesScript.slowFactorSkill2, monsterAttributesScript.slowTimeSkill2);
                cdTSkill2.StartCooldown();
                revealMeMonster();
                movementScript.stopMove();
            } else {
                attackSkill2Active = true;
            }
            skillMode = false;

			photonView.RPC ("PlayMonsterSoulHandsSound", PhotonTargets.All, null);
        }


        //hook
        else if (number == 21) {

            Vector3 hookTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            hookTarget.z = transform.position.z;

            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            if (Vector3.Distance(hookTarget, transform.position) > monsterAttributesScript.rangeSkill21) {
                hookTarget = hookTarget - transform.position;
                hookTarget.Normalize();
                hookTarget = hookTarget * monsterAttributesScript.rangeSkill21;
                hookTarget = hookTarget + transform.position;
            }
            revealMeMonster();
            photonView.RPC("MonsterSkill21", PhotonTargets.MasterClient, hookTarget, monsterAttributesScript.damageSkill21, 0);
            cdTSkill21.StartCooldown();
            skillMode = false;
            movementScript.stopMove();
			photonView.RPC ("PlayMonsterHookSound", PhotonTargets.All, null);
        }
        //silence
        else if (number == 22) {
            for (int i = 0; i < 3; i++) {
                string name = "";
                if (i == 0)
                    name = "Ranger";
                else if (i == 1)
                    name = "Tank";
                else if (i == 2)
                    name = "Healer";

                if (GameObject.Find(name) != null) {
                    if (Vector3.Distance(GameObject.Find(name).transform.position, transform.position) < monsterAttributesScript.rangeSkill22) {

                        Vector3 direction = GameObject.Find(name).transform.position - transform.position;
                        RaycastHit hit;
                        int layerMask = 1 << 16;
                        if (Physics.Raycast(transform.position, direction, out hit, 1000f, layerMask)) {
                            if (hit.collider.gameObject.tag == "TankBarriere") {
                                continue;
                            }
                            else {
                                photonView.RPC("MonsterSkill22", PhotonTargets.MasterClient, GameObject.Find(name).GetPhotonView().viewID, monsterAttributesScript.damageSkill22, monsterAttributesScript.timeSkill22);
                                photonView.RPC("startSilencedAni", PhotonTargets.All, GameObject.Find(name).GetPhotonView().viewID, monsterAttributesScript.timeSkill22);
                            }
                        }
                        else {
                            photonView.RPC("MonsterSkill22", PhotonTargets.MasterClient, GameObject.Find(name).GetPhotonView().viewID, monsterAttributesScript.damageSkill22, monsterAttributesScript.timeSkill22);
                            photonView.RPC("startSilencedAni", PhotonTargets.All, GameObject.Find(name).GetPhotonView().viewID, monsterAttributesScript.timeSkill22);
                        }
                    }
                }
            }
            animationScript.startAttackAnimationOne();
            cdTSkill22.StartCooldown();
            skillMode = false;
            movementScript.stopMove();
            photonView.RPC("StartMonsterSilenceAni", PhotonTargets.All);
			photonView.RPC ("PlayMonsterSilenceSound", PhotonTargets.All, null);
        }

        //flame charge
        else if (number == 3) {
            //abchecken ob aushalb der reichweite geklickt wurde
            //ziel berechnen
            Vector3 pointToMoveSkill3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pointToMoveSkill3.z = transform.position.z;

            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            if (Vector3.Distance(pointToMoveSkill3, transform.position) > monsterAttributesScript.rangeSkill3) {
                pointToMoveSkill3 = pointToMoveSkill3 - transform.position;
                pointToMoveSkill3.Normalize();
                pointToMoveSkill3 = pointToMoveSkill3 * monsterAttributesScript.rangeSkill3;
                pointToMoveSkill3 = pointToMoveSkill3 + transform.position;
            }
            movementScript.iCharged(pointToMoveSkill3);
            oldMovmentSpeed = monsterAttributesScript.movementSpeed;
            monsterAttributesScript.movementSpeed = monsterAttributesScript.movementSpeed * 3.0f;
            attackSkill3Active = true;
            currentCharging = true;
            cdTSkill3.StartCooldown();
            createFire = 0.0f;
            skillMode = false;

            photonView.RPC("StartChargeAniMonster", PhotonTargets.All);
			photonView.RPC ("PlayMonsterFlameChargeSound", PhotonTargets.All, null);
        }

        //blink explosion
        else if (number == 32) {

            //charge starten
            attackSkill32Active = true;
            GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.41f, 0.7f, 1.0f);
            movementScript.stopMove();
        }



        //ulti soul vertex
        else if (number == 4) {
            //nothing to do
			photonView.RPC ("PlayMonsterSoulVertexSound", PhotonTargets.All, null);
            movementScript.stopMove();
        }

        



        //lamp
        else if (number == 5) {
            //erstellen
            //abchecken ob aushalb der reichweite geklickt wurde
            //ziel berechnen
            Vector3 pointToMoveSkill3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pointToMoveSkill3.z = transform.position.z;

            //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
            if (Vector3.Distance(pointToMoveSkill3, transform.position) > monsterAttributesScript.rangeSkill5) {
                pointToMoveSkill3 = pointToMoveSkill3 - transform.position;
                pointToMoveSkill3.Normalize();
                pointToMoveSkill3 = pointToMoveSkill3 * monsterAttributesScript.rangeSkill5;
                pointToMoveSkill3 = pointToMoveSkill3 + transform.position;
            }

            //lamp erstellen
            photonView.RPC("MonsterSkill5", PhotonTargets.MasterClient, pointToMoveSkill3, 0);

            skillMode = false;
            movementScript.stopMove();
			photonView.RPC ("PlayMonsterLampSound", PhotonTargets.All, null);
        }



    }

    public void skillActive() {

        if (attackSkill12Active) {
            if (Vector3.Distance(transform.position, curTarget.transform.position) < monsterAttributesScript.rangeSkill12) {
                photonView.RPC("MonsterSkill11", PhotonTargets.MasterClient, mouseHitboxScriptMonster.mouseOverLastEnemyGO.GetPhotonView().viewID, monsterAttributesScript.damageSkill12);
                attackSkill12Active = false;
                revealMeMonster();
                cdTSkill12.StartCooldown();
                movementScript.stopMove();
                photonView.RPC("StartMonsterSwordStrikeAni", PhotonTargets.All, mouseHitboxScriptMonster.mouseOverLastEnemyGO.GetPhotonView().viewID);
					
				photonView.RPC ("PlayMonsterSwordStrikeSound", PhotonTargets.All, null);
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsEnemy(curTarget, monsterAttributesScript.rangeSkill12);
                }
            }
        }

        if (attackSkill2Active) {
            if (Vector3.Distance(transform.position, mousePos) < monsterAttributesScript.rangeSkill2) {

                Vector3 direction = mouseHitboxScriptMonster.mouseOverLastEnemyGO.transform.position - transform.position;
                RaycastHit hit;
                int layerMask = 1 << 16;
                if (Physics.Raycast(transform.position, direction, out hit, 1000f, layerMask)) {
                    if (hit.collider.gameObject.tag == "TankBarriere") {
                    }
                    else {
                        photonView.RPC("MonsterSkill2", PhotonTargets.MasterClient, mousePos, monsterAttributesScript.damageSkill2, monsterAttributesScript.slowFactorSkill2, monsterAttributesScript.slowTimeSkill2);
                    }
                }
                else {
                    photonView.RPC("MonsterSkill2", PhotonTargets.MasterClient, mousePos, monsterAttributesScript.damageSkill2, monsterAttributesScript.slowFactorSkill2, monsterAttributesScript.slowTimeSkill2);
                }
                revealMeMonster();
                attackSkill2Active = false;
                cdTSkill2.StartCooldown();
                movementScript.stopMove();
            } else {
                //zu ihm hin laufen
                if (!movementScript.moving) {
                    movementScript.moveTowardsPoint(mousePos, monsterAttributesScript.rangeSkill2);
                }
            }
        }


        if (attackSkill3Active) {
               //feuer erzeugen
            if (createFire <= Time.time) {
                photonView.RPC("MonsterCreateChargeFire", PhotonTargets.All, transform.position);
                createFire = Time.time + 0.1f;
            }
        }

        if (attackSkill32Active) {

            skill32Charge += 80 * Time.deltaTime;


            if (skill32Charge >= 100) {
                //abchecken ob aushalb der reichweite geklickt wurde
                //ziel berechnen
                Vector3 pointToMoveSkill3 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                pointToMoveSkill3.z = transform.position.z;

                //Wenn außerhalb der Range geklickt -> positon berechnen am Kreisrand
                if (Vector3.Distance(pointToMoveSkill3, transform.position) > monsterAttributesScript.rangeSkill32) {
                    pointToMoveSkill3 = pointToMoveSkill3 - transform.position;
                    pointToMoveSkill3.Normalize();
                    pointToMoveSkill3 = pointToMoveSkill3 * monsterAttributesScript.rangeSkill32;
                    pointToMoveSkill3 = pointToMoveSkill3 + transform.position;
                }

                photonView.RPC("MonsterSkill32Animation", PhotonTargets.All, pointToMoveSkill3, transform.position);

                movementScript.iBlinked(pointToMoveSkill3);
 
                //explosion
                photonView.RPC("MonsterSkill32", PhotonTargets.MasterClient, pointToMoveSkill3, monsterAttributesScript.damageSkill32, monsterAttributesScript.rangeSkill32Explosion);
                cdTSkill32.StartCooldown();
                skillMode = false;

                photonView.RPC("PlayMonsterJumpSound", PhotonTargets.All, null);

                skill32Charge = 0;
                attackSkill32Active = false;
                GetComponent<SpriteRenderer>().color = oldColor;
            }

        }




        //soul vulcano
        if (attackSkill41Active) {
            soulsplinter = 0;
            if (Time.time < skill41EndTime) {

                if (Time.time > skill41ShootTime) {
                    int inrange = 0;
                    List<GameObject> goList = new List<GameObject>();
                    if (GameObject.Find("Ranger") != null && GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive && Vector3.Distance(GameObject.Find("Ranger").transform.position, transform.position) <= monsterAttributesScript.rangeSkill41) {
                        inrange++;
                        goList.Add(GameObject.Find("Ranger"));
                    }
                    if (GameObject.Find("Healer") != null && GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive && Vector3.Distance(GameObject.Find("Healer").transform.position, transform.position) <= monsterAttributesScript.rangeSkill41) {
                        inrange++;
                        goList.Add(GameObject.Find("Healer"));
                    }
                    if (GameObject.Find("Tank") != null && GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive && Vector3.Distance(GameObject.Find("Tank").transform.position, transform.position) <= monsterAttributesScript.rangeSkill41) {
                        inrange++;
                        goList.Add(GameObject.Find("Tank"));
                    }

                    if (inrange != 0) {
                        int rnd = Random.Range(1, inrange+1);


                        photonView.RPC("MonsterSkill41", PhotonTargets.MasterClient, goList[rnd-1].GetPhotonView().viewID, monsterAttributesScript.damageSkill41, 0);
                    }

                    skill41ShootTime = Time.time + 0.2f;
                }
                revealMeMonster();
            }
            else {
                attackSkill41Active = false;
                cdTSkill41.StartCooldown();
                soulsplinter = 0;
            }
        }


    }

    public void disableSkills() {

        if (attackSkill32Active) {
            attackSkill32Active = false;
            skill32Charge = 0;
            cdTSkill32.StartCooldown();
            GetComponent<SpriteRenderer>().color = oldColor;
        }

        attackSkill12Active = false;
        attackSkill2Active = false;
        attackSkill3Active = false;
    }

    public void interruptChargeLamp(){
        chargeLampe = false;
    }

    private void checkUpKeyPressed() {

        if (!monsterControlScript.iAmTaunted&&!GameObject.Find("HUDManager").GetComponent<HUDScript>().monsterLoadout)
        {
            if (!attackSkill41Active)
            {
                if (Input.GetKeyDown(KeyCode.Q) && !cdTSkill1.IsOnCooldown() && !cdTSkill11.IsOnCooldown() && !cdTSkill12.IsOnCooldown())
                {
                    if (qSkill == 1)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 1;
                        interruptChargeLamp();
                        disableSkills();
                    }
                    else if (qSkill == 11)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 11;
                        interruptChargeLamp();
                        disableSkills();
                    }
                    else if (qSkill == 12)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 12;
                        interruptChargeLamp();
                        disableSkills();
                    }
                }
                else if (Input.GetKeyDown(KeyCode.W) && !cdTSkill2.IsOnCooldown() && !cdTSkill21.IsOnCooldown() && !cdTSkill22.IsOnCooldown())
                {
                    if (wSkill == 2)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 2;
                        interruptChargeLamp();
                        disableSkills();
                    }
                    else if (wSkill == 21)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 21;
                        interruptChargeLamp();
                        disableSkills();
                    }
                    else if (wSkill == 22)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 22;
                        interruptChargeLamp();
                        disableSkills();
                    }

                }
                else if (Input.GetKeyDown(KeyCode.E) && !cdTSkill3.IsOnCooldown() && !cdTSkill31.IsOnCooldown() && !cdTSkill32.IsOnCooldown())
                {
                    if (eSkill == 3)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 3;
                        interruptChargeLamp();
                        disableSkills();
                    }
                    else if (eSkill == 31)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 31;
                        interruptChargeLamp();
                        disableSkills();
                    }
                    else if (eSkill == 32)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 32;
                        interruptChargeLamp();
                        disableSkills();
                    }

                }
                else if (Input.GetKeyDown(KeyCode.R) && !cdTSkill4.IsOnCooldown() && !cdTSkill41.IsOnCooldown() && soulsplinter >= 100)
                {

                    if (rSkill == 4)
                    {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 4;
                        soulsplinter = 0;
                        interruptChargeLamp();
                        disableSkills();
                    }
                    else if (rSkill == 41)
                    {
                        if (!attackSkill41Active)
                        {
                            skillMode = true;
                            skillNumer = 41;
                            soulsplinter = 0;
                            PlaySkillActivatedSound();
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else
                        {
                            attackSkill41Active = false;
                            cdTSkill41.StartCooldown();
                            soulsplinter = 0;
                        }
                    }
                }
                else if (Input.GetKeyDown(KeyCode.T) && !cdTSkill5.IsOnCooldown())
                {
                    PlaySkillActivatedSound();
                    skillMode = true;
                    skillNumer = 5;
                    disableSkills();
                }
            }
           
        }

    }


    public void mouseClickOnSkill(int skill) {

        if (!currentCharging && monsterAttributesScript.isAlive && !GameObject.Find("GameManager").GetComponent<GameStarter>().MonsterLocked) {

            mouseClickSkill = true;

            if (!monsterControlScript.iAmTaunted && !GameObject.Find("HUDManager").GetComponent<HUDScript>().monsterLoadout) {
                if (!attackSkill41Active) {
                    if (skill == 1 && !cdTSkill1.IsOnCooldown() && !cdTSkill11.IsOnCooldown() && !cdTSkill12.IsOnCooldown()) {
                        if (qSkill == 1) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 1;
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else if (qSkill == 11) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 11;
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else if (qSkill == 12) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 12;
                            interruptChargeLamp();
                            disableSkills();
                        }
                    }
                    else if (skill == 2 && !cdTSkill2.IsOnCooldown() && !cdTSkill21.IsOnCooldown() && !cdTSkill22.IsOnCooldown()) {
                        if (wSkill == 2) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 2;
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else if (wSkill == 21) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 21;
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else if (wSkill == 22) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 22;
                            interruptChargeLamp();
                            disableSkills();
                        }

                    }
                    else if (skill == 3 && !cdTSkill3.IsOnCooldown() && !cdTSkill31.IsOnCooldown() && !cdTSkill32.IsOnCooldown()) {
                        if (eSkill == 3) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 3;
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else if (eSkill == 31) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 31;
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else if (eSkill == 32) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 32;
                            interruptChargeLamp();
                            disableSkills();
                        }

                    }
                    else if (skill == 4 && !cdTSkill4.IsOnCooldown() && !cdTSkill41.IsOnCooldown() && soulsplinter >= 100) {

                        if (rSkill == 4) {
                            PlaySkillActivatedSound();
                            skillMode = true;
                            skillNumer = 4;
                            soulsplinter = 0;
                            interruptChargeLamp();
                            disableSkills();
                        }
                        else if (rSkill == 41) {
                            if (!attackSkill41Active) {
                                skillMode = true;
                                skillNumer = 41;
                                soulsplinter = 0;
                                PlaySkillActivatedSound();
                                interruptChargeLamp();
                                disableSkills();
                            }
                            else {
                                attackSkill41Active = false;
                                cdTSkill41.StartCooldown();
                                soulsplinter = 0;
                            }
                        }
                    }
                    else if (skill == 5 && !cdTSkill5.IsOnCooldown()) {
                        PlaySkillActivatedSound();
                        skillMode = true;
                        skillNumer = 5;
                        disableSkills();
                    }
                }

            }

        }
    }




    void OnTriggerEnter(Collider coll) {
        if (currentCharging && coll.tag == "TankBarriere") {
            setOldMovementSpeed();
            movementScript.iMoved(transform.position);
        }

    }


    [RPC]
    void MonsterSkill11(int viewID, float damage) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (PhotonNetwork.isMasterClient) {
            if (trans.GetComponent<RangerAttributesScript>() != null) {
                trans.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, true);
            }
            else if (trans.GetComponent<TankAttributesScript>() != null) {
                trans.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, true);
            }
            else if (trans.GetComponent<HealerAttributesScript>() != null) {
                trans.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, true);
            }
        }
    }

    [RPC]
    void MonsterSkill2(Vector3 pos, float damage, float slowFactor, float slowTime) {
        GameObject go = PhotonNetwork.Instantiate("Monster/Skills/MonsterSkill2", pos, transform.rotation, 0) as GameObject;

        go.GetComponent<MonsterSkill2Script>().damage = damage;
        go.GetComponent<MonsterSkill2Script>().slowFactor = slowFactor;
        go.GetComponent<MonsterSkill2Script>().slowTime = slowTime;
        go.GetComponent<MonsterSkill2Script>().startTimer(1.3f);

    }


    [RPC]
    void MonsterSkill21(Vector3 target, float attackDamage, int goViewID) {
        GameObject go;

        if (PhotonNetwork.isMasterClient) {
            go = PhotonNetwork.Instantiate("Monster/Skills/MonsterSkill21", transform.position, transform.rotation, 0) as GameObject;
            photonView.RPC("MonsterSkill21", PhotonTargets.Others, target,
                                attackDamage, go.GetPhotonView().viewID);

        } else {
            go = PhotonView.Find(goViewID).gameObject;
        }


        drawLine.startDrawLine(this.transform, go.transform);
        MonsterSkill21Script mS = go.GetComponent<MonsterSkill21Script>();
        mS.damage = attackDamage;
        mS.target = target;
        mS.monsterViewID = this.gameObject.GetPhotonView().viewID;
    }


    public void setOldMovementSpeed() {
        monsterAttributesScript.movementSpeed = monsterAttributesScript.oldMovementSpeed;
        attackSkill3Active = false;
        currentCharging = false;
        photonView.RPC("StopChargeAniMonster", PhotonTargets.All);

    }

    [RPC]
    void MonsterCreateChargeFire(Vector3 target) {
        Transform go = Instantiate(chargeFire, target, transform.rotation) as Transform;
    }

    [RPC]
    void MonsterSkill31(bool b) {
        iAmAstral = b;
        if (b) {
            this.GetComponent<SpriteRenderer>().color = new Color(1.0f, 0.85f, 0.0f, 0.5f);
        }
        else {
            this.GetComponent<SpriteRenderer>().color = oldColor;
        }
    }

    
    [RPC]
    void MonsterSkill5(Vector3 target, int viewID) {
        if (PhotonNetwork.isMasterClient) {
            monsterLamp = PhotonNetwork.Instantiate("Monster/Skills/MonsterLamp", target, transform.rotation, 0) as GameObject;
            photonView.RPC("MonsterSkill5", PhotonTargets.Others, target, monsterLamp.GetPhotonView().viewID);

        }
        else {
            monsterLamp = PhotonView.Find(viewID).gameObject;
        }
    }

    
    [RPC]
    void MonsterDestrySkill5(int viewID) {
        PhotonNetwork.Destroy(PhotonView.Find(viewID).gameObject);
    }



    [RPC]
    void MonsterSkill4(int viewID, float attackDamage, int line) {
        GameObject go = PhotonView.Find(viewID).gameObject;

        go.GetComponent<Movement>().iAmHookedByMonster(this.gameObject.GetPhotonView().viewID, line);

        if (line == 1) {
            GameObject.Find("LineRenderer4").GetComponent<DrawLineScript>().startDrawLine(this.transform, PhotonView.Find(viewID).gameObject.transform);
        }
        else if (line == 2) {
            GameObject.Find("LineRenderer5").GetComponent<DrawLineScript>().startDrawLine(this.transform, PhotonView.Find(viewID).gameObject.transform);
        }
        else if (line == 3) {
            GameObject.Find("LineRenderer6").GetComponent<DrawLineScript>().startDrawLine(this.transform, PhotonView.Find(viewID).gameObject.transform);
        }



        if (PhotonNetwork.isMasterClient) {
            if (go.GetComponent<RangerAttributesScript>() != null) {
                go.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-attackDamage, true);
            }
            else if (go.GetComponent<HealerAttributesScript>() != null) {
                go.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-attackDamage, true);
            }
            else if (go.GetComponent<TankAttributesScript>() != null) {
                go.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-attackDamage, true);
            }
        }
    }

    [RPC]
    void MonsterSkill41(int targetViewID, float attackDamage, int goViewID) {
        GameObject go;

        if (PhotonNetwork.isMasterClient) {
            go = PhotonNetwork.Instantiate("Monster/Skills/MonsterSkill41", transform.position, transform.rotation, 0) as GameObject;
            photonView.RPC("MonsterSkill41", PhotonTargets.Others, targetViewID,
                                attackDamage, go.GetPhotonView().viewID);

        }
        else {
            go = PhotonView.Find(goViewID).gameObject;
        }

        MonsterSkill41Script mS = go.GetComponent<MonsterSkill41Script>();
        mS.damage = attackDamage;
        mS.targetViewID = targetViewID;
    }

    
    [RPC]
    void MonsterSkill22(int viewID, float damage, float time) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (PhotonNetwork.isMasterClient) {
            if (trans.GetComponent<RangerAttributesScript>() != null) {
                trans.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, true);
            }
            else if (trans.GetComponent<TankAttributesScript>() != null) {
                trans.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, true);
            }
            else if (trans.GetComponent<HealerAttributesScript>() != null) {
                trans.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, true);
            }
        }

        photonView.RPC("MonsterSkill22SilenceOnAll", PhotonTargets.All, viewID, time);
    }

    [RPC]
    void MonsterSkill22SilenceOnAll(int viewID, float time) {
        Transform trans = PhotonView.Find(viewID).transform;
        //fallunterscheidung
        if (trans.GetComponent<RangerAttributesScript>() != null) {
            trans.GetComponent<RangerSkillControlScript>().silenceMe(time);
        }
        else if (trans.GetComponent<TankAttributesScript>() != null) {
            trans.GetComponent<TankSkillControlScript>().silenceMe(time);
        }
        else if (trans.GetComponent<HealerAttributesScript>() != null) {
            trans.GetComponent<HealerSkillControlScript>().silenceMe(time);
        }
    }

    
    [RPC]
    void MonsterSkill32(Vector3 pos, float damage, float range) {
        if(PhotonNetwork.isMasterClient){
            for (int i = 0; i < 3; i++) {
                string name = "";
                if (i == 0)
                    name = "Ranger";
                else if (i == 1)
                    name = "Tank";
                else if (i == 2)
                    name = "Healer";

                if (GameObject.Find(name) != null) {
                    if (Vector3.Distance(GameObject.Find(name).transform.position, pos) < range) {
                        if (name == "Ranger") {
                            GameObject.Find(name).GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, true);
                        }
                        else if (name == "Healer") {
                            GameObject.Find(name).GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, true);
                        }
                        else if (name == "Tank") {
                            GameObject.Find(name).GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, true);
                        }
                    }
                }
            }
        }
    }


    public void revealMeMonster() {
       photonView.RPC("revealMeMonsterRPC", PhotonTargets.Others);
    }


    [RPC]
    public void revealMeMonsterRPC() {
        if (GameObject.Find("Healer") != null) {
            GameObject.Find("Healer").GetComponent<InviChecker>().revealMonster();
        }
        if (GameObject.Find("Tank") != null) {
            GameObject.Find("Tank").GetComponent<InviChecker>().revealMonster();
        }
        if (GameObject.Find("Ranger") != null) {
            GameObject.Find("Ranger").GetComponent<InviChecker>().revealMonster();
        }
    }

    [RPC]
    public void startSilencedAni(int viewID, float time) {
        GameObject go = Instantiate(silcenedAni, PhotonView.Find(viewID).transform.position, PhotonView.Find(viewID).transform.rotation) as GameObject;

        go.GetComponent<SilenceAniScript>().target = PhotonView.Find(viewID).gameObject;
        go.GetComponent<SilenceAniScript>().time = time+Time.time;
    }

    void OnGUI() {
        //ward
        if (chargeLampe) {
            float rx = Screen.width / native_width;
            float ry = Screen.height / native_height;
            GL.PushMatrix();
            GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
            GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));


                GUI.DrawTexture(new Rect(1920 / 2 - tex1.width / 2, 900 - tex1.height, tex1.width, tex1.height), tex1);

                GUI.BeginGroup(new Rect(1920 / 2 - tex2.width / 2, 900 - tex2.height, tex2.width * (chargeLampeTick / 100), tex2.height));
                GUI.DrawTexture(new Rect(0, 0, tex2.width, tex2.height), tex2);
                GUI.EndGroup();

            GL.PopMatrix();
        }

        //cast balken skill32
        if (attackSkill32Active) {
            float rx = Screen.width / native_width;
            float ry = Screen.height / native_height;
            GL.PushMatrix();
            GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
            GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));


            GUI.DrawTexture(new Rect(1920 / 2 - tex1.width / 2, 900 - tex1.height, tex1.width, tex1.height), tex1);

            GUI.BeginGroup(new Rect(1920 / 2 - tex2.width / 2, 900 - tex2.height, tex2.width * (skill32Charge / 100), tex2.height));
            GUI.DrawTexture(new Rect(0, 0, tex2.width, tex2.height), tex2);
            GUI.EndGroup();

            GL.PopMatrix();
        }
    }

	// Sound Methods
	private void PlaySkillActivatedSound() 
	{
		PlaySound(0,0.45f);
	}
	
	[RPC]
	public void PlayMonsterAOEFlameSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (1);
	}
	
	[RPC]
	public void PlayMonsterCleaveSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (2);
	}
	
	[RPC]
	public void PlayMonsterSwordStrikeSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (3);
	}
	
	[RPC]
	public void PlayMonsterSoulHandsSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (4);
	}
	
	[RPC]
	public void PlayMonsterHookSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (5);
	}
	
	[RPC]
	public void PlayMonsterSilenceSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (6);
	}
	
	[RPC]
	public void PlayMonsterFlameChargeSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (7);
	}
	
	[RPC]
	public void PlayMonsterAstralSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (8);
	}

	[RPC]
	public void PlayMonsterJumpSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (9);
	}

	[RPC]
	public void PlayMonsterSoulVertexSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (10);
	}

	[RPC]
	public void PlayMonsterSoulVulkanoSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (11);
	}

	[RPC]
	public void PlayMonsterLampSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (12);
	}

	[RPC]
	public void PlayMonsterLampPortSound()
	{
        if (GetComponent<SpriteRenderer>().enabled)
        PlaySound (13);
	}
	
	
	private void PlaySound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	private void PlaySound(int Number, float volume)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, volume*menuScript.scaleVolumeFactor);
	}

	private void PlayWinSound()
	{
		AudioSource.PlayClipAtPoint(winSound, transform.position, 0.75f*menuScript.scaleVolumeFactor);
	}
	
	private void PlayLoseSound()
	{
		AudioSource.PlayClipAtPoint(loseSound, transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}


    [RPC]
    public void StartCleaveAni() {
        GameObject go = Instantiate(cleaveAni1, transform.position, transform.rotation) as GameObject;
        GameObject go2 = Instantiate(cleaveAni2, transform.position, transform.rotation) as GameObject;

        go.GetComponent<DestroyAfterTimeScript>().target = this.gameObject;
    }



    [RPC]
    public void StartMonsterSwordStrikeAni(int viewID) {
        GameObject go = Instantiate(swordStrikeAni, PhotonView.Find(viewID).transform.position, transform.rotation) as GameObject;

        go.GetComponent<DestroyAfterTimeScript>().target = PhotonView.Find(viewID).gameObject;
    }


    [RPC]
    public void StartChargeAniMonster() {
        GameObject go = Instantiate(chargeAni, transform.position, transform.rotation) as GameObject;
        go.GetComponent<MonsterChargeAni>().target = this.gameObject;
    }

    [RPC]
    public void StopChargeAniMonster() {
        Destroy(GameObject.Find("MonsterChargeAni"));
    }

    
    [RPC]
    public void MonsterSkill32Animation(Vector3 blinkTarget, Vector3 p2) {
        Instantiate(jumpAni1, p2, transform.rotation);
        Instantiate(jumpAni2, blinkTarget, transform.rotation);
    }

    [RPC]
    public void StartMonsterAOEFlameAni(Vector3 nMouse) {
        GameObject go = Instantiate(aoeFlameAni, transform.position, transform.rotation) as GameObject;
        go.GetComponent<MonsterAOEFlameAniScript>().target = this.gameObject;
        go.GetComponent<MonsterAOEFlameAniScript>().mousePos = nMouse;

        Vector3 offset = transform.position - nMouse;
        offset.Normalize();
        offset = 1.2f * offset;
        go.GetComponent<MonsterAOEFlameAniScript>().offset = offset;

    }

     [RPC]
    public void StartMonsterSilenceAni() {
        GameObject go = Instantiate(silenceAni, transform.position, transform.rotation) as GameObject;
        go.GetComponent<DestroyAfterTimeScript>().target = this.gameObject;
    }


     public void showCircle(int which) {

         if (which == 1 && qSkill == 1) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill1 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 1 && qSkill == 11) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill11 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 1 && qSkill == 12) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill12 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill12 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 2 && wSkill == 2) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill2 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 2 && wSkill == 21) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill21 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 2 && wSkill == 22) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill22 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill22 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 3 && eSkill == 3) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill3 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 3 && eSkill == 32) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill32 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill32 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 4 && rSkill == 4) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill4 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill4 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 4 && rSkill == 41) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill41 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill41 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }
         else if (which == 5) {
             skillCircle.renderer.enabled = true;
             float x = (monsterAttributesScript.rangeSkill5 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.width);
             float y = (monsterAttributesScript.rangeSkill5 * 100 / skillCircle.GetComponent<SpriteRenderer>().sprite.rect.height);
             Vector3 scale = new Vector3(x, y, 1);
             skillCircle.transform.localScale = scale * 2;
         }




     }

}


