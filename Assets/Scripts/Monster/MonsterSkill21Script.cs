﻿using UnityEngine;
using System.Collections;

public class MonsterSkill21Script : Photon.MonoBehaviour {

    public float damage;
    private bool destroyed;
    private bool hit;
    private GameObject hittedEnemy;
    private float speed = 10.0f;
    public Vector3 target;
    public int monsterViewID;

    // Use this for initialization
    void Start() {
        name = "MonsterHook";
        destroyed = false;
        hit = false;
    }

    // Update is called once per frame
    void Update() {

        if (!hit) {
            float step = speed * Time.deltaTime;
            Vector3 moveTowards = Vector3.MoveTowards(transform.position, target, step);
            transform.position = moveTowards;

            Vector3 direction = target - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            angle += 180.0f;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);



        } else {
            transform.position = hittedEnemy.transform.position;
        }

        //destroy

        if (PhotonNetwork.isMasterClient && transform.position == target) {
            photonView.RPC("destroyMonsterHook", PhotonTargets.MasterClient, this.gameObject.GetPhotonView().viewID);
        }
    }

    void OnTriggerEnter(Collider collider) {

        if (collider.transform.tag.Equals("Team")) {

            GameObject.Find("LineRenderer3").GetComponent<DrawLineScript>().destination = collider.transform;

            if (PhotonNetwork.isMasterClient && !hit) {
                //leben anpassen im master
                if (collider.gameObject.GetComponent<RangerAttributesScript>() != null && collider.gameObject.GetComponent<RangerAttributesScript>().isAlive) {
                    collider.gameObject.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, true);

                    hit = true;
                    hittedEnemy = collider.gameObject;

                    //slow
                    photonView.RPC("informOtherHook", PhotonTargets.Others, collider.gameObject.GetPhotonView().viewID);

                    photonView.RPC("informTeamHook", PhotonTargets.All, collider.gameObject.GetPhotonView().viewID, monsterViewID);


                }
                else if (collider.gameObject.GetComponent<HealerAttributesScript>() != null && collider.gameObject.GetComponent<HealerAttributesScript>().isAlive) {
                    collider.gameObject.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, true);


                    hit = true;
                    hittedEnemy = collider.gameObject;

                    //slow
                    photonView.RPC("informOtherHook", PhotonTargets.Others, collider.gameObject.GetPhotonView().viewID);

                    photonView.RPC("informTeamHook", PhotonTargets.All, collider.gameObject.GetPhotonView().viewID, monsterViewID);

                }
                else if (collider.gameObject.GetComponent<TankAttributesScript>() != null && collider.gameObject.GetComponent<TankAttributesScript>().isAlive) {
                    collider.gameObject.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, true);

                    hit = true;
                    hittedEnemy = collider.gameObject;

                    //slow
                    photonView.RPC("informOtherHook", PhotonTargets.Others, collider.gameObject.GetPhotonView().viewID);

                    photonView.RPC("informTeamHook", PhotonTargets.All, collider.gameObject.GetPhotonView().viewID, monsterViewID);


                }

            } 
        }
    }

    void OnDestroy() {
        transform.renderer.enabled = false;
    }


    [RPC]
    void informOtherHook(int viewID) {
        hit = true;
        hittedEnemy = PhotonView.Find(viewID).gameObject;
    }

    [RPC]
    void destroyMonsterHook(int viewID) {
        PhotonNetwork.Destroy(PhotonView.Find(viewID).gameObject);
    }

    [RPC]
    void informTeamHook(int viewID, int monsterViewID) {
        PhotonView.Find(viewID).gameObject.GetComponent<Movement>().iAmHookedByMonster(monsterViewID, 0);
    }

}
