﻿using UnityEngine;
using System.Collections;

public class MonsterLampScript : Photon.MonoBehaviour {

    private Menu menu;


	// Use this for initialization
	void Start () {
        menu = GameObject.Find("Menu").GetComponent<Menu>();

        if (menu.iAmOnTeam != 2) {
            this.GetComponent<FogOfWarCircleRevealer>().enabled = false;
            enabled = false;
            this.GetComponent<FogOfWarRenderer>().enabled = true;
            return;
        }


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
