﻿using UnityEngine;
using System.Collections;

public class MonsterSkill41Script : Photon.MonoBehaviour {

    public float damage;
    public int targetViewID;
    private bool hit;
    private float speed;

	// Use this for initialization
	void Start () {

        speed = Random.RandomRange(0.3f, 0.6f);
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (targetViewID != 0) {
            Vector3 moveTowards = Vector3.MoveTowards(transform.position, PhotonView.Find(targetViewID).transform.position, speed);

            transform.position = moveTowards;

            Vector3 direction = PhotonView.Find(targetViewID).transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            angle += 180.0f;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);



        }

        if (PhotonNetwork.isMasterClient) {
            if (Vector3.Distance(transform.position, PhotonView.Find(targetViewID).transform.position) <= 0.1f) {
                PhotonNetwork.Destroy(this.gameObject);
            }
        }
	}

    void OnTriggerEnter(Collider collider) {

        if (collider.tag == "Team") {

            if (PhotonNetwork.isMasterClient && !hit) {
                //leben anpassen im master
                if (collider.gameObject.GetComponent<RangerAttributesScript>() != null) {
                    collider.gameObject.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, false);
                }
                else if (collider.gameObject.GetComponent<HealerAttributesScript>() != null) {
                    collider.gameObject.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, false);
                }
                else if (collider.gameObject.GetComponent<TankAttributesScript>() != null) {
                    collider.gameObject.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, false);
                }

                hit = true;

                PhotonNetwork.Destroy(this.gameObject);
            }
        }
    }


    void OnDestroy() {
        transform.renderer.enabled = false;
    }

}
