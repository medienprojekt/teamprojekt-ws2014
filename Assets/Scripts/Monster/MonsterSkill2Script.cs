﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterSkill2Script : Photon.MonoBehaviour {

    private List<GameObject> objectsInCricle;
    private bool timerActive;
    private float timerUp;
    public float slowTime;
    public float slowFactor;
    private bool destroyTimerActive;
    private float destroyTimer;
    public float damage;
    private Color curColor;
    public GameObject stunAni;

    // Use this for initialization
    void Start() {
        objectsInCricle = new List<GameObject>();
        curColor = GetComponent<SpriteRenderer>().color;
    }

    // Update is called once per frame
    void Update() {
        Vector3 v3 = transform.position;
        v3.z = 3;
        transform.position = v3;

        if (timerActive && Time.time >= timerUp) {

            //farbe ändern - ausgelöst
            photonView.RPC("changeColorMonsterSkill2", PhotonTargets.All);
            
            timerActive = false;
            destroyTimerActive = true;
            destroyTimer = Time.time + 0.5f;

            //slow auslösen
            if (PhotonNetwork.isMasterClient) {
                foreach (GameObject go in objectsInCricle) {
                    if (go.tag == "Team") {
                        photonView.RPC("makeTeamDizzy", PhotonTargets.All, go.GetPhotonView().viewID, slowTime, slowFactor, damage);
                        photonView.RPC("startStunAni", PhotonTargets.All, go.GetPhotonView().viewID, slowTime);
                    }
                }
            }
        }

    }

    void FixedUpdate() {
        if (destroyTimerActive) {
            curColor.a -= 0.02f;
            GetComponent<SpriteRenderer>().color = curColor;
            if (curColor.a <= 0.0f) {
                PhotonNetwork.Destroy(this.gameObject);
            }
        }

    }


    public void startTimer(float time) {
        timerActive = true;
        timerUp = time + Time.time;
    }


    void OnTriggerEnter(Collider other) {
        if (other.tag == "Team") {
            objectsInCricle.Add(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Team") {
            objectsInCricle.Remove(other.gameObject);
        }
    }


    [RPC]
    void makeTeamDizzy(int viewID, float time, float slowFactor, float damage) {
        GameObject go = PhotonView.Find(viewID).gameObject;

        if (go.GetComponent<RangerControlScript>() != null) {
            go.GetComponent<RangerControlScript>().makeMeDizzy(time, slowFactor);
        } else if (go.GetComponent<HealerControlScript>() != null) {
            go.GetComponent<HealerControlScript>().makeMeDizzy(time, slowFactor);
        } else if (go.GetComponent<TankControlScript>() != null) {
            go.GetComponent<TankControlScript>().makeMeDizzy(time, slowFactor);
        }

        //bonus dmg
        if (PhotonNetwork.isMasterClient) {
            if (go.GetComponent<RangerAttributesScript>() != null) {
                go.GetComponent<RangerAttributesScript>().AddjustCurrentHealth(-damage, true);
            } else if (go.GetComponent<HealerAttributesScript>() != null) {
                go.GetComponent<HealerAttributesScript>().AddjustCurrentHealth(-damage, true);
            } else if (go.GetComponent<TankAttributesScript>() != null) {
                go.GetComponent<TankAttributesScript>().AddjustCurrentHealth(-damage, true);
            }
        }


    }

    [RPC]
    public void changeColorMonsterSkill2() {
        this.GetComponent<Animator>().SetBool("Done", true);
    }


    [RPC]
    public void startStunAni(int viewID, float time) {
        GameObject go = Instantiate(stunAni, PhotonView.Find(viewID).transform.position, PhotonView.Find(viewID).transform.rotation) as GameObject;

        go.GetComponent<StunAniScript>().target = PhotonView.Find(viewID).gameObject;
        go.GetComponent<StunAniScript>().time = time + Time.time;
    }
}


