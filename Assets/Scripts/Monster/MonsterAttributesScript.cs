﻿using UnityEngine;
using System.Collections;

public class MonsterAttributesScript : Photon.MonoBehaviour {

	private Menu menuScript;

    //Health
    public float maxHealth = 3000;
    public float curHealth = 3000;
    public float healthBarLength;

    //Other Attributes
    public float attackDamage;
    public float attackSpeed;
    public float attackRange;
    public float movementSpeed = 3.0f;
    public string playerClass;

    public float oldMovementSpeed;

    private float oldMovmentspeed;
    private float slowReleaseTime;
    private bool isSlowed;

    public float cdSkill1 = 3.0f;
    public float cdSkill2 = 3.0f;
    public float cdSkill3 = 3.0f;
    public float cdSkill4 = 3.0f;
    public float cdSkill11 = 3.0f;
    public float cdSkill21 = 3.0f;
    public float cdSkill31 = 3.0f;
    public float cdSkill41 = 3.0f;
    public float cdSkill12 = 3.0f;
    public float cdSkill22 = 3.0f;
    public float cdSkill32 = 3.0f;
    public float cdSkill5 = 3.0f;


    public float damageSkill1 = 20.0f;
    public float rangeSkill1 = 5.0f;

    public float rangeSkill11 = 2.0f;
    public float damageSkill11 = 20.0f;

    public float rangeSkill12 = 0.5f;
    public float damageSkill12 = 40.0f;

    public float rangeSkill2 = 5.0f;
    public float slowFactorSkill2 = 0.5f;
    public float slowTimeSkill2 = 4.0f;
    public float damageSkill2 = 10.0f;

    public float damageSkill21 = 10.0f;
    public float rangeSkill21 = 8.0f;

    public float rangeSkill22 = 7.0f;
    public float timeSkill22 = 4.0f;
    public float damageSkill22 = 10.0f;

    public float rangeSkill3 = 8.0f;
    public float damageSkill3 = 4.0f;

    public float astralTimeSkill31 = 5.0f;

    public float rangeSkill32 = 8.0f;
    public float damageSkill32 = 20.0f;
    public float rangeSkill32Explosion = 2.0f;

    public float rangeSkill4 = 8.0f;
    public float damageSkill4 = 30.0f;

    public float rangeSkill41 = 15.0f;
    public float damageSkill41 = 50.0f;
    public float timeSkill41 = 8.0f;


    public float rangeSkill5 = 8.0f;

    private bool enrage;
    public float enrageBuff = 0.20f;


    public bool isAlive;

    private bool startHitAnimation;
    private float hitAnimationTimer;
    private Color oldColor;


    private MonsterSkillControlScript monsterSkillControlScript;
    private MonsterControlScript monsterControlScript;

	public AudioClip[] sounds;
	public AudioClip[] sounds2;
	public AudioSource asound;

    public Texture hitTex;
    private float alphaHit;

    public Texture deadTex;

    private Movement movementScript;

    // Use this for initialization
    void Start() {
		menuScript = GameObject.Find("Menu").GetComponent<Menu>();

        name = "Monster";
        isAlive = true;
        monsterSkillControlScript = this.GetComponent<MonsterSkillControlScript>();
        monsterControlScript = this.GetComponent<MonsterControlScript>();

        oldColor = renderer.material.color;

        healthBarLength = Screen.width / 2;
        attackRange = 1.0f;
        slowReleaseTime = 0;
        oldMovmentspeed = movementSpeed;
        isSlowed = false;

        //Attribute initialisieren
        oldMovementSpeed = movementSpeed;


		asound = GetComponent<AudioSource>();
		asound.clip = sounds [0];

        movementScript = GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update() {

        if (!isAlive) {
            curHealth = 0;
            movementScript.stopMove();
        }


        if (startHitAnimation && Time.time > hitAnimationTimer) {
            startHitAnimation = false;
            this.GetComponent<SpriteRenderer>().color = oldColor;

			PlayHitVoiceSound();
			PlayRPCHitSound();
        }

        if (PhotonNetwork.isMasterClient) {
            //alive checkup
            if (isAlive && curHealth <= 0) {
                photonView.RPC("deathRPCMonster", PhotonTargets.All);
            }
        }


        if (photonView.isMine == true) {

            if ((monsterSkillControlScript.iAmAstral && isSlowed) || (isSlowed && Time.time > slowReleaseTime)) {
                movementSpeed = oldMovmentspeed;
                isSlowed = false;
            }


            //enrage
            if (!enrage && curHealth <= maxHealth * 0.15f) {
                enrage = true;
                //buff
                attackDamage = attackDamage + attackDamage * enrageBuff;
                damageSkill1 = damageSkill1 + damageSkill1 * enrageBuff;
                damageSkill11 = damageSkill11 + damageSkill11 * enrageBuff;
                damageSkill12 = damageSkill12 + damageSkill12 * enrageBuff;
                damageSkill2 = damageSkill2 + damageSkill2 * enrageBuff;
                damageSkill21 = damageSkill21 + damageSkill21 * enrageBuff;
                damageSkill22 = damageSkill22 + damageSkill22 * enrageBuff;
                damageSkill3 = damageSkill3 + damageSkill3 * enrageBuff;
                damageSkill32 = damageSkill32 + damageSkill32 * enrageBuff;

                //SOUND HIER enrage sound
                photonView.RPC("monsterEnrage", PhotonTargets.All);
            }

        }
    }

    public void AddjustCurrentHealth(float adj) {

        if (adj < 0 && monsterControlScript.captureWard) {
            monsterControlScript.captureWard = false;
        }

        if (adj < 0) {
            monsterSkillControlScript.interruptChargeLamp();

        }

        if (!monsterSkillControlScript.iAmAstral) {

            if (adj < 0) {
                //hit animation
            startHitAnimation = true;
            hitAnimationTimer = Time.time + 0.1f;
            Color color = new Color(1.0f, 0.0f, 0.0f, renderer.material.color.a);
            this.GetComponent<SpriteRenderer>().color = color;
            alphaHit = 1.0f;
            }


            curHealth += adj;

            if (curHealth < 1) {
                curHealth = 0;
            }

            if (curHealth > maxHealth) {
                curHealth = maxHealth;
            }

            if (maxHealth < 1) {
                maxHealth = 1;
            }

            healthBarLength = (Screen.width / 2) * (curHealth / maxHealth);

            //wenn vom master gecallt
            if (PhotonNetwork.isMasterClient) {
                //bei allen anderen das leben anpassen
                photonView.RPC("AddjustOthersCurrentHealthMonster", PhotonTargets.Others, adj);

            }
        }
    }

    public void SlowMe(float slowTime) {
        if (!monsterSkillControlScript.iAmAstral) {
            oldMovmentspeed = movementSpeed;
            movementSpeed = oldMovmentspeed / 2;
            slowReleaseTime = Time.time + slowTime;
            isSlowed = true;
        }
    }

    [RPC]
    public void deathRPCMonster() {
        isAlive = false;
        curHealth = 0;
        if (photonView.isMine) {
            this.GetComponent<Movement>().iMoved(transform.position);
        }
    }


    [RPC]
    void AddjustOthersCurrentHealthMonster(float adj) {
        gameObject.GetComponent<MonsterAttributesScript>().AddjustCurrentHealth(adj);
    }


    [RPC]
    void monsterEnrage() {
        Color color = this.renderer.material.color;
        color.g = 0.0f;
        color.b = 0.0f;
        this.GetComponent<SpriteRenderer>().color = color;
    }

    void OnGUI() {


        if (photonView.isMine) {

            if (alphaHit > 0) {
                float rx = Screen.width / 1920.0f;
                float ry = Screen.height / 1080.0f;
                GL.PushMatrix();
                GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
                GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

                alphaHit -= 0.5f * Time.deltaTime;


                Color colPreviousGUIColor = GUI.color;

                GUI.color = new Color(colPreviousGUIColor.r, colPreviousGUIColor.g, colPreviousGUIColor.b, alphaHit);
                GUI.DrawTexture(new Rect(0, 0, hitTex.width, hitTex.height), hitTex);

                GUI.color = colPreviousGUIColor;

                GL.PopMatrix();
            }
            else if (GUI.color.a <= 1.0f) {
                GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            }

            if (!isAlive) {
                float rx = Screen.width / 1920.0f;
                float ry = Screen.height / 1080.0f;
                GL.PushMatrix();
                GL.MultMatrix(Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1)));
                GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));

                Color colPreviousGUIColor = GUI.color;

                GUI.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                GUI.DrawTexture(new Rect(0, 0, deadTex.width, deadTex.height), deadTex);

                GUI.color = colPreviousGUIColor;

                GL.PopMatrix();
            }

        }

    }

	//Sound Methods
	private void PlayHitSound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	private void PlayHitVoiceSound(int Number)
	{
		AudioSource.PlayClipAtPoint(sounds2[Number], transform.position, 1.00f*menuScript.scaleVolumeFactor);
	}
	
	[RPC]
	public void PlayHitSound()
	{
		int r = Random.Range (0, 3);
		PlayHitSound (r);
	}
	
	private void PlayRPCHitSound()
	{
		photonView.RPC ("PlayHitSound", PhotonTargets.Others, null);
	}
	
	public void PlayHitVoiceSound()
	{
		int r = Random.Range (0, 2);
		PlayHitVoiceSound (r);
	}
}

