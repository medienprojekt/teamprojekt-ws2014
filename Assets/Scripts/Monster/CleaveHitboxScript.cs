﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CleaveHitboxScript : Photon.MonoBehaviour {

    public List<GameObject> objectsInCricle;

	// Use this for initialization
	void Start () {
        objectsInCricle = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Team") {
            objectsInCricle.Add(other.gameObject);
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.tag == "Team") {
            objectsInCricle.Remove(other.gameObject);
        }
    }



}
