﻿using UnityEngine;
using System.Collections;

public class GameStarter : MonoBehaviour {


    public bool gameStarted;
    public bool HuntersLocked;
    public bool MonsterLocked;

    public bool hunterCountdown;
    public bool hunterCountdown_Started;
    public bool hunterCountdown_Finished;
    public string hunterCountdown_Number;


    public bool monsterCountdown;
    public bool monsterCountdown_Started;
    public bool monsterCountdown_Finished;
    public string  monsterCountdown_Number;

    private float hunterCountdown_time;
    private float monsterCountdown_time;

    public int whatAmI;
	// Use this for initialization
	void Start () {
        gameStarted = false;
        HuntersLocked = true;
        MonsterLocked = true;
        hunterCountdown = false;
        monsterCountdown = false;
        monsterCountdown_Started = false;
        hunterCountdown_Finished = false;
        hunterCountdown_Started = false;
        monsterCountdown_Finished = false;
        whatAmI = 0;
	}

    public void reset()
    {
        gameStarted = false;
        HuntersLocked = true;
        MonsterLocked = true;
        hunterCountdown = false;
        monsterCountdown = false;
        monsterCountdown_Started = false;
        hunterCountdown_Finished = false;
        hunterCountdown_Started = false;
        monsterCountdown_Finished = false;
        whatAmI = 0;

        GameObject.Find("AltarPrefab1").GetComponent<AltarsScript>().reset();
        GameObject.Find("AltarPrefab2").GetComponent<AltarsScript>().reset();
        GameObject.Find("AltarPrefab3").GetComponent<AltarsScript>().reset();
        GameObject.Find("AltarPrefab4").GetComponent<AltarsScript>().reset();

        GameObject[] wards = GameObject.FindGameObjectsWithTag("Ward");

        foreach (GameObject bush in wards) {
            bush.GetComponent<WardScript>().reset();
        }

        GameObject.Find("ShadowCircle").GetComponent<ShadowCircleScript>().reset();


    }

	// Update is called once per frame
	void Update () {

        if (gameStarted)
        {
            if (!hunterCountdown && !hunterCountdown_Finished)
            {
                hunterCountdown = true;
            }

            if (!monsterCountdown && !monsterCountdown_Finished)
            {
                monsterCountdown = true;
            }
        }


        if (hunterCountdown)
        {
            if (!hunterCountdown_Started)
            {
                hunterCountdown_time = Time.time;
                hunterCountdown_Started = true;
            }

            if (hunterCountdown_Started && (((hunterCountdown_time + 5) - Time.time) < 5) && (((hunterCountdown_time + 5) - Time.time) > 4))
            {
                hunterCountdown_Number = ""+5;
            }
            else if (hunterCountdown_Started && (((hunterCountdown_time + 5) - Time.time) < 4) && (((hunterCountdown_time + 5) - Time.time) > 3))
            {
                hunterCountdown_Number = "" + 4;
            }
            else if (hunterCountdown_Started && (((hunterCountdown_time + 5) - Time.time) < 3) && (((hunterCountdown_time + 5) - Time.time) > 2))
            {
                hunterCountdown_Number = "" + 3;
            }
            else if (hunterCountdown_Started && (((hunterCountdown_time + 5) - Time.time) < 2) && (((hunterCountdown_time + 5) - Time.time) > 1))
            {
                hunterCountdown_Number = "" + 2;
            }
            else if (hunterCountdown_Started && (((hunterCountdown_time + 5) - Time.time) < 1) && (((hunterCountdown_time + 5) - Time.time) > 0))
            {
                hunterCountdown_Number = "" + 1;
            }

            if (hunterCountdown_Started && Time.time > hunterCountdown_time + 5)
            {
                hunterCountdown_Finished = true;
                hunterCountdown = false;
                HuntersLocked = false;
            }

        }

        if (monsterCountdown)
        {
            if (!monsterCountdown_Started)
            {
                monsterCountdown_time = Time.time;
                monsterCountdown_Started = true;
            }

            if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 15) && (((monsterCountdown_time + 15) - Time.time) > 14))
            {
                monsterCountdown_Number = "" + 15;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 14) && (((monsterCountdown_time + 15) - Time.time) > 13))
            {
                monsterCountdown_Number = "" + 14;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 13) && (((monsterCountdown_time + 15) - Time.time) > 12))
            {
                monsterCountdown_Number = "" + 13;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 12) && (((monsterCountdown_time + 15) - Time.time) > 11))
            {
                monsterCountdown_Number = "" + 12;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 11) && (((monsterCountdown_time + 15) - Time.time) > 10))
            {
                monsterCountdown_Number = "" + 11;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 10) && (((monsterCountdown_time + 15) - Time.time) > 9))
            {
                monsterCountdown_Number = "" + 10;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 9) && (((monsterCountdown_time + 15) - Time.time) > 8))
            {
                monsterCountdown_Number = "" + 9;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 8) && (((monsterCountdown_time + 15) - Time.time) > 7))
            {
                monsterCountdown_Number = "" + 8;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 7) && (((monsterCountdown_time + 15) - Time.time) > 6))
            {
                monsterCountdown_Number = "" + 7;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 6) && (((monsterCountdown_time + 15) - Time.time) > 5))
            {
                monsterCountdown_Number = "" + 6;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 5) && (((monsterCountdown_time + 15) - Time.time) >4))
            {
                monsterCountdown_Number = "" + 5;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 4) && (((monsterCountdown_time + 15) - Time.time) > 3))
            {
                monsterCountdown_Number = "" + 4;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 3) && (((monsterCountdown_time + 15) - Time.time) > 2))
            {
                monsterCountdown_Number = "" + 3;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 2) && (((monsterCountdown_time + 15) - Time.time) > 1))
            {
                monsterCountdown_Number = "" + 2;
            }
            else if (monsterCountdown_Started && (((monsterCountdown_time + 15) - Time.time) < 1) && (((monsterCountdown_time + 15) - Time.time) > 0))
            {
                monsterCountdown_Number = "" + 1;
            }

           // Debug.Log("Monster Countdown: " + monsterCountdown_Number);

            if (monsterCountdown_Started && Time.time > monsterCountdown_time + 15)
            {
                monsterCountdown_Finished = true;
                monsterCountdown = false;
                MonsterLocked = false;
            }
        }

        if (MonsterLocked&&GameObject.Find("Monster")!=null)
        {
            
            GameObject.Find("Monster").GetComponent<SpriteRenderer>().enabled = false;
            if (whatAmI == 0)
            {
                GameObject.Find("Monster").GetComponent<FogOfWarCircleRevealer>().enabled = false;
            }
            GameObject.Find("Monster").GetComponent<BoxCollider>().enabled = false;
            if (GameObject.Find("Ranger") != null&&whatAmI==1)
            {
                GameObject.Find("Ranger").GetComponent<InviChecker>().enabled = false;
            }
            else if (GameObject.Find("Healer") != null && whatAmI == 2)
            {
                GameObject.Find("Healer").GetComponent<InviChecker>().enabled = false;
            }
            else if (GameObject.Find("Tank") != null && whatAmI == 3)
            {
                GameObject.Find("Tank").GetComponent<InviChecker>().enabled = false;
            }


        }
        else if (!MonsterLocked && GameObject.Find("Monster") != null)
        {
            GameObject.Find("Monster").GetComponent<SpriteRenderer>().enabled = true;
            if (whatAmI == 0)
            {
                GameObject.Find("Monster").GetComponent<FogOfWarCircleRevealer>().enabled = true;
            }
            GameObject.Find("Monster").GetComponent<BoxCollider>().enabled = true;

            if (GameObject.Find("Ranger") != null && whatAmI == 1)
            {
                GameObject.Find("Ranger").GetComponent<InviChecker>().enabled = true;
            }
            else if (GameObject.Find("Healer") != null && whatAmI == 2)
            {
                GameObject.Find("Healer").GetComponent<InviChecker>().enabled = true;
            }
            else if (GameObject.Find("Tank") != null && whatAmI == 3)
            {
                GameObject.Find("Tank").GetComponent<InviChecker>().enabled = true;
            }
        }
	}
}
