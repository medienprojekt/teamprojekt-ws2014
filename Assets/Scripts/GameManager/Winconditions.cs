﻿using UnityEngine;
using System.Collections;

public class Winconditions : MonoBehaviour {

    public bool huntersWin;
    public bool monsterWin;



	// Use this for initialization
	void Start () {
        huntersWin = false;
        monsterWin = false;
	}

    public void reset()
    {
        huntersWin = false;
        monsterWin = false;
    }

	// Update is called once per frame
	void Update () {

        if(GameObject.Find("Monster")!=null){
            if (!GameObject.Find("Monster").GetComponent<MonsterAttributesScript>().isAlive)
                {
                    huntersWin = true;
                }
        }

        if (GameObject.Find("Ranger") != null && GameObject.Find("Healer") != null && GameObject.Find("Tank") != null)
        {
            if (!GameObject.Find("Ranger").GetComponent<RangerAttributesScript>().isAlive && !GameObject.Find("Healer").GetComponent<HealerAttributesScript>().isAlive && !GameObject.Find("Tank").GetComponent<TankAttributesScript>().isAlive)
            {
                monsterWin = true;
            }
        }
	}
}
