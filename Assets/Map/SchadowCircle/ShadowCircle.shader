﻿Shader "Custom/ShadowCircle" {
	Properties {
		_Color("Main Color", Color) = ( 1, 1, 1, 1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_FogRadius ("FogRadius", Float) = 1.0
		_FogMaxRadius ("FogMaxRadius", Float) = 0.5
		_Circle_Pos ("_Circle_Pos", Vector) = (0,0,0,1)
		_Thickness ("_Thickness", Float) = 0.8
	}
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		LOD 200
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert

		sampler2D _MainTex;
		fixed4 _Color;
		float _FogRadius;
		float _FogMaxRadius;
		float4 _Circle_Pos;
		float _Thickness;
		

		struct Input {
			float2 uv_MainTex;
			float2 location;
			
		};
		
		float powerForPos(float4 pos, float2 nearVertex);
		
		void vert(inout appdata_full vertexData, out Input outData){
			float4 pos = mul(UNITY_MATRIX_MVP, vertexData.vertex);
			float4 posWorld = mul(_Object2World, vertexData.vertex);
			outData.uv_MainTex = vertexData.texcoord;
			outData.location = posWorld.xy;
		
		}

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 baseColor = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			
			float alpha = (_Thickness - powerForPos(_Circle_Pos, IN.location));
			
			o.Albedo = baseColor.rgb;
			o.Alpha = alpha;
		}
		
		float powerForPos(float4 pos, float2 nearVertex){
			float atten = clamp(_FogRadius - length(pos.xy - nearVertex.xy), 0.0, _FogRadius);
			return (1.0/_FogMaxRadius)*atten/_FogRadius;
		}
		
		
		
		ENDCG
	} 
	FallBack "Transparent/VertexLit"
}
