﻿using UnityEngine;
using System.Collections;

public class MouseClickScript : MonoBehaviour {

    private float time;

    // Use this for initialization
    void Start() {
        time = Time.time + 0.5f;
    }

    // Update is called once per frame
    void Update() {

        if (Time.time > time) {
            Destroy(this.gameObject);

        }


    }
}
