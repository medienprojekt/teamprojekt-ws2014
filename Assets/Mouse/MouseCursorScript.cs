﻿using UnityEngine;
using System.Collections;

public class MouseCursorScript : MonoBehaviour {

    private MouseHitboxScript mouseHitboxScript;
    private MouseHitboxScriptMonster mouseHitboxScriptMonster;
    private Menu menu;
    public Texture2D standardMouse0;
    public Texture2D attackMouse;
    public Texture2D crossMouse;
    private Vector2 offsetMouse;

    private int mouseState; // 1 normal, 2 angriff, 3 cross

	// Use this for initialization
	void Start () {
        mouseHitboxScript = GetComponent<MouseHitboxScript>();
        mouseHitboxScriptMonster = GetComponent<MouseHitboxScriptMonster>();
        menu = GameObject.Find("Menu").GetComponent<Menu>();
        offsetMouse = new Vector2(standardMouse0.width/2, standardMouse0.height/2 - 15.0f);
	}
	
	// Update is called once per frame
	void Update () {

       //ranger
        if (menu.iAm == "Ranger") {

            if (mouseState != 1 && !mouseHitboxScript.mouseOverEnemy && !mouseHitboxScript.mouseOverAltar
                && GameObject.Find("Ranger") != null && !GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().skillMode) {
                    Cursor.SetCursor(standardMouse0, offsetMouse, CursorMode.Auto);
                mouseState = 1;
            }
            if (mouseState != 2 && (mouseHitboxScript.mouseOverEnemy || mouseHitboxScript.mouseOverAltar)) {
                Cursor.SetCursor(attackMouse, offsetMouse, CursorMode.Auto);
                mouseState = 2;
            }

            if (mouseState != 3 && GameObject.Find("Ranger") != null && GameObject.Find("Ranger").GetComponent<RangerSkillControlScript>().skillMode) {
                Cursor.SetCursor(crossMouse, offsetMouse, CursorMode.Auto);
                mouseState = 3;
            }
        }

        else if (menu.iAm == "Healer") {

            if (mouseState != 1 && !mouseHitboxScript.mouseOverEnemy && !mouseHitboxScript.mouseOverAltar
                && GameObject.Find("Healer") != null && !GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().skillMode) {
                Cursor.SetCursor(standardMouse0, offsetMouse, CursorMode.Auto);
                mouseState = 1;
            }
            if (mouseState != 2 && (mouseHitboxScript.mouseOverEnemy || mouseHitboxScript.mouseOverAltar)) {
                Cursor.SetCursor(attackMouse, offsetMouse, CursorMode.Auto);
                mouseState = 2;
            }

            if (mouseState != 3 && GameObject.Find("Healer") != null && GameObject.Find("Healer").GetComponent<HealerSkillControlScript>().skillMode) {
                Cursor.SetCursor(crossMouse, offsetMouse, CursorMode.Auto);
                mouseState = 3;
            }
        }

        else if (menu.iAm == "Tank") {

            if (mouseState != 1 && !mouseHitboxScript.mouseOverEnemy && !mouseHitboxScript.mouseOverAltar
                && GameObject.Find("Tank") != null && !GameObject.Find("Tank").GetComponent<TankSkillControlScript>().skillMode) {
                Cursor.SetCursor(standardMouse0, offsetMouse, CursorMode.Auto);
                mouseState = 1;
            }
            if (mouseState != 2 && (mouseHitboxScript.mouseOverEnemy || mouseHitboxScript.mouseOverAltar)) {
                Cursor.SetCursor(attackMouse, offsetMouse, CursorMode.Auto);
                mouseState = 2;
            }

            if (mouseState != 3 && GameObject.Find("Tank") != null && GameObject.Find("Tank").GetComponent<TankSkillControlScript>().skillMode) {
                Cursor.SetCursor(crossMouse, offsetMouse, CursorMode.Auto);
                mouseState = 3;
            }
        }


        else if (menu.iAm == "Monster") {

            if (mouseState != 1 && !mouseHitboxScriptMonster.mouseOverEnemy
                && GameObject.Find("Monster") != null && !GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().skillMode) {
                Cursor.SetCursor(standardMouse0, offsetMouse, CursorMode.Auto);
                mouseState = 1;
            }
            if (mouseState != 2 && mouseHitboxScriptMonster.mouseOverEnemy) {
                Cursor.SetCursor(attackMouse, offsetMouse, CursorMode.Auto);
                mouseState = 2;
            }

            if (mouseState != 3 && GameObject.Find("Monster") != null && GameObject.Find("Monster").GetComponent<MonsterSkillControlScript>().skillMode) {
                Cursor.SetCursor(crossMouse, offsetMouse, CursorMode.Auto);
                mouseState = 3;
            }
        }


	}
}
