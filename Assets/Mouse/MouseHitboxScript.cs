﻿    using UnityEngine;
using System.Collections;

public class MouseHitboxScript : Photon.MonoBehaviour {

    public bool mouseOverEnemy;
    public GameObject mouseOverLastEnemyGO;
    public GameObject mouseOverCurEnemeyGO;

    public bool mouseOverTeammate;
    public GameObject mouseOverLastTeammateGO;

    public bool mouseOverPortal;
    public GameObject mouseOverPortalGO;

    public bool mouseOverWard;
    public GameObject mouseOverWardGO;

    public bool mouseOverAltar;
    public GameObject mouseOverAltarGO;

    public bool mouseOverWorldObject;

    // Use this for initialization
    void Start() {
        mouseOverEnemy = false;
        mouseOverTeammate = false;
        mouseOverPortal = false;
        mouseOverWard = false;
        mouseOverAltar = false;
    }

    // Update is called once per frame
    void Update() {
        Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        newPos.z = 2;
        transform.position = newPos;

        if (mouseOverLastEnemyGO != null && !mouseOverLastEnemyGO.GetComponent<SpriteRenderer>().enabled) {
            mouseOverLastEnemyGO = null;
            mouseOverEnemy = false;
        }


        //überprüfung ob das akutelle objekt noch exsistiert
        if (mouseOverPortal) {
            if (mouseOverPortalGO == null) {
                mouseOverPortal = false;
            }
        }

        if (mouseOverEnemy) {
            if (mouseOverLastEnemyGO == null) {
                mouseOverEnemy = false;
            }
        }

        if (mouseOverTeammate) {
            if (mouseOverLastTeammateGO == null) {
                mouseOverTeammate = false;
            }
        }

    }

    void OnTriggerEnter(Collider coll) {
        //TestCreep
        if (coll.tag.Equals("TestCreep")) {
            mouseOverEnemy = true;
            mouseOverLastEnemyGO = coll.gameObject;
            mouseOverCurEnemeyGO = coll.gameObject;
        }

        if (coll.tag.Equals("Monster") && coll.renderer.enabled) {
            mouseOverEnemy = true;
            mouseOverLastEnemyGO = coll.gameObject;
            mouseOverCurEnemeyGO = coll.gameObject;
        }

        if (coll.tag.Equals("Team") && coll.renderer.enabled) {
            mouseOverTeammate = true;
            mouseOverLastTeammateGO = coll.gameObject;
        }


        if (coll.tag.Equals("Portal")) {
            mouseOverPortal = true;
            mouseOverPortalGO = coll.gameObject;
        }

        if (coll.tag.Equals("Ward")) {
            mouseOverWard = true;
            mouseOverWardGO = coll.gameObject;
        }

        if (coll.tag.Equals("Altar")) {
            mouseOverAltar = true;
            mouseOverAltarGO = coll.gameObject;
        }

        if (coll.tag.Equals("WorldObjects")) {
            mouseOverWorldObject = true;
        }

    }

    void OnTriggerExit(Collider coll) {
        if (coll.tag.Equals("TestCreep")) {
            mouseOverEnemy = false;
            mouseOverCurEnemeyGO = null;
        }


        if (coll.tag.Equals("Monster")) {
            mouseOverEnemy = false;
            mouseOverCurEnemeyGO = null;
        }

        if (coll.tag.Equals("Team")) {
            mouseOverTeammate = false;
        }

        if (coll.tag.Equals("Portal")) {
            mouseOverPortal = false;
        }

        if (coll.tag.Equals("Ward")) {
            mouseOverWard = false;
            mouseOverWardGO = null;
        }

        if (coll.tag.Equals("Altar")) {
            mouseOverAltar = false;
            mouseOverAltarGO = null;
        }

        if (coll.tag.Equals("WorldObjects")) {
            mouseOverWorldObject = false;
        }

    }




}
