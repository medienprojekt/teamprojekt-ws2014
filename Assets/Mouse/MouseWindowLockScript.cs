﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class MouseWindowLockScript : MonoBehaviour {

    [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool ClipCursor(ref RECT rcClip);
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetClipCursor(out RECT rcClip);
    [DllImport("user32.dll")]
    static extern int GetForegroundWindow();
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool GetWindowRect(int hWnd, ref RECT lpRect);

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
        public RECT(int left, int top, int right, int bottom) {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
    }

    RECT currentClippingRect;
    RECT originalClippingRect = new RECT();

    void Start() {
    }

    void Update() {

    }

    void OnApplicationFocus(bool focusStatus) {
        if (!Application.isEditor) {

            if (focusStatus) {

                var hndl = GetForegroundWindow();
                GetWindowRect(hndl, ref currentClippingRect);
                GetClipCursor(out originalClippingRect);
                //anpassungen falls nicht fullscreen
                if (!Screen.fullScreen) {
                    currentClippingRect.Top = currentClippingRect.Top + 27;
                    currentClippingRect.Bottom = currentClippingRect.Bottom - 2;
                    currentClippingRect.Left = currentClippingRect.Left + 2;
                    currentClippingRect.Right = currentClippingRect.Right - 2;
                }
                ClipCursor(ref currentClippingRect);


            }
            else {
                ClipCursor(ref originalClippingRect);
            }
        }
    }

    void OnApplicationQuit() {
        if (!Application.isEditor) {
            ClipCursor(ref originalClippingRect);
        }
    }
}
