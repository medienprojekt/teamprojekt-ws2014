﻿using UnityEngine;
using System.Collections;

public class MouseHitboxScriptMonster : Photon.MonoBehaviour {

    public bool mouseOverEnemy;
    public GameObject mouseOverLastEnemyGO;
    public GameObject mouseOverCurEnemeyGO;

    public bool mouseOverTeammate;
    public GameObject mouseOverLastTeammateGO;

    public bool mouseOverWard;
    public GameObject mouseOverWardGO;

    // Use this for initialization
    void Start() {
        mouseOverEnemy = false;
        mouseOverTeammate = false;
        mouseOverWard = false;

        mouseOverLastEnemyGO = null;
    }

    // Update is called once per frame
    void Update() {
        Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        newPos.z = 2;
        transform.position = newPos;

        if (mouseOverLastEnemyGO != null && !mouseOverLastEnemyGO.GetComponent<SpriteRenderer>().enabled) {
            mouseOverLastEnemyGO = null;
            mouseOverEnemy = false;
        }


        if (mouseOverEnemy) {
            if (mouseOverLastEnemyGO == null) {
                mouseOverEnemy = false;
            }
        }

        if (mouseOverTeammate) {
            if (mouseOverLastTeammateGO == null) {
                mouseOverTeammate = false;
            }
        }

        if(mouseOverLastEnemyGO != null){
            if ((mouseOverLastEnemyGO.GetComponent<RangerAttributesScript>() != null && !mouseOverLastEnemyGO.GetComponent<RangerAttributesScript>().isAlive)
        || (mouseOverLastEnemyGO.GetComponent<HealerAttributesScript>() != null && !mouseOverLastEnemyGO.GetComponent<HealerAttributesScript>().isAlive)
        || (mouseOverLastEnemyGO.GetComponent<TankAttributesScript>() != null && !mouseOverLastEnemyGO.GetComponent<TankAttributesScript>().isAlive)) {
                mouseOverEnemy = false;
                mouseOverLastEnemyGO = null;
            }
        }
    }

    void OnTriggerEnter(Collider coll) {
        //TestCreep
        if (coll.tag.Equals("TestCreep")) {
            mouseOverEnemy = true;
            mouseOverLastEnemyGO = coll.gameObject;
            mouseOverCurEnemeyGO = coll.gameObject;
        }

        if (coll.tag.Equals("Team") && coll.renderer.enabled) {

            if ((coll.GetComponent<RangerAttributesScript>() != null && coll.GetComponent<RangerAttributesScript>().isAlive)
                || (coll.GetComponent<HealerAttributesScript>() != null && coll.GetComponent<HealerAttributesScript>().isAlive)
                || (coll.GetComponent<TankAttributesScript>() != null && coll.GetComponent<TankAttributesScript>().isAlive)) {

                mouseOverEnemy = true;
                mouseOverLastEnemyGO = coll.gameObject;
                mouseOverCurEnemeyGO = coll.gameObject;
            }

        }


        if (coll.tag.Equals("Ward")) {
            mouseOverWard = true;
            mouseOverWardGO = coll.gameObject;
        }

    }

    void OnTriggerExit(Collider coll) {
        if (coll.tag.Equals("TestCreep")) {
            mouseOverEnemy = false;
            mouseOverCurEnemeyGO = null;
        }

        if (coll.tag.Equals("Team") && coll.renderer.enabled) {
            mouseOverEnemy = false;
            mouseOverCurEnemeyGO = null;
        }

        if (coll.tag.Equals("Ward")) {
            mouseOverWard = false;
            mouseOverWardGO = null;
        }

    }




}
